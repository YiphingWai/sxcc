#ifndef __PARSE_H__
#define __PARSE_H__

#include "lex.h"

enum nodeKind 
{ 
	NK_TranslationUnit,     NK_Function,           NK_Declaration,
	NK_TypeName,            NK_Specifier,          NK_Specifiers,				
	NK_TypedefName,         NK_EnumSpecifier,      NK_Enumerator,			
	NK_StructSpecifier,     NK_UnionSpecifier,     NK_StructDeclaration,	
	NK_StructDeclarator,    NK_PointerDeclarator,  NK_ArrayDeclarator,		
	NK_FunctionDeclarator,  NK_ParameterTypeList,  NK_ParameterDeclaration,
	NK_NameDeclarator,      NK_InitDeclarator,     NK_Initializer,
	
/*	NK_DirectDeclarator,	NK_Declarator,		   NK_IdentifierList,
	NK_ExtDeclaration,		NK_SpecifierList,*/
	
	NK_Expression,

	// expression-statement
	NK_ExpressionStatement, 
	// labeled-statement
	NK_LabelStatement,     NK_CaseStatement, 		NK_DefaultStatement, 
	// selection-statement   
	NK_IfStatement,        NK_SwitchStatement,		
	// iteration-statement
	NK_WhileStatement,      NK_DoStatement,        NK_ForStatement,	
	// jump-statement	
	NK_GotoStatement,       NK_BreakStatement,     NK_ContinueStatement,		
	NK_ReturnStatement,     
	// compound-statement
	NK_CompoundStatement
};

#define NODE_COMMON enum nodeKind kind; void *next;
struct common_node
{
    NODE_COMMON
};

void do_error(char *, struct location *loc);
void do_expect(enum token_type);
#define	error	printf("(%s,%d):", __FILE__, __LINE__) , do_error
#define expect  do_expect

int is_decl_first(enum token_type);
int is_expr_first(enum token_type);
int is_stmt_first(enum token_type);
int is_type_tok(struct token *);
int is_typedef_name(char *);
#endif
