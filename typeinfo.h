#ifndef TYPEINFO
#error "You must define TYPEINFO macro before include this file"
#endif

/*
#define TYPE_COMMON \
    enum expr_type type;  \
    enum expr_type_qualer qualer;  \
    int align; \
    int size;       \
    struct type *basic_type;
    CHAR, UCHAR, SHORT, USHORT, INT, UINT, LONG, ULONG, LONGLONG, ULONGLONG, ENUM,
	FLOAT, DOUBLE, LONGDOUBLE, POINTER, VOID, UNION, STRUCT, ARRAY, FUNCTION
*/
/* TYPEINFO(name, qualer, align, size, basic_type) */
TYPEINFO(CHAR, 0, 1, 1, NULL)
TYPEINFO(UCHAR, 0, 1, 1, NULL)
TYPEINFO(SHORT, 0, 2, 2, NULL)
TYPEINFO(USHORT, 0, 2, 2, NULL)
TYPEINFO(INT, 0, 4, 4, NULL)
TYPEINFO(UINT, 0, 4, 4, NULL)
TYPEINFO(LONG, 0, 4, 4, NULL)
TYPEINFO(ULONG, 0, 4, 4, NULL)
// TYPEINFO(LONGLONG, 0, 8, 8, NULL)
// TYPEINFO(ULONGLONG, 0, 8, 8, NULL)
TYPEINFO(FLOAT, 0, 4, 4, NULL)
TYPEINFO(DOUBLE, 0, 8, 8, NULL)
// TYPEINFO(LONGDOUBLE, 0, 16, 16, NULL)
TYPEINFO(ENUM, 0, 4, 4, NULL)
// above are arithm type
TYPEINFO(POINTER, 0, 4, 4, NULL)
// above are scalar type
TYPEINFO(VOID, 0, 4, 4, NULL)
TYPEINFO(UNION, 0, 0, 0, NULL)
TYPEINFO(STRUCT, 0, 0, 0, NULL)
TYPEINFO(ARRAY, 0, 0, 0, NULL)
TYPEINFO(FUNCTION, 0, 0, 0, NULL)

