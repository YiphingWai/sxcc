#ifndef __TYPE_H__
#define __TYPE_H__

#include "vector.h"

enum type_categ
{
	#define TYPEINFO(name, qualer, align, size, basic_type) name,
        #include "typeinfo.h"
    #undef TYPEINFO
};
#define IS_ARITH_TYPE(type) (type->categ <= ENUM)
#define IS_RECORD_TYPE(type) (type->categ == STRUCT || type->categ == UNION)
#define IS_UNSIGNED(categ) (((categ) == UCHAR) || ((categ) == USHORT) || ((categ) == UINT) || ((categ) == ULONG))
#define IS_FUNCTION_POINTER(type)  (type->categ == POINTER && type->basic_type->categ == FUNCTION)
#define IS_VOID_POINTER(type)       (type->categ == POINTER && type->basic_type->categ == VOID)
#define IS_SCALAR_TYPE(type) (type->categ <= POINTER)
#define IS_INTEG_TYPE(type) (type->categ == ENUM || type->categ <= ULONG)
#define IS_FLOAT(categ) (categ == FLOAT || categ == DOUBLE)
#define IS_OBJECT_POINTER(type)     (type->categ == POINTER &&  type->basic_type->categ != FUNCTION)
#define IS_INCOMPLETE_POINTER(type) (type->categ == POINTER &&  type->basic_type->size == 0)

#define ALIGN(offset, align) (((offset) % (align)) ? ((offset) + (align) - ((offset) % (align))) : (offset))

// type qualifier
enum type_qualer
{ 
    CONST = 0x01, VOLATILE = 0x02,
};


#define TYPE_COMMON \
    enum type_categ categ;  \
    enum type_qualer qualer;  \
    int align; \
    int size;       \
    struct type *basic_type;

struct type
{
    TYPE_COMMON
};

struct array_type
{
    TYPE_COMMON
    int len;
};

struct parameter_type
{
	char *id;
	int sto_class_spec_tok;
	struct type *type; 
	struct symbol *sym;
};
struct func_type
{
    TYPE_COMMON
	int has_ellipsis;  
	struct vector *params;
};

struct record_field
{
    struct type *type;
	int offset;
	char *id;
};
struct record_type
{
    TYPE_COMMON
    char *id;
    struct vector *fields;
    int has_const_field;
	//for test whether it is incomplete type.
	/********************************************
		struct A;	//	It is considered an incomplete type here. So sizeof(struct A) is illegal.
		void f(int a[]){
			printf("%d \n",sizeof(a));
			printf("sizeof(struct A) = %d  \n",sizeof(struct A));------- incomplete type
		}
		struct A{
			int a[10];
		};
		see IsIncompleteType(ty)
	 ********************************************/
	int complete;
	int has_flex_array;
};
struct enum_type
{
	TYPE_COMMON
	char *id; 
	int complete;
};

extern const struct type basic_types[];
extern struct func_type *default_func_type;

void type_system_init();
struct type *create_enum(char *id);
int is_incomplete_rec_type(struct type *type);
int is_incomplete_enum_type(struct type *type);
int is_incomplete_type(struct type *type, int ignore_zero_size_array);
struct type *create_array(int len, struct type *basic_type);
struct type *create_pointer_type(struct type *basic_types);
struct type *create_function(struct type *return_type, int has_ellipsis, struct vector *params);
struct parameter_type *add_parameter(struct func_type *ftype, char *id, struct type *ptype);
struct type *create_record(char *id, enum type_categ categ);
struct record_field *add_field(struct record_type *rtype, char *id, struct type *ftype);
struct record_field *lookup_field(struct record_type *rtype, char *id);
int layout_record(struct record_type *rtype, int basic_offset);
struct type *qualify(struct type *ty, enum type_qualer qualer);
struct type *unqualify(struct type *ty);
struct type *greatest_common_type(struct type *type1, struct type *type2);
int is_compatible_type(struct type *type1, struct type *type2);
struct type *arith_common_type(struct type *type1, struct type *type2);
struct type *adjust_parameter(struct type *para_type);


#endif
