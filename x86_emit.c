#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include "tran.h"
#include "x86_reg.h"
#include "x86_emit.h"
#include "type.h"
#include "hash.h"
#include "symtab.h"
#include "sxcc.h"
#include "opt.h"
#include <stdarg.h>

#define STACK_ALIGN_SIZE 4
#define IS_SHARED_TEMP(sym) ((sym)->kind == SK_Temp && ((struct symbol_temp*)sym)->is_shared_tmp)

FILE *emit_fp;
void emit(const char *fmt, ...)
{
	char tmp[1024];
	va_list ap;

	va_start(ap, fmt);
	vsnprintf(tmp,sizeof(tmp),fmt, ap);
	va_end(ap);

    fprintf(emit_fp, "%s", tmp);
//	PutString(tmp);
}

char* format_name(const char *fmt, ...)
{
	char *buf;
	va_list ap;

    if (!(buf = calloc(1, 256)))
    {
        rep_error("calloc err");
        exit(-1);
    }

	va_start(ap, fmt);
	vsnprintf(buf, 256, fmt, ap);
	va_end(ap);

	return buf;
}

void align_emit(int align)
{
    emit("align %d\n", align);
}

void external_emit(struct proc_block *pb)
{
    int i;
    struct symbol *sym;
    for (i = 0; i < pb->externals->len; i++)
    {
        sym = pb->externals->get(pb->externals, i);
        emit("extern %s\n", sym->aname);
    }
}

void global_constant_emit()
{
    int i, j, cnt, flag = 1;
    char *str;
    struct symbol *sym;
    for (i = 0, cnt = 0; sym = constant_tab->hash_iter(constant_tab, &i, &cnt);)
    {
        if (sym->kind == SK_String)
        {
            str = sym->value.p;
            emit("%s: db ", sym->aname);
            for (j = 0; str[j]; j++)
            {
                if (!IS_LETTER_OR_DIGIT(str[j]))
                {
                    emit("%d,", str[j]);
                }
                else
                {
                    emit("'%c',", str[j]); 
                }
            }
            emit("0\n");
        }
        else if (sym->kind == SK_Constant && (sym->type->categ == FLOAT || sym->type->categ == DOUBLE))
        {
            align_emit(sym->type->align);
            emit("%s: ", sym->aname);
            if (sym->type->categ == FLOAT)
            {
                emit("dd %d\n", *(unsigned int*)&(sym->value));
            }
            else
            {
                emit("dq %ld\n", *(unsigned long long*)&(sym->value));
            }
        }
    }
}
// emit .long .byte etc.
void integ_constant_emit(int size, union val value)
{
    unsigned int *p;
    p = &value;
    switch (size)
    {
        case 1:
            emit("db %d\n", p[0]);
            break;
        case 2:
            emit("dw %d\n", p[0]);
            break;
        case 4:
            emit("dd %d\n", p[0]);
            break;
        case 8:
            emit("dq %ld\n", *(long long *)p);
            break;
        default:
            assert(0);
    }
}

void emit_init_data(struct init_data *idata)
{
    int cast_size = 0;
    int size = 0;
    struct expr_node *expr;
    for (; idata; idata = idata->next)
    {
        expr = idata->expr;
        cast_size = 0;
        // put resb to align 
        assert(size <= idata->offset);
        if (size < idata->offset)
        {
            emit("resb %d\n", idata->offset - size);
        }
        while (1)
        {
            if (expr->op == OP_CAST)
            {
                // means the variable is not int type
                /*
                if (expr->type->size != 4)
                {
                    rep_error("wrong init data\n");
                    exit(-1);
                }
                */
                assert(!cast_size);
                cast_size = expr->type->size;
                assert(expr->child[0]->op == OP_CONST);
                
                expr = cast_fold(expr->type, expr->child[0]);
            }
            else if (expr->op == OP_ADD)
            {
                // the only legal expr is : int i = &j + 5;
                if ((expr->child[0]->op != OP_ADDRESS) || (expr->child[0]->child[0]->op != OP_ID) ||
                    (expr->child[1]->op != OP_CONST) || (expr->type->size != 4))
                {
                    rep_error("wrong init data\n");
                    exit(-1);
                }
                emit("dd %s + %d\n", expr->child[0]->child[0]->value.p, expr->child[1]->value.i);
                size += expr->type->size;
                break;
            }
            else if (expr->op == OP_STR)
            {
                if (expr->type->size != 4)
                {
                    rep_error("wrong init data\n");
                    exit(-1);
                }
                emit("dd %s\n", expr->sym->aname);
                size += expr->type->size;
                break;
            }
            else if (expr->op == OP_CONST)
            {
                cast_size = cast_size ? cast_size : expr->type->size;
                integ_constant_emit(cast_size, expr->value);
                size += cast_size;
                break;
            }
            else if (expr->op == OP_ADDRESS)
            {
                if (expr->type->size != 4)
                {
                    rep_error("wrong init data\n");
                    exit(-1);
                }
                emit("dd %s\n", expr->child[0]->sym->name);
                size += expr->type->size;
                break;
            }
            else
            {
                rep_error("wrong init data\n");
                exit(-1);
            }
        }
    }
}

void global_variable_emit(struct proc_block *pb)
{
    int i, t = 0;
    struct symbol_var *sym;
    struct init_data *idata;
    for (i = 0; i < pb->locals->len; i++)
    {
        sym = pb->locals->get(pb->locals, i);
        assert(sym->kind == SK_Variable);
        
        if (sym->init_data)
        {
            sym->aname = sym->name;
            align_emit(sym->type->align);
            if (sym->sto_class_spec_tok != TK_STATIC)
            {
                emit("global %s\n", sym->name);
            }
            emit("%s: ", sym->name);
            sym->aname = sym->name;
            emit_init_data(sym->init_data);
        }
    }
    emit("section .bss\n\n");
    for (i = 0; i < pb->locals->len; i++)
    {
        sym = pb->locals->get(pb->locals, i);
        assert(sym->kind == SK_Variable);

        if (!sym->init_data && sym->type->size)
        {
            sym->aname = sym->name;
            align_emit(sym->type->align);
            if (sym->sto_class_spec_tok != TK_STATIC)
            {
                emit("global %s\n", sym->name);
            }
            emit("%s: resb %d\n", sym->name, sym->type->size);
        }
    }
}

struct symbol_reg *_find_shared_temp_reg()
{
    int i = 0, j = -1, k = 0;
    for (i = 0; i < 18; i++)
    {
        if (regs[i].used && !regs[i].link && regs[i].user && IS_SHARED_TEMP(regs[i].user))
        {
            return &regs[i];
        }
    }
    return NULL;
}

struct symbol_reg *find_shared_temp_reg(int need_wb)
{
    int i = 0, j = -1, k = 0;
    for (i = 0; i < 18; i++)
    {
        if (regs[i].used && !regs[i].link && regs[i].user && IS_SHARED_TEMP(regs[i].user))
        {
            if (regs[i].is_reserved)    // a reg which binding to a shared temp from current ir
            {
                // we must check which shared temp need write back since at most one var can be write back in one certain IR
                if (need_wb)
                {
                    // if current shared temp need write back, that means the data in last shared temp need not to written back since at most one var can be write back in one certain IR
                    regs[i].need_wb = 0;
                    //emit("; -!- ");
                }
                // reach here means the current ir have two shared temp and we have to allocate reg for each of them
                // however, one of these two regs (the one which donn't need written back) will be release in exit_ir_emit() after we finish emitting current ir
                return NULL;
            }
            else if (regs[i].need_wb && need_wb)   // a reg which binding to a shared temp from former ir and need to write back
            {
                write_back_reg(&regs[i]);
            }
            assert(k++ <= 0);
            j = i;
        }
    }
    if (j == -1)
    {
        return NULL;
    }
    return &regs[j];
}

char *direct_mem_fetch(struct symbol_id *sym, struct vector *release_list)
{
    char *name;
    struct symbol_reg *reg;
    
    if (sym->kind == SK_Constant || sym->kind == SK_EnumConstant || sym->kind == SK_String ||
        sym->kind == SK_Function)
    {
       if (sym->kind == SK_Constant && IS_FLOAT(sym->type->categ))
        {
            name = format_name("[%s]", sym->aname);
            release_list->push_back(release_list, name);
            return name;
        }
        return sym->aname;
    }

    assert(sym->kind == SK_Temp || sym->kind == SK_Variable);
    if (sym->reg)
    {
        release_reg(sym->reg);
    }
    if ((sym->kind == SK_Temp) && (((struct symbol_temp *)sym)->is_shared_tmp) && (reg = _find_shared_temp_reg()))
    {
        release_reg(reg);
    }

    name = format_name("[%s]", sym->aname);
    release_list->push_back(release_list, name);
    return name;
}

char *reg_fetch(struct symbol_id *sym, int need_val, int need_wb, struct vector *release_list)
{
    struct symbol_reg *reg;

    assert(sym->type->size == 1 || sym->type->size == 2 || sym->type->size == 4);
    if (sym->kind == SK_Temp || sym->kind == SK_Variable)
    {
        if (IS_SHARED_TEMP(sym))
        {
            if (reg = find_shared_temp_reg(need_wb))
            {
                if (reg_to_size[reg->dec] == sym->type->size)
                {
                    reg_change_user(reg, sym);
                    reserve_reg(sym);
                    release_list->push_back(release_list, sym->reg);
                    reg->need_wb |= need_wb;
                    return reg->aname;
                }
                else
                {
                    release_reg(reg);
                }
            }
        }
        if (sym->reg)
        {
            reserve_reg(sym);
            release_list->push_back(release_list, sym->reg);
            sym->reg->need_wb |= need_wb;
            return sym->reg->aname;
        }
        sym->reg = alloc_reg(sym, sym->type->size, -1, 1);
        if (need_val)
        {
            emit("mov %s, [%s]\n", sym->reg->aname, sym->aname);
        }
    }
    else if (sym->kind == SK_Constant || sym->kind == SK_EnumConstant || sym->kind == SK_String)
    {
        assert(need_val);
        sym->reg = alloc_reg(sym, sym->type->size, -1, 1);
        emit("mov %s, %s\n", sym->reg->aname, sym->aname);
    }
    else
    {
        assert(0);
    }
    release_list->push_back(release_list, sym->reg);
    sym->reg->need_wb |= need_wb;
    return sym->reg->aname;
}

char *spec_reg_fetch(struct symbol_id *sym, enum reg reg, int need_val, int need_wb, struct vector *release_list)
{
    struct symbol_reg *temp_reg, *old_reg;
    assert(sym->kind == SK_Temp || sym->kind == SK_Variable);

    if (IS_SHARED_TEMP(sym))
    {
        if (temp_reg = find_shared_temp_reg(sym))
        {
            if (temp_reg->dec == reg)
            {
                reg_change_user(temp_reg, sym);
                temp_reg->need_wb |= need_wb;
                reserve_reg(sym);
                release_list->push_back(release_list, sym->reg);
                return temp_reg->aname;
            }
            else
            {
                release_reg(temp_reg);
            }
        }
    }

    if (sym->reg && sym->reg->dec == reg)
    {
        reserve_reg(sym);
        release_list->push_back(release_list, sym->reg);
        sym->reg->need_wb |= need_wb;
        return sym->reg->aname;
    }

    temp_reg = alloc_reg(NULL, 0, reg, 1);
    old_reg = sym->reg;

    if (old_reg)
    {
        if (need_val)
        {
            emit("mov %s, %s\n", temp_reg->aname, old_reg->aname);
        }
        old_reg->need_wb = 0;
        release_reg(old_reg);
    }
    else
    {
        if (need_val)
        {
            emit("mov %s, [%s]\n", temp_reg->aname, sym->aname);
        }
    }
    reg_change_user(temp_reg, sym);

    release_list->push_back(release_list, sym->reg);
    sym->reg->need_wb |= need_wb;
    return sym->reg->aname;
}

char *prio_reg_fetch(struct symbol_id *sym, int need_wb, struct vector *release_list)
{
    char *name;
    struct symbol_reg *reg;

    if (sym->kind == SK_Constant || sym->kind == SK_EnumConstant || sym->kind == SK_String ||
        sym->kind == SK_Function)
    {
        if (sym->kind == SK_Constant && IS_FLOAT(sym->type->categ))
        {
            name = format_name("[%s]", sym->aname);
            release_list->push_back(release_list, name);
            return name;
        }
        return sym->aname;
    }
    assert(sym->kind == SK_Temp || sym->kind == SK_Variable);
    if (IS_SHARED_TEMP(sym) && (reg = find_shared_temp_reg(need_wb)))
    {
        reg_change_user(reg, sym);
/*        reserve_reg(sym);
        release_list->push_back(release_list, sym->reg);
        reg->need_wb |= need_wb;
        return reg->aname;*/
    }
    
    if (sym->reg)
    {
        reserve_reg(sym);
        release_list->push_back(release_list, sym->reg);
        sym->reg->need_wb |= need_wb;
        return sym->reg->aname;
    }

    name = format_name("[%s]", sym->aname);
    release_list->push_back(release_list, name);
    return name;
}

void change_user(struct symbol_reg *reg, struct symbol_id *new_user, int need_wb, struct vector *release_list)
{
    struct symbol_reg *temp_reg;

    if (reg->user && (reg->user == new_user || (IS_SHARED_TEMP(reg->user) && IS_SHARED_TEMP(new_user))))
    {
        reg_change_user(reg, new_user);
        reg->need_wb |= need_wb;
        reserve_reg(new_user);
        release_list->push_back(release_list, reg);
        return;
    }
    if (IS_SHARED_TEMP(new_user))
    {
        write_back_reg(reg);
        temp_reg = find_shared_temp_reg(need_wb);
        assert(!temp_reg || temp_reg->user != new_user);
        assert(temp_reg != reg);

        if (temp_reg)
        {
            temp_reg->need_wb = 0;
            release_reg(temp_reg);
        }

        reg_change_user(reg, new_user);
        reg->need_wb |= need_wb;
        reserve_reg(new_user);
        release_list->push_back(release_list, reg);
        return;
    }
    if (reg->user && IS_SHARED_TEMP(reg->user))
    {
//        temp_reg = find_shared_temp_reg(reg->user);
//        assert(temp_reg == reg);
        write_back_reg(reg);
        reg_change_user(reg, new_user);
        reg->need_wb |= need_wb;
        reserve_reg(new_user);
        release_list->push_back(release_list, reg);
        return;
    }
    write_back_reg(reg);
    reg_change_user(reg, new_user);
    reg->need_wb |= need_wb;
    reserve_reg(new_user);
    release_list->push_back(release_list, reg);
}

void exit_ir_emit(struct vector *release_list)
{
    struct symbol_reg *reg;
    struct symbol_reg *temp_reg_o = NULL, *temp_reg_n = NULL;
    while(release_list->len)
    {
        reg = release_list->pop_back(release_list);
        if (((char *)reg)[0] == '[')
        {
            free(reg);
        }
        else
        {
            assert(reg->kind == SK_Register);
            assert(!reg->link);

            if (reg->user)
            {
                unreserve_reg(reg);
                // begin clear unused shared temp's reg
                if (IS_SHARED_TEMP(reg->user))
                {
                    if (temp_reg_n != reg && temp_reg_o != reg)
                    {
                        assert(!temp_reg_o);
                        temp_reg_o = temp_reg_n;
                        temp_reg_n = reg;
                    }
                }
                // end clear unused shared temp's reg
            }
            else
            {
                release_reg(reg);
            }
        }  
    }
    vector_destroy(release_list);
    // end clear unused shared temp's reg
    if (temp_reg_o && temp_reg_n)
    {
        if (temp_reg_o->need_wb && temp_reg_n->need_wb)
        {
            assert(0);
        }
        else if (!temp_reg_o->need_wb && !temp_reg_n->need_wb)
        {
            release_reg(temp_reg_o);
            release_reg(temp_reg_n);
        }
        else
        {
            temp_reg_o->need_wb ? release_reg(temp_reg_n) : release_reg(temp_reg_o);
        }
    }
    // end clear unused shared temp's reg
    emit("\n");
    just_for_debug();
}

/************************************************************************/

void ir_equality_integ_emit(struct ir *ir)
{
    struct symbol_id *rsltop;
 //   struct symbol_reg *rslt_reg;
    char *lop_name, *rop_name, *rsltop_name;
    struct vector *release_list;
    release_list = vector_init(0);

    assert(ir->rsltop->kind == SK_Temp && ir->rsltop->type->size == 4);
    rsltop = ir->rsltop;
/*
    if (rsltop->reg && rsltop->reg != &regs[EAX])
    {
        release_reg(rsltop->reg);
        rslt_reg = alloc_reg(ir->rsltop, 0, EAX, 1);
    }
    else if (!rsltop->reg)
    {
        rslt_reg = alloc_reg(ir->rsltop, 0, EAX, 1);
    }
    else
    {
        rslt_reg = rsltop->reg;
        reserve_reg(rsltop);
    }
    
    release_list->push_back(release_list, rslt_reg);
    */
    rsltop_name = spec_reg_fetch(rsltop, EAX, 0, 1, release_list);

    lop_name = reg_fetch(ir->lop, 1, 0, release_list);
    rop_name = prio_reg_fetch(ir->rop, 0, release_list);
    emit("cmp %s, %s\n", lop_name, rop_name);
    emit("lahf\n");
    if (ir->ircode == NEQ)
    {
        emit("not ah\n");
    }

    emit("and %s, 4000h\n", rsltop_name);
    emit("shr %s, 14\n", rsltop_name);
    
    exit_ir_emit(release_list);
}

void ir_equality_float_emit(struct ir *ir)
{
    struct symbol_id *rsltop;
    char *lop_name, *rop_name, *rsltop_name;
    struct symbol_reg *tmp_reg;
    struct vector *release_list;
    release_list = vector_init(0);

    assert(ir->rsltop->kind == SK_Temp || ir->rsltop->type->size == 4);
    rsltop = ir->rsltop;

/*
    if (rsltop->reg && rsltop->reg != &regs[EAX])
    {
        release_reg(rsltop->reg);
        rslt_reg = alloc_reg(ir->rsltop, 0, EAX, 1);
    }
    else if (!rsltop->reg)
    {
        rslt_reg = alloc_reg(ir->rsltop, 0, EAX, 1);
    }
    else
    {
        rslt_reg = rsltop->reg;
        reserve_reg(rsltop);
    }
    rslt_reg->need_wb = 1;
    release_list->push_back(release_list, rslt_reg);
*/
    rsltop_name = spec_reg_fetch(rsltop, EAX, 0, 1, release_list);

    lop_name = direct_mem_fetch(ir->lop, release_list);
    rop_name = direct_mem_fetch(ir->rop, release_list);

    ir->lop->type->size == 4 ? emit("fld dword %s\n", lop_name) : emit("fld qword %s\n", lop_name);
    ir->rop->type->size == 4 ? emit("fcomp dword %s\n", rop_name) : emit("fcomp qword %s\n", rop_name);
    emit("fstsw ax\n");
    if (ir->ircode == NEQ)
    {
        emit("not ax\n");
    }

    emit("and %s, 4000h\n", rsltop_name);
    emit("shr %s, 14\n", rsltop_name);
    
    exit_ir_emit(release_list);
}


/************************************************************************/

void ir_relation_integ_emit(struct ir *ir)
{
    int is_unsigned;
    struct symbol *label;
    struct symbol_id *rsltop;
    char *lop_name, *rop_name, *rsltop_name;
    struct vector *release_list;
    release_list = vector_init(0);

    assert(ir->rsltop->kind == SK_Temp && ir->rsltop->type->size == 4);
    assert(ir->lop->type->categ / 2 == ir->rop->type->categ / 2);    // both are signed or unsigned 
    rsltop = ir->rsltop;
    is_unsigned = ir->lop->type->categ % 2;
/*
    if (rsltop->reg)
    {
        rslt_reg = rsltop->reg;
        reserve_reg(rsltop);
    }
    else
    {
        rslt_reg = alloc_reg(ir->rsltop, 0, -1, 1);
    }
    rslt_reg->need_wb = 1;
    release_list->push_back(release_list, rslt_reg);
*/
    rsltop_name = reg_fetch(rsltop, 0, 1, release_list);

    label = sym_create_label();

    lop_name = reg_fetch(ir->lop, 1, 0, release_list);
    rop_name = prio_reg_fetch(ir->rop, 0, release_list);
    emit("mov %s, 1\n", rsltop_name);
    emit("cmp %s, %s\n", lop_name, rop_name);
    switch (ir->ircode)
    {
        case LS:
            is_unsigned ? emit("jb %s\n", label->aname) : emit("jl %s\n", label->aname);
            break;
        case GT:
            is_unsigned ? emit("ja %s\n", label->aname) : emit("jg %s\n", label->aname);
            break;
        case LE:
            is_unsigned ? emit("jbe %s\n", label->aname) : emit("jle %s\n", label->aname);
            break;
        case GE:
            is_unsigned ? emit("jae %s\n", label->aname) : emit("jge %s\n", label->aname);
            break;
    }
    
    emit("mov %s, 0\n", rsltop_name);
    emit("%s:\n", label->aname);

    free(label->name);
    free(label);
    exit_ir_emit(release_list);
}

void ir_relation_float_emit(struct ir *ir)
{
    struct symbol *label;
    struct symbol_id *rsltop;
    char *lop_name, *rop_name, *rsltop_name;
    struct symbol_reg *tmp_reg;
    struct vector *release_list;
    release_list = vector_init(0);

    assert(ir->rsltop->kind == SK_Temp && ir->rsltop->type->size == 4);
    rsltop = ir->rsltop;
/*
    if (rsltop->reg && rsltop->reg != &regs[EAX])
    {
        release_reg(rsltop->reg);
        rslt_reg = alloc_reg(ir->rsltop, 0, EAX, 1);
    }
    else if (!rsltop->reg)
    {
        rslt_reg = alloc_reg(ir->rsltop, 0, EAX, 1);
    }
    else
    {
        rslt_reg = rsltop->reg;
        reserve_reg(rsltop);
    }
    rslt_reg->need_wb = 1;
    release_list->push_back(release_list, rslt_reg);
*/
    tmp_reg = alloc_reg(NULL, 0, EAX, 1);
    release_list->push_back(release_list, tmp_reg);

    rsltop_name = reg_fetch(rsltop, 0, 1, release_list);

    label = sym_create_label();

    lop_name = direct_mem_fetch(ir->lop, release_list);
    rop_name = direct_mem_fetch(ir->rop, release_list);
    emit("mov %s, 1\n", rsltop_name);
    ir->lop->type->size == 4 ? emit("fld dword %s\n", lop_name) : emit("fld qword %s\n", lop_name);
    ir->rop->type->size == 4 ? emit("fcomp dword %s\n", rop_name) : emit("fcomp qword %s\n", rop_name);
    emit("fstsw ax\n");
    emit("and ax, 4500h\n");
    switch (ir->ircode)
    {
        case LS:
            emit("xor ax, 100h\n");
            emit("jz %s\n", label->aname);
            break;
        case GT:
            emit("xor ax, 0h\n");
            emit("jz %s\n", label->aname);
            break;
        case LE:
            emit("xor ax, 0h\n");
            emit("jnz %s\n", label->aname);
            break;
        case GE:
            emit("xor ax, 100h\n");
            emit("jnz %s\n", label->aname);
            break;
    }
    emit("mov %s, 0\n", rsltop_name);
    emit("%s:\n", label->aname);
    free(label->name);
    free(label);
    exit_ir_emit(release_list);
}

/************************************************************************/
// binrary op expect for MUL DIV MOD
void ir_binrary_integ_emit(struct ir *ir)
{
    int is_unsigned;
    struct symbol *label, *label1;
    struct symbol_id *rsltop;
    char *lop_name, *rop_name;
    struct vector *release_list;
    release_list = vector_init(0);

    is_unsigned = ir->lop->type->categ % 2;
    assert(ir->rsltop->kind == SK_Temp && ir->rsltop->type->size == 4);
    if (((struct symbol_id *)ir->rsltop)->reg)
    {
        release_reg(((struct symbol_id *)ir->rsltop)->reg);
    }

    if (ir->ircode != SHL && ir->ircode != SHR)
    {
        lop_name = reg_fetch(ir->lop, 1, 0, release_list);
        if (ir->rop)
        {
            rop_name = prio_reg_fetch(ir->rop, 0, release_list);
        }
    }
    else
    {
        rop_name = spec_reg_fetch(ir->rop, CL, 1, 0, release_list);
        lop_name = reg_fetch(ir->lop, 1, 0, release_list);
    }
    
    switch (ir->ircode)
    {
        case ADD:
            emit("add %s, %s\n", lop_name, rop_name);
            break;
        case SUB:
            emit("sub %s, %s\n", lop_name, rop_name);
            break;
        case SHR:
            is_unsigned ? emit("shr %s, %s\n", lop_name, rop_name) : emit("sar %s, %s\n", lop_name, rop_name);
            break;
        case SHL:
            emit("shl %s, cl\n", lop_name);
            break;
        /*
        case AND:
            label = sym_create_label();
            emit("and %s, %s\n", lop_name, rop_name);
            emit("jz %s\n", label->aname);
            emit("mov %s, 1h\n", lop_name);
            emit("%s:\n", label->aname);
            free(label->name);
            free(label);
            break;
        case OR:
            label = sym_create_label();
            emit("or %s, %s\n", lop_name, rop_name);
            emit("jz %s\n", label->aname);
            emit("mov %s, 1h\n", lop_name);
            emit("%s:\n", label->aname);
            free(label->name);
            free(label);
            break;
        */
        case NOT:
            label = sym_create_label();
            label1 = sym_create_label();
            emit("xor %s, 0h\n", lop_name);
            emit("jz %s\n", label->aname);
            emit("mov %s, 0h\n", lop_name);
            emit("jmp %s\n", label1->aname);
            emit("%s:\n", label->aname);
            emit("mov %s, 1h\n", lop_name);
            emit("%s:\n", label1->aname);
            free(label->name);
            free(label);
            free(label1->name);
            free(label1);
            break;
        case COMP:
            emit("not %s\n", lop_name);
            break;
        case NEG:
            emit("not %s\n", lop_name);
            emit("add %s, 1h\n", lop_name);
            break;
        case BAND:
            emit("and %s, %s\n", lop_name, rop_name);
            break;
        case BOR:
            emit("or %s, %s\n", lop_name, rop_name);
            break;
        case XOR:
            emit("xor %s, %s\n", lop_name, rop_name);
            break;
        default:
            assert(0);
    }

    change_user(((struct symbol_id *)ir->lop)->reg, ir->rsltop, 1, release_list);
    ((struct symbol_id *)ir->rsltop)->reg->need_wb = 1;
    exit_ir_emit(release_list);
}

void ir_mult_integ_emit(struct ir *ir)
{
    int is_unsigned;
    struct symbol_reg *tmp_reg;
    struct symbol_id *rsltop;
    char *lop_name, *rop_name;
    struct vector *release_list;
    release_list = vector_init(0);

    is_unsigned = ir->lop->type->categ % 2;
    assert(ir->rsltop->kind == SK_Temp && ir->rsltop->type->size == 4);

    if (((struct symbol_id *)ir->rsltop)->reg)
    {
        release_reg(((struct symbol_id *)ir->rsltop)->reg);
    }
    
    tmp_reg = alloc_reg(NULL, 0, EDX, 1);
    release_list->push_back(release_list, tmp_reg);
    lop_name = spec_reg_fetch(ir->lop, EAX, 1, 0, release_list);
    rop_name = reg_fetch(ir->rop, 1, 0, release_list);
    
    switch(ir->ircode)
    {
        case MUL:
            is_unsigned ? emit("mul %s\n", rop_name) : emit("imul %s\n", rop_name);
            change_user(&regs[EAX], ir->rsltop, 1, release_list);
            break;
        case DIV:
            emit("mov edx, 0h\n");
            is_unsigned ? emit("div %s\n", rop_name) : emit("idiv %s\n", rop_name);
            change_user(&regs[EAX], ir->rsltop, 1, release_list);
            break;
        case MOD:
            emit("mov edx, 0h\n");
            is_unsigned ? emit("div %s\n", rop_name) : emit("idiv %s\n", rop_name);
            change_user(&regs[EDX], ir->rsltop, 1, release_list);
            break;
        default:
            assert(0);
    }
    exit_ir_emit(release_list);
}

// binrary op of float include MUL DIV MOD but except for OR AND
void ir_arith_float_emit(struct ir *ir)
{
    enum reg aim_reg;
    struct symbol_id *rsltop;
    char *lop_name, *rop_name;
    struct symbol_reg *rslt_reg, *tmp_reg;
    struct vector *release_list;
    release_list = vector_init(0);

    assert(ir->rsltop->kind == SK_Temp);
    // rsltop must be a temp variable
    if (((struct symbol_id *)ir->rsltop)->reg)
    {
        assert(0);
        // release_reg(((struct symbol_id *)ir->rsltop)->reg);
    }

    lop_name = direct_mem_fetch(ir->lop, release_list);
    if (ir->rop)
    {
        rop_name = direct_mem_fetch(ir->rop, release_list);
    }
    

    if (ir->ircode == NEG)
    {
        emit("fldz\n");
    }
    else
    {
        ir->lop->type->size == 4 ? emit("fld dword %s\n", lop_name) : emit("fld qword %s\n", lop_name);
    }
    
    switch (ir->ircode)
    {
        case ADD:
            ir->rop->type->size == 4 ? emit("fadd dword %s\n", rop_name) : emit("fadd qword %s\n", rop_name);
            break;
        case SUB:
            ir->rop->type->size == 4 ? emit("fsub dword %s\n", rop_name) : emit("fsub qword %s\n", rop_name);
            break;
        case MUL:
            ir->rop->type->size == 4 ? emit("fmul dword %s\n", rop_name) : emit("fmul qword %s\n", rop_name);
            break;
        case DIV:
            ir->rop->type->size == 4 ? emit("fdiv dword %s\n", rop_name) : emit("fdiv qword %s\n", rop_name);
            break;
        case NEG:
            ir->lop->type->size == 4 ? emit("fsub dword %s\n", lop_name) : emit("fsub qword %s\n", lop_name);
            break;
        default:
            assert(0);
    }
    
    ir->lop->type->size == 4 ? emit("fstp dword [%s]\n", ir->rsltop->aname) : 
                                emit("fstp qword [%s]\n", ir->rsltop->aname);
    exit_ir_emit(release_list);
}

// not yet implemented
// logical AND OR NOT for float point
void ir_logical_float_emit(struct ir *ir)
{
    struct symbol *label;
    char *lop_name, *rop_name, *rsltop_name;
    struct vector *release_list;
    release_list = vector_init(0);

    rsltop_name = spec_reg_fetch(ir->rsltop, EAX, 0, 1, release_list);

    label = sym_create_label();

    lop_name = direct_mem_fetch(ir->lop, release_list);
    if (ir->rop)
    {
        rop_name = direct_mem_fetch(ir->rop, release_list);
    }

    switch (ir->ircode)
    {
        /*
        case AND:
            emit("mov %s, 0\n", rsltop_name);
            emit("fldz\n");
            ir->rop->type->size == 4 ? emit("fcom dword %s\n", lop_name) : emit("fcom qword %s\n", lop_name);
            emit("fstsw ax\n");
            emit("and ax, 4000h\n");
            emit("jnz %s\n", label->aname);
            ir->rop->type->size == 4 ? emit("fcomp dword %s\n", rop_name) : emit("fcomp qword %s\n", rop_name);
            emit("fstsw ax\n");
            emit("and ax, 4000h\n");
            emit("jnz %s\n", label->aname);
            emit("mov %s, 1\n", rsltop_name);
            emit("%s:\n", label->aname);
            break;
        case OR:
            emit("mov %s, 1\n", rsltop_name);
            emit("fldz\n");
            ir->rop->type->size == 4 ? emit("fcom dword %s\n", lop_name) : emit("fcom qword %s\n", lop_name);
            emit("fstsw ax\n");
            emit("and ax, 4000h\n");
            emit("jz %s\n", label->aname);
            ir->rop->type->size == 4 ? emit("fcomp dword %s\n", rop_name) : emit("fcomp qword %s\n", rop_name);
            emit("fstsw ax\n");
            emit("and ax, 4000h\n");
            emit("jz %s\n", label->aname);
            emit("mov %s, 0\n", rsltop_name);
            emit("%s:\n", label->aname);
            break;
        */
        case NOT:
            emit("mov %s, 1\n", rsltop_name);
            emit("fldz\n");
            ir->lop->type->size == 4 ? emit("fcomp dword %s\n", lop_name) : emit("fcomp qword %s\n", lop_name);
            emit("fstsw ax\n");
            emit("and ax, 4000h\n");
            emit("jnz %s\n", label->aname);
            emit("mov %s, 0\n", rsltop_name);
            emit("%s:\n", label->aname);
            break;
        default:
            assert(0);
    }
    free(label->name);
    free(label);
    exit_ir_emit(release_list);
}

void ir_get_addr_emit(struct ir *ir)
{
    char *rsltop_name, *lop_name;
    struct vector *release_list;
    release_list = vector_init(0);

    assert(ir->lop->kind != SK_Temp);
    assert(ir->rsltop->kind == SK_Temp && ir->rsltop->type->size == 4);

    rsltop_name = reg_fetch(ir->rsltop, 0, 1, release_list);

    emit("lea %s, [%s]\n", rsltop_name, ir->lop->aname);
    
    exit_ir_emit(release_list);
}

// load t0 t0
void ir_load_emit(struct ir *ir)
{
    struct symbol_reg *tmp_reg;
    char *rsltop_name, *lop_name;
    struct vector *release_list;
    release_list = vector_init(0);

    assert(ir->lop->type->size == 4);

    if (ir->rsltop->type->size <= 4)
    {
        lop_name = reg_fetch(ir->lop, 1, 0, release_list);
        rsltop_name = reg_fetch(ir->rsltop, 0, 1, release_list);
        emit("mov %s, [%s]\n", rsltop_name, lop_name);
    }
    else
    {
        assert(ir->rsltop->type->size == 8);
        assert(((struct symbol_id*)ir->rsltop)->reg == NULL);
        lop_name = reg_fetch(ir->lop, 1, 0, release_list);
        tmp_reg = alloc_reg(NULL, 4, -1, 1);
        release_list->push_back(release_list, tmp_reg);
        emit("mov %s, [%s]\n", tmp_reg->aname, lop_name);
        emit("mov [%s], %s\n", ir->rsltop->aname, tmp_reg->aname);
        emit("mov %s, [%s + 4]\n", tmp_reg->aname, lop_name);
        emit("mov [%s + 4], %s\n", ir->rsltop->aname, tmp_reg->aname);
    }
    

    exit_ir_emit(release_list);
}

void ir_store_emit(struct ir *ir)
{
    struct symbol_reg *tmp_reg;
    char *rsltop_name, *lop_name;
    struct vector *release_list;
    release_list = vector_init(0);

    if (ir->lop->type->size <= 4)
    {
        lop_name = reg_fetch(ir->lop, 1, 0, release_list);
        rsltop_name = reg_fetch(ir->rsltop, 1, 1, release_list);
        ((struct symbol_id*)ir->rsltop)->reg->need_wb = 0;
        emit("mov [%s], %s\n", rsltop_name, lop_name);
    }
    else
    {
        assert(ir->lop->type->size == 8);
        assert((ir->lop != SK_Temp && ir->lop != SK_Variable) || ((struct symbol_id*)ir->lop)->reg == NULL);
        tmp_reg = alloc_reg(NULL, 4, -1, 1);
        rsltop_name = reg_fetch(ir->rsltop, 1, 1, release_list);
        release_list->push_back(release_list, tmp_reg);
        emit("mov %s, [%s]\n", tmp_reg->aname, ir->lop->aname);
        emit("mov [%s], %s\n", rsltop_name, tmp_reg->aname);
        emit("mov %s, [%s + 4]\n", tmp_reg->aname, ir->lop->aname);
        emit("mov [%s + 4], %s\n", rsltop_name, tmp_reg->aname);
    }
    
    exit_ir_emit(release_list);
}

void ir_label_emit(struct ir *ir)
{
    if (!((struct symbol_label*)ir->rsltop)->is_func_label)
    {
        emit("%s:\n", ir->rsltop->aname);
    }
}

void move_block_emit(char *to, char *from, int size)
{
    struct vector *release_list;
    release_list = vector_init(0);

    // direct_mem_fetch()
    release_list->push_back(release_list, alloc_reg(NULL, 0, EDI, 1));
    release_list->push_back(release_list, alloc_reg(NULL, 0, ESI, 1));
    release_list->push_back(release_list, alloc_reg(NULL, 0, ECX, 1));

    emit("lea edi, [%s]\n", to);
    emit("lea esi, [%s]\n", from);
    emit("mov ecx, %d\n", size);
    emit("rep movsb\n");
    exit_ir_emit(release_list);
}

void ir_assign_emit(struct ir *ir)
{
    char *rsltop_name, *lop_name;
    struct symbol_reg *tmp_reg;
    struct vector *release_list;
    release_list = vector_init(0);
    if (ir->rsltop == ir->lop || IS_SHARED_TEMP(ir->rsltop) && IS_SHARED_TEMP(ir->lop))
    {
        goto out;
    }
    switch(ir->rsltop->type->size)
    {
        case 1:
        case 2:
        case 4:
            rsltop_name = prio_reg_fetch(ir->rsltop, 1, release_list);
            lop_name = reg_fetch(ir->lop, 1, 0, release_list);
            emit("mov %s, %s\n", rsltop_name, lop_name);
            break;
        case 8:
            assert(!((struct symbol_id *)ir->rsltop)->reg && !((struct symbol_id *)ir->lop)->reg);
            tmp_reg = alloc_reg(NULL, 4, -1, 1);
            release_list->push_back(release_list, tmp_reg);
            emit("mov %s, [%s]\n", tmp_reg->aname, ir->lop->aname);
            emit("mov [%s], %s\n", ir->rsltop->aname, tmp_reg->aname);
            emit("mov %s, [%s + 4]\n", tmp_reg->aname, ir->lop->aname);
            emit("mov [%s + 4], %s\n", ir->rsltop->aname, tmp_reg->aname);
            break;
        default:
            assert(!((struct symbol_id *)ir->rsltop)->reg && !((struct symbol_id *)ir->lop)->reg);
            assert(ir->rsltop->type->size == ir->lop->type->size);
            move_block_emit(ir->rsltop->aname, ir->lop->aname, ir->lop->type->size);
            break;
    }
out:
    exit_ir_emit(release_list);
}

void ir_ret_emit(struct ir *ir)
{
    int i;
    char *return_val_name;
    struct vector *release_list;
    release_list = vector_init(0);

    
    if (ir->rsltop->type->categ == FLOAT)
    {
        return_val_name = prio_reg_fetch(ir->rsltop, 0, release_list);
        emit("fld dword %s\n", return_val_name);
    }
    else if (ir->rsltop->type->categ == DOUBLE)
    {
        return_val_name = prio_reg_fetch(ir->rsltop, 0, release_list);
        emit("fld qword %s\n", return_val_name);
    }
    else if (IS_RECORD_TYPE(ir->rsltop->type) && ir->rsltop->type->size != 1 && ir->rsltop->type->size != 2 &&
            ir->rsltop->type->size != 4 && ir->rsltop->type->size != 8)
    {
        assert(!((struct symbol_id *)ir->rsltop)->reg);
        release_list->push_back(release_list, alloc_reg(NULL, 0, EDI, 1));
        release_list->push_back(release_list, alloc_reg(NULL, 0, ESI, 1));
        release_list->push_back(release_list, alloc_reg(NULL, 0, ECX, 1));
        emit("mov edi, [ebp + 20]\n");
        emit("lea esi, [%s]\n", ir->rsltop->aname);
        emit("mov ecx, %d\n", ir->rsltop->type->size);
        emit("rep movsb\n");
    }
    else
    {
        switch (ir->rsltop->type->size)
        {
        case 1:
            release_list->push_back(release_list, alloc_reg(NULL, 0, AL, 1));
            return_val_name = prio_reg_fetch(ir->rsltop, 0, release_list);
            emit("mov al, %s\n", return_val_name);
            break;
        case 2:
            release_list->push_back(release_list, alloc_reg(NULL, 0, AX, 1));
            return_val_name = prio_reg_fetch(ir->rsltop, 0, release_list);
            emit("mov ax, %s\n", return_val_name);
            break;
        case 4:
            release_list->push_back(release_list, alloc_reg(NULL, 0, EAX, 1));
            return_val_name = prio_reg_fetch(ir->rsltop, 0, release_list);
            emit("mov eax, %s\n", return_val_name);
            break;
        case 8:
            release_list->push_back(release_list, alloc_reg(NULL, 0, EAX, 1));
            release_list->push_back(release_list, alloc_reg(NULL, 0, EDX, 1));
            return_val_name = direct_mem_fetch(ir->rsltop, release_list);
            emit("mov eax, %s\n", return_val_name);
            emit("mov edx, [%s + 4]\n", ir->rsltop->aname);
            break;
        default:
            assert(0);
            break;
        }
    }
    
    exit_ir_emit(release_list);
}

void ir_call_emit(struct ir *ir)
{
    int is_unsigned;
    int i, j, stk_size;
    char *arg_name, *rslt_name, *func_name;
    struct symbol_reg *reg;
    struct symbol_reg *esi, *edi;
    struct type *return_type;
    struct arg_list *arg_list_head;
    struct symbol_reg *temp_reg_o = NULL, *temp_reg_n = NULL;
    struct vector *release_list;
    release_list = vector_init(0);
    
    stk_size = 0;
    for (arg_list_head = ir->rop; arg_list_head; arg_list_head = arg_list_head->next)
    {
        // we are sure that esp has been four-byte aligned
        is_unsigned = arg_list_head->sym->type->categ % 2;
        switch (arg_list_head->sym->type->size)
        {
            case 1:
                arg_name = prio_reg_fetch(arg_list_head->sym, 0, release_list);
                reg = alloc_reg(NULL, 4, -1, 1);
                release_list->push_back(release_list, reg);
                is_unsigned ? emit("movzx %s, byte %s\n", reg->aname, arg_name) : 
                                emit("movsx %s, byte %s\n", reg->aname, arg_name);
                emit("push %s\n", reg->aname);
                stk_size += 4;
                release_reg(reg);
                break;
            case 2:
                arg_name = prio_reg_fetch(arg_list_head->sym, 0, release_list);
                reg = alloc_reg(NULL, 4, -1, 1);
                release_list->push_back(release_list, reg);
                is_unsigned ? emit("movzx %s, word %s\n", reg->aname, arg_name) : 
                                emit("movsx %s, word %s\n", reg->aname, arg_name);
                emit("push %s\n", reg->aname);
                stk_size += 4;
                release_reg(reg);
                break;
            case 4:
                arg_name = prio_reg_fetch(arg_list_head->sym, 0, release_list);
                emit("push dword %s\n", arg_name);
                stk_size += 4;
                break;
            case 8:
                emit("push dword [%s + 4]\n", arg_list_head->sym->aname);
                emit("push dword [%s]\n", arg_list_head->sym->aname);
                stk_size += 8;
                break;
            default:
                if (arg_list_head->sym->type->size % 4)
                {
                    i = 4 - arg_list_head->sym->type->size % 4;
                    for (; i > 0; i--)
                    {
                        emit("push byte 0\n");
                        stk_size++;
                    }
                }
                esi = alloc_reg(NULL, 0, ESI, 1);
                edi = alloc_reg(NULL, 0, EDI, 1);
                reg = alloc_reg(NULL, 0, ECX, 1);
                release_list->push_back(release_list, esi);
                release_list->push_back(release_list, edi);
                release_list->push_back(release_list, reg);
                emit("sub esp, %d\n", arg_list_head->sym->type->size);
                emit("lea esi, [%s]\n", arg_list_head->sym->aname);
                emit("mov edi, esp\n");
                emit("mov ecx, %d\n", arg_list_head->sym->type->size);
                emit("rep movsb\n");
                release_reg(esi);
                release_reg(edi);
                release_reg(reg);
                break;
        }
    }
    assert(stk_size % 4 == 0);
    if (ir->lop->type->categ == POINTER)
    {
        assert(ir->lop->type->basic_type->categ == FUNCTION);
        return_type = ir->lop->type->basic_type->basic_type;
        func_name = prio_reg_fetch(ir->lop, 0, release_list);
    }
    else
    {
        assert(ir->lop->type->categ == FUNCTION);
        return_type = ir->lop->type->basic_type;
        func_name = ir->lop->aname;
    }
    
    assert(return_type->size == ir->rsltop->type->size);
    if (IS_RECORD_TYPE(return_type) && return_type->size != 1 && return_type->size != 2 &&
            return_type->size != 4 && return_type->size != 8)
    {
        reg = alloc_reg(NULL, 4, -1, 1);
        release_list->push_back(release_list, reg);
        emit("lea %s, [%s]\n", reg->aname, ir->rsltop->aname);
        emit("push dword %s\n", reg->aname);
        stk_size += 4;
    }
    release_reg(&regs[EAX]);
    release_reg(&regs[ECX]);
    release_reg(&regs[EDX]);
    emit("call %s\n", func_name);

// return

    if (stk_size)
    {
        emit("add esp, %d\n", stk_size);
    }
    rslt_name = direct_mem_fetch(ir->rsltop, release_list);
    if (return_type->categ == FLOAT)
    {
        emit("fstp dword %s\n", rslt_name);
    }
    else if (return_type->categ == DOUBLE)
    {
        emit("fstp qword %s\n", rslt_name);
    }
    else
    {
        switch (return_type->size)
        {
        case 1:
            emit("mov %s, al\n", rslt_name);
            break;
        case 2:
            emit("mov %s, ax\n", rslt_name);
            break;
        case 4:
            emit("mov %s, eax\n", rslt_name);
            break;
        case 8:
            emit("mov %s, eax\n", rslt_name);
            emit("mov [%s + 4], edx\n", ir->rsltop->aname);
            break;
        default:
            assert(return_type->categ == STRUCT || return_type->categ == UNION);
            break;
        }
    }

    // exit_ir_emit(release_list);
    while(release_list->len)
    {
        reg = release_list->pop_back(release_list);
        if (((char *)reg)[0] == '[')
        {
            free(reg);
        }
        else
        {
            assert(reg->kind == SK_Register);
            assert(!reg->link);
            if (reg->user)
            {
                unreserve_reg(reg);
                 // begin clear unused shared temp's reg
                if (IS_SHARED_TEMP(reg->user))
                {
                    assert(!temp_reg_o);
                    temp_reg_o = temp_reg_n;
                    temp_reg_n = reg;
                }
                // end clear unused shared temp's reg
            }
            else if (reg->dec != EAX && reg->dec != ECX && reg->dec != EDX)
            {
                release_reg(reg);
            }
        }  
    }
    vector_destroy(release_list);
    // end clear unused shared temp's reg
    if (temp_reg_o && temp_reg_n)
    {
        if (temp_reg_o->need_wb && temp_reg_n->need_wb)
        {
            assert(0);
        }
        else if (!temp_reg_o->need_wb && !temp_reg_n->need_wb)
        {
            release_reg(temp_reg_o);
            release_reg(temp_reg_n);
        }
        else
        {
            temp_reg_o->need_wb ? release_reg(temp_reg_n) : release_reg(temp_reg_o);
        }
    }
    // end clear unused shared temp's reg
    just_for_debug();
}

void ir_jt_jnt_emit(struct ir *ir)
{
    char *rsltop_name, *lop_name;
    struct symbol_reg *tmp_reg;
    struct symbol *label;
    struct vector *release_list;
    release_list = vector_init(0);

    
    if (ir->lop->type->categ == FLOAT || ir->lop->type->categ == DOUBLE)
    {
        release_list->push_back(release_list, alloc_reg(NULL, 0, AX, 1));
        lop_name = direct_mem_fetch(ir->lop, release_list);
        ir->lop->type->categ == FLOAT ? emit("fld dword %s\n", lop_name) :  emit("fstp qword %s\n", lop_name);
        emit("ftst\n");
        emit("fstsw ax\n");
        emit("and ax, 4500h\n");
        emit("xor ax, 4000h\n");
        ir->ircode == JNT ? emit("jz %s\n", ir->rsltop->aname) : emit("jnz %s\n", ir->rsltop->aname);
    }
    else
    {
        switch (ir->lop->type->size)
        {
        case 1:
            lop_name = prio_reg_fetch(ir->lop, 0, release_list);
            emit("cmp byte %s, 0h\n", lop_name);
            ir->ircode == JNT ? emit("jz %s\n", ir->rsltop->aname) : emit("jnz %s\n", ir->rsltop->aname);
            break;
        case 2:
            lop_name = prio_reg_fetch(ir->lop, 0, release_list);
            emit("cmp word %s, 0h\n", lop_name);
            ir->ircode == JNT ? emit("jz %s\n", ir->rsltop->aname) : emit("jnz %s\n", ir->rsltop->aname);
            break;
        case 4:
            lop_name = prio_reg_fetch(ir->lop, 0, release_list);
            emit("cmp dword %s, 0h\n", lop_name);
            ir->ircode == JNT ? emit("jz %s\n", ir->rsltop->aname) : emit("jnz %s\n", ir->rsltop->aname);
            break;
        case 8:
            assert(!((struct symbol_id*)ir->lop)->reg);
            tmp_reg = alloc_reg(NULL, 4, -1, 1);
            release_list->push_back(release_list, tmp_reg);
            label = sym_create_label();
            emit("mov %s, [%s]\n", tmp_reg->aname, ir->lop->aname);
            emit("cmp %s, 0h\n", tmp_reg->aname);
            ir->ircode == JNT ? emit("jnz %s\n", label->aname) : emit("jz %s\n", label->aname);
            emit("mov %s, [%s + 4]\n", tmp_reg->aname, ir->lop->aname);
            emit("cmp %s, 0h\n", tmp_reg->aname);
            ir->ircode == JNT ? emit("jnz %s\n", label->aname) : emit("jz %s\n", label->aname);
            emit("jmp %s\n", ir->rsltop->aname);
            emit("%s:\n", label->aname);
            free(label->aname);
            free(label);
            break;
        default:
            assert(0);
            break;
        }
    }
    exit_ir_emit(release_list);
}

void ir_je_jne_emit(struct ir *ir)
{
    char *rsltop_name, *lop_name, *rop_name;
    struct symbol_reg *tmp_reg;
    struct symbol *label;
    struct vector *release_list;
    release_list = vector_init(0);

    if (ir->lop->type->categ == FLOAT || ir->lop->type->categ == DOUBLE)
    {
        release_list->push_back(release_list, alloc_reg(NULL, 0, AX, 1));
        lop_name = direct_mem_fetch(ir->lop, release_list);
        rop_name = direct_mem_fetch(ir->rop, release_list);
        ir->lop->type->categ == FLOAT ? emit("fld dword %s\n", lop_name) :  emit("fld qword %s\n", lop_name);
        ir->rop->type->categ == FLOAT ? emit("fcomp dword %s\n", rop_name) :  emit("fcomp qword %s\n", rop_name);
        emit("fstsw ax\n");
        emit("and ax, 4500h\n");
        emit("xor ax, 4000h\n");
        ir->ircode == JE ? emit("jz %s\n", ir->rsltop->aname) : emit("jnz %s\n", ir->rsltop->aname);
    }
    else
    {
        switch (ir->lop->type->size)
        {
        case 1:
        case 2:
        case 4:
            lop_name = reg_fetch(ir->lop, 1, 0, release_list);
            rop_name = prio_reg_fetch(ir->rop, 0, release_list);
            emit("cmp %s, %s\n", lop_name, rop_name);
            ir->ircode == JE ? emit("jz %s\n", ir->rsltop->aname) : emit("jnz %s\n", ir->rsltop->aname);
            break;
        case 8:
            assert(!((struct symbol_id*)ir->lop)->reg && !((struct symbol_id*)ir->rop)->reg);
            tmp_reg = alloc_reg(NULL, 4, -1, 1);
            release_list->push_back(release_list, tmp_reg);
            label = sym_create_label();
            emit("mov %s, [%s]\n", tmp_reg->aname, ir->lop->aname);
            emit("cmp %s, [%s]\n", tmp_reg->aname, ir->rop->aname);
            ir->ircode == JE ? emit("jnz %s\n", label->aname) : emit("jz %s\n", label->aname);
            emit("mov %s, [%s + 4]\n", tmp_reg->aname, ir->lop->aname);
            emit("cmp %s, [%s]\n", tmp_reg->aname, ir->rop->aname);
            ir->ircode == JE ? emit("jnz %s\n", label->aname) : emit("jz %s\n", label->aname);
            emit("jmp %s\n", ir->rsltop->aname);
            emit("%s:\n", label->aname);
            free(label->aname);
            free(label);
            break;
        default:
            assert(0);
            break;
        }
    }
    exit_ir_emit(release_list);

}

void ir_jmp_emit(struct ir *ir)
{
    emit("jmp %s\n", ir->rsltop->aname);
}

void ir_inc_dec_integ_emit(struct ir *ir)
{
    int scale;
    char *lop_name;
    struct symbol_reg *tmp_reg;
    struct symbol *label;
    struct vector *release_list;
    release_list = vector_init(0);

    assert(ir->rsltop == ir->lop);
    if (IS_FLOAT(ir->rsltop->type->categ))
    {
        lop_name = direct_mem_fetch(ir->lop, release_list);
        emit("fld1\n");
        if (ir->ircode == INC)
        {
            ir->rsltop->type->categ == FLOAT ? emit("fadd dword %s\n", lop_name) : emit("fadd qword %s\n", lop_name);
        }
        else
        {
            ir->rsltop->type->categ == FLOAT ? emit("fsub dword %s\n", lop_name) : emit("fsub qword %s\n", lop_name);
            emit("fchs\n");
        }
        ir->rsltop->type->categ == FLOAT ? emit("fstp dword%s\n", lop_name) : emit("fstp qword %s\n", lop_name);
    }
    else
    {
        lop_name = prio_reg_fetch(ir->lop, 1, release_list);
        assert(ir->rop->kind == SK_Constant);
        scale = ir->rop->value.i;
        switch (ir->rsltop->type->size)
        {
        case 1:
            if (scale == 1)
            {
                ir->ircode == INC ? emit("inc byte %s\n", lop_name) : emit("dec byte %s\n", lop_name);
            }
            else
            {
                ir->ircode == INC ? emit("add byte %s, %d\n", lop_name, scale) : emit("sub byte %s, %d\n", lop_name, scale);
            }
            break;
        case 2:
            if (scale == 1)
            {
                ir->ircode == INC ? emit("inc word %s\n", lop_name): emit("dec word %s\n", lop_name);
            }
            else
            {
                ir->ircode == INC ? emit("add word %s, %d\n", lop_name, scale) : emit("sub word %s, %d\n", lop_name, scale);
            }
            break;
        case 4:
            if (scale == 1)
            {
                ir->ircode == INC ? emit("inc dword %s\n", lop_name): emit("dec dword %s\n", lop_name);
            }
            else
            {
                ir->ircode == INC ? emit("add dword %s, %d\n", lop_name, scale) : emit("sub dword %s, %d\n", lop_name, scale);
            }
            
            break;
        case 8:
            if (scale == 1)
            {
                ir->ircode == INC ? emit("inc qword %s\n", lop_name): emit("dec qword %s\n", lop_name);
            }
            else
            {
                ir->ircode == INC ? emit("add qword %s, %d\n", lop_name, scale) : emit("sub qword %s, %d\n", lop_name, scale);
            }
            break;
        default:
            assert(0);
        }
    }
    exit_ir_emit(release_list);
}

void ir_cast_emit(struct ir *ir)
{
    char *rsltop_name, *lop_name, *rop_name;
    struct symbol_reg *tmp_reg;
    struct symbol *label;
    struct vector *release_list;
    release_list = vector_init(0);

    switch (ir->ircode)
    {
    case CAST_I1_I4:
        rsltop_name = reg_fetch(ir->rsltop, 0, 1, release_list);
        lop_name = prio_reg_fetch(ir->lop, 0, release_list);
        emit("movsx %s, byte %s\n", rsltop_name, lop_name);
        break;
    case CAST_I2_I4:
        rsltop_name = reg_fetch(ir->rsltop, 0, 1, release_list);
        lop_name = prio_reg_fetch(ir->lop, 0, release_list);
        emit("movsx %s, word %s\n", rsltop_name, lop_name);
        break;
    case CAST_U1_I4:
        rsltop_name = reg_fetch(ir->rsltop, 0, 1, release_list);
        lop_name = prio_reg_fetch(ir->lop, 0, release_list);
        emit("movzx %s, byte %s\n", rsltop_name, lop_name);
        break;
    case CAST_U2_I4:
        rsltop_name = reg_fetch(ir->rsltop, 0, 1, release_list);
        lop_name = prio_reg_fetch(ir->lop, 0, release_list);
        emit("movzx %s, word %s\n", rsltop_name, lop_name);
        break;
    case CAST_I4_I1:
    case CAST_I4_U1:
        rsltop_name = prio_reg_fetch(ir->rsltop, 1, release_list);
        lop_name = spec_reg_fetch(ir->lop, EAX, 1, 0, release_list);
        emit("mov %s, al\n", rsltop_name);
        break;
    case CAST_I4_U2:
    case CAST_I4_I2:
        rsltop_name = prio_reg_fetch(ir->rsltop, 1, release_list);
        lop_name = spec_reg_fetch(ir->lop, EAX, 1, 0, release_list);
        emit("mov %s, ax\n", rsltop_name);
        break;
    case CAST_I4_F4:
    case CAST_I4_F8:
        rsltop_name = direct_mem_fetch(ir->rsltop, release_list);
        lop_name = direct_mem_fetch(ir->lop, release_list);
        emit("fild dword%s\n", lop_name);
        ir->ircode == CAST_I4_F4 ? emit("fstp dword%s\n", rsltop_name) : emit("fstp qword%s\n", rsltop_name);
        break;
    case CAST_U4_F4:
    case CAST_U4_F8:
        rsltop_name = direct_mem_fetch(ir->rsltop, release_list);
        lop_name = prio_reg_fetch(ir->lop, 0, release_list);
        emit("push dword 0h\n", lop_name);
        emit("push dword %s\n", lop_name);
        emit("fild qword [esp]\n");
        emit("add esp, 8h\n");
        ir->ircode == CAST_U4_F4 ? emit("fstp dword%s\n", rsltop_name) : emit("fstp qword%s\n", rsltop_name);
        break;
    case CAST_F4_U4:
    case CAST_F4_I4:
    case CAST_F8_I4:
    case CAST_F8_U4:
        rsltop_name = direct_mem_fetch(ir->rsltop, release_list);
        lop_name = direct_mem_fetch(ir->lop, release_list);
        if (ir->ircode == CAST_F8_I4 || ir->ircode == CAST_F8_U4)
        {
            emit("fld qword %s\n", lop_name);
        }
        else
        {
            emit("fld dword %s\n", lop_name);
        }
        emit("fisttp dword %s\n", rsltop_name);
        break;
    case CAST_F4_F8:
        rsltop_name = direct_mem_fetch(ir->rsltop, release_list);
        lop_name = direct_mem_fetch(ir->lop, release_list);
        emit("fld dword %s\n", lop_name);
        emit("fstp qword %s\n", rsltop_name);
        break;
    case CAST_F8_F4:
        rsltop_name = direct_mem_fetch(ir->rsltop, release_list);
        lop_name = direct_mem_fetch(ir->lop, release_list);
        emit("fld qword %s\n", lop_name);
        emit("fstp dword %s\n", rsltop_name);
        break;
        break;
/////////////////////////////////////////////
    case CAST_I1_U1:
    case CAST_U1_I1:
    case CAST_I2_U2:
    case CAST_U2_I2:
    case CAST_I4_U4:
    case CAST_U4_I4:
        if (!(ir->rsltop == ir->lop || IS_SHARED_TEMP(ir->rsltop) && IS_SHARED_TEMP(ir->lop)))
        {
            rsltop_name = reg_fetch(ir->rsltop, 0, 1, release_list);
            lop_name = prio_reg_fetch(ir->lop, 1, release_list);
            emit("mov %s, %s\n", rsltop_name, lop_name);
        }
        break;
    default:
        assert(0);
        break;
    }
    exit_ir_emit(release_list);
}

void write_back_var(struct basic_block *bb)
{
    int i;
    int flag;
    do
	{
		flag = 0;
		for (i = 0; i < 18; i++)
		{
			if (regs[i].used && regs[i].link == NULL)
			{
				if (!((regs[i].user && IS_SHARED_TEMP(regs[i].user)) || 
                    (regs[i].user && !find_item_from_sets(bb->out_set, regs[i].user))))
                {
                    release_reg(&regs[i]);
				    flag = 1;
                }
			}
		}
	} while(flag);
}

void discard_temp(struct basic_block *bb)
{
    int i;
    int flag;
    do
	{
		flag = 0;
		for (i = 0; i < 18; i++)
		{
			if (regs[i].used && regs[i].link == NULL)
			{
				if ((regs[i].user && IS_SHARED_TEMP(regs[i].user)) || 
                    (regs[i].user && !find_item_from_sets(bb->out_set, regs[i].user)))
                {
                    regs[i].need_wb = 0;
                    release_reg(&regs[i]);
                    flag = 1;
                }
			}
		}
	} while(flag);
}

void basic_block_emit(struct basic_block *bb)
{
    int i, write_back = 0;
    struct ir *ir;
    for (i = 0; i < bb->ir_list->len; i++)
    {
        ir = bb->ir_list->get(bb->ir_list, i);
        if ((i == bb->ir_list->len - 1) && (IS_JMP_IR(ir->ircode)))
        {
            write_back_var(bb);
            write_back = 1;
        }
        switch (ir->ircode)
        {
        case EQ:
        case NEQ:
            IS_FLOAT(ir->lop->type->categ) ? ir_equality_float_emit(ir) : ir_equality_integ_emit(ir);
            break;
        case LS:
        case GT:
        case LE:
        case GE:
            IS_FLOAT(ir->lop->type->categ) ? ir_relation_float_emit(ir) : ir_relation_integ_emit(ir);
            break;
        case ADD:
        case SUB:
        case NEG:
            IS_FLOAT(ir->lop->type->categ) ? ir_arith_float_emit(ir) : ir_binrary_integ_emit(ir);
            break;
        case SHR:
        case SHL:
        case BAND:
        case BOR:
        case XOR:
        case COMP:
            IS_FLOAT(ir->lop->type->categ) ? assert(0) : ir_binrary_integ_emit(ir);
            break;
        /*case AND:
        case OR:*/
        case NOT:
            IS_FLOAT(ir->lop->type->categ) ? ir_logical_float_emit(ir) : ir_binrary_integ_emit(ir);
            break;
        case MUL:
        case DIV:
        case MOD:
            IS_FLOAT(ir->lop->type->categ) ? ir_arith_float_emit(ir) : ir_mult_integ_emit(ir);
            break;
        case ASSIGN:
            ir_assign_emit(ir);
            break;
        case INC:
        case DEC:
            ir_inc_dec_integ_emit(ir);
            break;
        case GETADDR:
            ir_get_addr_emit(ir);
            break;
        case LOAD:
            ir_load_emit(ir);
            break;
        case STORE:
            ir_store_emit(ir);
            break;
        case LABEL:
            ir_label_emit(ir);
            break;
        case RET:
            ir_ret_emit(ir);
            break;
        case CALL:
            ir_call_emit(ir);
            break;
        case JT:
        case JNT:
            ir_jt_jnt_emit(ir);
            break;
        case JE:
        case JNE:
            ir_je_jne_emit(ir);
            break;
        case JMP:
            ir_jmp_emit(ir);
            break;
        default:
            assert(ir->ircode >= CAST_I1_I4 && ir->ircode <= CAST_I4_U2);
            ir_cast_emit(ir);
            break;
        }
    }
// release all reg (discard shared temp's reg and write back others)
    discard_temp(bb);
	if (!write_back)
    {
        write_back_var(bb);
    }
    emit(";---------------------------------\n");

}

void function_emit(struct proc_block *pb)
{
    int i, cnt, base;
    char *share_mem_name;
    struct basic_block *bb;
    struct type *return_type;
    struct symbol *sym;
    if (pb->func_sym->sto_class_spec_tok != TK_STATIC)
    {
        emit("global %s\n", pb->func_sym->name);
    }
    emit("%s:\n", pb->func_sym->name);
    return_type = pb->func_sym->type->basic_type;
    if (IS_RECORD_TYPE(return_type) && return_type->size != 1 && return_type->size != 2 &&
            return_type->size != 4 && return_type->size != 8)
    {
        ALLOC(sym, struct symbol);
        sym->kind = SK_Variable;
        sym->name = "@ret_rec";
        sym->type = create_pointer_type(return_type);
        pb->params->push_back(pb->params, sym);
    }
    // alloc parameters
    base = 20;
    for (i =0; i < pb->params->len; i++)
    {
        sym = pb->params->get(pb->params, i);
        sym->aname = format_name("ebp + %d", base);
        base += sym->type->size;
        base = ALIGN(base, STACK_ALIGN_SIZE);
    }
    // alloc local variable
    base = 0;
    for (i = 0; i < pb->locals->len; i++)
    {
        sym = pb->locals->get(pb->locals, i);
        base += sym->type->size;
        base = ALIGN(base, STACK_ALIGN_SIZE);
        sym->aname = format_name("ebp - %d", base);
    }
    // alloc shared_mem_size
    if (pb->shared_mem_size)
    {
        base += pb->shared_mem_size;
        base = ALIGN(base, STACK_ALIGN_SIZE);
        share_mem_name = format_name("ebp - %d", base);
    }
    // alloc temp veriable
    for (i = 0, cnt = 0; sym = pb->temps->hash_iter(pb->temps, &i, &cnt);)
    {
        if (((struct symbol_temp *)sym)->is_shared_tmp)
        {
            sym->aname = share_mem_name;
        }
        else
        {
            base += sym->type->size;
            base = ALIGN(base, STACK_ALIGN_SIZE);
            sym->aname = format_name("ebp - %d", base);
        }
    }
    emit("push ebp\n");
    emit("push ebx\n");
    emit("push esi\n");
    emit("push edi\n");
    emit("mov ebp, esp\n");
    emit("sub esp, %d\n", base);

    for (bb = pb->bblist_head; bb; bb = bb->next)
    {
        basic_block_emit(bb);
    }

    emit("mov esp, ebp\n");
    emit("pop edi\n");
    emit("pop esi\n");
    emit("pop ebx\n");
    emit("pop ebp\n");
    emit("ret\n");

}

void translation_unit_emit(struct proc_block *pb, FILE *fp)
{
    int cnt = 0;
    emit_fp = fp;
    x86reg_init();
    emit("; Code auto-generated by SXCC\n\n");
    emit("section .data\n\n");
    global_constant_emit();
    while(pb)
    {
        external_emit(pb);
        if (pb->kind == NK_Declaration)
        {
            assert(cnt++ == 0);
            global_variable_emit(pb);
            emit("section .text\n\n");
            assert(pb->next && pb->next->kind == NK_Function);
        }
        else
        {
            function_emit(pb);
        }
        pb = pb->next;
    }
}




