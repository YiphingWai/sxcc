#ifndef __HASH_H__
#define __HASH_H__

// must be 4k+3, k is an integral
#define DEFAULT_HASHTAB_SIZE (1031)
struct hash_item
{
    int flag;
    unsigned int id;
    void *user_data;
};

struct hash_table
{
    int magic;
    struct hash_item *hash_barrel;
    int M;
    int R;

    int (*insert)(struct hash_table *, char *, void *);
    int (*delete)(struct hash_table *, char *);
    int (*find)(struct hash_table *, char *, void **);
    void *(*hash_iter)(struct hash_table *, int *, int *);
    void (*clear)(struct hash_table *);
};
struct hash_table *hash_init();
int hash_destory(struct hash_table *);
#endif