
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "expr.h"
#include "fold.h"
#include "decl.h"
#include "type.h"
#include "symtab.h"
#include "sxcc.h"



/*
OPINFO(OP_ID,            16,   "id",     primary,        NOP)
OPINFO(OP_CONST,         16,   "const",  primary,        NOP)
OPINFO(OP_STR,           16,   "str",    primary,        NOP)
*/
struct expr_node *primary_expression_check(struct expr_node *expr)
{
    struct symbol *sym;

    if (expr->op == OP_CONST)
    {
        // we do not add the const into symtab because we may add some constant nodes during expression check
        // we will do this during translation
        return expr;
    }

    if (expr->op == OP_STR)
    {
        expr->sym = sym_add_string(expr->type, expr->value.p, NULL);
        expr->is_left_val = 0;
        return expr;
    }

    if (expr->op == OP_ID)  // typedef name, enum name, function name, variable name
    {
        if (!(sym = sym_lookup_id(expr->value.p)))
        {
            rep_error("undeclared identifier: %s\n", expr->value.p);
            sym = sym_add_variable(expr->value.p, &basic_types[INT], 0, 1, NULL);
            goto ok;
        }
        else if (sym->kind == SK_TypedefName)
        {
            rep_error("typedef name: %s can not be modified");
        }
        else if (sym->kind == SK_EnumConstant)
        {
            expr->op = OP_CONST;
            expr->type = &basic_types[INT];
            expr->value = sym->value;
            expr->is_left_val = 0;
            expr->sym = sym;
            return expr;
        }
        else    // SK_Variable or SK_Function
        {
ok:
            expr->type = sym->type;
            expr->value = sym->value;
            expr->is_left_val = sym->kind == SK_Variable;
            expr->sym = sym;
            return expr;
        }
    }
    assert(0);
    return expr;
}

static struct expr_node *create_cast_expr(struct type *dst_type, struct expr_node *expr)
{
    struct expr_node *cast;
    cast = alloc_expr_node();
    cast->op = OP_CAST;
    cast->type = dst_type;
    cast->child[0] = expr;
    cast->is_left_val = expr->is_left_val;
    cast->next = expr->next;
    return cast;
}

struct expr_node *cast(struct type *dst_type, struct expr_node *expr)
{
    enum type_categ dst_categ, src_categ;

    dst_categ = dst_type->categ;
    src_categ = expr->type->categ;

    if (dst_categ == VOID)
    {
        return create_cast_expr(dst_type, expr);
    }
    // the two types are of the same size but different sign
    if (src_categ < FLOAT && dst_type < FLOAT && src_categ / 2 == dst_categ / 2)
    {
        /*
        if (src_categ != dst_categ && src_categ >= INT && src_categ <= ULONG)
        {
            // int uint long ulong
            return create_cast_expr(dst_type, expr);
        }
        
       expr->type = dst_type;
       return expr;
       */
        return create_cast_expr(dst_type, expr);
    }
    // the two types are of different size
    // first, do integral promotion if necessary
    if (src_categ < INT)
    {
        expr = create_cast_expr(&basic_types[INT], expr);
        src_categ = INT;
    }
    // second, we transfer src to dst type
    if (src_categ != dst_categ)
    {
        // now src_categ must not smaller than INT
        if (src_categ != INT && dst_categ < INT)
        {
            // now src_categ must bigger than INT and dst_categ is smaller than INT
            // that means we need to across "INT".
            // in such cases, we should transfer src_type to INT first
            expr = create_cast_expr(&basic_types[INT], expr);
        }
        // then, we finish transfer.
        expr = create_cast_expr(dst_type, expr);
    }
    expr->type = dst_type;
    return expr;
}


struct expr_node *adjust(struct expr_node *expr)
{
    struct type *type;
    if (expr->type->categ == ARRAY || expr->type->categ == FUNCTION)
    {
        ALLOC(type, struct type);
        type->categ = POINTER;
        type->size = basic_types[POINTER].size;
        type->align = basic_types[POINTER].align;
        type->qualer = expr->type->qualer;
        type->basic_type = expr->type->categ == ARRAY ? expr->type->basic_type : expr->type;
        
        expr->is_array = expr->type->categ == ARRAY;
        expr->is_func = expr->type->categ == FUNCTION;
        expr->type = type;
    }
    return expr;
}
/*
An integral constant expression with the value 0, or such an
expression cast to type void * , is called a null pointer constant
1. 0
2. (void *)0
*/
static int is_null_constant(struct expr_node *expr)
{
    if (expr->op == OP_CONST && expr->value.i == 0)
    {
        return 1;
    }
    if (expr->op == OP_CAST && expr->type->categ == POINTER && expr->type->basic_type->categ == VOID && 
        expr->child[0]->op == OP_CONST && expr->child[0]->value.i == 0)
    {
        return 1;
    }
	return 0;
}

int can_assign(struct type *left_type, struct expr_node *right_expr)
{
    struct type *right_type;

    right_type = right_expr->type;

    if (left_type == right_type)
    {
        return 1;
    }
    
    /* the left operand has qualified or unqualified arithmetic type and
    the right has arithmetic type */
    if (IS_ARITH_TYPE(left_type) && IS_ARITH_TYPE(right_type))
    {
        return 1;
    }
    /* both operands are pointers to qualified or unqualified versions of
    compatible types, and the type pointed to by the left has all the
    qualifiers of the type pointed to by the right */
    if (left_type->categ == POINTER && right_type->categ == POINTER)
    {
        if ((left_type->basic_type->qualer | right_type->basic_type->qualer == left_type->basic_type->qualer) &&
            is_compatible_type(unqualify(left_type->basic_type), unqualify(right_type->basic_type)))
        {
            return 1;
        }
    }
    /* the left operand is a pointer and the right is a null pointer constant */
    if (left_type->categ == POINTER && is_null_constant(right_expr))
    {
        return 1;
    }
    /* one operand is a pointer to an object or incomplete type and the
    other is a pointer to a qualified or unqualified version of void, and
    the type pointed to by the left has all the qualifiers of the type
    pointed to by the right */
    if ((((left_type->categ == POINTER) && IS_VOID_POINTER(right_type)) || 
        ((right_type->categ == POINTER) && IS_VOID_POINTER(left_type))) && 
        (left_type->basic_type->qualer | right_type->basic_type->qualer == left_type->basic_type->qualer))
    {
        return 1;
    }
    /* the left operand has a qualified or unqualified version of a
    structure or union type compatible with the type of the right */
    if ((left_type->categ == STRUCT || left_type->categ == UNION) && 
        is_compatible_type(left_type, right_type))
    {
        return 1;
    }

    /*[ both types are pointer, we give a warning ]*/
    if (left_type->categ == POINTER && right_type->categ == POINTER)
    {
        return 1;
    }
    /* [one of type is pointer and the other is INTEG and they are of the same size] */
    if (((left_type->categ == POINTER && IS_INTEG_TYPE(right_type)) ||
        (right_type->categ == POINTER && IS_INTEG_TYPE(left_type))) &&
        left_type->size == right_type->size)
    {
        /*should cast a warning here*/
        return 1;
    }
    return 0;
}

 /* 
    If the expression that denotes the called function has a type that
    does not include a prototype, the integral promotions are performed on
    each argument and arguments that have type float are promoted to double.  
    These are called the default argument promotions. 

static struct expr_node *promote_argument(struct expr_node *arg)
{
    enum type_categ categ;
	struct type *type;
    categ = arg->type->categ;
    type = arg->type;
    type = categ < INT ? &basic_types[INT] : (categ == FLOAT ? &basic_types[DOUBLE] : type);
	//PRINT_DEBUG_INFO(("%s",TypeToString(ty)));
	return cast(type, arg);
}
*/
struct expr_node *integ_promotion(struct expr_node *integ)
{
    return integ->type->categ < INT ? cast(&basic_types[INT], integ) : integ;
}
/*
check:
1. if the number of argument and parameter are the same.
2. if the kind of argument and parameter are compatible.
*/
struct expr_node *argument_expression_check(struct func_type *func, struct expr_node *arg, int arg_no)
{
    int para_num;
    struct type *type;
    struct parameter_type *para;

    if (!func->params)
    {
        // finish
        return NULL;
    }

    para_num = func->params->len;
    
    if (!para_num)
    {
        // finish
        return NULL;
    }
    if ((arg_no >= para_num) && !(func->has_ellipsis))
    {
        // finish
        return NULL;
    }
    
    arg = adjust(expression_check(arg));
    if (arg_no < para_num)
    {
        para = func->params->get(func->params, arg_no);
        if (!can_assign(para->type, arg))
        {
            rep_error("incompatible argument");
        }
        
        arg = cast(para->type, arg);
    }
    else    // for variadic function only
    {
        type = qualify(arg->type->categ < INT ? &basic_types[INT] : 
                        (arg->type->categ == FLOAT ? &basic_types[DOUBLE] : arg->type), arg->type->qualer);
        arg = cast(type, arg);
    }
    return arg;
}



/*
1. func(argu);
2. struct_arr[5].func_array[6](argu);
*/
struct expr_node *call_expression_check(struct expr_node *expr)
{
    int arg_no = 0, para_num = 0;
    struct symbol *sym;
    struct func_type *func_type;
    struct expr_node **arg;

    struct vector *params = NULL;
    struct parameter_type *para;
    // expr->type == sym->type at expr_primary_check
    if ((expr->child[0]->op == OP_ID) && !(sym = sym_lookup_id(expr->child[0]->value.p)))
    {
        params = vector_init(0);
        expr->child[0]->type = create_function(&basic_types[INT], 0, params);
        expr->child[0]->sym = sym_add_function(expr->child[0]->value.p, expr->child[0]->type, 0, NULL);
        //expr->child[0]->type = default_func_type;
        //expr->child[0]->sym = sym_add_function(expr->child[0]->value.p, default_func_type, 0, NULL);
    }
    expr->child[0]->sym = sym;
    
    expr->child[0] = adjust(expression_check(expr->child[0]));
    expr->child[0]->is_left_val = 0;
    // func(argu); function type func has been changed into pointer type
    func_type = expr->child[0]->type;

    if (!IS_FUNCTION_POINTER(func_type))
    {
        rep_error("the left operand must be function or function pointer");
    }
    func_type = func_type->basic_type;

    arg = &(expr->child[1]);

    while (*arg)
    {
        if (params)
        {
            *arg = adjust(expression_check(*arg));
            ALLOC(para, struct parameter_type);
            /*
            para->id = NULL;
            para->sto_class_spec_tok = 0;
            */
            para->type = (*arg)->type;
            params->push_back(params, para);
        }
        else
        {
            *arg = argument_expression_check(func_type, *arg, arg_no);
        }
        if (*arg)
        {
            arg = &((*arg)->next);
            arg_no++;
        }
    }
    para_num = func_type->params ? func_type->params->len : 0;
    if (arg_no > para_num)
    {
        if (!func_type->has_ellipsis)
        {
            rep_error("too many augments");
            exit(1);
        }
    }
    else if (arg_no < para_num)
    {
        rep_error("too few augments");
        exit(1);
    }
    
    expr->type = func_type->basic_type;
    expr->is_left_val = 0;
    return expr;
}

struct expr_node *create_pointer_offset_node(int basic_type_size, struct expr_node *offset)
{
    struct expr_node *mul, *constant;

    constant = alloc_expr_node();
    constant->op = OP_CONST;
    constant->type = &basic_types[INT];
    constant->value.i = basic_type_size;
    constant->is_left_val = 0;

    mul = alloc_expr_node();
    mul->op = OP_MUL;
    mul->type = offset->type;
    mul->child[0] = offset;
    mul->child[1] = constant;
    mul->is_left_val = 0;
    mul->next = offset->next;
    return constant_fold(mul);
}

/*
    1.int arr[1][2][3];
    2.int ***p[1][2][3];

    type of arr: [1]->[2]->[3]->INT 
    type of p: *->*->*->INT
*/
struct expr_node *index_expression_check(struct expr_node *expr)
{
    struct expr_node *deref, *add;
    // postorder traversal 
    expr->child[0] = adjust(expression_check(expr->child[0]));
    expr->child[1] = adjust(expression_check(expr->child[1]));
    if (expr->child[0]->type->categ != POINTER)
    {
        rep_error("the left operand must be array or pointer");
        exit(1);
    }
    if (!IS_INTEG_TYPE(expr->child[1]->type))
    {
        rep_error("the index must be an integer");
        exit(1);
    }
   
    expr->type = expr->child[0]->type->basic_type;
    expr->child[1] = integ_promotion(expr->child[1]);   // very important!
    expr->child[1] = create_pointer_offset_node(expr->type->size, expr->child[1]);
    
    if (expr->type->categ != ARRAY) //it means we are dealing with muti-pointer, the deref is needed
    {
        add = alloc_expr_node();
        add->op = OP_ADD;
        add->type = expr->child[0]->type;
        add->child[0] = expr->child[0];
        add->child[1] = expr->child[1];

        deref = alloc_expr_node();
        deref->op = OP_DEREF;
        deref->type = expr->type;
        deref->child[0] = add;
        deref->next = expr->next;
        expr = deref;
    }
    expr->is_left_val = 1;
    return expr;
}
/*
struct record_field *find_field(struct record_type *type, int *offset)
{
    int i;
    struct record_field *field;
    for (i = 0; i < type->fields->len; i++)
    {
        field = type->fields->get(type->fields, i);
        if (!strcmp(field->id, expr->child[1]->value.p))
        {
            goto ok;
        }
    }
}
*/
struct expr_node *member_expression_check(struct expr_node *expr)
{
    //const int *i; *((int *)i) = 5;
    int i;
    struct record_field *field;
    struct record_type *type;
    expr->child[0] = adjust(expression_check(expr->child[0]));
//    expr->child[1] = adjust(expression_check(expr->child[1]));
    if (expr->child[1]->op != OP_ID)
    {
        rep_error("the right operand must be a member");
        exit(1);
    }

    if (expr->op == OP_MEMBER)
    {
        if (!IS_RECORD_TYPE(expr->child[0]->type))
        {
            rep_error("the left operand must be a struct or union");
            exit(1);
        }
        type = expr->child[0]->type;
        /*
            struct.a = 1;  --  legal
            func().a = 1;  --  illegal
        */
        expr->is_left_val = expr->child[0]->is_left_val;
    }
    else
    {
        expr->child[0]->is_left_val = 0;
        if (!(expr->child[0]->type->categ == POINTER && IS_RECORD_TYPE(expr->child[0]->type->basic_type)))
        {
            rep_error("the left operand must be a struct or union");
            exit(1);
        }
        type = expr->child[0]->type->basic_type;
        /*
            struct->a = 1;  --  legal
            func()->a = 1;  --  legal
        */
        expr->is_left_val = 1;
    }
    if (field = lookup_field(type, expr->child[1]->value.p))
    {
        goto ok;
    }
    rep_error("struct or union member %s doesn't exist", expr->child[1]->value.p);
    exit(1);

ok:
    expr->type = qualify(field->type, type->qualer);
    expr->child[1]->op = OP_CONST;
    expr->child[1]->type = &basic_types[INT];
    expr->child[1]->is_left_val = 0;
    expr->child[1]->value.i = field->offset;
    expr->child[1]->sym = field;
    return expr;
}
/*
The operand of the postfix increment or decrement operator shall
have qualified or unqualified scalar type and shall be a modifiable
lvalue.
*/
struct expr_node *inc_dec_expression_check(struct expr_node *expr)
{
    // ARRAY and FUNCTION can not ++, so we here don't call adjust so that we can detect this error.
    expr->child[0] = expression_check(expr->child[0]);
    if (!IS_SCALAR_TYPE(expr->child[0]->type))
    {
        rep_error("The operand must have scalar type");
        exit(1);
    }
    if (!(expr->child[0]->is_left_val))
    {
        rep_error("The operand must be a modifiable lvalue");
        exit(1);
    }
    expr->inc_dec_op = expr->op;
//    expr->op = (expr->op == OP_PREINC || expr->op == OP_POSTINC) ? OP_ADD : OP_SUB;
    expr->type = expr->child[0]->type;
    expr->is_left_val = 0;//expr->child[0]->is_left_val;
    expr->child[1] = alloc_expr_node();
    expr->child[1]->op = OP_CONST;
    expr->child[1]->type = expr->child[0]->type;
    expr->child[1]->is_left_val = 0;
    if (expr->child[0]->type->categ == FLOAT)
    {
        expr->child[1]->value.f = 1;
    }
    else if (expr->child[0]->type->categ == DOUBLE)
    {
        expr->child[1]->value.d = 1;
    }
    else if(expr->child[0]->type->categ == POINTER)
    {
        expr->child[1]->value.i = expr->child[0]->type->basic_type->size;
    }
    else
    {
        expr->child[1]->value.i = 1;
    }
    return expr;
}


/*
OPINFO(OP_INDEX,         15,   "[]",     postfix,        NOP)
OPINFO(OP_CALL,          15,   "call",   postfix,        NOP)
OPINFO(OP_MEMBER,        15,   ".",      postfix,        NOP)
OPINFO(OP_PTR_MEMBER,    15,   "->",     postfix,        NOP)
OPINFO(OP_POSTINC,       15,   "++",     postfix,        INC)
OPINFO(OP_POSTDEC,       15,   "--",     postfix,        DEC)
*/
struct expr_node *postfix_expression_check(struct expr_node *expr)
{
    
    if (expr->op == OP_INDEX)
    {
        return index_expression_check(expr);
    }
    if (expr->op == OP_CALL)
    {
        return call_expression_check(expr);
    }
    if (expr->op == OP_MEMBER || expr->op == OP_PTR_MEMBER)
    {
        return member_expression_check(expr);
    }
    if (expr->op == OP_POSTINC || expr->op == OP_POSTDEC)
    {
        return inc_dec_expression_check(expr);
    }
    assert(0);
}
/*
Unless the type name specifies void type, the type name shall
specify qualified or unqualified scalar type and the operand shall
have scalar type.
*/
struct expr_node *cast_expression_check(struct expr_node *expr)
{
    struct type *dst_type;
    struct expr_node *cast_node;
    expr->child[1] = adjust(expression_check(expr->child[1]));
    type_name_check((struct decl_type_name_node *)expr->child[0]);
    dst_type = ((struct decl_type_name_node *)expr->child[0])->type;
    if ((!IS_SCALAR_TYPE(unqualify(dst_type)) || !IS_SCALAR_TYPE(expr->child[1]->type)) && (dst_type->categ != VOID))
    {
        rep_error("cast to the type is not allowed");
        exit(1);
    }
    cast_node = cast_fold(dst_type, expr->child[1]);
    cast_node->next = expr->next;
    return cast_node;
}
/*
The operand of the unary & operator shall be either a function
designator or an lvalue that designates an object that is not a
bit-field and is not declared with the register storage-class
specifier.

The result of the unary & (address-of) operator is a pointer to the
object or function designated by its operand.  If the operand has type
`` type ,'' the result has type ``pointer to type .''
*/
struct expr_node *address_expression_check(struct expr_node *expr)
{
    /*
        1. &*p
        2. &(*p + 5)
        3. &p[5]
        4. &(pp[5] + 5)
        5. &a
    */
    int scale;
    struct expr_node *pointer;
    /*
    int a[4];
    the address of:
        a -> 0
        &a -> 0
        a + 1 -> 4
        &a + 1 -> 16
    so we don't call adjust to make sure the type of &a is *->int[4]
    */
    expr->child[0] = expression_check(expr->child[0]);
    if (expr->child[0]->type->categ != FUNCTION && (expr->child[0]->in_reg || !expr->child[0]->is_left_val))
    {
        rep_error("invaild address expression");
        exit(1);
    }
    /*
        &*a
    */
    if (expr->child[0]->op == OP_DEREF)
    {
        expr->child[0]->child[0]->is_left_val = 0;
        expr->child[0]->child[0]->next = expr->next;
        return expr->child[0]->child[0];
    }
    /*
        &arr[3]
              array-int
                |
        &->[]->arr
           |
           int
    */
    if (expr->child[0]->op == OP_INDEX)
    {
        expr->child[0]->op = OP_ADD;
        scale = expr->child[0]->child[0]->type->basic_type->size;
        expr->child[0]->child[1] = create_pointer_offset_node(scale, expr->child[0]->child[1]);
        expr->child[0]->type = create_pointer_type(expr->child[0]->type);
        expr->child[0]->is_left_val = 0;
        expr->child[0]->next = expr->next;
        return expr->child[0];
    }

    expr->type = create_pointer_type(expr->child[0]->type);
    expr->is_left_val = 0;
    return expr;
}
/*
The operand of the unary * operator shall have pointer type.  

The unary * operator denotes indirection.  If the operand points to
a function, the result is a function designator; if it points to an
object, the result is an lvalue designating the object.  If the
operand has type ``pointer to type ,'' the result has type `` type .''
If an invalid value has been assigned to the pointer, the behavior of
the unary * operator is undefined.
*/
struct expr_node *deref_expression_check(struct expr_node *expr)
{
    struct expr_node *p;
    expr->child[0] = adjust(expression_check(expr->child[0]));
    if (expr->child[0]->type->categ != POINTER)
    {
        rep_error("the left operand must be a pointer");
        exit(1);
    }
    /*
        *&a --> a
    */
    if (expr->child[0]->op == OP_ADDRESS)
    {
        expr->child[0]->child[0]->type = expr->child[0]->type->basic_type;
        expr->child[0]->child[0]->next = expr->next;
        return expr->child[0]->child[0];
    }
    /*
        int arr[5][6];
        *arr --> arr[0]
    */
    if (expr->child[0]->is_array)
    {
        expr->op = OP_INDEX;
        expr->child[1] = alloc_expr_node();
        expr->child[1]->op = OP_CONST;
        expr->child[1]->is_left_val = 0;
        expr->child[1]->type = &basic_types[INT];
        expr->child[1]->value.i = 0;

        /*
            int arr[5][6];  *arr --> {[6]->int} --> rvalue
            int arr[5];     *arr --> {int} --> lvalue
        */
        expr->type = expr->child[0]->type->basic_type;
        expr->is_left_val = expr->type != ARRAY;
        return expr;
    }
    /*
        int arr[5][6];
        *(arr + 5) --> arr[5]
    */
    if (expr->child[0]->op == OP_ADD && expr->child[0]->is_array)
    {
        expr->child[0]->op = OP_INDEX;
        expr->child[0]->type = expr->child[0]->type->basic_type;

        /*
            int arr[5][6];  *(arr + 5) --> {[6]->int} --> rvalue
            int arr[5];     *(arr + 5) --> {int} --> lvalue
        */
        expr->child[0]->is_left_val = expr->child[0]->type->basic_type != ARRAY;
        expr->child[0]->next = expr->next;
        
        expr->child[0]->child[1] = create_pointer_offset_node(expr->child[0]->type->size, expr->child[1]);
        free(expr);
        return expr->child[0];
    }

    if (expr->child[0]->type->categ == POINTER && !expr->child[0]->is_array)
    {
        /*
            int (*ptr2)[4]  *ptr2; --> {[4]->int} --> ptr2[0]
        */
        if (expr->child[0]->type->basic_type->categ == ARRAY)
        {
            expr->is_left_val = 0;
            expr->op = OP_INDEX;
            expr->child[1] = alloc_expr_node();
            expr->child[1]->op = OP_CONST;
            expr->child[1]->is_left_val = 0;
            expr->child[1]->type = &basic_types[INT];
            expr->child[1]->value.i = 0;
            expr->type = expr->child[0]->type->basic_type;
            return expr;
        }

        if (expr->child[0]->type->basic_type->categ == FUNCTION)
        {
            expr->child[0]->next = expr->next;
            free(expr);
            return expr->child[0];
        }

        expr->type = expr->child[0]->type->basic_type;
        expr->is_left_val = 1;
        return expr;
    }

    assert(0);
    return expr;
}

struct expr_node *pos_neg_expression_check(struct expr_node *expr)
{
    expr->child[0] = adjust(expression_check(expr->child[0]));
    if (!IS_ARITH_TYPE(expr->child[0]->type))
    {
        rep_error("the operand must be arith type");
        exit(1);
    }
    expr->child[0] = integ_promotion(expr->child[0]);
    if (expr->op == OP_POS)
    {
        expr->child[0]->is_left_val = 0;
        expr->child[0]->next = expr->next;
        return expr->child[0];
    }
    expr->is_left_val = 0;
    expr->type = expr->child[0]->type;
    return constant_fold(expr);
}

struct expr_node *comp_expression_check(struct expr_node *expr)
{
    expr->child[0] = adjust(expression_check(expr->child[0]));
    if (!IS_INTEG_TYPE(expr->child[0]->type))
    {
        rep_error("the operand must be integ type");
        exit(1);
    }
    expr->child[0] = integ_promotion(expr->child[0]);
    expr->is_left_val = 0;
    expr->type = expr->child[0]->type;
    return constant_fold(expr);
}

struct expr_node *not_expression_check(struct expr_node *expr)
{
    expr->child[0] = adjust(expression_check(expr->child[0]));
    if (!IS_SCALAR_TYPE(expr->child[0]->type))
    {
        rep_error("the operand must be integ type");
        exit(1);
    }
    expr->child[0] = integ_promotion(expr->child[0]);
    expr->is_left_val = 0;
    expr->type = &basic_types[INT];
    return constant_fold(expr);
}
struct expr_node *sizeof_expression_check(struct expr_node *expr)
{
    struct type *type;
    if (expr->child[0]) //type name
    {
        type_name_check((struct decl_type_name_node *)(expr->child[0]));
        type = ((struct decl_type_name_node *)(expr->child[0]))->type;
    }
    else    // unary_expression
    {
        expr->child[0] = expression_check(expr->child[1]);
        type = expr->child[0]->type;
    }
    if (type->categ == FUNCTION || is_incomplete_type(type, 0))
    {
        rep_error("the operand must not be function or incomplete type");
        exit(1);
    }
    expr->op = OP_CONST;
    expr->type = &basic_types[INT];
    expr->value.i = type->size;
    expr->is_left_val = 0;
    return expr;
}
/************************************************************
        unary-expression:
				   postfix-expression
				   ++  unary-expression
				   --  unary-expression
				   unary-operator cast-expression
				   sizeof  unary-expression
				   sizeof (  type-name )
 
		   unary-operator: one of
				   &  *  +	-  ~  !
				   
 OPINFO(OP_CAST,		  14,	"cast",   Unary,		  NOP)
 OPINFO(OP_PREINC,		  14,	"++",	  Unary,		  NOP)
 OPINFO(OP_PREDEC,		  14,	"--",	  Unary,		  NOP)
 OPINFO(OP_ADDRESS, 	  14,	"&",	  Unary,		  ADDR)
 OPINFO(OP_DEREF,		  14,	"*",	  Unary,		  DEREF)
 OPINFO(OP_POS, 		  14,	"+",	  Unary,		  NOP)
 OPINFO(OP_NEG, 		  14,	"-",	  Unary,		  NEG)
 OPINFO(OP_COMP,		  14,	"~",	  Unary,		  BCOM)
 OPINFO(OP_NOT, 		  14,	"!",	  Unary,		  NOP)
 OPINFO(OP_SIZEOF,		  14,	"sizeof", Unary,		  NOP)
 ************************************************************/
struct expr_node *unary_expression_check(struct expr_node *expr)
{
    switch (expr->op)
    {
        case OP_CAST:
            return cast_expression_check(expr);
        case OP_PREINC:
        case OP_PREDEC:
        case OP_POSTINC:
        case OP_POSTDEC:
            return inc_dec_expression_check(expr);
        case OP_ADDRESS:
            return address_expression_check(expr);
        case OP_DEREF:
            return deref_expression_check(expr);
        case OP_POS:
        case OP_NEG:
            return pos_neg_expression_check(expr);
        case OP_COMP:
            return comp_expression_check(expr);
        case OP_NOT:
            return not_expression_check(expr);
        case OP_SIZEOF:
            return sizeof_expression_check(expr);
    }
    assert(0);
    return expr;
}
/*
Each of the operands shall have arithmetic type.  The operands of
the % operator shall have integral type.
*/
struct expr_node *mul_expression_check(struct expr_node *expr)
{
    struct type *common_type;
    expr->child[0] = adjust(expression_check(expr->child[0]));
    expr->child[1] = adjust(expression_check(expr->child[1]));
    if (expr->op == OP_MOD && (!IS_INTEG_TYPE(expr->child[0]->type) || !IS_INTEG_TYPE(expr->child[1]->type)))
    {
        rep_error("both operands must have integral type");
        exit(1);
    }
    if ((!IS_ARITH_TYPE(expr->child[0]->type) || !IS_ARITH_TYPE(expr->child[1]->type)))
    {
        rep_error("both operands must have arithmetic type");
        exit(1);
    }
    /*
    The usual arithmetic conversions are performed on the operands.
    */
    common_type = arith_common_type(expr->child[0]->type, expr->child[1]->type);
    expr->child[0] = cast(common_type, expr->child[0]);
    expr->child[1] = cast(common_type, expr->child[1]);
    expr->type = common_type;
    return constant_fold(expr);
}

/*
For addition, either both operands shall have arithmetic type, or
one operand shall be a pointer to an object type and the other shall
have integral type.  (Incrementing is equivalent to adding 1.)

For subtraction, one of the following shall hold: 
* both operands have arithmetic type; 
* both operands are pointers to qualified or unqualified versions of
compatible object types; or
* the left operand is a pointer to an object type and the right
operand has integral type.  (Decrementing is equivalent to subtracting 1.)
*/
struct expr_node *add_sub_expression_check(struct expr_node *expr)
{
    struct expr_node *p;
    struct type *common_type;
    expr->child[0] = adjust(expression_check(expr->child[0]));
    expr->child[1] = adjust(expression_check(expr->child[1]));

    if (IS_ARITH_TYPE(expr->child[0]->type) && IS_ARITH_TYPE(expr->child[1]->type))
    {
        common_type = arith_common_type(expr->child[0]->type, expr->child[1]->type);
        expr->type = common_type;
        expr->child[0] = cast(common_type, expr->child[0]);
        expr->child[1] = cast(common_type, expr->child[1]);
        expr->is_left_val = 0;
        return constant_fold(expr);
    }
    if (expr->op == OP_ADD)
    {
        if (IS_OBJECT_POINTER(expr->child[0]->type) && IS_INTEG_TYPE(expr->child[1]->type))
        {
            goto ok;
        }
        if (IS_OBJECT_POINTER(expr->child[1]->type) && IS_INTEG_TYPE(expr->child[0]->type))
        {
            p = expr->child[1];
            expr->child[1] = expr->child[0];
            expr->child[0] = p;
            goto ok;
        }
    }
    else
    {
        if ((expr->child[0]->type->categ == POINTER) && (expr->child[1]->type->categ == POINTER) &&
            is_compatible_type(unqualify(expr->child[0]->type->basic_type), unqualify(expr->child[1]->type->basic_type)))
        {
            expr->type = &basic_types[INT];
            expr->is_left_val = 0;

            p = alloc_expr_node();
            p->op = OP_DIV;
            p->type = &basic_types[INT];
            p->is_left_val = 0;
            p->child[0] = expr;

            p->child[1] = alloc_expr_node();
            p->child[1]->op = OP_CONST;
            p->child[1]->is_left_val = 0;
            p->child[1]->type = &basic_types[INT];
            p->child[1]->value.i = expr->child[0]->type->basic_type->size;
            
            p->next = expr->next;
            return p;
        }
        if (IS_OBJECT_POINTER(expr->child[0]->type) && IS_INTEG_TYPE(expr->child[1]->type))
        {
            goto ok;
        }
    }
    rep_error("illegal expression");
    exit(1);
ok:
    expr->child[1] = integ_promotion(expr->child[1]);
    expr->child[1] = create_pointer_offset_node(expr->child[0]->type->basic_type->size, expr->child[1]);
    expr->type = expr->child[0]->type;
    expr->is_left_val = 0;
    return expr;
}
/*
Each of the operands shall have integral type.
*/
struct expr_node *shift_expression_check(struct expr_node *expr)
{
    expr->child[0] = integ_promotion(adjust(expression_check(expr->child[0])));
    expr->child[1] = integ_promotion(adjust(expression_check(expr->child[1])));
    if ((expr->child[0]->op == OP_CONST) && (expr->op == OP_LSHIFT || expr->op == OP_RSHIFT))
    {
        if (expr->child[0]->value.i == 0)
        {
            expr->child[0]->next = expr->next;
            return expr->child[0];
        }
    }
    if ((expr->child[1]->op == OP_CONST) && (expr->op == OP_LSHIFT || expr->op == OP_RSHIFT))
    {
        if (expr->child[1]->value.i == 0)
        {
            expr->child[0]->is_left_val = 0;
            expr->child[0]->next = expr->next;
            return expr->child[0];
        }
        if (expr->child[1]->value.i < 0)
        {
            expr->child[1]->value.i = -expr->child[1]->value.i;
            if (expr->op == OP_LSHIFT)
            {
                expr->op = OP_RSHIFT;
            }
            else
            {
                expr->op = OP_LSHIFT;
            }
        }
    }
    if (IS_INTEG_TYPE(expr->child[0]->type) && IS_INTEG_TYPE(expr->child[1]->type))
    {
        /*
        The integral promotions are performed on each of the operands.  The
        type of the result is that of the promoted left operand.
         */
        expr->is_left_val = 0;
        expr->type = expr->child[0]->type;
        return constant_fold(expr);
    }
    
    rep_error("both operands must have integral type");
    exit(1);
}
/*
 One of the following shall hold: 

 * both operands have arithmetic type; 

 * both operands are pointers to qualified or unqualified versions of
   compatible object types; or

 * both operands are pointers to qualified or unqualified versions of
   compatible incomplete types.
*/
struct expr_node *relation_expression_check(struct expr_node *expr)
{
    struct type *common_type;
    expr->child[0] = adjust(expression_check(expr->child[0]));
    expr->child[1] = adjust(expression_check(expr->child[1]));
    /*
        If both of the operands have arithmetic type, the usual arithmetic
    conversions are performed.
    */
    if (IS_ARITH_TYPE(expr->child[0]->type) && IS_ARITH_TYPE(expr->child[1]->type))
    {
        common_type = arith_common_type(expr->child[0]->type, expr->child[1]->type);
        expr->child[0] = cast(common_type, expr->child[0]);
        expr->child[1] = cast(common_type, expr->child[1]);
        expr->type = &basic_types[INT];
        expr->is_left_val = 0;
        return constant_fold(expr);
    }

    if (IS_OBJECT_POINTER(expr->child[0]->type) && IS_OBJECT_POINTER(expr->child[1]->type) &&
        is_compatible_type(unqualify(expr->child[0]->type->basic_type), unqualify(expr->child[1]->type->basic_type)))
    {
        expr->type = &basic_types[INT];
        expr->is_left_val = 0;
        return expr;
    }
    
    if (IS_INCOMPLETE_POINTER(expr->child[0]->type) && IS_INCOMPLETE_POINTER(expr->child[1]->type) &&
        is_compatible_type(unqualify(expr->child[0]->type->basic_type), unqualify(expr->child[1]->type->basic_type)))
    {
        expr->type = &basic_types[INT];
        expr->is_left_val = 0;
        return expr;
    }
    rep_error("illegal expression");
    exit(1);
    return expr;
}
/*
One of the following shall hold: 

 * both operands have arithmetic type; 

 * both operands are pointers to qualified or unqualified versions of
   compatible types;

 * one operand is a pointer to an object or incomplete type and the
   other is a qualified or unqualified version of void ; or

 * one operand is a pointer and the other is a null pointer constant.  
*/
static struct expr_node *equality_expression_check(struct expr_node *expr)
{
    struct type *common_type;
    expr->child[0] = adjust(expression_check(expr->child[0]));
    expr->child[1] = adjust(expression_check(expr->child[1]));
    
    if (IS_ARITH_TYPE(expr->child[0]->type) && IS_ARITH_TYPE(expr->child[1]->type))
    {
        common_type = arith_common_type(expr->child[0]->type, expr->child[1]->type);
        expr->child[0] = cast(common_type, expr->child[0]);
        expr->child[1] = cast(common_type, expr->child[1]);
        expr->type = &basic_types[INT];
        expr->is_left_val = 0;
        return constant_fold(expr);
    }
    if (expr->child[0]->type->categ == POINTER && expr->child[1]->type->categ == POINTER &&
        is_compatible_type(unqualify(expr->child[0]->type->basic_type), unqualify(expr->child[1]->type->basic_type)))
    {
        goto ok;
    }
    if (((IS_OBJECT_POINTER(expr->child[0]->type) || IS_INCOMPLETE_POINTER(expr->child[0]->type)) && 
        expr->child[1]->type->categ == VOID) ||
        ((IS_OBJECT_POINTER(expr->child[1]->type) || IS_INCOMPLETE_POINTER(expr->child[1]->type)) && 
        expr->child[0]->type->categ == VOID))
    {
        goto ok;
    }
    if ((expr->child[0]->type->categ == POINTER && is_null_constant(expr->child[1])) || 
        (expr->child[1]->type->categ == POINTER && is_null_constant(expr->child[0])))
    {
        goto ok;
    }
    rep_error("illegal expression");
    exit(1);
ok:
    expr->type = &basic_types[INT];
    expr->is_left_val = 0;
    return expr;
}

/*
Each of the operands shall have integral type.  
*/
static struct expr_node *bitwise_expression_check(struct expr_node *expr)
{
    struct type *common_type;
    expr->child[0] = adjust(expression_check(expr->child[0]));
    expr->child[1] = adjust(expression_check(expr->child[1]));
    
    if (IS_INTEG_TYPE(expr->child[0]->type) && IS_INTEG_TYPE(expr->child[1]->type))
    {
        common_type = arith_common_type(expr->child[0]->type, expr->child[1]->type);
        expr->child[0] = cast(common_type, expr->child[0]);
        expr->child[1] = cast(common_type, expr->child[1]);
        expr->type = expr->child[0]->type;
        expr->is_left_val = 0;
        return constant_fold(expr);
    }
    rep_error("illegal expression");
    exit(1);
}
/*
 Each of the operands shall have scalar type.  
*/
static struct expr_node *logical_expression_check(struct expr_node *expr)
{
    int rslt;
    struct type *common_type;
    struct expr_node *constant_expr, *other_expr;

    expr->child[0] = adjust(expression_check(expr->child[0]));
    expr->child[1] = adjust(expression_check(expr->child[1]));

    if (IS_SCALAR_TYPE(expr->child[0]->type) && IS_SCALAR_TYPE(expr->child[1]->type))
    {
        if (expr->child[0]->op == OP_CONST || expr->child[1]->op == OP_CONST)
        {
            constant_expr = expr->child[0]->op == OP_CONST ? expr->child[0] : expr->child[1];
            other_expr = expr->child[0]->op == OP_CONST ? expr->child[1] : expr->child[0];
            switch (constant_expr->type->categ)
            {
                case CHAR:
                case UCHAR:
                case SHORT:
                case USHORT:
                case INT:
                    rslt = constant_expr->value.i != 0;
                    break;
                case UINT:
                    rslt = constant_expr->value.ui != 0;
                    break;
                case LONG:
                    rslt = constant_expr->value.l != 0;
                    break;
                case ULONG:
                    rslt = constant_expr->value.ul != 0;
                    break;
                case FLOAT:
                    rslt = constant_expr->value.f != 0;
                    break;
                case DOUBLE:
                    rslt = constant_expr->value.d != 0;
                    break;
                default:
                    assert(0);
            }
            if (expr->op == OP_AND)
            {
                if (rslt)
                {
                    other_expr->next = expr->next;
                    expr = other_expr; 
                }
                else
                {
                    expr->op = OP_CONST;
                    expr->type = &basic_types[INT];
                    expr->value.i = 0;
                }
            }
            else //if (expr->op == OP_OR)
            {
                assert(expr->op == OP_OR);
                if (rslt)
                {
                    expr->op = OP_CONST;
                    expr->type = &basic_types[INT];
                    expr->value.i = 1;
                }
                else
                {
                    other_expr->next = expr->next;
                    expr = other_expr; 
                }
            }
        }
        else
        {
            common_type = arith_common_type(expr->child[0]->type, expr->child[1]->type);
            expr->child[0] = cast(common_type, expr->child[0]);
            expr->child[1] = cast(common_type, expr->child[1]);
            expr->type = &basic_types[INT];
        }
        
        expr->is_left_val = 0;
        return constant_fold(expr);
    }
    rep_error("illegal expression");
    exit(1);
}

static struct expr_node *binary_expression_check(struct expr_node *expr)
{
    switch (expr->op)
    {
        case OP_AND:    // &&
        case OP_OR:     // ||
            return logical_expression_check(expr);
        case OP_BITAND: // &
        case OP_BITOR:  // |
        case OP_BITXOR: // ^
            return bitwise_expression_check(expr);
        case OP_EQUAL:  //  ==
        case OP_UNEQUAL:    // !=
            return equality_expression_check(expr);
        case OP_GREAT:  // >
        case OP_GREAT_EQ:   // >=
        case OP_LESS:   // <
        case OP_LESS_EQ:    // <=
            return relation_expression_check(expr);
        case OP_LSHIFT:     // <<
        case OP_RSHIFT:     // >>
            return shift_expression_check(expr);
        case OP_ADD:        // +
        case OP_SUB:        // -
            return add_sub_expression_check(expr);
        case OP_MUL:        // *
        case OP_DIV:        // /
        case OP_MOD:        // %
            return mul_expression_check(expr);
    }
    assert(0);
    return expr;
}

int is_modifiable_lvalue(struct expr_node *expr)
{
    /*
        modifiable lvalue:
        1. must be lvalue
        2. must not has qualifier 'const'
        3. for struct/union, there must be no any const filed.
    */
    if ((expr->is_left_val) && !(expr->type->qualer & CONST))
    {
        if (IS_RECORD_TYPE(expr->type))
        {
            if (!((struct record_type *)(expr->type))->has_const_field)
            {
                return 1;
            }
        }
        else
        {
            return 1;
        }
    }
    return 0;
}

static struct expr_node *simple_assignment_expression_check(struct expr_node *expr)
{
    struct type *ltype, *rtype;
    expr->child[0] = adjust(expression_check(expr->child[0]));
    expr->child[1] = adjust(expression_check(expr->child[1]));
    
    if (!is_modifiable_lvalue(expr->child[0]))
    {
        rep_error("the left operand should have a modifiable lvalue");
        exit(1);
    }
    if (can_assign(expr->child[0]->type, expr->child[1]))
    {
        goto ok;
    }
    

    rep_error("the left operand cannot be assigned to the right operand");
    exit(1);
ok:    
    /*
        The type of an assignment expression is the type of the left operand unless the left
        operand has qualified type, in which case it is the unqualified
        version of the type of the left operand.
    */
    expr->type = unqualify(expr->child[0]->type);
    /*
    In simple assignment ( = ), the value of the right operand is
    converted to the type of the assignment expression and replaces the
    value stored in the object designated by the left operand.
    */
    expr->child[1] = cast(expr->type, expr->child[1]);
    expr->is_left_val = 0;
    return expr;
}

static struct expr_node *compound_assignment_expression_check(struct expr_node *expr)
{
    struct type *ltype, *rtype;
    struct expr_node *bin_op;

    bin_op = alloc_expr_node();
    switch (expr->op)
    {
        case OP_BITOR_ASSIGN:
            bin_op->op = OP_BITOR;
            break;
        case OP_BITXOR_ASSIGN:
            bin_op->op = OP_BITXOR;
            break;
        case OP_BITAND_ASSIGN:
            bin_op->op = OP_BITAND;
            break;
        case OP_LSHIFT_ASSIGN:
            bin_op->op = OP_LSHIFT;
            break;
        case OP_RSHIFT_ASSIGN:
            bin_op->op = OP_RSHIFT;
            break;
        case OP_ADD_ASSIGN:
            bin_op->op = OP_ADD;
            break;
        case OP_SUB_ASSIGN:
            bin_op->op = OP_SUB;
            break;
        case OP_MUL_ASSIGN:
            bin_op->op = OP_MUL;
            break;
        case OP_DIV_ASSIGN:
            bin_op->op = OP_DIV;
            break;
        case OP_MOD_ASSIGN:
            bin_op->op = OP_MOD;
            break;
        default:
            assert(0);
    }
    bin_op->child[0] = expr->child[0];
    bin_op->child[1] = expr->child[1];
    bin_op = adjust(expression_check(bin_op));
    bin_op->is_left_val = 0;

    expr->child[1] = bin_op;

    ltype = bin_op->child[0]->type;
    rtype = bin_op->child[1]->type;
    
    if (!is_modifiable_lvalue(bin_op->child[0]))
    {
        rep_error("the left operand should have a modifiable lvalue");
        exit(1);
    }

    /*
    the left operand shall be
    a pointer to an object type and the right shall have integral type
    */
    if (IS_OBJECT_POINTER(ltype) && IS_INTEG_TYPE(rtype))
    {
        goto ok;
    }
   /*
   the left operand shall have qualified or unqualified arithmetic type
    and the right shall have arithmetic type
   */
    if (IS_ARITH_TYPE(unqualify(ltype)) && IS_ARITH_TYPE(rtype))
    {
        goto ok;
    }
    rep_error("illegal expression");
    exit(1);
ok:
//    expr->op = OP_ASSIGN;
    expr->type = unqualify(expr->child[0]->type);
    expr->child[1] = cast(expr->type, expr->child[1]);
    expr->is_left_val = 0;
    return expr;
}
static struct expr_node *conditional_expression_check(struct cond_expr_node *expr)
{
    int qualer;
    struct type *type1, *type2, *type3, *common_type;
    expr->child[0] = adjust(expression_check(expr->child[0]));
    expr->child[1]->child[0] = adjust(expression_check(expr->child[1]->child[0]));
    expr->child[1]->child[1] = adjust(expression_check(expr->child[1]->child[1]));
    type1 = expr->child[0]->type;
    type2 = expr->child[1]->child[0]->type;
    type3 = expr->child[1]->child[1]->type;

    /* The first operand shall have scalar type */
    if (!IS_SCALAR_TYPE(type1))
    {
        rep_error("the first operand should have scalar type");
        exit(1);
    }
    /* One of the following shall hold for the second and third operands: */
    /* both operands have arithmetic type */
    if (IS_ARITH_TYPE(type2) && IS_ARITH_TYPE(type3))
    {
        /* If both the second and third operands have arithmetic type, the
        usual arithmetic conversions are performed to bring them to a common
        type and the result has that type.*/
        common_type = arith_common_type(type2, type3);
        expr->child[1]->child[0] = cast(common_type, expr->child[1]->child[0]);
        expr->child[1]->child[1] = cast(common_type, expr->child[1]->child[1]);
        expr->type = common_type;
        goto ok;
    }
    /* both operands have compatible structure or union types;  */
    if (IS_RECORD_TYPE(type2) && IS_RECORD_TYPE(type3) && is_compatible_type(type2, type3))
    {
        expr->type = type2;
        goto ok;
    }
    /* both operands have void type; */
    if (type2->categ == VOID && type3->categ == VOID)
    {
        expr->type = type2;
        goto ok;
    }
    /*both operands are pointers to qualified or unqualified versions of
    compatible types;*/
    if (type2->categ == POINTER && type3->categ == POINTER &&
        is_compatible_type(unqualify(type2->basic_type), unqualify(type3->basic_type)))
    {
        /*If both the second and third operands are pointers or one is a null
        pointer constant and the other is a pointer, the result type is a
        pointer to a type qualified with all the type qualifiers of the types
        pointed-to by both operands
        Furthermore, if both operands are
		pointers to compatible types or differently qualified versions of a
		compatible type, the result has the composite type;
        */
        qualer = type2->basic_type->qualer | type3->basic_type->qualer;
        expr->type = create_pointer_type(qualify(greatest_common_type(
                        unqualify(type2->basic_type), unqualify(type3->basic_type)), qualer));
        goto ok;
    }
    /*one operand is a pointer and the other is a null pointer constant;*/
    if (type2->categ == POINTER && is_null_constant(expr->child[1]->child[1]))
    {
        /*if one operand is a null pointer constant, the result has the type of the other operand*/
        expr->type = type2;
        goto ok;
    }
    if (type3->categ == POINTER && is_null_constant(expr->child[1]->child[0]))
    {
        /*if one operand is a null pointer constant, the result has the type of the other operand*/
        expr->type = type3;
        goto ok;
    }
    /*one operand is a pointer to an object or incomplete type and the
    other is a pointer to a qualified or unqualified version of void*/
    if ((IS_OBJECT_POINTER(type2) || IS_INCOMPLETE_POINTER(type2)) && IS_VOID_POINTER(type3))
    {
        /*one operand is a pointer to void or a qualified version of
        void, in which case the other operand is converted to type pointer to
        void, and the result has that type*/
        expr->child[1]->child[0] = cast(unqualify(type3), expr->child[1]->child[0]);
        expr->type = unqualify(type3);
        goto ok;
    }
    if ((IS_OBJECT_POINTER(type3) || IS_INCOMPLETE_POINTER(type3)) && IS_VOID_POINTER(type2))
    {
        /*one operand is a pointer to void or a qualified version of
        void, in which case the other operand is converted to type pointer to
        void, and the result has that type*/
        expr->child[1]->child[1] = cast(unqualify(type2), expr->child[1]->child[1]);
        expr->type = unqualify(type2);
        goto ok;
    }
    rep_error("illegal expression");
    exit(1);
ok:
    expr->is_left_val = 0;
    expr->else_lbl = sym_create_label();
    expr->end_lbl = sym_create_label();
    return constant_fold(expr);
}

struct expr_node *comma_expression_check(struct expr_node *expr)
{
    expr->child[0] = adjust(expression_check(expr->child[0]));
    expr->child[1] = adjust(expression_check(expr->child[1]));
    expr->type = expr->child[1]->type;
    expr->is_left_val = 0;
    return constant_fold(expr);
}

struct expr_node *expression_check(struct expr_node *expr)
{
    switch (expr->op)
    {
        case OP_COMMA:
            return comma_expression_check(expr);
        case OP_ASSIGN:
            return simple_assignment_expression_check(expr);
        case OP_BITOR_ASSIGN:
        case OP_BITXOR_ASSIGN:
        case OP_BITAND_ASSIGN:
        case OP_LSHIFT_ASSIGN:
        case OP_RSHIFT_ASSIGN:
        case OP_ADD_ASSIGN:
        case OP_SUB_ASSIGN:
        case OP_MUL_ASSIGN:
        case OP_DIV_ASSIGN:
        case OP_MOD_ASSIGN:
            return compound_assignment_expression_check(expr);
        case OP_QUESTION:
            return conditional_expression_check(expr);
        case OP_OR:
        case OP_AND:
        case OP_BITOR:
        case OP_BITXOR:
        case OP_BITAND:
        case OP_EQUAL:
        case OP_UNEQUAL:
        case OP_GREAT:
        case OP_LESS:
        case OP_GREAT_EQ:
        case OP_LESS_EQ:
        case OP_LSHIFT:
        case OP_RSHIFT:
        case OP_ADD:
        case OP_SUB:
        case OP_MUL:
        case OP_DIV:
        case OP_MOD:
            return binary_expression_check(expr);
        case OP_CAST:
        case OP_PREINC:
        case OP_PREDEC:
        case OP_ADDRESS:
        case OP_DEREF:
        case OP_POS:
        case OP_NEG:
        case OP_COMP:
        case OP_NOT:
        case OP_SIZEOF:
            return unary_expression_check(expr);
        case OP_INDEX:
        case OP_CALL:
        case OP_MEMBER:
        case OP_PTR_MEMBER:
        case OP_POSTINC:
        case OP_POSTDEC:
            return postfix_expression_check(expr);
        case OP_ID:
        case OP_CONST:
        case OP_STR:
            return primary_expression_check(expr);
    }
    assert(0);
    return expr;
}

struct expr_node *constant_expression_check(struct expr_node *expr)
{
    expr = expression_check(expr);
    if (! (expr->op == OP_CONST && IS_INTEG_TYPE(expr->type)))
	{
		return NULL;
	}
	return expr;
}
