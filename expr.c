#include <stdlib.h>
#include <assert.h>
#include "expr.h"
#include "decl.h"
#include "sxcc.h"

#define TOK_IS_BINARY_OP(tok_type) ((tok_type >= TK_OR) && (tok_type <= TK_MOD))
#define BINARY_OP_TOK2OP(tok_type) (tok_type - TK_OR + OP_OR)
#define BINARY_OP_TOK2PREC(tok_type) (prec_tab[BINARY_OP_TOK2OP(tok_type)])
#define HIGHEST_BINARY_OP_PREC (prec_tab[OP_MUL])
#define ASSIGN_OP_TOK2OP(tok_type) (tok_type - TK_ASSIGN + OP_ASSIGN)

static int prec_tab[] =
{
    #define OPINFO(op, prec, name, func, opcode) prec,
        #include "opinfo.h"
    #undef OPINFO
};

struct expr_node *alloc_expr_node()
{
    struct expr_node *node;
    
    ALLOC(node, struct expr_node);

    node->kind = NK_Expression;
    return node;
}

/*
primary-expression:
                  identifier
                  constant
                  string-literal
                  (  expression )
*/
struct expr_node *primary_expression()
{
    struct array_type *type;
    struct token *tok, *tok1;
    struct expr_node *node = alloc_expr_node();
    tok = lex_get_token();
    if (tok->type == TK_ID)
    {
        node->op = OP_ID;
        node->value.p = tok->token_strings;
    }
    else if ((tok->type >= TK_INTCONST) && (tok->type <= TK_DOUBLECONST))
    {
        node->value = tok->value;
        if (tok->type == TK_INTCONST)
        {
            node->type = &basic_types[INT];
        }
        else if (tok->type == TK_UINTCONST)
        {
            node->type = &basic_types[UINT];
        }
        else if (tok->type == TK_LONGCONST)
        {
            node->type = &basic_types[LONG];
        }
        else if (tok->type == TK_ULONGCONST)
        {
            node->type = &basic_types[ULONG];
        }
        else if (tok->type == TK_FLOATCONST)
        {
            node->type = &basic_types[FLOAT];
        }
        else// if (tok->type == TK_DOUBLECONST)
        {
            node->type = &basic_types[DOUBLE];
        }
        node->op = OP_CONST;
    }
    else if (tok->type == TK_STRING)
    {
        ALLOC(type, struct array_type);
        
        type->categ = ARRAY;
        type->size = strlen(tok->token_strings);
        type->basic_type = &basic_types[CHAR];
        type->len = type->size;

        node->op = OP_STR;
        node->type = (struct type *)type;
        node->value.p = tok->token_strings;
    }
    else if (tok->type == TK_LPAREN)
    {
        free(node);
        node = expression();
        expect(TK_RPAREN);
    }
    else 
    {
        error("expect id, string, number or '('!", tok->loc);
        exit(1);
    }
    free(tok);
    return node;
}

/*
argument-expression-list:
                  assignment-expression
                  argument-expression-list ,  assignment-expression

we change it to :

argument-expression-list:
                  assignment-expression _argument-expression-list

_argument-expression-list:
                  ,  assignment-expression _argument-expression-list
                  NONE
*/
struct expr_node *argument_expression_list()
{
    struct token *tok, *tok1;
    struct expr_node *node, *p;

    node = assignment_expression();
    p = node;
    while(1)
    {
        tok = lex_get_token();
        if (tok->type != TK_COMMA)
        {
            lex_push(tok);
            break;
        }
        free(tok);
        p->next = assignment_expression();
        p = p->next;
    }
    return node;
}

/*
postfix-expression:
                  primary-expression
                  postfix-expression [  expression ] 
                  postfix-expression (  argument-expression-list<opt> ) 
                  postfix-expression .   identifier
                  postfix-expression ->  identifier
                  postfix-expression ++ 
                  postfix-expression --

we change it into:
postfix-expression:
            primary-expression _postfix-expression

_postfix-expression:
            [expression] _postfix-expression
            (argument-expression-list<opt>) _postfix-expression
            .identifier _postfix-expression
            ->identifier _postfix-expression
            ++ _postfix-expression
            -- _postfix-expression
            NONE

*/
struct expr_node *_postfix_expression(struct expr_node **root)
{
    struct expr_node *node, *father;
    struct token *tok;

    tok = lex_get_token();
    switch (tok->type)
    {
        case TK_LBRACKET:
            free(tok);
            node = alloc_expr_node();
            node->op = OP_INDEX;
            node->child[1] = expression();
            expect(TK_RBRACKET);
            break;
        case TK_LPAREN:
            free(tok);
            node = alloc_expr_node();
            node->op = OP_CALL;
            tok = lex_get_token();
            if (tok->type != TK_RPAREN)
            {
                lex_push(tok);
                node->child[1] = argument_expression_list();
                expect(TK_RPAREN);
            }
            else
            {
                free(tok);
            }
            break;
        case TK_DOT:
        case TK_POINTER:
            node = alloc_expr_node();
            node->op = tok->type == TK_DOT ? OP_MEMBER : OP_PTR_MEMBER;
            free(tok);
            tok = lex_get_token();
            if (tok->type != TK_ID)
            {
                error("expect identifier", tok->loc);
                exit(1);
            }
            node->child[1] = alloc_expr_node();
            node->child[1]->op = OP_ID;
            node->child[1]->value.p = tok->token_strings;
            free(tok);
            break;
        case TK_INC:
        case TK_DEC:
            node = alloc_expr_node();
            node->op = tok->type == TK_INC ? OP_POSTINC : OP_POSTDEC;
            free(tok);
            break;
        default:
            lex_push(tok);
            *root = NULL;
            return NULL;
    }
    father = _postfix_expression(root);
    if (!father)
    {
        *root = node;
    }
    else
    {
        father->child[0] = node;
    }
    return node;
}
struct expr_node *postfix_expression()
{
    struct expr_node *father, *pri_expr, *root;
    struct token *tok;

    pri_expr = primary_expression();

    father = _postfix_expression(&root);

    if (!father)
    {
        root = pri_expr;
    }
    else
    {
        father->child[0] = pri_expr;
    }
    return root;
}

/*
unary-expression:
                  postfix-expression
                  ++  unary-expression
                  --  unary-expression
                  unary-operator cast-expression
                  sizeof  unary-expression
                  sizeof (  type-name )

          unary-operator: one of
                  &  *  +  -  ~  !
*/
struct expr_node *unary_expression()
{
    struct token *tok, *tok1;
    struct expr_node *node = alloc_expr_node();

    tok = lex_get_token();
    if (tok->type == TK_INC || tok->type == TK_DEC)
    {
        node->op = (tok->type == TK_INC) ? OP_PREINC : OP_PREDEC;
        node->child[0] = unary_expression();
        free(tok);
    }
    else if (tok->type == TK_SIZEOF)
    {
        free(tok);
        node->op = OP_SIZEOF;
        tok = lex_get_token();
        if (tok->type == TK_LPAREN)
        {
            tok1 = lex_get_token();
            if (is_type_specifier(tok1, 0) || is_type_qualifier(tok1))
            {
                free(tok);
                lex_push(tok1);
                node->child[0] = type_name();
                 expect(TK_RPAREN);
            }
            else
            {
                lex_push(tok1);
                lex_push(tok);
                node->child[1] = unary_expression();
            }
        }
        else
        {
            lex_push(tok);
            node->child[1] = unary_expression();
        }
    }
    else if (tok->type == TK_BITAND || tok->type == TK_MUL || tok->type == TK_ADD ||
                tok->type == TK_SUB || tok->type == TK_COMP || tok->type == TK_NOT)
    {
        node->op = (tok->type == TK_BITAND) ? OP_ADDRESS : (tok->type == TK_MUL) ? OP_DEREF :
                (tok->type == TK_ADD) ? OP_POS : (tok->type == TK_SUB) ? OP_NEG :
                (tok->type == TK_COMP) ? OP_COMP : OP_NOT;
        node->child[0] = cast_expression();
        free(tok);
    }
    else
    {
        lex_push(tok);
        node = postfix_expression();
    }
    
    return node;
}

/*
cast-expression:
                  unary-expression
                  ( type-name )  cast-expression
*/

struct expr_node *cast_expression()
{
    struct token *tok, *tok1;
    struct expr_node *node = alloc_expr_node();

    node->op = OP_CAST;
    tok = lex_get_token();
    if (tok->type == TK_LPAREN)
    {
        tok1 = lex_get_token();
        if (is_type_qualifier(tok1) || is_type_specifier(tok1, 0))   //type-name
        {
            free(tok);
            lex_push(tok1);
            node->child[0] = type_name();
            expect(TK_RPAREN);
            node->child[1] = cast_expression();
        }
        else
        {
            lex_push(tok1);
            goto unary_expr;
        }
    }
    else 
    {
unary_expr:
        lex_push(tok);
        node = unary_expression();
    }
    return node;
}
/*
logical-OR-expression:
                  logical-AND-expression
                  logical-OR-expression ||  logical-AND-expression
logical-AND-expression:
                  inclusive-OR-expression
                  logical-AND-expression &&  inclusive-OR-expression
inclusive-OR-expression:
                  exclusive-OR-expression
                  inclusive-OR-expression |  exclusive-OR-expression
exclusive-OR-expression:
                  AND-expression
                  exclusive-OR-expression ^  AND-expression
AND-expression:
                  equality-expression
                  AND-expression &  equality-expression
equality-expression:
                  relational-expression
                  equality-expression ==  relational-expression
                  equality-expression !=  relational-expression
relational-expression:
                  shift-expression
                  relational-expression <   shift-expression
                  relational-expression >   shift-expression
                  relational-expression <=  shift-expression
                  relational-expression >=  shift-expression
shift-expression:
                  additive-expression
                  shift-expression <<  additive-expression
                  shift-expression >>  additive-expression
additive-expression:
                  multiplicative-expression
                  additive-expression +  multiplicative-expression
                  additive-expression -  multiplicative-expression
multiplicative-expression:
                  cast-expression
                  multiplicative-expression *  cast-expression
                  multiplicative-expression /  cast-expression
                  multiplicative-expression %  cast-expression
*/
struct expr_node *binary_expression(int prec)
{
    struct token *tok;
    struct expr_node *node, *p;

    if (prec == HIGHEST_BINARY_OP_PREC)
    {
        node = cast_expression();
    }
    else
    {
        node = binary_expression(prec + 1);
    }
    tok = lex_get_token();
    while (TOK_IS_BINARY_OP(tok->type) && (BINARY_OP_TOK2PREC(tok->type) == prec))
    {
        p = alloc_expr_node();
        p->op = BINARY_OP_TOK2OP(tok->type);
        p->child[0] = node;
        if (prec == HIGHEST_BINARY_OP_PREC)
        {
            p->child[1] = cast_expression();
        }
        else
        {
            p->child[1] = binary_expression(prec + 1);
        }
        node = p;
        free(tok);
        tok = lex_get_token();
    }
    lex_push(tok);
    return node;
}

/*
conditional-expression:
                  logical-OR-expression
                  logical-OR-expression ?  expression :  conditional-expression
*/
struct expr_node *conditional_expression()
{
    struct token *tok;
    struct expr_node *node, *p;
    struct cond_expr_node *cond_expr;

    node = binary_expression(prec_tab[OP_OR]);
    tok = lex_get_token();

    if (tok->type == TK_QUESTION)
    {
        free(tok);
        ALLOC(cond_expr, struct cond_expr_node);
        
        cond_expr->kind = NK_Expression;
        cond_expr->op = OP_QUESTION;
        cond_expr->child[0] = node;

        p = alloc_expr_node();
        p->op = OP_COLON;
        p->child[0] = expression();
        expect(TK_COLON);
        p->child[1] = conditional_expression();
        

        cond_expr->child[1] = p;
    }
    else
    {
        lex_push(tok);
        return node;
    }
    return cond_expr;
}

/*
assignment-expression:
                  conditional-expression
                  unary-expression assignment-operator assignment-expression

          assignment-operator: one of
                  =  *=  /=  %=  +=  -=  <<=  >>=  &=  ^=  |=

we modify it to:
assignment-expression:
                  conditional-expression
                  conditional-expression assignment-operator assignment-expression

          assignment-operator: one of
                  =  *=  /=  %=  +=  -=  <<=  >>=  &=  ^=  |=
*/
struct expr_node *assignment_expression()
{
    struct token *tok;
    struct expr_node *node, *p;

    node = conditional_expression();
    tok = lex_get_token();

    if ((tok->type >= TK_ASSIGN) && (tok->type <= TK_MOD_ASSIGN))
    {
        p = alloc_expr_node();
        p->op = ASSIGN_OP_TOK2OP(tok->type);
        p->child[0] = node;
        p->child[1] = assignment_expression();
        node = p;
        free(tok);
    }
    else
    {
        lex_push(tok);
    }
    return node;
}

/*
expression:
                  assignment-expression
                  expression ,  assignment-expression

we change it into:

expression:
                assignment-expression _expression
_expression:
                ,assignment-expression _expression
                NONE
*/
static struct expr_node *_expression(struct expr_node **root)
{
    struct expr_node *node, *father, *arg;
    struct token *tok;

    tok = lex_get_token();
    if (tok->type != TK_COMMA)
    {
        lex_push(tok);
        *root = NULL;
        return NULL;
    }
    free(tok);

    arg = assignment_expression();

    node = alloc_expr_node();
    node->op = OP_COMMA;
    node->child[1] = arg;

    father = _expression(root);

    if (!father)
    {
        *root = node;
    }
    else
    {
        father->child[0] = node;
    }
    return node;
}
struct expr_node *expression()
{
    struct expr_node *node, *father, *arg, *root;
    struct token *tok;

    arg = assignment_expression();

    father = _expression(&root);
    if (!father)
    {
        root = arg;
    }
    else
    {
        father->child[0] = arg;
    }
    return root;
}
/*
constant-expression:
                  conditional-expression
*/
struct expr_node *constant_expression()
{
    return conditional_expression();
}


