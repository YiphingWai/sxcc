#include <stdlib.h>
#include <assert.h>
#include "vector.h"

#define VECTOR_MAGIC ('v' << 24 + 'e' << 16 + 'c' << 8 + 't')
int vector_insert(struct vector *vec, int pos, void *item)
{
    int i;
    assert(vec->magic == VECTOR_MAGIC);
    if (pos > vec->len)
    {
        return 1;
    }
    if (vec->len >= vec->size)
    {
        if (vec->size)
        {
            vec->data = (void **)realloc(vec->data, 2 * (vec->size) * sizeof(int *));
            if (!(vec->data))
            {
                printf("realloc err!\n");
                exit(-1);
            }
            vec->size *= 2;
        }
        else
        {
            vec->data = (void **)calloc(1, 4 * sizeof(void *));
            if (!(vec->data))
            {
                printf("calloc err!\n");
                exit(-1);
            }
            vec->size = 4;
        }
    }
    if (vec->len)
    {
        for (i = vec->len - 1; i >= pos; i--)
        {
            vec->data[i + 1] = vec->data[i];
        }
    }
    vec->data[pos] = item;
    vec->len++;
    return 0;
}

int vector_push_back(struct vector *vec, void *item)
{
    return vector_insert(vec, vec->len, item);
}

void *vector_get(struct vector *vec, int pos)
{
    assert(vec->magic == VECTOR_MAGIC);
    if (pos < 0)
    {
        return NULL;
    }
    if (pos >= vec->len)
    {
        return NULL;
    }
    return vec->data[pos];
}

void *vector_get_back(struct vector *vec)
{
    return vector_get(vec, vec->len - 1);
}

void *vector_erase(struct vector *vec, int pos)
{
    int i;
    void *item;
    assert(vec->magic == VECTOR_MAGIC);
    if (pos < 0)
    {
        return NULL;
    }
    if (pos >= vec->len)
    {
        return NULL;
    }
    item = vec->data[pos];
    for (i = pos + 1; i < vec->len; i++)
    {
        vec->data[i - 1] = vec->data[i];
    }
    vec->len--;
    return item;
}

void *vector_pop_back(struct vector *vec)
{
    return vector_erase(vec, vec->len - 1);
}

void vector_clear(struct vector *vec)
{
    assert(vec->magic == VECTOR_MAGIC);
    vec->len = 0;
}

struct vector *vector_init(int size)
{
    struct vector *vec;

    vec = (struct vector *)calloc(1, sizeof(struct vector));
    if (!vec)
    {
        printf("calloc err!\n");
        return NULL;
    }
    if (size)
    {
        vec->data = (void **)calloc(1, size * sizeof(void *));
        if (!(vec->data))
        {
            free(vec);
            printf("calloc err!\n");
            return NULL;
        }
    }
    vec->size = size;
    vec->len = 0;
    vec->erase = vector_erase;
    vec->get = vector_get;
    vec->get_back = vector_get_back;
    vec->insert = vector_insert;
    vec->pop_back = vector_pop_back;
    vec->clear = vector_clear;
    vec->push_back = vector_push_back;
    vec->magic = VECTOR_MAGIC;
    return vec;
}

void vector_destroy(struct vector *vec)
{
    if (!vec)
    {
        return;
    }
    if (vec->data)
    {
        free(vec->data);
    }
    free(vec);
}
