#ifndef __VECTOR_H__
#define __VECTOR_H__

struct vector
{
    int magic;
    void **data;
    int len;
	int size;

    int (*insert)(struct vector *, int , void *);
    int (*push_back)(struct vector *, void *);
    void *(*get)(struct vector *, int );
    void *(*get_back)(struct vector *);
    void *(*erase)(struct vector *, int );
    void (*clear)(struct vector *);
    void *(*pop_back)(struct vector *);
};


struct vector *vector_init(int );
void vector_destroy(struct vector *);












#endif
