#include "sxcc.h"
#include "type.h"
#include "hash.h"
#include "vector.h"
#include <string.h>
#include <stdlib.h>
#include <assert.h>

// actually, basic_types consist only of (U)CHAR (U)INT (U)LONG FLOAT DOUBLE VOID ENUM
// or arith type + VOID ENUM
// the rest item are invaild
// {name, qualer, align, size, basic_type}
const struct type basic_types[FUNCTION - INT + 1] =
{
    #define TYPEINFO(name, qualer, align, size, basic_type) {name, qualer, align, size, basic_type},
        #include "typeinfo.h"
    #undef TYPEINFO
};

struct func_type *default_func_type = NULL;

struct type *create_array(int len, struct type *basic_type)
{
    struct array_type *aty;
    ALLOC(aty, struct array_type);
	aty->categ = ARRAY;
	aty->qualer = 0;
	aty->size = len * basic_type->size;
	aty->align = basic_type->align;
	aty->basic_type = basic_type;
	aty->len = len;

    return (struct type *)aty;
}


struct type *create_pointer_type(struct type *basic_type)
{
    struct type *pty;
    ALLOC(pty, struct type);
	pty->categ = POINTER;
	pty->qualer = 0;
	pty->align = basic_types[POINTER].align;
	pty->size = basic_types[POINTER].size;
	pty->basic_type = basic_type;

    return pty;
}

struct type *create_function(struct type *return_type, int has_ellipsis, struct vector *params)
{
    struct func_type *fty;
    ALLOC(fty, struct func_type);
    fty->categ = FUNCTION;
	fty->qualer = 0;
	fty->size = basic_types[POINTER].size;
	fty->align = basic_types[POINTER].align;
	fty->has_ellipsis = has_ellipsis;
	fty->basic_type = return_type;
    fty->params = params;

	return (struct func_type *)fty;
}

struct parameter_type *add_parameter(struct func_type *ftype, char *id, struct type *ptype)
{
    struct parameter_type *pty;
    ALLOC(pty, struct parameter_type);
    pty->id = id;
    pty->type = ptype;
    ftype->params->push_back(ftype->params, pty);
    return pty;
}

struct type *create_record(char *id, enum type_categ categ)
{
    struct record_type *rty;
    ALLOC(rty, struct record_type);
	/*************************************
		When Starting parsing struct /union,
		its size is unclear yet.
		So rty->size is 0 when calling this function.
	 *************************************/
	rty->categ = categ;
	rty->id = id;
    rty->fields = vector_init(0);
	return (struct type *)rty;
}

struct record_field *add_field(struct record_type *rtype, char *id, struct type *ftype)
{
    struct record_field *field;

    if (ftype->qualer & CONST)
    {
        rtype->has_const_field = 1;
    }
    ALLOC(field, struct record_field);

    field->id = id;
    field->type = ftype;
    rtype->fields->push_back(rtype->fields, field);
    return field;
}

struct record_field *lookup_field(struct record_type *rtype, char *id)
{
    int i;
    struct record_field *field, *p;
    for (i = 0; i < rtype->fields->len; i++)
    {
        field = rtype->fields->get(rtype->fields, i);
        if (field->id)
        {
            if (!strcmp(field->id, id))
            {
                return field;
            }
        }
        else if ((field->type->categ == STRUCT || field->type->categ == UNION) && !(field->id))
        {
            p = lookup_field(field->type, id);
            if (p)
            {
                return p;
            }
        }
        else if (!field->id)
        {
            continue;
        }
        else
        {
            rep_error("illegal field");
            exit(1);
        }
    }
    return NULL;
}
/**
 * For unamed struct/union field in a struct/union, the offset of fields in the 
 * unnamed struct/union should be fix up.
 */
/******************************************
	struct Data{
		int a;		-----	offset is 0
		struct{
			int b1;	-----0
			int b2;	-----4
			int b3;	-----8		
		};			-----	offset is  4
		double c;	-----	offset is 16
		struct{
			int d1;	----	0
			int d2;	----	4
			int d3;	----	8
		}d;					//  
	}
	When we know the offset for 'b' is 4,
	we call AddOffset(...) to calculate the offset of 
	b1,b2,b3 .
		AddOffset(..,  4)
			b1:		offset	0+4		4
			b2:		offset    4+4		8
			b3:		offset	4+8		12
		But for d1,d2,d3, we don't call AddOffset(..).
	For example:
		Data data;
		data.d.d1;	----------      data.d ------ knowing the offset of d in is enough.
					---------- 	d.d1    ------ knowing the offset of d1 in data is also enough.
		data.b1;	----------	we must know the offset of b1 in data.
 ******************************************/
static int fill_record_align(struct record_type *rtype)
{
    int i, align = 0, max_align = 0;
    struct record_field *field;

    for (i = 0; i < rtype->fields->len; i++)
    {
        field = rtype->fields->get(rtype->fields, i);
        if (field->type->categ == STRUCT || field->type->categ == UNION)
        {
            align = fill_record_align(field->type);
        }
        else
        {
            align = field->type->align;
        }
        max_align = (max_align > align) ? max_align : align;
    }
    rtype->align = max_align;
    return max_align;
}

int layout_record(struct record_type *rtype, int basic_offset)
{
    int i, offset, offset_max;
    struct record_field *field;

    fill_record_align(rtype);
    offset = offset_max = basic_offset;

    for (i = 0; i < rtype->fields->len; i++)
    {
        field = rtype->fields->get(rtype->fields, i);
        offset = ALIGN(offset, field->type->align);
        field->offset = offset;
        if (field->type->categ == STRUCT || field->type->categ == UNION)
        {
            if (field->id)
            {
                offset += layout_record(field->type, 0);
            }
            else
            {
                offset = layout_record(field->type, offset);
            }
        }
        else
        {
            offset += field->type->size;
        }
        if (IS_RECORD_TYPE(field->type))
        {
            field->type->size = offset - field->offset;
        }
        offset_max = (offset_max > offset) ? offset_max : offset;
        offset = (rtype->categ == UNION) ? basic_offset : offset;
    }
    rtype->size = offset_max;
    return offset_max;
}

struct type *create_enum(char *id)
{
    struct enum_type *ety;

    ALLOC(ety, struct enum_type);
    ety->categ = ENUM;
	ety->id = id;

	// enumeration type is compatilbe with int
	ety->basic_type = &basic_types[INT];
	ety->size = ety->basic_type->size;
	ety->align = ety->basic_type->align;
	ety->qualer = 0;
    return (struct type *)ety;
}

static struct type *clone_type(struct type *ty)
{
    char *name[256];
    struct enum_type *ety;
    struct record_type *rty;
    struct func_type *fty;
    struct array_type *aty;
    struct type *bty;

    switch(ty->categ)
    {
        case ARRAY:
            ALLOC(aty, struct array_type);
            *aty = *(struct array_type *)ty;
            return (struct type *)aty;
        case FUNCTION:
            ALLOC(fty, struct func_type);
            *fty = *(struct func_type *)ty;
            return (struct type *)fty;
        case ENUM:
            ALLOC(ety, struct enum_type);
            *ety = *(struct enum_type *)ty;
            return (struct type *)ety;
        case STRUCT:
        case UNION:
            ALLOC(rty, struct record_type);
            *rty = *(struct record_type *)ty;
            return (struct type *)rty;
        default:
            ALLOC(bty, struct type);
            *bty = *(struct type *)ty;
            return bty;
    }
    // never reach here
    return ty;
}

struct type *qualify(struct type *ty, enum type_qualer qualer)
{
    struct type *qty;
    
    if (!qualer || (qualer | ty->qualer == ty->qualer))
    {
        return ty;
    }
    qty = clone_type(ty);
    qty->qualer |= qualer;
    qty->basic_type = (ty->qualer) ? ty->basic_type : ty;

    return (struct type *)qty;
}

struct type *unqualify(struct type *ty)
{
    while (ty->qualer)
    {
        ty = ty->basic_type;
    }
    return ty;
}

static int is_compatible_function(struct func_type *ftype1, struct func_type *ftype2)
{
    int i, para_len1, para_len2;
    struct parameter_type *para1, *para2;

    if (!is_compatible_type(ftype1->basic_type, ftype2->basic_type))
    {
        return 0;
    }
    para_len1 = ftype1->params ? ftype1->params->len : 0;
    para_len2 = ftype2->params ? ftype2->params->len : 0;
    if (para_len1 != para_len2)
    {
        return 0;
    }
    if (ftype1->has_ellipsis != ftype2->has_ellipsis)
    {
        return 0;
    }
    for (i = 0; i < para_len1; i++)
    {
        para1 = ftype1->params->get(ftype1->params, i);
        para2 = ftype2->params->get(ftype2->params, i);
        if (!is_compatible_type(para1->type, para2->type))
        {
            return 0;
        }
    }
    return 1;
}

/* for two varible with the same name, we should find the greatest common type */
struct type *greatest_common_type(struct type *type1, struct type *type2)
{
    int i;
    struct type *pty;
    struct func_type *ftype1, *ftype2, *fty;

    if (!is_compatible_type(type1, type2))
    {
        rep_error("the two types are not compatible");
        exit(1);
    }
    /*If one type is an array of known size, the composite type is an
    array of that size.*/
    if (type1->categ == ARRAY)
    {
        return type1->size ? type1 : type2;
    }
    /* if both types are pointer, their greatest common type is a pointer 
    of their basic type's greatest common type which have the same qualer*/
    if (type1->categ == POINTER)
    {
        /* find greatest common type recursively */
        ALLOC(pty, struct type);
        pty->categ  = POINTER;
        pty->qualer = 0;
        pty->align = basic_types[POINTER].align;
        pty->size = basic_types[POINTER].size;
        pty->basic_type = greatest_common_type(type1->basic_type, type2->basic_type);
        return qualify(pty, type1->qualer);
    }
    /*If both types have parameter type lists, the type of each parameter
    in the composite parameter type list is the composite type of the
    corresponding parameters.*/
    if (type1->categ == FUNCTION)
    {
        ftype1 = (struct func_type *)type1;
        ftype2 = (struct func_type *)type2;
        ALLOC(fty, struct func_type);
        *fty = *ftype1;
        fty->basic_type = greatest_common_type(type1->basic_type, type2->basic_type);
        fty->params = vector_init(0);
        for (i = 0; (ftype1->params) && (i < ftype1->params->len); i++)
        {
            add_parameter(fty, ((struct parameter_type *)ftype1->params->get(ftype1->params, i))->id, 
                            greatest_common_type(((struct parameter_type *)ftype1->params->get(ftype1->params, i))->type, 
                                                    ((struct parameter_type *)ftype2->params->get(ftype2->params, i))->type));
        }
        return (struct type *)fty;
    }
    return type1;
}

int is_compatible_type(struct type *type1, struct type *type2)
{
    if (type1 == type2)
    {
        return 1;
    }
    if (type1->qualer != type2->qualer)
    {
        return 0;
    }
    type1 = unqualify(type1);
    type2 = unqualify(type2);
    if (type1->categ != type2->categ)
    {
        return 0;
    }
    if (type1->categ == POINTER)
    {
        return is_compatible_type(type1->basic_type, type2->basic_type);
    }
    if (type1->categ == ARRAY)
    {
        return (type1->size == 0 || type1->size == 0 || type1->size == type2->size) && 
                is_compatible_type(type1->basic_type, type2->basic_type);
    }
    if (type1->categ == FUNCTION)
    {
        return is_compatible_function((struct func_type *)type1, (struct func_type *)type2);
    }
    // record type
    return type1 == type2;
}
/*
incomplete means we don't know the size of the type
1. for enum, union and struct, it is incomplete type when we haven't found its declaration yet
2. for array, it is incomplete type when its len is 0 or its basic type is incomplete
*/
int is_incomplete_rec_type(struct type *type)
{
    type = unqualify(type);
    if (type->categ == STRUCT || type->categ == UNION)
    {
        return !(((struct record_type *)type)->complete);
    }
    return 0;
}

int is_incomplete_enum_type(struct type *type)
{
    type = unqualify(type);
    if (type->categ == ENUM)
    {
        return !(((struct enum_type *)type)->complete);
    }
    return 0;
}

int is_incomplete_type(struct type *type, int ignore_zero_size_array)
{
    type = unqualify(type);
    switch (type->categ)
    {
        case ENUM:
            return is_incomplete_enum_type(type);
        case STRUCT:
        case UNION:
            return is_incomplete_rec_type(type);
        case ARRAY:
            if (ignore_zero_size_array)
            {
                return is_incomplete_type(type->basic_type, ignore_zero_size_array);
            }
            else
            {
                if (((struct array_type *)type)->len == 0)
                {
                    return 1;
                }
                return is_incomplete_type(type->basic_type, ignore_zero_size_array);
            }
    }
    return 0;
}

struct type *arith_common_type(struct type *type1, struct type *type2)
{
    struct type *type_tmp;
    // if either operand has type double, the other operand is converted to double.
    if (type1->categ == DOUBLE || type2->categ == DOUBLE)
    {
        return &basic_types[DOUBLE];
    }
    // Otherwise, if either operand has type float, the other operand is converted to float.  
    if (type1->categ == FLOAT || type2->categ == FLOAT)
    {
        return &basic_types[FLOAT];
    }
    // Otherwise, the integral promotions are performed on both operands.	
    // promote to INT if one of them or both is/are <= INT
    type1 = (type1->categ < INT) ? &basic_types[INT] : type1;
    type2 = (type2->categ < INT) ? &basic_types[INT] : type2;

    // If either operand has type unsigned long int, the other operand is converted to unsigned long int.
    if (type1->categ == ULONG || type2->categ == ULONG)
    {
        return &basic_types[ULONG];
    }
    /*Otherwise, if one operand has type long int and the other has type unsigned int,
    and a long int cannot represent all the values of an unsigned int, 
    both operands are converted to unsigned long int.  */
    if ((type1->categ == LONG && type2->categ == UINT) || (type1->categ == UINT && type2->categ == LONG))
    {
        return &basic_types[ULONG];
    }
    // Otherwise, if either operand has type long int, the other operand is converted to long int.
    if (type1->categ == LONG || type2->categ == LONG)
    {
        return &basic_types[LONG];
    }
    // Otherwise, if either operand has type unsigned int, the other operand is converted to unsigned int. 
    if (type1->categ == UINT || type2->categ == UINT)
    {
        return &basic_types[UINT];
    }
    // Otherwise, both operands have type int.
    return &basic_types[INT];
}
/*
Array expressions and function 
designators as arguments are converted to pointers before the call.  A
declaration of a parameter as "array of type" shall be adjusted to
"pointer to type", and a declaration of a parameter as "function
returning type" shall be adjusted to "pointer to function returning type"
*/
struct type *adjust_parameter(struct type *para_type)
{
    struct type *pty;
    para_type = unqualify(para_type);
    if (para_type->categ == ARRAY || para_type->categ == FUNCTION)
    {
        ALLOC(pty, struct type);
        pty->categ = POINTER;
        pty->size = basic_types[POINTER].size;
        pty->qualer = basic_types[POINTER].qualer;
        pty->align = basic_types[POINTER].align;
        pty->basic_type = (para_type->categ == ARRAY) ? para_type->basic_type : para_type;
        return pty;
    }
    return para_type;
}


void type_system_init()
{
    ALLOC(default_func_type, struct func_type);
    // create default_func_type: int f()
    default_func_type->basic_type = &basic_types[INT];
    default_func_type->categ = FUNCTION;
    default_func_type->align = default_func_type->size = basic_types[POINTER].size;
    default_func_type->params = vector_init(0);
}
