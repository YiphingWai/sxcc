#ifndef __OPT_H__
#define __OPT_H__



#define EXECUTE_BOP_OPT(type, val1, op, val2, rslt) \
    if (type->categ == INT) {rslt.i = val1.i op val2.i;} \
    else if (type->categ == UINT) {rslt.ui = val1.ui op val2.ui;} \
    else if (type->categ == LONG) {rslt.l = val1.l op val2.l;} \
    else if (type->categ == ULONG) {rslt.ul = val1.ul op val2.ul;} \
    else if (type->categ == FLOAT) {rslt.f = val1.f op val2.f;} \
    else if (type->categ == DOUBLE) {rslt.d = val1.d op val2.d;} \
    else {assert(0);}

#define EXECUTE_ROP_OPT(optype, val1, op, val2, rslt) \
    if (optype->categ == INT) {rslt.i = val1.i op val2.i;} \
    else if (optype->categ == UINT) {rslt.i = val1.ui op val2.ui;} \
    else if (optype->categ == LONG) {rslt.i = val1.l op val2.l;} \
    else if (optype->categ == ULONG) {rslt.i = val1.ul op val2.ul;} \
    else if (optype->categ == FLOAT) {rslt.i = val1.f op val2.f;} \
    else if (optype->categ == DOUBLE) {rslt.i = val1.d op val2.d;} \
    else if (optype->categ == POINTER) {rslt.i = val1.i op val2.i;} \
    else {assert(0);}

#define EXECUTE_SOP_OPT(type, op, val, rslt) \
    if (type->categ == INT) {rslt.i = op(val.i);} \
    else if (type->categ == UINT) {rslt.ui = op(val.ui);} \
    else if (type->categ == LONG) {rslt.l = op(val.l);} \
    else if (type->categ == ULONG) {rslt.ul = op(val.ul);} \
    else if (type->categ == FLOAT) {rslt.f = op(val.f);} \
    else if (type->categ == DOUBLE) {rslt.d = op(val.d);} \
    else {assert(0);}
#define EXECUTE_ARITH_SOP_OPT(type, op, val, rslt) \
    if (type->categ == INT) {rslt.i = op(val.i);} \
    else if (type->categ == UINT) {rslt.ui = op(val.ui);} \
    else if (type->categ == LONG) {rslt.l = op(val.l);} \
    else if (type->categ == ULONG) {rslt.ul = op(val.ul);}\
    else {assert(0);}
#define EXECUTE_ARITH_BOP_OPT(type, val1, op, val2, val) \
    if (type->categ == INT) {val.i = val1.i op val2.i;} \
    else if (type->categ == UINT) {val.ui = val1.ui op val2.ui;} \
    else if (type->categ == LONG) {val.l = val1.l op val2.l;} \
    else if (type->categ == ULONG) {val.ul = val1.ul op val2.ul;} \
    else {assert(0);}
#define IS_EQUAL(type, val, num) \
    ((type->categ == INT) ? (val.i == num) : (type->categ == UINT) ? (val.ui == num) :\
    (type->categ == LONG) ? (val.l == num) : (type->categ == ULONG) ? (val.ul == num) :\
    (type->categ == FLOAT) ? (val.f == num) : (val.d == num))

void *find_item_from_sets(struct hash_table *sets, void *item);










#endif
