#ifndef IRINFO
#error "You must define IRINFO macro before include this file"
#endif

IRINFO(EQ,      "EQ")       // ==
IRINFO(NEQ,     "NEQ")      // !=
IRINFO(LS,      "LS")       // <
IRINFO(GT,      "GT")       // >
IRINFO(LE,      "LE")       // <=
IRINFO(GE,      "GE")       // >=
IRINFO(ADD,     "ADD")      // +
IRINFO(SUB,     "SUB")      // -
IRINFO(MUL,     "MUL")      // *
IRINFO(DIV,     "DIV")      // /
IRINFO(MOD,     "MOD")      // %
IRINFO(SHR,     "SHR")      // >>
IRINFO(SHL,     "SHL")      // <<
IRINFO(BAND,    "BAND")     // &
IRINFO(BOR,     "BOR")      // |
IRINFO(XOR,     "XOR")      // ^
IRINFO(ASSIGN,  "ASSIGN")   // =
IRINFO(NOT,     "NOT")      // !
IRINFO(COMP,    "COMP")     // ~
IRINFO(NEG,     "NEG")      // -
IRINFO(GETADDR, "GETADDR")     // &
IRINFO(LOAD,    "LOAD")     // *
IRINFO(STORE,   "STORE")     // *
IRINFO(DEC,     "DEC")      // --
IRINFO(INC,     "INC")      // ++
//IRINFO(RETURN,  "RETURN")
IRINFO(LABEL,   "LABEL")
IRINFO(RET,     "RET")
IRINFO(CALL,    "CALL")
IRINFO(JT,      "JT")
IRINFO(JE,      "JE")
IRINFO(JNT,     "JNT")
IRINFO(JNE,     "JNE")
IRINFO(JMP,     "JMP")
IRINFO(CAST_I1_I4,    "CAST_I1_I4")
IRINFO(CAST_U1_I4,    "CAST_U1_I4")
IRINFO(CAST_I2_I4,    "CAST_I2_I4")
IRINFO(CAST_U2_I4,    "CAST_U2_I4")
IRINFO(CAST_I1_U1,    "CAST_I1_U1")
IRINFO(CAST_U1_I1,    "CAST_U1_I1")
IRINFO(CAST_I2_U2,    "CAST_I2_U2")
IRINFO(CAST_U2_I2,    "CAST_U2_I2")
IRINFO(CAST_I4_U4,    "CAST_I4_U4")
IRINFO(CAST_U4_I4,    "CAST_U4_I4")
IRINFO(CAST_I4_F4,    "CAST_I4_F4")
IRINFO(CAST_I4_F8,    "CAST_I4_F8")
IRINFO(CAST_U4_F4,    "CAST_U4_F4")
IRINFO(CAST_U4_F8,    "CAST_U4_F8")
IRINFO(CAST_F4_I4,    "CAST_F4_I4")
IRINFO(CAST_F4_U4,    "CAST_F4_U4")
IRINFO(CAST_F4_F8,    "CAST_F4_F8")
IRINFO(CAST_F8_I4,    "CAST_F8_I4")
IRINFO(CAST_F8_U4,    "CAST_F8_U4")
IRINFO(CAST_F8_F4,    "CAST_F8_F4")
IRINFO(CAST_I4_U1,    "CAST_I4_U1")
IRINFO(CAST_I4_I1,    "CAST_I4_I1")
IRINFO(CAST_I4_I2,    "CAST_I4_I2")
IRINFO(CAST_I4_U2,    "CAST_I4_U2")





