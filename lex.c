#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "lex.h"
#include "hash.h"
#include "sxcc.h"

#define MOVE_FORWARD() do {cur++;curr_loc.offset++;}while(0)

static char *pfile = NULL;
static int file_len = 0;
static int cur = 0;
static struct hash_table *ht;
static int tok_stack_top = 0;
static struct token *tok_stack[2];

static struct token_tab tok_name[]=
{
    #define TOKEN(k,s) k,s,
        #include "token.h"
    #undef TOKEN
};

static struct location curr_loc;

enum states
{
    IDLE, _NUM, HEX, OCT, _DEC, INT, FLO, STR, KW, 
};

char *tok_type2name(enum token_type type)
{
    return tok_name[type].kw;
}

int lex_init(char *p, int len)
{
    int i;

    pfile = p;
    file_len = len;
    cur = 0;
    curr_loc.line = 0;
    curr_loc.offset = 0;

    if (!(ht = hash_init()))
    {
        rep_error("hash init err!\n");
        exit(-1);
    }
    for (i = 0; i <= TK_SIZEOF; i++)
    {
        ht->insert(ht, tok_name[i].kw, &tok_name[i]);
    }

    return 0;
}

int lex_destory()
{
    return hash_destory(ht);
}

void lex_push(struct token *tok)
{
    if (!tok)
    {
        rep_error("can not push NULL back!\n");
        exit(-1);
    }
    assert(tok->type);
    tok_stack[tok_stack_top++] = tok;
}

char escape_char()
{
    char rslt;
    char *p;
    assert(pfile[cur] == '\\');

    MOVE_FORWARD();
    if (pfile[cur] == '\n')
    {
        MOVE_FORWARD();
    }
    else if (pfile[cur] == 'a')
    {
        rslt = 7;
        MOVE_FORWARD();
    }
    else if (pfile[cur] == 'b')
    {
        rslt = 8;
        MOVE_FORWARD();
    }
    else if (pfile[cur] == 'f')
    {
        rslt = 12;
        MOVE_FORWARD();
    }
    else if (pfile[cur] == 'n')
    {
        rslt = 10;
        MOVE_FORWARD();
    }
    else if (pfile[cur] == 'r')
    {
        rslt = 13;
        MOVE_FORWARD();
    }
    else if (pfile[cur] == 't')
    {
        rslt = 9;
        MOVE_FORWARD();
    }
    else if (pfile[cur] == 'v')
    {
        rslt = 11;
        MOVE_FORWARD();
    }
    else if (pfile[cur] == '\\')
    {
        rslt = '\\';
        MOVE_FORWARD();
    }
    else if (pfile[cur] == '\'')
    {
        rslt = '\'';
        MOVE_FORWARD();
    }
    else if (pfile[cur] == '\"')
    {
        rslt = '\"';
        MOVE_FORWARD();
    }
    else if (pfile[cur] == '0')
    {
        rslt = 0;
        MOVE_FORWARD();
    }
    else if (pfile[cur] == 'x')
    {
        MOVE_FORWARD();
        rslt = (unsigned char)strtol(&pfile[cur], &p, 16);
        if ((p == NULL) || (p - pfile == cur))
        {
            lex_error("string format err! expect number in 16\n");
            return NULL;
        }
        cur = p - pfile;
    }
    else if (IS_OCT_DIGIT(pfile[cur]))
    {
        rslt = (unsigned char)strtol(&pfile[cur], &p, 8);
        if ((p == NULL) || (p - pfile == cur))
        {
            lex_error("string format err! expect number in 8\n");
            return NULL;
        }
        cur = p - pfile;
    }
    else
    {
        lex_error("string format err! unknown escape character!\n");
        return NULL;
    }
    return rslt;
}

struct token *lex_get_token()
{
    int str_ptr = 0;
    int loop = 1;
    char c;
    char *p;
    struct token *tok;
    struct token_tab *pkw;
    enum states state = IDLE;
    if (!pfile)
    {
        rep_error("You must setup scanner first!\n");
        exit(-1);
    }
    if (tok_stack_top)
    {
        tok = tok_stack[--tok_stack_top];
        goto out;
    }
    ALLOC(tok, struct token);
    ALLOC(tok->loc, struct location);
    
    while(loop)
    {
       switch (state)
       {
            case IDLE:
                if (cur >= file_len)
                {
                    tok->type = TK_END;
                    goto out;
                }
                *tok->loc = curr_loc;
                if (IS_DIGIT(pfile[cur]))
                {
                    state = _NUM;
                    tok->token_strings = pfile + cur;
                }
                else if (pfile[cur] == '"')
                {
                    state = STR;
                    MOVE_FORWARD();
                    tok->type = TK_STRING;
                    tok->token_strings = (char*)calloc(1, 1024);
                    if (!tok->token_strings)
                    {
                        rep_error("calloc err\n");
                        exit(-1);
                    }
                    str_ptr = 0;
                }
                else if (pfile[cur] == ',')
                {
                    tok->type = TK_COMMA;
                    MOVE_FORWARD();
                    loop = 0;
                }
                else if (pfile[cur] == '?')
                {
                    tok->type = TK_QUESTION;
                    MOVE_FORWARD();
                    loop = 0;
                }
                else if (pfile[cur] == ':')
                {
                    tok->type = TK_COLON;
                    MOVE_FORWARD();
                    loop = 0;
                }
                else if (pfile[cur] == '=')
                {
                    MOVE_FORWARD();
                    if (pfile[cur] == '=')
                    {
                        tok->type = TK_EQUAL;
                        MOVE_FORWARD();
                    }
                    else
                    {
                        tok->type = TK_ASSIGN;
                    }
                    loop = 0;
                }
                else if (pfile[cur] == '|')
                {
                    MOVE_FORWARD();
                    if (pfile[cur] == '|')
                    {
                        tok->type = TK_OR;
                        MOVE_FORWARD();
                    }
                    else if (pfile[cur] == '=')
                    {
                        tok->type = TK_BITOR_ASSIGN;
                        MOVE_FORWARD();
                    }
                    else
                    {
                        tok->type = TK_BITOR;
                    }
                    loop = 0;
                }
                else if (pfile[cur] == '^')
                {
                    MOVE_FORWARD();
                    if (pfile[cur] == '=')
                    {
                        tok->type = TK_BITXOR_ASSIGN;
                        MOVE_FORWARD();
                    }
                    else 
                    {
                        tok->type = TK_BITXOR;
                    }
                    loop = 0;
                }
                else if (pfile[cur] == '&')
                {
                    MOVE_FORWARD();
                    if (pfile[cur] == '&')
                    {
                        tok->type = TK_AND;
                        MOVE_FORWARD();
                    }
                    else if (pfile[cur] == '=')
                    {
                        tok->type = TK_BITAND_ASSIGN;
                        MOVE_FORWARD();
                    }
                    else
                    {
                        tok->type = TK_BITAND;
                    }
                    loop = 0;
                }
                else if (pfile[cur] == '<')
                {
                    MOVE_FORWARD();
                    if (pfile[cur] == '<')
                    {
                        MOVE_FORWARD();
                        if (pfile[cur] == '=')
                        {
                            tok->type = TK_LSHIFT_ASSIGN;
                        }
                        else
                        {
                            tok->type = TK_LSHIFT;
                        }
                    }
                    else if (pfile[cur] == '=')
                    {
                        MOVE_FORWARD();
                        tok->type = TK_LESS_EQ;
                    }
                    else 
                    {
                        tok->type = TK_LESS;
                    }
                    loop = 0;
                }
                else if (pfile[cur] == '>')
                {
                    MOVE_FORWARD();
                    if (pfile[cur] == '>')
                    {
                        MOVE_FORWARD();
                        if (pfile[cur] == '=')
                        {
                            tok->type = TK_RSHIFT_ASSIGN;
                        }
                        else
                        {
                            tok->type = TK_RSHIFT;
                        }
                    }
                    else if (pfile[cur] == '=')
                    {
                        MOVE_FORWARD();
                        tok->type = TK_GREAT_EQ;
                    }
                    else 
                    {
                        tok->type = TK_GREAT;
                    }
                    loop = 0;
                }
                else if (pfile[cur] == '+')
                {
                    MOVE_FORWARD();
                    if (pfile[cur] == '+')
                    {
                        MOVE_FORWARD();
                        tok->type = TK_INC;
                    }
                    else if (pfile[cur] == '=')
                    {
                        MOVE_FORWARD();
                        tok->type = TK_ADD_ASSIGN;
                    }
                    else 
                    {
                        tok->type = TK_ADD;
                    }
                    loop = 0;
                }
                else if (pfile[cur] == '-')
                {
                    MOVE_FORWARD();
                    if (pfile[cur] == '-')
                    {
                        MOVE_FORWARD();
                        tok->type = TK_DEC;
                    }
                    else if (pfile[cur] == '=')
                    {
                        MOVE_FORWARD();
                        tok->type = TK_SUB_ASSIGN;
                    }
                    else if (pfile[cur] == '>')
                    {
                        MOVE_FORWARD();
                        tok->type = TK_POINTER;
                    }
                    else 
                    {
                        tok->type = TK_SUB;
                    }
                    loop = 0;
                }
                else if (pfile[cur] == '*')
                {
                    MOVE_FORWARD();
                    if (pfile[cur] == '=')
                    {
                        MOVE_FORWARD();
                        tok->type = TK_MUL_ASSIGN;
                    }
                    else 
                    {
                        tok->type = TK_MUL;
                    }
                    loop = 0;
                }
                else if (pfile[cur] == '/')
                {
                    MOVE_FORWARD();
                    if (pfile[cur] == '=')
                    {
                        MOVE_FORWARD();
                        tok->type = TK_DIV_ASSIGN;
                    }
                    else 
                    {
                        tok->type = TK_DIV;
                    }
                    loop = 0;
                }
                else if (pfile[cur] == '%')
                {
                    MOVE_FORWARD();
                    if (pfile[cur] == '=')
                    {
                        MOVE_FORWARD();
                        tok->type = TK_MOD_ASSIGN;
                    }
                    else 
                    {
                        tok->type = TK_MOD;
                    }
                    loop = 0;
                }
                else if (pfile[cur] == '!')
                {
                    MOVE_FORWARD();
                    if (pfile[cur] == '=')
                    {
                        MOVE_FORWARD();
                        tok->type = TK_UNEQUAL;
                    }
                    else 
                    {
                        tok->type = TK_NOT;
                    }
                    loop = 0;
                }
                else if (pfile[cur] == '~')
                {
                    MOVE_FORWARD();
                    tok->type = TK_COMP;
                    loop = 0;
                }
                else if (pfile[cur] == '(')
                {
                    MOVE_FORWARD();
                    tok->type = TK_LPAREN;
                    loop = 0;
                }
                else if (pfile[cur] == ')')
                {
                    MOVE_FORWARD();
                    tok->type = TK_RPAREN;
                    loop = 0;
                }
                else if (pfile[cur] == '[')
                {
                    MOVE_FORWARD();
                    tok->type = TK_LBRACKET;
                    loop = 0;
                }
                else if (pfile[cur] == ']')
                {
                    MOVE_FORWARD();
                    tok->type = TK_RBRACKET;
                    loop = 0;
                }
                else if (pfile[cur] == '{')
                {
                    MOVE_FORWARD();
                    tok->type = TK_LBRACE;
                    loop = 0;
                }
                else if (pfile[cur] == '}')
                {
                    MOVE_FORWARD();
                    tok->type = TK_RBRACE;
                    loop = 0;
                }
                else if (pfile[cur] == ';')
                {
                    MOVE_FORWARD();
                    tok->type = TK_SEMICOLON;
                    loop = 0;
                }
                else if (pfile[cur] == '#')
                {
                    MOVE_FORWARD();
                    MOVE_FORWARD();
                    assert(IS_DIGIT(pfile[cur]));
                    curr_loc.line = strtol(&pfile[cur], NULL, 10);
                    for (; IS_DIGIT(pfile[cur]); cur++);
                    assert(pfile[cur++] == ' ');
                    assert(pfile[cur++] == '\"');
                    curr_loc.files = &pfile[cur];
                    for (; pfile[cur] != '\"'; cur++);
                    pfile[cur] = '\0';
                    for (; pfile[cur] != '\n'; cur++);
                    cur++;
                    curr_loc.offset = 0;
                }
                else if (pfile[cur] == '\n')
                {
                    cur++;
                    curr_loc.offset = 0;
                    curr_loc.line++;
                }
                else if (pfile[cur] == ' ')
                {
                    MOVE_FORWARD();
                }
                else if (pfile[cur] == '\t')
                {
                    MOVE_FORWARD();
                }
                else if (pfile[cur] == '.')
                {
                    MOVE_FORWARD();
                    if (pfile[cur] == '.' && pfile[cur + 1] == '.')
                    {
                        cur+=2;
                        tok->type = TK_ELLIPSIS;
                    }
                    else 
                    {
                        tok->type = TK_DOT;
                    }
                    loop = 0;
                }
                else if (pfile[cur] == '\'')
                {
                    MOVE_FORWARD();
                    if (pfile[cur] == '\\')
                    {
                        tok->value.i = escape_char();
                    }
                    else
                    {
                        tok->value.i = pfile[cur];
                        MOVE_FORWARD();
                    }
                    if (pfile[cur] != '\'')
                    {
                        lex_error("string format err! expect '''\n");
                        exit(1);
                    }
                    MOVE_FORWARD();
                    tok->type = TK_INTCONST;
                    loop = 0;
                }
                else if (IS_LETTER(pfile[cur]))
                {
                    state = KW;
                    tok->token_strings = pfile + cur;
                }
            break;
            
            case KW:
                if (IS_LETTER_OR_DIGIT(pfile[cur]))
                {
                    MOVE_FORWARD();
                }
                else
                {
                    c = pfile[cur];
                    pfile[cur] = '\0';
                    if (ht->find(ht, tok->token_strings, &pkw))
                    {
                        tok->type = TK_ID;
                        p = (char *)calloc(1, strlen(tok->token_strings) + 1);
                        if (!p)
                        {
                            rep_error("calloc fail!\n");
                            return -1;
                        }
                        strcpy(p, tok->token_strings);
                        tok->token_strings = p;
                    }
                    else
                    {
                        tok->type = pkw->type;
                    }
                    pfile[cur] = c;
                    loop = 0;
                }
            break;

            case STR:
                if (pfile[cur] == '\\')
                {
                    tok->token_strings[str_ptr++] = escape_char();
                }
                else if (pfile[cur] == '\"')
                {
                    MOVE_FORWARD();
                    loop = 0;
                }
                else
                {
                    if (str_ptr % 1024 == 1023)
                    {
                        tok->token_strings = (char *)realloc(tok->token_strings, str_ptr + 1025);
                        memset(tok->token_strings + str_ptr, 0, 1025);
                    }
                    tok->token_strings[str_ptr++] = pfile[cur];
                    MOVE_FORWARD();
                }
            break;

            case _NUM:
                if (pfile[cur] == '0')
                {
                    if ((pfile[cur + 1] == 'x') || (pfile[cur + 1] == 'X'))
                    {
                        state = HEX;
                        cur += 2;
                    }
                    else if (IS_OCT_DIGIT(pfile[cur + 1]))
                    {
                        state = OCT;
                        cur += 1;
                    }
                    else
                    {
                        state = _DEC;
                    }
                }
                else
                {
                    state = _DEC;
                }
            break;

            case OCT:
                if (IS_OCT_DIGIT(pfile[cur]))
                {
                    MOVE_FORWARD();
                }
                else if (pfile[cur] == 'L' || pfile[cur] == 'l')
                {
                    MOVE_FORWARD();
                    if(pfile[cur] == 'U' || pfile[cur] == 'u')
                    {
                        tok->type = TK_ULONGCONST;
                        tok->value.ul = strtoul(tok->token_strings, NULL, 8);
                        MOVE_FORWARD();
                    }
                    else
                    {
                        tok->type = TK_LONGCONST;
                        tok->value.l = strtol(tok->token_strings, NULL, 8);
                    }
                    loop = 0;
                }
                else if (pfile[cur] == 'U' || pfile[cur] == 'u')
                {
                    MOVE_FORWARD();
                    if(pfile[cur] == 'L' || pfile[cur] == 'l')
                    {
                        tok->type = TK_ULONGCONST;
                        tok->value.ul = strtoul(tok->token_strings, NULL, 8);
                        MOVE_FORWARD();
                    }
                    else
                    {
                        tok->type = TK_UINTCONST;
                        tok->value.l = strtol(tok->token_strings, NULL, 8);
                    }
                    loop = 0;
                }
                else
                {
                    tok->type = TK_INTCONST;
                    tok->value.i = strtol(tok->token_strings, NULL, 8);
                    loop = 0;
                }
            break;

            case HEX:
                if (IS_HEX_DIGIT(pfile[cur]))
                {
                    MOVE_FORWARD();
                }
                else if (pfile[cur] == 'L' || pfile[cur] == 'l')
                {
                    MOVE_FORWARD();
                    if(pfile[cur] == 'U' || pfile[cur] == 'u')
                    {
                        tok->type = TK_ULONGCONST;
                        tok->value.ul = strtoul(tok->token_strings, NULL, 16);
                        MOVE_FORWARD();
                    }
                    else
                    {
                        tok->type = TK_LONGCONST;
                        tok->value.l = strtol(tok->token_strings, NULL, 16);
                    }
                    loop = 0;
                }
                else if (pfile[cur] == 'U' || pfile[cur] == 'u')
                {
                    MOVE_FORWARD();
                    if(pfile[cur] == 'L' || pfile[cur] == 'l')
                    {
                        tok->type = TK_ULONGCONST;
                        tok->value.ul = strtoul(tok->token_strings, NULL, 16);
                        MOVE_FORWARD();
                    }
                    else
                    {
                        tok->type = TK_UINTCONST;
                        tok->value.l = strtoul(tok->token_strings, NULL, 16);
                    }
                    loop = 0;
                }
                else
                {
                    tok->type = TK_INTCONST;
                    tok->value.i = strtol(tok->token_strings, NULL, 16);
                    loop = 0;
                }
            break;

            case _DEC:
                if (IS_DIGIT(pfile[cur]))
                {
                    MOVE_FORWARD();
                }
                else if (pfile[cur] == '.')
                {
                    state = FLO;
                    MOVE_FORWARD();
                }
                else if (pfile[cur] == 'L' || pfile[cur] == 'l')
                {
                    MOVE_FORWARD();
                    if (pfile[cur] == 'U' || pfile[cur] == 'u')
                    {
                        tok->type = TK_ULONGCONST;
                        tok->value.ul = strtoul(tok->token_strings, NULL, 10);
                        MOVE_FORWARD();
                    }
                    else
                    {
                        tok->type = TK_LONGCONST;
                        tok->value.ul = strtol(tok->token_strings, NULL, 10);
                    }
                    loop = 0;
                }
                else if (pfile[cur] == 'U' || pfile[cur] == 'u')
                {
                    MOVE_FORWARD();
                    if(pfile[cur] == 'L' || pfile[cur] == 'l')
                    {
                        tok->type = TK_ULONGCONST;
                        tok->value.ul = strtoul(tok->token_strings, NULL, 10);
                        MOVE_FORWARD();
                    }
                    else
                    {
                        tok->type = TK_UINTCONST;
                        tok->value.l = strtoul(tok->token_strings, NULL, 10);
                    }
                    loop = 0;
                }
                else
                {
                    tok->type = TK_INTCONST;
                    tok->value.i = strtol(tok->token_strings, NULL, 10);
                    loop = 0;
                }
            break;

            case FLO:   //still need to be added
                if (IS_DIGIT(pfile[cur]))
                {
                    MOVE_FORWARD();
                }
                else if (pfile[cur] == 'F' || pfile[cur] == 'f')
                {
                    tok->type = TK_FLOATCONST;
                    tok->value.f = strtof(tok->token_strings, NULL);
                    MOVE_FORWARD();
                    loop = 0;
                }
                else if (pfile[cur] == 'L' || pfile[cur] == 'l')
                {
                    tok->type = TK_DOUBLECONST;
                    tok->value.d = strtod(tok->token_strings, NULL);
                    MOVE_FORWARD();
                    loop = 0;
                }
                else
                {
                    tok->type = TK_DOUBLECONST;
                    tok->value.d = strtod(tok->token_strings, NULL);
                    loop = 0;
                }
                
            break;
       }
    }
out:
    assert(tok->type != 0);
    return tok;
}

