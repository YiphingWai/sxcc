#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include "tran.h"
#include "x86_reg.h"
#include "x86_emit.h"
#include "type.h"
#include "hash.h"
#include "symtab.h"
#include "sxcc.h"
#include "opt.h"

struct symbol_reg regs[18];

static const char reg_rely[18][5]=
{
	{AL,AX,AH,-1,-1},   // the regs which EAX relies on
	{BL,BX,BH,-1,-1},
	{CL,CX,CH,-1,-1},
	{DL,DX,DH,-1,-1},

	{AL,EAX,AH,-1,-1},
	{BL,EBX,BH,-1,-1},
	{CL,ECX,CH,-1,-1},
	{DL,EDX,DH,-1,-1},

	{EAX,AX,-1,-1,-1},
	{EBX,BX,-1,-1,-1},
	{ECX,CX,-1,-1,-1},
	{EDX,DX,-1,-1,-1},

	{EAX,AX,-1,-1,-1},
	{EBX,BX,-1,-1,-1},
	{ECX,CX,-1,-1,-1},
	{EDX,DX,-1,-1,-1},

	{-1,-1,-1,-1,-1},
	{-1,-1,-1,-1,-1}
};
const char reg_to_size[] = {4,4,4,4,2,2,2,2,1,1,1,1,1,1,1,1,4,4};

static char size_to_reg[3][8] =
{
	{AL,BL,CL,DL,AH,BH,CH,DH},
	{AX,BX,CX,DX,-1,-1,-1,-1},
	{EAX,EBX,ECX,EDX,EDI,ESI,-1,-1}
};

void *find_items_from_array(struct vector *vec, void *item)
{
	int i;
	void *p;
	for (i = 0; i < vec->len; i++)
	{
		if ((p = vec->get(vec, i)) == item)
		{
			return p;
		}
	}
	return NULL;
}

void register_reg_use(enum reg reg, int reserve, struct symbol_id *sym)
{
	int i, r;
	for (i = 0; (r = reg_rely[reg][i]) != -1; i++)
	{
		regs[r].used = 1;
		regs[r].link = &regs[reg];
		regs[r].user = sym;
		regs[r].need_wb = 0;
		regs[r].ref_cnt = sym ? sym->def_cnt + sym->ref_cnt : 0;
		regs[r].is_reserved = reserve;
	}
	regs[reg].used = 1;
	regs[reg].link = NULL;
	regs[reg].user = sym;
	regs[reg].need_wb = 0;
	regs[reg].ref_cnt = sym ? sym->def_cnt + sym->ref_cnt : 0;;
	regs[reg].is_reserved = reserve;

	if (sym)
	{
		((struct symbol_id *)sym)->reg = &regs[reg];
	}
}

static enum reg select_and_spill_reg(int size)
{
	int i, r, selected, min_ref = 0x7fffffff;

	for (i = 0; (r = size_to_reg[size][i]) != -1; i++)
	{
		if (!regs[r].is_reserved && min_ref > regs[r].ref_cnt)
		{
			min_ref = regs[r].ref_cnt;
			selected = r;
		}
	}
	if (min_ref == 0x7fffffff)
	{
		rep_error("there has no reg can be allocated");
		assert(0);
	}
	release_reg(&regs[selected]);
	return selected;
}

void release_reg(struct symbol_reg *reg)
{
	int i, r, dec;
	assert(reg->kind == SK_Register);
	dec = reg->dec;
	
	if (!reg->used)
	{
		return;
	}

	for (; reg->link; reg = reg->link);

	if (reg->need_wb && reg->user)
	{
		emit("mov [%s], %s\n", reg->user->aname, reg->aname);
	}
	dec = reg->dec;

	for (i = 0; (r = reg_rely[dec][i]) != -1; i++)
	{
		regs[r].used = 0;
		regs[r].link = NULL;
		regs[r].user = NULL;
		regs[r].need_wb = 0;
		regs[r].ref_cnt = 0;
		regs[r].is_reserved = 0;
	}
	if (regs[dec].user)
	{
		regs[dec].user->reg = NULL;
	}
	regs[dec].used = 0;
	regs[dec].link = NULL;
	regs[dec].user = NULL;
	regs[dec].need_wb = 0;
	regs[dec].ref_cnt = 0;
	regs[dec].is_reserved = 0;
}

void write_back_reg(struct symbol_reg *reg)
{
	int i, r, dec;
	assert(reg->kind == SK_Register);
	dec = reg->dec;
	
	if (!reg->used)
	{
		return;
	}

	for (; reg->link; reg = reg->link);

	if (reg->need_wb && reg->user)
	{
		emit("mov [%s], %s\n", reg->user->aname, reg->aname);
	}
	dec = reg->dec;

	for (i = 0; (r = reg_rely[dec][i]) != -1; i++)
	{
		regs[r].need_wb = 0;
	}
	regs[dec].need_wb = 0;
}


struct symbol *alloc_reg(struct symbol_id *sym, int reg_size, int target, int reserve)
{
	int i, r;
	int size;
	enum reg reg;
	assert(target != -1 || reg_size == 1 || reg_size == 2 || reg_size == 4);
	// only pseudo_temps can reserve a reg
	
    size = reg_size / 2;
	if (target == -1)
	{
		for (i = 0; (r = size_to_reg[size][i]) != -1; i++)
		{
			if (!regs[r].used && !regs[r].is_reserved)
			{
				register_reg_use(r, reserve, sym);
				return &regs[r];
			}
		}
		// reach here means no available reg to distribute
		reg = select_and_spill_reg(size);
		register_reg_use(reg, reserve, sym);
		return &regs[reg];
	}
	else
	{
		if (regs[target].is_reserved)
		{
			rep_error("the target reg has been reserved");
			exit(-1);
		}
		
		if (!regs[target].used)
		{
			register_reg_use(target, reserve, sym);
			return &regs[target];
		}

		release_reg(&regs[target]);
		register_reg_use(target, reserve, sym);
		return &regs[target];
	}
}

struct symbol_reg *reserve_reg(struct symbol_id *sym)
{
	int i, r, dec;

	assert(sym->reg);
	assert(sym->reg->used != NULL);
	assert(sym->reg->link == NULL);
	assert(sym->reg->user == sym);
	dec = sym->reg->dec;

	for (i = 0; (r = reg_rely[dec][i]) != -1; i++)
	{
		regs[r].is_reserved = 1;
	}

	regs[dec].is_reserved = 1;
	return &regs[dec];
}

void unreserve_reg(struct symbol_reg *reg)
{
	int i, r, dec;
	assert(reg->kind == SK_Register);
//	assert(sym->reg);
	assert(reg->used);
	assert(reg->link == NULL);
	dec = reg->dec;

	for (i = 0; (r = reg_rely[dec][i]) != -1; i++)
	{
		regs[r].is_reserved = 0;
	}

	regs[dec].is_reserved = 0;
	return &regs[dec];
}

// change user of reg but don't write back the value of reg
void reg_change_user(struct symbol_reg *reg, struct symbol_id *new_user)
{
	int i, r, dec;
	struct symbol_id *old_user;
	assert(reg->used != NULL);
	assert(reg->link == NULL);
//	assert(reg->user);

	old_user = reg->user;
	if (old_user == new_user)
	{
		return;
	}
	
	assert(!new_user->reg);
	dec = reg->dec;

	for (i = 0; (r = reg_rely[dec][i]) != -1; i++)
	{
		assert(regs[r].user == old_user);
		regs[r].user = new_user;
	}
	assert(regs[dec].user == old_user);
	regs[dec].user = new_user;
	new_user->reg = &regs[dec];
	if (old_user)
	{
		old_user->reg = NULL;
	}
}

void just_for_debug()
{
	int i;
	for (i = 0; i < 18; i++)
	{
		if (regs[i].is_reserved)
		{
			assert(0);
		}
	}
}


void x86reg_init()
{
	int i;
	
	regs[EAX].name = "eax";
	regs[EBX].name = "ebx";
	regs[ECX].name = "ecx";
	regs[EDX].name = "edx";
	regs[AX].name = "ax";
	regs[BX].name = "bx";
	regs[CX].name = "cx";
	regs[DX].name = "dx";
	regs[AH].name = "ah";
	regs[BH].name = "bh";
	regs[CH].name = "ch";
	regs[DH].name = "dh";
	regs[AL].name = "al";
	regs[BL].name = "bl";
	regs[CL].name = "cl";
	regs[DL].name = "dl";
	regs[ESI].name = "esi";
	regs[EDI].name = "edi";
	for (i = 0; i < 18; i++)
	{
		regs[i].kind = SK_Register;
		regs[i].aname = regs[i].name;
		regs[i].dec = i;
	}
	forbidden_regs = vector_init(0);
}






