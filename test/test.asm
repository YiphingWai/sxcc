; Code auto-generated by SXCC

section .data

@STR2: db 10,0
@STR0: db 'A',58,32,0
@STR1: db 32,37,'d',32,0
@STR6: db 'S','o','l','u','t','i','o','n',32,'o','f',32,'T','o','w','e','r',32,'o','f',32,'H','a','n','o','i',32,'P','r','o','b','l','e','m',32,'w','i','t','h',32,37,'d',32,'D','i','s','k','s',10,10,0
@STR9: db 37,'d',10,0
@STR4: db 'C',58,32,0
@STR8: db 10,10,'S','u','b','s','e','q','u','e','n','t',32,'s','t','a','t','e','s',58,10,10,0
@STR7: db 'S','t','a','r','t','i','n','g',32,'s','t','a','t','e',58,10,0
@STR5: db 45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,45,10,0
@STR3: db 'B',58,32,0
extern __ctype_get_mb_cur_max
extern atof
extern atoi
extern atol
extern strtod
extern strtol
extern strtoul
extern rand
extern srand
extern rand_r
extern malloc
extern calloc
extern realloc
extern free
extern abort
extern atexit
extern exit
extern getenv
extern system
extern bsearch
extern qsort
extern abs
extern labs
extern div
extern ldiv
extern mblen
extern mbtowc
extern wctomb
extern mbstowcs
extern wcstombs
extern _IO_2_1_stdin_
extern _IO_2_1_stdout_
extern _IO_2_1_stderr_
extern __underflow
extern __uflow
extern __overflow
extern _IO_getc
extern _IO_putc
extern _IO_feof
extern _IO_ferror
extern _IO_peekc_locked
extern _IO_flockfile
extern _IO_funlockfile
extern _IO_ftrylockfile
extern _IO_vfscanf
extern _IO_vfprintf
extern _IO_padn
extern _IO_sgetn
extern _IO_seekoff
extern _IO_seekpos
extern _IO_free_backup_area
extern stdin
extern stdout
extern stderr
extern remove
extern rename
extern tmpfile
extern tmpnam
extern fclose
extern fflush
extern fopen
extern freopen
extern fdopen
extern setbuf
extern setvbuf
extern fprintf
extern printf
extern sprintf
extern vfprintf
extern vprintf
extern vsprintf
extern fscanf
extern scanf
extern sscanf
extern fgetc
extern getc
extern getchar
extern getc_unlocked
extern getchar_unlocked
extern fputc
extern putc
extern putchar
extern putc_unlocked
extern putchar_unlocked
extern fgets
extern gets
extern fputs
extern puts
extern ungetc
extern fread
extern fwrite
extern fseek
extern ftell
extern rewind
extern fgetpos
extern fsetpos
extern clearerr
extern feof
extern ferror
extern perror
extern fileno
extern ctermid
extern flockfile
extern ftrylockfile
extern funlockfile
extern acos
extern __acos
extern asin
extern __asin
extern atan
extern __atan
extern atan2
extern __atan2
extern cos
extern __cos
extern sin
extern __sin
extern tan
extern __tan
extern cosh
extern __cosh
extern sinh
extern __sinh
extern tanh
extern __tanh
extern exp
extern __exp
extern frexp
extern __frexp
extern ldexp
extern __ldexp
extern log
extern __log
extern log10
extern __log10
extern modf
extern __modf
extern pow
extern __pow
extern sqrt
extern __sqrt
extern ceil
extern __ceil
extern fabs
extern __fabs
extern floor
extern __floor
extern fmod
extern __fmod
extern __isinf
extern __finite
extern __isnan
extern memcpy
extern memmove
extern memset
extern memcmp
extern memchr
extern strcpy
extern strncpy
extern strcat
extern strncat
extern strcmp
extern strncmp
extern strcoll
extern strxfrm
extern strchr
extern strrchr
extern strcspn
extern strspn
extern strpbrk
extern strstr
extern strtok
extern __strtok_r
extern strtok_r
extern strlen
extern strerror
extern __bzero
align 8
__huge_val: db 0
db 0
db 0
db 0
db 0
db 0
db 240
db 127
section .bss

align 4
global A
A: resb 16
align 4
global B
B: resb 16
align 4
global C
C: resb 16
section .text

global PrintAll
PrintAll:
push ebp
push ebx
push esi
push edi
mov ebp, esp
sub esp, 76
lea eax, [@STR0]

push dword eax
mov [ebp - 8], eax
call printf
add esp, 4
mov [ebp - 76], eax
mov eax, 0
mov [ebp - 4], eax

;---------------------------------
@L2:
mov ebx, [ebp - 4]
mov eax, 1
cmp ebx, 4
jl @L41
mov eax, 0
@L41:

cmp dword eax, 0h
jz @L5

;---------------------------------
@L3:
lea eax, [A]

mov [ebp - 32], eax
mov eax, [ebp - 4]
mov ebx, 4
imul ebx

mov ecx, [ebp - 32]
add ecx, eax

mov eax, [ecx]

mov [ebp - 8], ecx
lea ecx, [@STR1]

push dword eax
push dword ecx
mov [ebp - 20], eax
mov [ebp - 8], ecx
call printf
add esp, 8
mov [ebp - 16], eax
;---------------------------------
@L4:
inc dword [ebp - 4]

jmp @L2
;---------------------------------
@L5:
lea eax, [@STR2]

push dword eax
mov [ebp - 8], eax
call printf
add esp, 4
mov [ebp - 36], eax
lea eax, [@STR3]

push dword eax
mov [ebp - 8], eax
call printf
add esp, 4
mov [ebp - 60], eax
mov eax, 0
mov [ebp - 4], eax

;---------------------------------
@L6:
mov ebx, [ebp - 4]
mov eax, 1
cmp ebx, 4
jl @L42
mov eax, 0
@L42:

cmp dword eax, 0h
jz @L9

;---------------------------------
@L7:
lea eax, [B]

mov [ebp - 64], eax
mov eax, [ebp - 4]
mov ebx, 4
imul ebx

mov ecx, [ebp - 64]
add ecx, eax

mov eax, [ecx]

mov [ebp - 8], ecx
lea ecx, [@STR1]

push dword eax
push dword ecx
mov [ebp - 44], eax
mov [ebp - 8], ecx
call printf
add esp, 8
mov [ebp - 48], eax
;---------------------------------
@L8:
inc dword [ebp - 4]

jmp @L6
;---------------------------------
@L9:
lea eax, [@STR2]

push dword eax
mov [ebp - 8], eax
call printf
add esp, 4
mov [ebp - 40], eax
lea eax, [@STR4]

push dword eax
mov [ebp - 8], eax
call printf
add esp, 4
mov [ebp - 24], eax
mov eax, 0
mov [ebp - 4], eax

;---------------------------------
@L10:
mov ebx, [ebp - 4]
mov eax, 1
cmp ebx, 4
jl @L43
mov eax, 0
@L43:

cmp dword eax, 0h
jz @L13

;---------------------------------
@L11:
lea eax, [C]

mov [ebp - 56], eax
mov eax, [ebp - 4]
mov ebx, 4
imul ebx

mov ecx, [ebp - 56]
add ecx, eax

mov eax, [ecx]

mov [ebp - 8], ecx
lea ecx, [@STR1]

push dword eax
push dword ecx
mov [ebp - 52], eax
mov [ebp - 8], ecx
call printf
add esp, 8
mov [ebp - 68], eax
;---------------------------------
@L12:
inc dword [ebp - 4]

jmp @L10
;---------------------------------
@L13:
lea eax, [@STR2]

push dword eax
mov [ebp - 8], eax
call printf
add esp, 4
mov [ebp - 12], eax
lea eax, [@STR5]

push dword eax
mov [ebp - 8], eax
call printf
add esp, 4
mov [ebp - 72], eax
mov eax, 0
mov [ebp - 28], eax

;---------------------------------
@L1:
mov eax, [ebp - 28]

;---------------------------------
mov esp, ebp
pop edi
pop esi
pop ebx
pop ebp
ret
global Move
Move:
push ebp
push ebx
push esi
push edi
mov ebp, esp
sub esp, 32
mov eax, 0
mov [ebp - 4], eax

mov ebx, 0
mov [ebp - 8], ebx

;---------------------------------
@L16:
mov eax, 0
mov [ebp - 28], eax

mov ecx, [ebp - 4]
mov ebx, 1
cmp ecx, 4
jl @L44
mov ebx, 0
@L44:

cmp dword ebx, 0h
jz @L39

;---------------------------------
mov eax, [ebp - 4]
mov ebx, 4
imul ebx

mov ecx, [ebp + 20]
add ecx, eax

mov eax, [ecx]

mov [ebp - 12], eax
mov ecx, [ebp - 12]
cmp ecx, 0
lahf
and eax, 4000h
shr eax, 14

cmp dword eax, 0h
jz @L39

;---------------------------------
mov eax, 1
mov [ebp - 28], eax

;---------------------------------
@L39:
cmp dword [ebp - 28], 0h
jz @L17

;---------------------------------
inc dword [ebp - 4]

jmp @L16
;---------------------------------
@L17:
;---------------------------------
@L18:
mov eax, 0
mov [ebp - 24], eax

mov ecx, [ebp - 8]
mov ebx, 1
cmp ecx, 4
jl @L45
mov ebx, 0
@L45:

cmp dword ebx, 0h
jz @L40

;---------------------------------
mov eax, [ebp - 8]
mov ebx, 4
imul ebx

mov ecx, [ebp + 24]
add ecx, eax

mov eax, [ecx]

mov [ebp - 12], eax
mov ecx, [ebp - 12]
cmp ecx, 0
lahf
and eax, 4000h
shr eax, 14

cmp dword eax, 0h
jz @L40

;---------------------------------
mov eax, 1
mov [ebp - 24], eax

;---------------------------------
@L40:
cmp dword [ebp - 24], 0h
jz @L19

;---------------------------------
inc dword [ebp - 8]

jmp @L18
;---------------------------------
@L19:
mov eax, [ebp - 8]
sub eax, 1

mov [ebp - 12], eax
mov ebx, 4
imul ebx

mov ecx, [ebp + 24]
add ecx, eax

mov [ebp - 12], eax
mov eax, [ebp - 4]
mov edi, 4
imul edi

mov edx, [ebp + 20]
add edx, eax

mov eax, [edx]

mov [ecx], eax

mov [ebp - 12], eax
mov eax, [ebp - 4]
mov esi, 4
imul esi

mov edx, [ebp + 20]
add edx, eax

mov eax, 0
mov [ebp - 12], edx
mov [edx], eax

call PrintAll
mov [ebp - 20], eax
mov eax, [ebp - 8]
sub eax, 1

mov [ebp - 12], eax
mov ecx, 4
imul ecx

mov edx, [ebp + 24]
add edx, eax

mov eax, [edx]

mov [ebp - 32], eax

;---------------------------------
@L15:
mov eax, [ebp - 32]

;---------------------------------
mov esp, ebp
pop edi
pop esi
pop ebx
pop ebp
ret
global Hanoi
Hanoi:
push ebp
push ebx
push esi
push edi
mov ebp, esp
sub esp, 28
mov ebx, [ebp + 20]
cmp ebx, 1
lahf
and eax, 4000h
shr eax, 14

cmp dword eax, 0h
jz @L22

;---------------------------------
push dword [ebp + 28]
push dword [ebp + 24]
call Move
add esp, 8
mov [ebp - 24], eax
mov eax, 0
mov [ebp - 20], eax

jmp @L21
;---------------------------------
@L22:
mov eax, [ebp + 20]
sub eax, 1

push dword [ebp + 28]
push dword [ebp + 32]
push dword [ebp + 24]
push dword eax
mov [ebp - 8], eax
call Hanoi
add esp, 16
mov [ebp - 12], eax
push dword [ebp + 28]
push dword [ebp + 24]
call Move
add esp, 8
mov [ebp - 16], eax
mov eax, [ebp + 20]
sub eax, 1

push dword [ebp + 24]
push dword [ebp + 28]
push dword [ebp + 32]
push dword eax
mov [ebp - 8], eax
call Hanoi
add esp, 16
mov [ebp - 28], eax
mov eax, 0
mov [ebp - 20], eax

;---------------------------------
@L21:
mov eax, [ebp - 20]

;---------------------------------
mov esp, ebp
pop edi
pop esi
pop ebx
pop ebp
ret
global f
f:
push ebp
push ebx
push esi
push edi
mov ebp, esp
sub esp, 56
mov eax, 0
mov [ebp - 4], eax

;---------------------------------
@L25:
mov ebx, [ebp - 4]
mov eax, 1
cmp ebx, 4
jl @L46
mov eax, 0
@L46:

cmp dword eax, 0h
jz @L28

;---------------------------------
@L26:
lea eax, [A]

mov [ebp - 36], eax
mov eax, [ebp - 4]
mov ebx, 4
imul ebx

mov ecx, [ebp - 36]
add ecx, eax

mov edx, [ebp - 4]
add edx, 1
mov [ebp - 8], eax

mov [ecx], edx

;---------------------------------
@L27:
inc dword [ebp - 4]

jmp @L25
;---------------------------------
@L28:
mov eax, 0
mov [ebp - 4], eax

;---------------------------------
@L29:
mov ebx, [ebp - 4]
mov eax, 1
cmp ebx, 4
jl @L47
mov eax, 0
@L47:

cmp dword eax, 0h
jz @L32

;---------------------------------
@L30:
lea eax, [B]

mov [ebp - 16], eax
mov eax, [ebp - 4]
mov ebx, 4
imul ebx

mov ecx, [ebp - 16]
add ecx, eax

mov eax, 0
mov [ebp - 8], ecx
mov [ecx], eax

;---------------------------------
@L31:
inc dword [ebp - 4]

jmp @L29
;---------------------------------
@L32:
mov eax, 0
mov [ebp - 4], eax

;---------------------------------
@L33:
mov ebx, [ebp - 4]
mov eax, 1
cmp ebx, 4
jl @L48
mov eax, 0
@L48:

cmp dword eax, 0h
jz @L36

;---------------------------------
@L34:
lea eax, [C]

mov [ebp - 56], eax
mov eax, [ebp - 4]
mov ebx, 4
imul ebx

mov ecx, [ebp - 56]
add ecx, eax

mov eax, 0
mov [ebp - 8], ecx
mov [ecx], eax

;---------------------------------
@L35:
inc dword [ebp - 4]

jmp @L33
;---------------------------------
@L36:
lea eax, [@STR6]

push dword 4
push dword eax
mov [ebp - 8], eax
call printf
add esp, 8
mov [ebp - 32], eax
lea eax, [@STR7]

push dword eax
mov [ebp - 8], eax
call printf
add esp, 4
mov [ebp - 12], eax
call PrintAll
mov [ebp - 52], eax
lea eax, [@STR8]

push dword eax
mov [ebp - 8], eax
call printf
add esp, 4
mov [ebp - 20], eax
lea eax, [C]

lea ebx, [B]

lea ecx, [A]

push dword eax
push dword ebx
push dword ecx
push dword 4
mov [ebp - 44], eax
mov [ebp - 8], ecx
call Hanoi
add esp, 16
mov [ebp - 28], eax
mov eax, 0
mov [ebp - 40], eax

;---------------------------------
@L24:
mov eax, [ebp - 40]

;---------------------------------
mov esp, ebp
pop edi
pop esi
pop ebx
pop ebp
ret
global main
main:
push ebp
push ebx
push esi
push edi
mov ebp, esp
sub esp, 16
call f
mov [ebp - 12], eax
lea eax, [@STR9]

push dword [ebp - 12]
push dword eax
mov [ebp - 4], eax
call printf
add esp, 8
mov [ebp - 8], eax
mov eax, 0
mov [ebp - 16], eax

;---------------------------------
@L38:
mov eax, [ebp - 16]

;---------------------------------
mov esp, ebp
pop edi
pop esi
pop ebx
pop ebp
ret
