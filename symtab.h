#ifndef __SYMTAB_H__
#define __SYMTAB_H__
#include "lex.h"
#define MAX_NESTING_LEVEL 15
#define MAX_SYM_KEY_LENTH 256


enum sym_kind
{ 
	SK_Tag,    SK_TypedefName, SK_EnumConstant, SK_Constant, SK_Variable, SK_Temp,
    SK_String, SK_Label,       SK_Function,     SK_Register
};

#define IS_CONST_SYMBOL(sym) (sym->kind == SK_Constant || sym->kind == SK_EnumConstant)


/*
    sto_class_spec_tok: TK_EXTERN / TK_STATIC / TK_TYPEDEF / TK_AUTO / TK_REGISTER
    defined: whether the variable/label/function has been defined
*/
#define SYMBOL_COMMON \
        enum sym_kind kind;\
        char *name;\
        char *aname;\
        struct type *type;\
        int level;\
        int sto_class_spec_tok;\
        int defined;\
        union val value;

#define SYMBOL_ID_COMMON \
        SYMBOL_COMMON\
        struct symbol_reg *reg;\
        int ref_cnt;\
        int def_cnt;\

struct symbol
{
    SYMBOL_COMMON
};

struct symbol_id
{
    SYMBOL_ID_COMMON
};

struct symbol_var
{
    SYMBOL_ID_COMMON

    struct init_data *init_data;
// for optimize and asm emit
    int is_para;
    int is_global;

};
enum {SF_DEREF = 1, SF_ADDR = 2};
struct symbol_temp
{
    SYMBOL_ID_COMMON
// for ir emit
    int flag;
    struct symbol *link;
// for memory sharing optimize
    int is_shared_tmp;
};

struct symbol_label
{
    SYMBOL_COMMON

    struct vector *precs;   // used for flow graph construction
    struct basic_block *belonging_bb;
// for asm emit
    int is_func_label;      // func label will not be emit
};
struct symbol_func
{
    SYMBOL_COMMON
};
struct symbol_reg
{
    SYMBOL_COMMON
    int dec;
    int need_wb;
    int ref_cnt;
    int used;
    int is_reserved;
    struct symbol_temp *user;
    struct symbol_reg *link;
};

/*
extern int temp_counter;
extern int label_counter;
extern int string_counter;
*/
extern int cur_nesting_lvl;
extern struct hash_table *constant_tab;
void symtab_init();
void enter_scope();
//void enter_param_scope();
void exit_scope();
//void exit_param_scope();
struct symbol *sym_add_constant(struct type *type, union val value);
struct symbol *sym_add_enum_tag(char *name, struct type *type, struct location *loc);
struct symbol *sym_add_enumerator(char *name, struct type *type, union val value, struct location *loc);
struct symbol *sym_add_typedef(char *id, struct type *type, struct location *loc);
struct symbol *sym_add_variable(char *id, struct type *type, int sto_class_spec_tok, int defined, struct location *loc);
struct symbol *sym_add_function(char *id, struct type *type, int sto_class_spec_tok, struct location *loc);
struct symbol *sym_add_tag(char *id, struct type *type, struct location *loc);
struct symbol *sym_create_temp(struct type *type);
struct symbol *sym_create_offset();
struct symbol *sym_create_label();
struct symbol *sym_add_string(struct type *type, char *str, struct location *loc);
struct symbol *sym_lookup_tag(char *id);
struct symbol *sym_lookup_id(char *id);
int sym_delete_id(char *id);
struct symbol *sym_id_traverse_in_cur_lvl(int *start, int *cnt);
struct symbol *sym_tag_traverse_in_cur_lvl(int *start, int *cnt);
#endif
