#ifndef __LEX_H__
#define __LEX_H__

enum token_type
{
    #define TOKEN(k, s) k,
        #include "token.h"
    #undef  TOKEN
    TOK_END
};

struct token_tab
{
    int type;
    char kw[10];
};

struct location
{
    int line;
    int offset;
    char *files;
};

union val
{
    int i;
    unsigned int ui;
    long l;
    unsigned long ul;
    float f;
    double d;
    void *p;
};

struct token
{
    enum token_type type;
    union val value;
    struct location *loc;
    char *token_strings;
};

#define IS_DIGIT(c)         (c >= '0' && c <= '9')
#define IS_OCT_DIGIT(c)      (c >= '0' && c <= '7')
#define IS_HEX_DIGIT(c)      (IS_DIGIT(c) || (c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f'))
#define IS_LETTER(c)        ((c >= 'a' && c <= 'z') || (c == '_') || (c >= 'A' && c <= 'Z'))
#define IS_LETTER_OR_DIGIT(c) (IS_LETTER(c) || IS_DIGIT(c))

int lex_init(char *, int );
int lex_destory();
struct token *lex_get_token();
void lex_push(struct token *);
char *tok_type2name(enum token_type);

#endif