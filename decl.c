#include <stdlib.h>
#include <assert.h>
#include "expr.h"
#include "decl.h"
#include "stmt.h"
#include "sxcc.h"
#include "hash.h"

static struct hash_table *typedef_names;
int is_typedef_name(char *name)
{
    return !typedef_names->find(typedef_names, name, NULL);
}
char *get_declor_name(struct decl_common_declor_node *declor)
{
    if (declor->kind == NK_NameDeclarator)
    {
        return declor->id;
    }
    assert(declor->next_declor);
    return get_declor_name(declor->next_declor);
}

/**
 *  init-declarator:
 *		declarator
 *      declarator = initializer
 */
struct decl_init_declor_node *init_declarator()
{
    struct decl_init_declor_node *node;
    struct token *tok;

    node = (struct decl_init_declor_node *)calloc(1, sizeof(struct decl_init_declor_node));
    if (!node)
    {
        printf("calloc error!\n");
        exit(-1);
    }
    node->kind = NK_InitDeclarator;

    node->declor = declarator(DEC_CONCRETE);
    
    tok = lex_get_token();

    if(tok->type == TK_ASSIGN)
    {
        node->initer = initializer();
        free(tok);
    }
    else
    {
        lex_push(tok);
    }
    return node;
}
/*
init-declarator-list:
    init-declarator
    init-declarator-list ,  init-declarator
*/
struct decl_init_declor_node *init_declarator_list()
{
    struct decl_init_declor_node *node, *p;
    struct token *tok;
    node = init_declarator();
    p = node;
    while(1)
    {
        tok = lex_get_token();
        if (tok->type == TK_COMMA)
        {
            free(tok);
            p->next = init_declarator();
            p = p->next;
        }
        else
        {
            lex_push(tok);
            break;
        }
    }
    return node;
}

/*
typedef-name:
    identifier
*/
struct decl_typedef_spec_node *typedef_name()
{
    struct decl_typedef_spec_node *node;
    struct token *tok;
    ALLOC(node, struct decl_typedef_spec_node);
    node->kind = NK_TypedefName;

    tok = lex_get_token();
    if (tok->type == TK_ID)
    {
        node->name = tok->token_strings;
        free(tok);
    }
    else
    {
        error("expect identifier", tok->loc);
        exit(1);
    }
    return node;
}

/*
type-qualifier:
    const
    volatile
*/
int is_type_qualifier(struct token *tok)
{
    switch (tok->type)
    {
        case TK_CONST:
        case TK_VOLATILE:
            return 1;
        
        default:
            return 0;
    }
}
struct decl_specer_node *type_qualifier()
{
    struct decl_specer_node *node;
    struct token *tok;
    ALLOC(node, struct decl_specer_node);
    
    node->kind = NK_Specifier;

    tok = lex_get_token();
    switch (tok->type)
    {
        case TK_CONST:
        case TK_VOLATILE:
            node->tok_type = tok->type;
            free(tok);
            break;
        default:
            error("expect type-qualifier", tok->loc);
            exit(1);
    }
    return node;
}
/*
type-specifier:
    void
    char
    short
    int
    long
    float
    double
    signed
    unsigned
    struct-or-union-specifier
    enum-specifier
    typedef-name
*/
int is_type_specifier(struct token *tok, int has_type_specifier)
{
    switch (tok->type)
    {
        case TK_VOID:
        case TK_CHAR:
        case TK_SHORT:
        case TK_INT:
        case TK_LONG:
        case TK_FLOAT:
        case TK_DOUBLE:
        case TK_SIGNED:
        case TK_UNSIGNED:
        case TK_STRUCT:
        case TK_UNION:
        case TK_ENUM:
            return 1;
        case TK_ID:
            /*
                some extreme cases:
                1.  
                    typedef char VAR    -- VAR is inserted to typedef_name list
                    {
                        typedef int VAR     -- if we do nothing, VAR will be seen as a type_specifier, but in here, it should be a declor
                    }
                2.  
                    typedef char VAR
                    {
                        typedef volatile VAR VAR
                    }
                    
                3.
                    typedef char VAR
                    {
                        VAR VAR
                    }
                to determine which group the id 'VAR' belongs to, we have the following criteria:
                when we meet an id which has been add to typedef_name list
                1. if there has been one or more type_specifier(s), the id is declor
                2. otherwise, the id is type_specifier
            */
            if (is_typedef_name(tok->token_strings))
            {
                if (has_type_specifier)
                {
                    return 0;
                }
                return 1;
            }
            return 0;
        default:

            return 0;
    }
}
struct decl_specer_node *type_specifier()
{
    struct decl_specer_node *node;
    struct token *tok;
    ALLOC(node, struct decl_specer_node);
    node->kind = NK_Specifier;

    tok = lex_get_token();
    node->tok_type = tok->type;
    switch (tok->type)
    {
        case TK_VOID:
        case TK_CHAR:
        case TK_SHORT:
        case TK_INT:
        case TK_LONG:
        case TK_FLOAT:
        case TK_DOUBLE:
        case TK_SIGNED:
        case TK_UNSIGNED:
            free(tok);
            break;
        case TK_STRUCT:
        case TK_UNION:
            lex_push(tok);
            node->priv_node = (struct decl_specer_node *)struct_or_union_specifier();
            break;
        case TK_ENUM:
            lex_push(tok);
            node->priv_node = (struct decl_specer_node *)enum_specifier();
            break;
        case TK_ID:
            lex_push(tok);
            node->priv_node = (struct decl_specer_node *)typedef_name();
            break;
        default:
            error("expect type-specifier", tok->loc);
            exit(1);
    }
    return node;
}

/*
storage-class-specifier:
    typedef
    extern
    static
    auto
    register
*/
int is_storage_class_specifier(struct token *tok)
{
    switch (tok->type)
    {
        case TK_TYPEDEF:
        case TK_EXTERN:
        case TK_STATIC:
        case TK_AUTO:
        case TK_REGISTER:
            return 1;
        default:
            return 0;
    }
}
struct decl_specer_node *storage_class_specifier()
{
    struct decl_specer_node *node;
    struct token *tok;
    ALLOC(node, struct decl_specer_node);
    
    node->kind = NK_Specifier;

    tok = lex_get_token();
    switch (tok->type)
    {
        case TK_TYPEDEF:
        case TK_EXTERN:
        case TK_STATIC:
        case TK_AUTO:
        case TK_REGISTER:
            node->tok_type = tok->type;
            free(tok);
            break;
        default:
            error("expect storage-class-specifier", tok->loc);
            exit(1);
    }
    return node;
}
/*
struct-declaration:
    specifier-qualifier-list struct-declarator-list ;
*/
struct decl_struct_decl_node *struct_declaration()
{
    struct decl_struct_decl_node *node;
    struct token *tok;
    ALLOC(node, struct decl_struct_decl_node);
    node->kind = NK_StructDeclaration;
    node->specer_list = specifier_qualifier_list();
    tok = lex_get_token();
    // an extension to C89, supports anonymous struct/union member in struct/union
	if (tok->type == TK_SEMICOLON)
	{	
		free(tok);
        return node;	
	}
    else
    {
        lex_push(tok);
        node->declor_list = struct_declarator_list();
        expect(TK_SEMICOLON);
    }
    return node;
}
/*
struct-declaration-list:
    struct-declaration
    struct-declaration-list struct-declaration
*/
struct decl_struct_decl_node *struct_declaration_list()
{
    struct decl_struct_decl_node *node, *p;
    struct token *tok;
    node = struct_declaration();
    p = node;
    while(1)
    {
        tok = lex_get_token();
        if (is_type_specifier(tok, 0) || is_type_qualifier(tok))
        {
            lex_push(tok);
            p->next = struct_declaration();
            p = p->next;
        }
        else
        {
            lex_push(tok);
            break;
        }
    }
    return node;
}

/*
struct-or-union-specifier:
    struct-or-union identifier<opt> {  struct-declaration-list } 
    struct-or-union identifier
*/
struct decl_struct_spec_node *struct_or_union_specifier()
{
    struct decl_struct_spec_node *node;
    struct token *tok;
    ALLOC(node, struct decl_struct_spec_node);
    
    tok = lex_get_token();
    if (tok->type == TK_STRUCT)
    {
        node->kind = NK_StructSpecifier;
    }
    else if (tok->type == TK_UNION)
    {
        node->kind = NK_UnionSpecifier;
    }
    else 
    {
        error("expect 'struct' or 'union'", tok->loc);
        exit(1);
    }
    free(tok);
    tok = lex_get_token();
    if (tok->type == TK_ID)
    {
        node->id = tok->token_strings;
        free(tok);
    }
    else if (tok->type == TK_LBRACE)
    {
        lex_push(tok);
    }
    else
    {
        error("expect identifier or '{'", tok->loc);
        exit(1);
    }
    tok = lex_get_token();
    if (tok->type == TK_LBRACE)
    {
        free(tok);
        node->decl_list = struct_declaration_list();
        expect(TK_RBRACE);
    }
    else
    {
        lex_push(tok);
    }
    return node;
}
/*
specifier-qualifier-list:
    type-specifier specifier-qualifier-list<opt>
    type-qualifier specifier-qualifier-list<opt>
*/
struct decl_specer_list_node *specifier_qualifier_list()
{
    int flag = 0;
    struct decl_specer_list_node *node;
    struct decl_specer_node **spec_tail, **qual_tail;
    struct token *tok;

    ALLOC(node, struct decl_specer_list_node);
    node->kind = NK_Specifiers;
    spec_tail = &(node->ty_specer);
    qual_tail = &(node->ty_qualer);

    while(1)
    {
        tok = lex_get_token();
        if (is_type_specifier(tok, 0))
        {
            lex_push(tok);
            *spec_tail = type_specifier();
            spec_tail = &((*spec_tail)->next);
            flag = 1;
        }
        else if (is_type_qualifier(tok))
        {
            lex_push(tok);
            *qual_tail = type_qualifier();
            qual_tail = &((*qual_tail)->next);
            flag = 1;
        }
        else if(!flag)
        {
            error("expect type-specifier or type-qualifier", tok->loc);
            exit(1);
        }
        else
        {
            lex_push(tok);
            break;
        }
    }
    return node;
}
/*
struct-declarator-list:
    struct-declarator
    struct-declarator-list ,  struct-declarator
*/

struct decl_common_declor_node *struct_declarator_list()
{
    struct decl_common_declor_node *node, *p;
    struct token *tok;
    node = struct_declarator();
    p = node;
    while(1)
    {
        tok = lex_get_token();
        if (tok->type != TK_COMMA)
        {
            lex_push(tok);
            break;
        }
        free(tok);
        p->next = struct_declarator();
        p = p->next;
    }
    return node;
}
/*
struct-declarator:
    declarator
    declarator<opt> :  constant-expression
*/
struct decl_common_declor_node *struct_declarator()
{
    return declarator(DEC_CONCRETE);
}
/*
enumerator:
    enumeration-constant
    enumeration-constant = constant-expression

enumeration-constant:
    identifier
*/
struct decl_enumor_node *enumerator()
{
    struct decl_enumor_node *node;
    struct token *tok;

    ALLOC(node, struct decl_enumor_node);
    node->kind = NK_Enumerator;
    tok = lex_get_token();
    if (tok->type != TK_ID)
    {
        error("expect identifier", tok->loc);
        exit(1);
    }
    node->id = tok->token_strings;
    free(tok);
    tok = lex_get_token();
    if (tok->type == TK_ASSIGN)
    {
        free(tok);
        node->const_expr = constant_expression();
    }
    else
    {
        lex_push(tok);
    }
    return node;
}
/*
enumerator-list:
    enumerator
    enumerator-list , enumerator
*/
struct decl_enumor_node *enumerator_list()
{
    struct decl_enumor_node *node, *p;
    struct token *tok;
    node = enumerator();
    p = node;
    while(1)
    {
        tok = lex_get_token();
        if (tok->type != TK_COMMA)
        {
            lex_push(tok);
            break;
        }
        free(tok);
        tok = lex_get_token();
        if (tok->type == TK_RBRACE)
        {
            lex_push(tok);
            break;
        }
        else
        {
            lex_push(tok);
        }
        p->next = enumerator();
        p = p->next;
    }
    return node;
}
/*
enum-specifier:
    enum  identifier<opt> { enumerator-list }
    enum  identifier
*/
struct decl_enum_spec_node *enum_specifier()
{
    struct decl_enum_spec_node *node;
    struct token *tok;
    ALLOC(node, struct decl_enum_spec_node);
    
    tok = lex_get_token();
    if (tok->type == TK_ENUM)
    {
        node->kind = NK_EnumSpecifier;
    }
    else 
    {
        error("expect 'enum'", tok->loc);
        exit(1);
    }
    free(tok);
    tok = lex_get_token();
    if (tok->type == TK_ID)
    {
        node->id = tok->token_strings;
        free(tok);
    }
    else if (tok->type == TK_LBRACE)
    {
        lex_push(tok);
    }
    else
    {
        error("expect identifier or '{'", tok->loc);
        exit(1);
    }
    tok = lex_get_token();
    if (tok->type == TK_LBRACE)
    {
        free(tok);
        node->enumor = enumerator_list();
        expect(TK_RBRACE);
    }
    else
    {
        lex_push(tok);
    }
    return node;
}
/*
parameter-list:
    parameter-declaration
    parameter-list ,  parameter-declaration
*/
struct decl_para_node *parameter_list()
{
    struct decl_para_node *node, *p;
    struct token *tok, *tok1;
    node = parameter_declaration();
    p = node;
    while(1)
    {
        tok = lex_get_token();
        if (tok->type != TK_COMMA)
        {
            lex_push(tok);
            break;
        }
        
        tok1 = lex_get_token();
        if (tok1->type == TK_ELLIPSIS)
        {
            lex_push(tok1);
            lex_push(tok);
            break;
        }
        else
        {
            free(tok);
            lex_push(tok1);
        }
        p->next = parameter_declaration();
        p = p->next;
    }
    return node;
}
/*
parameter-type-list:
    parameter-list
    parameter-list , ...
*/
static struct decl_para_type_list_node *parameter_type_list()
{
    struct decl_para_type_list_node *node;
    struct token *tok;

    ALLOC(node, struct decl_para_type_list_node);
    node->kind = NK_ParameterTypeList;
    node->para_list = parameter_list();
    tok = lex_get_token();
    
    if (tok->type == TK_COMMA)
    {
        expect(TK_ELLIPSIS);
        node->has_ellipsis = 1;
    }
    else
    {
        lex_push(tok);
    }
    return node;
}
/*
direct-declarator:
    identifier
    (  declarator ) 
    direct-declarator [  constant-expression<opt> ] 
    direct-declarator (  parameter-type-list ) 

direct-abstract-declarator:
    (  abstract-declarator ) 
    direct-abstract-declarator<opt> [  constant-expression<opt> ] 
    direct-abstract-declarator<opt> (  parameter-type-list<opt> )

we change it into:

direct-declarator:
    identifier _direct-declarator
    (  declarator ) _direct-declarator

_direct-declarator:
    [  constant-expression<opt> ] _direct-declarator
    (  parameter-type-list ) _direct-declarator
    NONE

direct-abstract-declarator:
    (  abstract-declarator )<opt>  _direct-abstract-declarator

_direct-abstract-declarator:
    [  constant-expression<opt> ] _direct-abstract-declarator
    (  parameter-type-list<opt> ) _direct-abstract-declarator
    NONE
*/
static struct decl_common_declor_node *_direct_declarator(int sel, struct decl_common_declor_node **tail)
{
    struct decl_common_declor_node *node, *_tail;
    struct token *tok;
    tok = lex_get_token();
    if (tok->type == TK_LBRACKET)
    {
        free(tok);
        ALLOC(node, struct decl_array_declor_node);
        node->kind = NK_ArrayDeclarator;
        tok = lex_get_token();
        if (tok->type == TK_RBRACKET)
        {
            free(tok);
        }
        else
        {
            lex_push(tok);
            assert(node->kind == NK_ArrayDeclarator);
            ((struct decl_array_declor_node *)node)->const_expr = constant_expression();
            expect(TK_RBRACKET);
        }
    }
    else if (tok->type == TK_LPAREN) 
    {
        free(tok);
        ALLOC(node, struct decl_func_declor_node);
        node->kind = NK_FunctionDeclarator;
        tok = lex_get_token();
        if (tok->type == TK_RPAREN)
        {
            /*
            if (sel & DEC_ABSTRACT)
            {
                free(tok);
            }
            else
            {
                error("the parameter list can not be empty", tok->loc.line, tok->loc.offset);
                exit(1);
            }
            */
            free(tok);
        }
        else
        {
            lex_push(tok);
            assert(node->kind == NK_FunctionDeclarator);
            ((struct decl_func_declor_node *)node)->para_type_list = parameter_type_list();
            expect(TK_RPAREN);
        }
    }
    else
    {
        lex_push(tok);
        return NULL;
    }
    node->next_declor = _direct_declarator(sel, &_tail);
    if (!node->next_declor)
    {
        assert(node);
        *tail = node;
    }
    else
    {
        *tail = _tail;
    }
    return node;
}

struct decl_common_declor_node *direct_declarator(int sel, struct decl_common_declor_node **tail)
{
    struct decl_common_declor_node *node, *next, *_tail1, *_tail2;
    struct token *tok, *tok1;

    tok = lex_get_token();
    if (tok->type == TK_ID)
    {
        ALLOC(node, struct decl_name_declor_node);
        node->kind = NK_NameDeclarator;
        ((struct decl_name_declor_node *)node)->id = tok->token_strings;
        _tail1 = node;
    }
    else if (tok->type == TK_LPAREN)
    {
        tok1 = lex_get_token();
        // 1.parameter-type-list(ABSTRACT)
        if ((is_storage_class_specifier(tok1) || is_type_qualifier(tok1) || is_type_specifier(tok1, 0)) &&
            (sel & DEC_ABSTRACT))
        {
            lex_push(tok1);
            lex_push(tok);
            node = NULL;
        }
        else
        {
            // 2.declarator(CONCRETE)
            // 3.abstract-declarator(ABSTRACT)
            free(tok);
            lex_push(tok1);
            node = _declarator(sel, &_tail1);
            expect(TK_RPAREN);
        }
    }
    else if (tok->type == TK_LBRACKET && (sel & DEC_ABSTRACT)) // 4.constant-expression(ABSTRACT)
    {
        lex_push(tok);
        node = NULL;
    }
    else if (sel & DEC_ABSTRACT) // direct-abstract-declarator -> _direct-abstract-declarator -> None
    {
        lex_push(tok);
        node = NULL;
    }
    else
    {
        assert(0);
    }
    next = _direct_declarator(sel, &_tail2);
    if (next)
    {
        if (!node)
        {
            node = next;
        }
        else
        {
            _tail1->next_declor = next;
        }
        *tail = _tail2;
    }
    else if (node)
    {
        *tail = _tail1;
    }
    /* else // direct-abstract-declarator --> _direct-abstract-declarator --> None
    {
        node = NULL;
    }
    */
    return node;
}
/*
declarator:
    pointer<opt> direct-declarator

abstract-declarator:
    pointer
    pointer<opt> direct-abstract-declarator

pointer:
    *  type-qualifier-list<opt>
    *  type-qualifier-list<opt> pointer

type-qualifier-list:
    valatile
    const
*/

struct decl_common_declor_node *_declarator(int sel, struct decl_common_declor_node **tail)
{
    struct decl_common_declor_node *node = NULL, *last = NULL, **p, *father, *_tail;
    struct token *tok;

    p = &node;
    tok = lex_get_token();
    while (tok->type == TK_MUL)
    {
        free(tok);
        ALLOC(*p, struct decl_pointer_declor_node);
        (*p)->kind = NK_PointerDeclarator;
        tok = lex_get_token();
        if (tok->type == TK_CONST || tok->type == TK_VOLATILE)
        {
            ((struct decl_pointer_declor_node *)*p)->qual |= tok->type == TK_CONST ? CONST : VOLATILE;
            free(tok);
            tok = lex_get_token();
        }
        last = (*p);
        p = &((*p)->next_declor);
    }
    lex_push(tok);

    father = direct_declarator(sel, &_tail);

    if (!father)    // int *, ...
    {
        *tail = last;
        return node;
    }
    
    if (node)
    {
        _tail->next_declor = node;
        *tail = last;
    }
    else
    {
        *tail = _tail;
    }
    
    return father;
}

struct decl_common_declor_node *declarator(int sel)
{
    struct decl_common_declor_node *tail;
    return _declarator(sel, &tail);
}



/*
type-name:
    specifier-qualifier-list abstract-declarator<opt>
*/
struct decl_type_name_node *type_name()
{
    struct decl_type_name_node *node;
    struct token *tok;

    ALLOC(node, struct decl_type_name_node);
    node->kind = NK_TypeName;
    node->spec = specifier_qualifier_list();
    tok = lex_get_token();
    if (tok->type == TK_MUL || tok->type == TK_LPAREN || tok->type == TK_LBRACKET)  //the first set of 'abstract-declarator'
    {
        lex_push(tok);
        node->absdeclor = declarator(DEC_ABSTRACT);
    }
    else
    {
        lex_push(tok);
    }
    return node;
}
/*
declaration-specifiers:
    storage-class-specifier declaration-specifiers<opt>
    type-specifier declaration-specifiers<opt>
    type-qualifier declaration-specifiers<opt>
*/
struct decl_specer_list_node *declaration_specifiers()
{
    int flag = 0, has_typedef_name = 0;
    struct decl_specer_list_node *node; 
    struct decl_specer_node **stg_spec_tail, **ty_spec_tail, **ty_qual_tail;
    struct token *tok;

    ALLOC(node, struct decl_specer_list_node);
    
    node->kind = NK_Specifiers;
    stg_spec_tail = &(node->stg_specer);
    ty_spec_tail = &(node->ty_specer);
    ty_qual_tail = &(node->ty_qualer);
    while(1)
    {
        tok = lex_get_token();
        if (is_type_specifier(tok, node->ty_specer))
        {
            /*
                some extreme cases:
                1.  
                    typedef char VAR    -- VAR is inserted to typedef_name list
                    {
                        typedef int VAR     -- if we do nothing, VAR will be seen as a type_specifier, but it should be a declor
                    }
                2.  
                    typedef char VAR
                    {
                        typedef volatile VAR VAR
                    }
                    
                3.
                    typedef char VAR
                    {
                        VAR VAR
                    }
                to determine which group the id 'VAR' should belongs to, we have the following criteria:
                when we meet a id has been add to typedef_name list
                1. if there has been a type_specifier, the id is declor
                2. otherwise, the id is type_specifier
            */
            lex_push(tok);
            *ty_spec_tail = type_specifier();
            ty_spec_tail = &((*ty_spec_tail)->next);
            flag = 1;
        }
        else if (is_type_qualifier(tok))
        {
            lex_push(tok);
            *ty_qual_tail = type_qualifier();
            ty_qual_tail = &((*ty_qual_tail)->next);
            flag = 1;
        }
        else if (is_storage_class_specifier(tok))
        {
            if (*stg_spec_tail)
            {
                error("more than one storage class may not be specified", tok->loc);
                exit(1);
            }
            node->sto_class_spec_tok = tok->type;
            lex_push(tok);
            *stg_spec_tail = storage_class_specifier();
            stg_spec_tail = &((*stg_spec_tail)->next);

            flag = 1;
        }
        else if (!flag)
        {
            error("expect specifier or qualifier", tok->loc);
            exit(1);
        }
        else
        {
            lex_push(tok);
            break;
        }
    }
    return node;
}
/*
parameter-declaration:
    declaration-specifiers declarator
    declaration-specifiers abstract-declarator<opt>

one declaration, such as "int a" in "f(int a, int b, int c)", "" in "f()" and "int" in "f(int, int, int)"
*/
struct decl_para_node *parameter_declaration()
{
    struct decl_para_node *node;
    struct token *tok;

    ALLOC(node, struct decl_para_node);
    node->kind = NK_ParameterDeclaration;
    node->spec = declaration_specifiers();
    tok = lex_get_token();
    if (tok->type == TK_MUL || tok->type == TK_LPAREN || tok->type == TK_ID || tok->type == TK_LBRACKET)    // the first set of declarator and abstract-declarator
    {
        lex_push(tok);
        node->declor = declarator(DEC_ABSTRACT | DEC_CONCRETE);
    }
    else
    {
        lex_push(tok);
    }
    return node;
}
/*
initializer:
    assignment-expression
    {  initializer-list } 
    {  initializer-list , }
*/
struct decl_initer_node *initializer()
{
    struct decl_initer_node *node;
    struct token *tok;
    ALLOC(node, struct decl_initer_node);
    node->kind = NK_Initializer;
    tok = lex_get_token();
    if (tok->type == TK_LBRACE)
    {
        free(tok);
        node->initer_list = initializer_list();
        tok = lex_get_token();
        if (tok->type == TK_COMMA)
        {
            free(tok);
            expect(TK_RBRACE);
        }
        else if (tok->type == TK_RBRACE)
        {
            free(tok);
        }
        else
        {
            error("expect '}'", tok->loc);
            exit(1);
        }
    }
    else if (is_expr_first(tok->type))
    {
        lex_push(tok);
        node->assign_expr = assignment_expression();
    }
    else
    {
        error("expect assignment-expression or '{'", tok->loc);
        exit(1);
    }
    return node;
}
/*
initializer-list:
    initializer
    initializer-list ,  initializer
*/
struct decl_initer_node *initializer_list()
{
    struct decl_initer_node *node, *p;
    struct token *tok, *tok1;

    node = initializer();
    p = node;
    while(1)
    {
        tok = lex_get_token();
        if (tok->type == TK_COMMA)
        {
            tok1 = lex_get_token();
            if (tok1->type == TK_RBRACE)
            {
                lex_push(tok1);
                lex_push(tok);
                break;
            }
            else
            {
                lex_push(tok1);
            }
            free(tok);
            p->next = initializer();
            p = p->next;
        }
        else
        {
            lex_push(tok);
            break;
        }
    }
    return node;
}

/*
declaration:
    declaration-specifiers init-declarator-list<opt> ;
*/
struct decl_node *declaration()
{
    struct decl_node *node;
    struct token *tok;
    char *declor_name;
    ALLOC(node, struct decl_node);
    
    node->kind = NK_Declaration;
    node->spec = declaration_specifiers();
    tok = lex_get_token();
    if (tok->type == TK_SEMICOLON)
    {
        free(tok);
    }
    else
    {
        lex_push(tok);
        node->init_declor_list = init_declarator_list();
        expect(TK_SEMICOLON);
    }
    if (node->spec->sto_class_spec_tok == TK_TYPEDEF)
    {
        declor_name = get_declor_name(node->init_declor_list->declor);
        // we don't care if the typedef names are redefined or not, we simpily record the id is a typedef name
        if (typedef_names->find(typedef_names, declor_name, NULL))
        {
            typedef_names->insert(typedef_names, declor_name, declor_name);
        }
    }
    return node;
}

/*
declaration-list:
    declaration
    declaration-list declaration
*/
struct decl_node *declaration_list()
{
    struct decl_node *node, *p;
    struct token *tok;
    int i;
    char *str;
    node = declaration();
    p = node;
    while(1)
    {
        tok = lex_get_token();
        if (is_storage_class_specifier(tok) || is_type_qualifier(tok) || is_type_specifier(tok, 0))
        {
            lex_push(tok);
            p->next = declaration();
            p = p->next;
        }
        else
        {
            lex_push(tok);
            break;
        }
    }
    return node;
}

/*
external-declaration:
    function-definition
    declaration

function-definition:
    declaration-specifiers declarator compound-statement

declaration:
    declaration-specifiers init-declarator-list<opt> ;

init-declarator-list:
    init-declarator
    init-declarator-list ,  init-declarator 
init-declarator:
    declarator
    declarator =  initializer
*/
struct common_node *external_declaration()
{
    struct decl_node *decl;
    struct decl_func_node *func;

    struct decl_specer_list_node *spec;
    
    char *declor_name;
    struct decl_common_declor_node *declor;
    struct decl_init_declor_node *init_declor;

    struct decl_ext_decl_node *node;
    struct token *tok;

    spec = declaration_specifiers();
    tok = lex_get_token();
    // int ;
    if (tok->type == TK_SEMICOLON)
    {
        free(tok);
        ALLOC(decl, struct decl_node);
        decl->kind = NK_Declaration;
        decl->spec = spec;
        node = (struct common_node *)decl;
        return node;
    }
    else
    {
        lex_push(tok);
    }
    declor = declarator(DEC_CONCRETE);
// end common header
// declaration
    tok = lex_get_token();
    if (tok->type == TK_ASSIGN)
    {
        free(tok);
        ALLOC(decl, struct decl_node);
        decl->kind = NK_Declaration;

        ALLOC(init_declor, struct decl_init_declor_node);
        init_declor->kind = NK_InitDeclarator;
        init_declor->declor = declor;
        init_declor->initer = initializer();
        tok = lex_get_token();
        if (tok->type == TK_SEMICOLON)
        {
            free(tok);
        }
        else if (tok->type == TK_COMMA)
        {
            free(tok);
            init_declor->next = init_declarator_list();
            expect(TK_SEMICOLON);
        }
        else
        {
            error("expect ',' or ';'", tok->loc);
            exit(-1);
        }
        decl->init_declor_list = init_declor;
        decl->spec = spec;
        node = (struct common_node *)decl;
    }
    else if (tok->type == TK_COMMA)
    {
        free(tok);
        ALLOC(decl, struct decl_node);
        decl->kind = NK_Declaration;

        ALLOC(init_declor, struct decl_init_declor_node);
        init_declor->kind = NK_InitDeclarator;
        init_declor->declor = declor;
        init_declor->next = init_declarator_list();
        expect(TK_SEMICOLON);
        decl->init_declor_list = init_declor;
        decl->spec = spec;
        node = (struct common_node *)decl;
    }
    else if (tok->type == TK_SEMICOLON)
    {
        free(tok);
        ALLOC(decl, struct decl_node);
        decl->kind = NK_Declaration;

        ALLOC(init_declor, struct decl_init_declor_node);
        init_declor->kind = NK_InitDeclarator;
        init_declor->declor = declor;
        decl->spec = spec;
        decl->init_declor_list = init_declor;
        node = (struct common_node *)decl;

        if (spec->sto_class_spec_tok == TK_TYPEDEF)
        {
            declor_name = get_declor_name(declor);
            // we don't care if the typedef names are redefined or not, we simpily record the id is a typedef name
            if (typedef_names->find(typedef_names, declor_name, NULL))
            {
                typedef_names->insert(typedef_names, declor_name, declor_name);
            }
        }
    }
// end declaration
// function-definition
    else if (tok->type == TK_LBRACE)
    {
        lex_push(tok);
        ALLOC(func, struct decl_func_node);
        func->kind = NK_Function;

        func->spec = spec;
        func->declor = declor;
        assert(declor->next_declor->kind == NK_FunctionDeclarator);
        func->func_declor = declor->next_declor;
        ((struct decl_func_declor_node *)func->func_declor)->is_definition = 1;
        func->comp = compound_statement();

        node = (struct common_node *)func;
    }
// end function-definition
    else
    {
        error("expect function-definition or declaration", tok->loc);
        exit(1);
    }
    return node;
}
/*
translation-unit:
    external-declaration
    translation-unit external-declaration
*/
struct decl_trans_unit_node *translation_unit()
{
    struct decl_trans_unit_node *node;
    struct common_node *p;
    struct token *tok;

    typedef_names = hash_init();

    ALLOC(node, struct decl_trans_unit_node);
    node->kind = NK_TranslationUnit;
    node->ext_decl = external_declaration();
    p = node->ext_decl;
    while(1)
    {
        tok = lex_get_token();
        if (is_storage_class_specifier(tok) || is_type_specifier(tok, 0) || is_type_qualifier(tok))
        {
            lex_push(tok);
            p->next = external_declaration();
            p = p->next;
        }
        else
        {
            if (tok->type != TK_END)
            {
                rep_error("err declaration");
                exit(1);
            }
            lex_push(tok);
            break;
        }
    }
    hash_destory(typedef_names);
    return node;
}
