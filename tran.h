#ifndef __TRANSLATE_H__
#define __TRANSLATE_H__
#include "symtab.h"
#include "decl.h"
#if 0
enum
{
EQ = 0x00,
    EQ_1, EQ_2, EQ_4, EQ_8, EQ_4F, EQ_8F,
NEQ = 0x10,
    NEQ_1, NEQ_2, NEQ_4, NEQ_8, NEQ_4F, NEQ_8F,
LS = 0x20,
    LS_1, LS_1U, LS_2, LS_2U, LS_4, LS_4U, LS_8, LS_8U, LS_4F, LS_8F,
GT = 0X30,
    GT_1, GT_1U, GT_2, GT_2U, GT_4, GT_4U, GT_8, GT_8U, GT_4F, GT_8F,
LE = 0X40,
    LE_1, LE_1U, LE_2, LE_2U, LE_4, LE_4U, LE_8, LE_8U, LE_4F, LE_8F,
GE = 0X50,
    GE_1, GE_1U, GE_2, GE_2U, GE_4, GE_4U, GE_8, GE_8U, GE_4F, GE_8F,
ADD = 0X60,
    /*ADD_1, ADD_2,*/ ADD_4, ADD_8, ADD_4F, ADD_8F,
SUB = 0X70,
    /*SUB_1, SUB_2,*/ SUB_4, SUB_8, SUB_4F, SUB_8F,
MUL = 0X80,
    /*MUL_1, MUL_2,*/ MUL_4, MUL_8, MUL_4F, MUL_8F,
DIV = 0X90,
    /*DIV_1, DIV_2,*/ DIV_4, DIV_8, DIV_4F, DIV_8F,
MOD = 0XA0,
    /*MOD_1, MOD_2,*/ MOD_4, MOD_8,
SHR = 0XB0,
    /*SHR_1, SHR_1U,*/ SHR_2, SHR_2U, SHR_4, SHR_4U, SHR_8, SHR_8U,
SHL = 0XC0,
    /*SHL_1, SHL_1U,*/ SHL_2, SHL_2U, SHL_4, SHL_4U, SHL_8, SHL_8U,
AND = 0XD0,
    AND_1, AND_2, AND_4, AND_8,
OR = 0XE0,
    OR_1, OR_2, OR_4, OR_8,
BAND = 0XD0,
    /*BAND_1, BAND_2,*/ BAND_4, BAND_8,
BOR = 0XE0,
    /*BOR_1, BOR_2,*/ BOR_4, BOR_8,
XOR = 0XF0,
    /*XOR_1, XOR_2,*/ XOR_4, XOR_8,
NOT = 0X100,
    NOT_1, NOT_2, NOT_4, NOT_8,
COMP = 0X110,
    COMP_1, COMP_2, COMP_4, COMP_8,
ASSIGN = 0X120,
    ASSIGN_1, ASSIGN_2, ASSIGN_4, ASSIGN_8, ASSIGN_4F, ASSIGN_8F, ASSIGN_N,
JT = 0X130,
    JT_1, JT_2, JT_4, JT_8, JT_4F, JT_8F,
JNT = 0X140,
    JNT_1, JNT_2, JNT_4, JNT_8, JNT_4F, JNT_8F,
JE = 0X150,
    JE_1, JE_2, JE_4, JE_8, JE_4F, JE_8F,
JNE = 0X160,
    JNE_1, JNE_2, JNE_4, JNE_8, JNE_4F, JNE_8F,

JMP = 0X170,
LABEL = 0X180,
RETURN = 0X190,
RET = 0X1A0,
CALL = 0X1B0,
NEG = 0x1C0,
ADDR = 0x1D0,

CAST = 0X1F0,
    CAST_I1_I4, CAST_U1_I4, CAST_I2_I4, CAST_U2_I4, CAST_I1_U1, CAST_U1_I1, CAST_I2_U2, CAST_U2_I2, CAST_I4_U4,
    CAST_U4_I4, CAST_I4_F4, CAST_I4_F8, CAST_U4_F4, CAST_U4_F8, CAST_F4_I4, CAST_F4_U4, CAST_F4_F8, CAST_F8_I4, 
    CAST_F8_U4, CAST_F8_F4, CAST_I4_I1, CAST_I4_U1, CAST_I4_I2, CAST_I4_U2,
};
#endif

#define IS_JMP_IR(ircode) (ircode >= JT && ircode <= JMP)
#define IS_BOP_IR(ircode) (ircode >= EQ && ircode <= NEG)
#define IS_CAST_IR(ircode) (ircode >= CAST_I1_I4 && ircode <= CAST_I4_U2)

enum ircode
{
    #define IRINFO(ircode, str) ircode,
        #include "irinfo.h"
    #undef  IRINFO
};


struct basic_block
{
//    struct ir *ir_list_head;
    struct vector *ir_list;
    struct basic_block *next;

// for optimize
    struct vector *precs;
    struct vector *succs;
    struct hash_table *use_set;
    struct hash_table *def_set;
//    struct hash_table *in_set;
    struct hash_table *out_set;
};

struct proc_block
{
    int kind;   // function or declaration
    struct symbol_func *func_sym; //
    struct proc_block *next;
    struct vector *params;
    struct vector *locals;
    struct vector *externals;
    struct hash_table *temps;
// for ir emit
    struct basic_block *bblist_head;
    struct basic_block *bblist_tail;
    int link;     //use in flow graph construction. if link is set, curr basic block will be added to new basic block's prec list
// for optimizion
    struct hash_table *ref_vars;
// for asm emit
    struct symbol_temp *return_val;
    int shared_mem_size;
};

// for generate ir of call only
struct arg_list
{
    struct symbol *sym;
    struct vector *ud_chain;
    struct arg_list *next;
};
// pre_proc_*: (for ir and asm) process before reference. it could be ADDR or DEREF
struct ir
{
    enum ircode ircode;
    struct symbol *lop;
//    int pre_proc_lop;
//    int lop_is_ia;
    struct symbol *rop;
//    int pre_proc_rop;
//    int rop_is_ia;
    struct symbol *rsltop;
//    int pre_proc_rsltop;
//    int rsltop_is_ia;
// for optimize
    int unused;
    struct vector *du_chain;
//    struct vector *ud_chain_rsltop;
    struct vector *ud_chain_lop;
    struct vector *ud_chain_rop;
// for temp mem share
    int ir_no;          // the order this ir in the basic block
    struct basic_block *bb_belonging;   // the basic block this ir belong to
};

enum
{
    I1, U1, I2, U2, I4, U4, F4, F8,
};



extern struct proc_block *pb_head;
extern struct proc_block *cur_pb;

int get_type_size(int categ);

void translation_unit_translate(struct decl_trans_unit_node *trans_unit);
void add_ir(int ircode, struct symbol *rsltop, struct symbol *lop, struct symbol *rop);
char *get_irstr(int ir_code);
void test_and_create_bb(int test);

void opt_init();
void optimize(struct proc_block *pb);
int share_temp_memory(struct proc_block *pb);

#endif
