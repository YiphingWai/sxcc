#ifndef __FOLD_H__
#define __FOLD_H__


#include "expr.h"



// binrary operator
#define EXECUTE_BOP(expr1, op, expr2, val) \
    if (expr1->type->categ == INT && expr2->type->categ == INT) {val.i = expr1->value.i op expr2->value.i;} \
    else if (expr1->type->categ == UINT && expr2->type->categ == UINT) {val.ui = expr1->value.ui op expr2->value.ui;} \
    else if (expr1->type->categ == LONG && expr2->type->categ == LONG) {val.l = expr1->value.l op expr2->value.l;} \
    else if (expr1->type->categ == ULONG && expr2->type->categ == ULONG) {val.ul = expr1->value.ul op expr2->value.ul;} \
    else if (expr1->type->categ == FLOAT && expr2->type->categ == FLOAT) {val.f = expr1->value.f op expr2->value.f;} \
    else if (expr1->type->categ == DOUBLE && expr2->type->categ == DOUBLE) {val.d = expr1->value.d op expr2->value.d;} \
    else {assert(0);}
// relational operator
#define EXECUTE_ROP(expr1, op, expr2, val) \
    if (expr1->type->categ == INT && expr2->type->categ == INT) {val.i = expr1->value.i op expr2->value.i;} \
    else if (expr1->type->categ == UINT && expr2->type->categ == UINT) {val.i = expr1->value.ui op expr2->value.ui;} \
    else if (expr1->type->categ == LONG && expr2->type->categ == LONG) {val.i = expr1->value.l op expr2->value.l;} \
    else if (expr1->type->categ == ULONG && expr2->type->categ == ULONG) {val.i = expr1->value.ul op expr2->value.ul;} \
    else if (expr1->type->categ == FLOAT && expr2->type->categ == FLOAT) {val.i = expr1->value.f op expr2->value.f;} \
    else if (expr1->type->categ == DOUBLE && expr2->type->categ == DOUBLE) {val.i = expr1->value.d op expr2->value.d;} \
    else {assert(0);}


struct expr_node *constant_fold(struct expr_node *expr);
struct expr_node *cast_fold(struct type *dst_type, struct expr_node *expr);







#endif
