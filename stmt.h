#ifndef __STMT_H__
#define __STMT_H__

#include "parse.h"
#include "expr.h"
#include "decl.h"

// expression-statement
// NK_ExpressionStatement
struct stmt_expr_node
{
    NODE_COMMON
    struct expr_node *expr;
};

// labeled-statement
// NK_LabelStatement
struct stmt_lbl_node
{
    NODE_COMMON

    struct common_node *stmt;
    char *name;     // for LabelStatement
// fill by ir emit
    struct symbol *lbl_sym;
};
// NK_CaseStatement
struct stmt_case_node
{
    NODE_COMMON
//    struct common_node *stmt;
    struct expr_node *expr; //for CaseStatement
// fill by ir emit
    struct symbol *lbl_sym;
};
// NK_DefaultStatement
struct stmt_default_node
{
    NODE_COMMON
//    struct common_node *stmt;
// fill by ir emit
    struct symbol *lbl_sym;
};
// selection-statement
// NK_IfStatement, 
struct stmt_if_node
{
    NODE_COMMON

    struct expr_node *expr;
    struct common_node *then_stmt;
    struct common_node *else_stmt;
// fill by semetic check    
    struct symbol *else_lbl;
    struct symbol *end_lbl;
};
// NK_SwitchStatement
struct stmt_switch_node
{
    NODE_COMMON
    struct expr_node *expr;
    struct common_node *stmt;
// fill by semetic check    
    struct vector *case_stmts;
    struct stmt_default_node *default_stmt;
    struct symbol *end_lbl;
};
// iteration-statement
// NK_WhileStatement
struct stmt_while_node
{
    NODE_COMMON
    struct expr_node *expr;
    struct common_node *stmt;
// fill by semetic check 
    struct symbol *begin_lbl;
    struct symbol *end_lbl;
};
// NK_DoStatement
struct stmt_do_node
{
    NODE_COMMON

    struct expr_node *expr;
    struct common_node *stmt;
// fill by semetic check 
    struct symbol *begin_lbl;
    struct symbol *cond_lbl;
    struct symbol *end_lbl;
};
// NK_ForStatement
struct stmt_for_node
{
    NODE_COMMON

    struct expr_node *expr1, *expr2, *expr3;
    struct common_node *stmt;
// fill by semetic check 
    struct symbol *cond_lbl;
    struct symbol *iter_lbl;
    struct symbol *inc_lbl;
    struct symbol *end_lbl;
};
// jump-statement
// NK_GotoStatement
struct stmt_goto_node
{
    NODE_COMMON

    char *lab_name;     // for goto
// fill by semetic check 
    struct symbol *dest_lbl;
};
// NK_BreakStatement
struct stmt_break_node
{
    NODE_COMMON
// fill by semetic check 
    struct symbol *dest_lbl;   
};
// NK_ContinueStatement
struct stmt_continue_node
{
    NODE_COMMON
// fill by semetic check 
    struct symbol *dest_lbl;  
};
// NK_ReturnStatement
struct stmt_return_node
{
    NODE_COMMON
// fill by semetic check 
    struct symbol *dest_lbl;
    struct symbol *return_val;
    struct expr_node *expr; // for return
};
// compound-statement
// NK_CompoundStatement
struct stmt_comp_node
{
    NODE_COMMON

    struct decl_node *decl_list;
    struct common_node *stmt;
};


struct stmt_expr_node *expression_statement();
struct stmt_lbl_node *labeled_statement();
struct stmt_sel_node *selection_statement();
struct stmt_iter_node *iteration_statement();
struct stmt_jmp_node *jump_statement();
struct stmt_comp_node *compound_statement();
struct common_node *statement();



////////////////////////////////////////
struct stmt_comp_node *compound_statement_check(struct stmt_comp_node *comp_stmt);
struct common_node *statement_check(struct common_node *stmt);
////////////////////////////////////////

#endif
