#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>
#include "tran.h"
#include "fold.h"
#include "type.h"
#include "hash.h"
#include "symtab.h"
#include "sxcc.h"
#include "opt.h"

struct hash_table *global_vars_static = NULL;
struct hash_table *global_vars_extern = NULL;
//struct vector *global_ref_vars = NULL;
void (*record_def_cb)(struct symbol *sym) = NULL;
void (*record_use_cb)(struct symbol *sym) = NULL;

void add_item_to_sets(struct hash_table *sets, void *item)
{
    char name[256];
    sprintf(name, "%p", item);
    sets->insert(sets, name, item);
}

void test_and_add_item_to_sets(struct hash_table *sets, void *item)
{
    char name[256];
    sprintf(name, "%p", item);
    if (!sets->find(sets, name, NULL))
    {
        return ;
    }
    sets->insert(sets, name, item);
}

void *find_item_from_sets(struct hash_table *sets, void *item)
{
    char name[256];
    void *rslt;
    sprintf(name, "%p", item);
    if (!sets->find(sets, name, &rslt))
    {
        return rslt;
    }
    return NULL;
}

void delete_item_from_sets(struct hash_table *sets, void *item)
{
    char name[256];
    void *rslt;
    sprintf(name, "%p", item);
    sets->delete(sets, name);
}

void *delete_item_from_array(struct vector *vec, void *item)
{
    int i;
    void *p;
    for (i = 0; i < vec->len; i++)
    {
        p = vec->get(vec, i);
        if (item == p)
        {
            vec->erase(vec, i);
            break;
        }
    }
    return p;
}
// use for succs array, which number of members can only be 0, 1, 2
void *delete_another_item_from_succs(struct vector *vec, void *item)
{
    int i;
    void *p;
    for (i = 0; i < vec->len; i++)
    {
        p = vec->get(vec, i);
        if (item != p)
        {
            vec->erase(vec, i);
            break;
        }
    }
    return p;
}

void record_def(struct basic_block *bb, struct symbol *item)
{
    if (item->kind == SK_Variable || item->kind == SK_Temp)
    {
        test_and_add_item_to_sets(bb->def_set, item);
        if (record_def_cb)
        {
            record_def_cb(item);
        }
    }
}

void record_all_ref_def(struct proc_block *pb, struct basic_block *bb)
{
    int i, cnt;
    struct symbol *sym;
    for (i = 0, cnt = 0; sym = pb->ref_vars->hash_iter(pb->ref_vars, &i, &cnt);)
    {
        record_def(bb, sym);
    }
    for (i = 0, cnt = 0; sym = pb_head->ref_vars->hash_iter(pb_head->ref_vars, &i, &cnt);)
    {
        record_def(bb, sym);
    }
}

void record_use(struct basic_block *bb, struct symbol *item)
{
    if (item->kind == SK_Variable || item->kind == SK_Temp)
    {
        if (!find_item_from_sets(bb->def_set, item))
        {
            test_and_add_item_to_sets(bb->use_set, item);
        }
        if (record_use_cb)
        {
            record_use_cb(item);
        }
    }
}

void record_all_ref_use(struct proc_block *pb, struct basic_block *bb)
{
    int i, cnt;
    struct symbol *sym;
    for (i = 0, cnt = 0; sym = pb->ref_vars->hash_iter(pb->ref_vars, &i, &cnt);)
    {
        record_use(bb, sym);
    }
    for (i = 0, cnt = 0; sym = pb_head->ref_vars->hash_iter(pb_head->ref_vars, &i, &cnt);)
    {
        record_use(bb, sym);
    }
}

void get_def_use_set(struct proc_block *pb, int redo)
{
    int i, j, cnt;
    struct ir *ir;
    struct symbol *sym;
    struct basic_block *bb;
    struct arg_list *arg_list_head;

    if (redo)
    {
        pb->ref_vars->clear(pb->ref_vars);
    }

    for (bb = pb->bblist_head; bb; bb = bb->next)
    {
        if (redo)
        {
            bb->def_set->clear(bb->def_set);
            bb->use_set->clear(bb->use_set);
            bb->out_set->clear(bb->out_set);
        }
        for (j = 0; j < bb->ir_list->len; j++)
        {
            ir = bb->ir_list->get(bb->ir_list, j);
            if (redo)
            {
                ir->du_chain->clear(ir->du_chain);
                if (ir->lop)
                {
                    ir->ud_chain_lop->clear(ir->ud_chain_lop);
                }
                if (ir->rop)
                {
                    ir->ud_chain_rop->clear(ir->ud_chain_rop);
                }
                ir->ir_no = j;
                ir->bb_belonging = bb;
            }
            
            if (IS_BOP_IR(ir->ircode))
            {
                record_use(bb, ir->lop);
                if (ir->rop)    // ADD T.0, a, b
                {
                    record_use(bb, ir->rop);
                }
                record_def(bb, ir->rsltop);
            }
            else if (ir->ircode == INC || ir->ircode == DEC)
            {
                // record_use MUST be called before record_def
                record_use(bb, ir->lop);
                record_def(bb, ir->rsltop);
            }
            else if (ir->ircode == LOAD)
            {
                record_use(bb, ir->lop);(bb, ir->lop);
                record_all_ref_use(pb, bb);
                record_def(bb, ir->rsltop);
            }
            else if (ir->ircode == STORE)
            {
                record_use(bb, ir->rsltop);
                record_use(bb, ir->lop);
                record_all_ref_def(pb, bb);
            }
            else if (ir->ircode == GETADDR)
            {
                record_use(bb, ir->lop);
                record_def(bb, ir->rsltop);
                test_and_add_item_to_sets(pb->ref_vars, ir->lop);
            }
            else if (ir->ircode == CALL)
            {
                if (ir->lop->defined)
                {
                    for (i = 0, cnt = 0; sym = global_vars_static->hash_iter(global_vars_static, &i, &cnt);)
                    {
                        record_use(bb, sym);
                        record_def(bb, sym);
                    }
                }
                for (i = 0, cnt = 0; sym = global_vars_extern->hash_iter(global_vars_extern, &i, &cnt);)
                {
                    record_use(bb, sym);
                    record_def(bb, sym);
                }

                arg_list_head = (struct arg_list *)(ir->rop);
                for (; arg_list_head; arg_list_head = arg_list_head->next)
                {
                    record_use(bb, arg_list_head->sym);
                }
                record_def(bb, ir->rsltop);
            }
            else if (ir->ircode == JT || ir->ircode == JNT)
            {
//                record_use(bb, ir->rsltop);
//                assert(!ir->pre_proc_lop);
                record_use(bb, ir->lop);
            }
            else if (ir->ircode == JE || ir->ircode == JNE)
            {
//                record_use(bb, ir->rsltop);
//                assert(!ir->pre_proc_lop);
                record_use(bb, ir->lop);
//                assert(!ir->pre_proc_rop);
                record_use(bb, ir->rop);
            }
            else if (ir->ircode == JMP)
            {
//                record_use(bb, ir->rsltop);
            }
            else if (IS_CAST_IR(ir->ircode))
            {
                record_def(bb, ir->rsltop);
//                assert(!ir->pre_proc_lop);
                record_use(bb, ir->lop);
            }
            else if (ir->ircode == RET)
            {
                record_use(bb, ir->rsltop);
            }
            else 
            {
                assert(ir->ircode == LABEL);
            }
        }
    }
    
}

int get_out_set(struct basic_block *bb)
{
    int i, j, cnt;
    int iter_flag = 0;
    struct symbol *sym;
    struct basic_block *succ;

    if (bb->succs)
    {
        for (i = 0; i < bb->succs->len; i++)
        {
            succ = bb->succs->get(bb->succs, i);
            j = 0;
            cnt = 0;
            while (sym = succ->use_set->hash_iter(succ->use_set, &j, &cnt))
            {
                if (!find_item_from_sets(bb->out_set, sym))
                {
                    add_item_to_sets(bb->out_set, sym);
                    iter_flag = 1;
                }
            }
        }
    }
    return iter_flag;
}

int get_in_set(struct basic_block *bb)
{
    int i, cnt;
    int iter_flag = 0;
    struct symbol *sym;
    i = 0;
    cnt = 0;
    while (sym = bb->out_set->hash_iter(bb->out_set, &i, &cnt))
    {
        if (!find_item_from_sets(bb->def_set, sym))
        {
            if (!find_item_from_sets(bb->use_set, sym))
            {
                add_item_to_sets(bb->use_set, sym);
                iter_flag = 1;
            }
        }
    }
    return iter_flag;
}

void get_in_out_set(struct proc_block *pb)
{
    int iter_flag = 1;
    struct ir *ir;
    struct basic_block *bb;
    struct arg_list *arg_list_head;

    while (iter_flag)
    {
        iter_flag = 0;
        for (bb = pb->bblist_head; bb; bb = bb->next)
        {
            iter_flag |= get_out_set(bb);
            iter_flag |= get_in_set(bb);
        }
    }
}


int check_and_record_du_chain(struct symbol *op, struct ir *ir, struct vector *du_chain)
{
    struct arg_list *arg;
    if (ir->ircode == RET && ir->rsltop == op)  // in this case, op must have been assigned, search ended
    {
        du_chain->push_back(du_chain, ir);
    }
    else if (ir->ircode == CALL)
    {
        arg = ir->rop;
        if (ir->lop == op)
        {
            du_chain->push_back(du_chain, ir);
        }
        else
        {
            for (; arg; arg = arg->next)
            {
                if (arg->sym == op)
                {
                    du_chain->push_back(du_chain, ir);
                    break;
                }
            }
        }
    }
    else if (ir->ircode == STORE)
    {
        if (ir->rsltop == op)
        {
            du_chain->push_back(du_chain, ir);
        }
        else if (ir->lop == op)
        {
            du_chain->push_back(du_chain, ir);
        }
    }
    else
    {
        if (ir->lop == op)
        {
            du_chain->push_back(du_chain, ir);
        }
        else if (ir->rop == op)
        {
            du_chain->push_back(du_chain, ir);
        }
    }

    if (ir->rsltop == op && ir->ircode != STORE)   // found an ir which op is assigned, search is ended
    {
        return 0;
    }
    return 1;
}
int visit_flag = 0;
static struct hash_table *visited;
void get_du_chain_for_operand(struct symbol *op, int ir_no, struct basic_block *ori_bb, 
                    struct basic_block *cur_bb, struct vector *du_chain)
{
    int i;
    int end;
    struct basic_block *succ_bb;
    struct ir *ir;

    int cnt = 0;
    struct symbol *sym;

    end = cur_bb->ir_list->len;
    if (find_item_from_sets(visited, cur_bb))
    {
        return;
    }

    if (cur_bb == ori_bb)
    {
        if (visit_flag)
        {
            add_item_to_sets(visited, cur_bb);
            end = ir_no + 1;
        }
        else
        {
            visit_flag = 1;
            goto pass;
        }
    }
    else
    {
        add_item_to_sets(visited, cur_bb);
    }
    
    for (i = 0; i < end; i++)
    {
        ir = cur_bb->ir_list->get(cur_bb->ir_list, i);
        if (!check_and_record_du_chain(op, ir, du_chain))
        {
            return;
        }
    }
    
pass:
    for (i = 0; i < cur_bb->succs->len; i++)
    {
        succ_bb = cur_bb->succs->get(cur_bb->succs, i);
        if (find_item_from_sets(succ_bb->use_set, op))
        {
            get_du_chain_for_operand(op, ir_no, ori_bb, succ_bb, du_chain);
        }
        else if (succ_bb == 0x6fc150 && op == 0x6bb9a0)
        {
            i = 0;
            cnt = 0;
            while (sym = succ_bb->use_set->hash_iter(succ_bb->use_set, &i, &cnt))
            {
                printf("%s ", sym->name);
            }
        }
        
    }
}
int get_du_chain_for_operand_in_bb(struct symbol *op, int ir_no, struct basic_block *cur_bb, 
                                     struct vector *du_chain)
{
    struct ir *ir;

    for (; ir_no < cur_bb->ir_list->len; ir_no++)
    {
        ir = cur_bb->ir_list->get(cur_bb->ir_list, ir_no);
        if (!check_and_record_du_chain(op, ir, du_chain))
        {
            return 0;
        }
    }
    return 1;
}

void get_du_chain(struct proc_block *pb)
{
    int i, j;
    struct ir *ir;
    struct basic_block *bb;
    for (bb = pb->bblist_head; bb; bb = bb->next)
    {
        for (i = 0; i < bb->ir_list->len; i++)
        {
            ir = bb->ir_list->get(bb->ir_list, i);
            if (IS_JMP_IR(ir->ircode) || ir->ircode == LABEL || ir->ircode == STORE)
            {
                continue;
            }

            if (get_du_chain_for_operand_in_bb(ir->rsltop, i + 1, bb, ir->du_chain))
            {
                if (visited)
                {
                    visited->clear(visited);
                }
                else
                {
                    visited = hash_init();
                }
                visit_flag = 0;
                get_du_chain_for_operand(ir->rsltop, i, bb, bb, ir->du_chain);
            }
        }
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////
int has_been_addressed(struct symbol *op, struct proc_block *pb)
{
    if (find_item_from_sets(pb->ref_vars, op))
    {
        return 1;
    }
    if (find_item_from_sets(pb_head->ref_vars, op))
    {
        return 1;
    }
    return 0;
}
int check_and_record_ud_chain(struct symbol *op, struct ir *ir, struct proc_block *pb, struct vector *ud_chain)
{
    if (op->kind == SK_Variable && ((struct symbol_var *)op)->is_global &&
        (ir->ircode == CALL || ir->ircode == STORE))
    {
        // for global variable, all function call and indirect addressing are seen as definition
        goto record;
    }
    if (has_been_addressed(op, pb) && (ir->ircode == CALL || ir->ircode == STORE))
    {
        // for variable which has been addressed, all function call and indirect addressing are seen as definition
        goto record;
    }
    if (ir->rsltop == op && !IS_JMP_IR(ir->ircode) && ir->ircode != LABEL && ir->ircode != STORE)   // found an ir which op is assigned, search is ended
    {
        goto record;
    }
    return 1;

record:
    ud_chain->push_back(ud_chain, ir);
    return 0;
}

void get_ud_chain_for_operand(struct symbol *op, int ir_no, struct basic_block *ori_bb, 
                    struct basic_block *cur_bb, struct proc_block *pb, struct vector *ud_chain)
{
    int i;
    int end = 0;
    struct basic_block *prec_bb;
    struct ir *ir;

    if (find_item_from_sets(visited, cur_bb))
    {
        return;
    }

    if (cur_bb == ori_bb)
    {
        if (visit_flag)
        {
            // 若能第二次访问自身，说明有环路，此时应检查并记录本bb中在原ir后面的那些ir（包括原ir本身）
            add_item_to_sets(visited, cur_bb);
            end = ir_no;
        }
        else
        {
            visit_flag = 1;
            // 第一次访问自身basic_block时，必要的记录工作已经在get_ud_chain_for_operand_in_bb做过了，此处直接跳过
            goto pass;
        }
    }
    else
    {
        add_item_to_sets(visited, cur_bb);
    }
    
    for (i = cur_bb->ir_list->len - 1; i >= end; i--)
    {
        ir = cur_bb->ir_list->get(cur_bb->ir_list, i);
        if (!check_and_record_ud_chain(op, ir, pb, ud_chain))
        {
            return;
        }
    }

    if (cur_bb == pb->bblist_head && op->kind == SK_Variable && 
        (((struct symbol_var *)op)->is_global || ((struct symbol_var *)op)->is_para))
    {
        // reach here means we the op hasn't been init in current function
        // for global variables and arguments, we push a NULL into its ud chain to indicate there are a uninit path
        // the info will be used in propagate
        ud_chain->push_back(ud_chain, NULL);
    }
    
pass:
    for (i = 0; cur_bb->precs && i < cur_bb->precs->len; i++)
    {
        prec_bb = cur_bb->precs->get(cur_bb->precs, i);
        if (find_item_from_sets(prec_bb->out_set, op))
        {
            get_ud_chain_for_operand(op, ir_no, ori_bb, prec_bb, pb, ud_chain);
        }
    }
}

// return 0 means we have found an ir in this bb which define the op, further search is not necessery.
// return 1 means we have not found such ir in this bb yet and we need to call get_ud_chain_for_operand() to find definition ir from other bb
int get_ud_chain_for_operand_in_bb(struct symbol *op, int ir_no, struct basic_block *cur_bb, 
                                    struct proc_block *pb, struct vector *ud_chain)
{
    int i;
    struct ir *ir;

    for (i = ir_no; i >= 0; i--)
    {
        ir = cur_bb->ir_list->get(cur_bb->ir_list, i);
        if (!check_and_record_ud_chain(op, ir, pb, ud_chain))
        {
            return 0;
        }
    }
   
    if (cur_bb == pb->bblist_head && op->kind == SK_Variable && 
        (((struct symbol_var *)op)->is_global || ((struct symbol_var *)op)->is_para))
    {
        // reach here means we the op hasn't been init in current function
        // for global variables and arguments, we push a NULL into its ud chain to indicate there are a uninit path
        // the info will be used in propagate
        ud_chain->push_back(ud_chain, NULL);
    }
    
    return 1;
}

void get_ud_chain(struct proc_block *pb)
{
    int i;
    struct symbol *op;
    struct arg_list *arg;
    struct ir *ir;
    struct basic_block *bb;
    for (bb = pb->bblist_head; bb; bb = bb->next)
    {
        for (i = 0; i < bb->ir_list->len; i++)
        {
            ir = bb->ir_list->get(bb->ir_list, i);
            if (ir->ircode == LABEL)
            {
                continue;
            }
            if (ir->ircode == CALL)
            {
                for (arg = ir->rop; arg; arg = arg->next)
                {
                    op = arg->sym;
                    if (get_ud_chain_for_operand_in_bb(op, i - 1, bb, pb, arg->ud_chain))
                    {
                        if (visited)
                        {
                            visited->clear(visited);
                        }
                        else
                        {
                            visited = hash_init();
                        }
                        visit_flag = 0;
                        get_ud_chain_for_operand(op, i, bb, bb, pb, arg->ud_chain);
                    }
                }
            }
            else if (ir->ircode == RET)
            {
                op = ir->rsltop;
                if (get_ud_chain_for_operand_in_bb(op, i - 1, bb, pb, ir->ud_chain_lop))
                {
                    if (visited)
                    {
                        visited->clear(visited);
                    }
                    else
                    {
                        visited = hash_init();
                    }
                    visit_flag = 0;
                    get_ud_chain_for_operand(op, i, bb, bb, pb, ir->ud_chain_lop);
                }
            }
            else if (ir->ircode == GETADDR)
            {
                /*
                why don't optimize this case?
                op = ir->rsltop;
                if (op && ir->rsltop_is_ia)
                {
                    if (get_ud_chain_for_operand_in_bb(op, i - 1, bb, ir->ud_chain_rsltop))
                    {
                        if (visited)
                        {
                            visited->clear(visited);
                        }
                        else
                        {
                            visited = hash_init();
                        }
                        visit_flag = 0;
                        get_ud_chain_for_operand(op, i, bb, bb, ir->ud_chain_rsltop);
                    }
                }
                */
            }
            else
            {
                /*
                op = ir->rsltop;
                if (op && ir->pre_proc_rsltop == DEREF)
                {
                    if (get_ud_chain_for_operand_in_bb(op, i - 1, bb, ir->ud_chain_rsltop))
                    {
                        if (visited)
                        {
                            visited->clear(visited);
                        }
                        else
                        {
                            visited = hash_init();
                        }
                        visit_flag = 0;
                        get_ud_chain_for_operand(op, i, bb, bb, ir->ud_chain_rsltop);
                    }
                }
                */
                op = ir->lop;
                if (op)
                {
                    if (get_ud_chain_for_operand_in_bb(op, i - 1, bb, pb, ir->ud_chain_lop))
                    {
                        if (visited)
                        {
                            visited->clear(visited);
                        }
                        else
                        {
                            visited = hash_init();
                        }
                        visit_flag = 0;
                        get_ud_chain_for_operand(op, i, bb, bb, pb, ir->ud_chain_lop);
                    }
                }
                op = ir->rop;
                if (op)
                {
                    if (get_ud_chain_for_operand_in_bb(op, i - 1, bb, pb, ir->ud_chain_rop))
                    {
                        if (visited)
                        {
                            visited->clear(visited);
                        }
                        else
                        {
                            visited = hash_init();
                        }
                        visit_flag = 0;
                        get_ud_chain_for_operand(op, i, bb, bb, pb, ir->ud_chain_rop);
                    }
                }
            }
        }
    }
}
//--------------------------------------------------------------------------------------
struct symbol *do_constant_cast(struct ir *ir)
{
    union val value;
    if (!IS_CONST_SYMBOL(ir->lop))
    {
        return ir->lop;
    }
    switch (ir->ircode)
    {   // src dst
        case CAST_I4_U4:
            value.ui = (unsigned int)ir->lop->value.i;
            break;
        case CAST_U4_I4:
            value.i = (int)ir->lop->value.ui;
            break;
        case CAST_I4_F4:
            value.f = (float)ir->lop->value.i;
            break;
        case CAST_I4_F8:
            value.d = (double)ir->lop->value.i;
            break;
        case CAST_U4_F4:
            value.f = (float)ir->lop->value.ui;
            break;
        case CAST_U4_F8:
            value.d = (double)ir->lop->value.ui;
            break;
        case CAST_F4_I4:
            value.i = (int)ir->lop->value.f;
            break;
        case CAST_F4_U4:
            value.ui = (unsigned int)ir->lop->value.f;
            break;
        case CAST_F4_F8:
            value.d = (double)ir->lop->value.f;
            break;
        case CAST_F8_I4:
            value.i = (int)ir->lop->value.d;
            break;
        case CAST_F8_U4:
            value.ui = (unsigned int)ir->lop->value.d;
            break;
        case CAST_F8_F4:
            value.f = (float)ir->lop->value.d;
            break;
        case CAST_I4_U1:
            value.i = (unsigned char)ir->lop->value.i;
            break;
        case CAST_I4_I1:
            value.i = (char)ir->lop->value.i;
            break;
        case CAST_I4_I2:
            value.i = (short)ir->lop->value.i;
            break;
        case CAST_I4_U2:
            value.i = (unsigned short)ir->lop->value.i;
            break;
        case CAST_I1_I4:
            value.i = (unsigned short)ir->lop->value.i;
            break;
/*        case CAST_U1_I4:
            break;
        case CAST_I2_I4:
            break;
        case CAST_U2_I4:
            break;
        case CAST_I1_U1:
            break;
        case CAST_U1_I1:
            break;
        case CAST_I2_U2:
            break;
        case CAST_U2_I2:
            break;*/
        default:
            ir->lop->type = ir->rsltop->type;
            return ir->lop;
    }
    return sym_add_constant(ir->rsltop->type, value);
}
/*
struct symbol *do_constant_inc_dec (struct ir *ir)
{
    union val value;
    switch (ir->rsltop->type->categ)
    {
        case CHAR:
        case UCHAR:
        case SHORT:
        case USHORT:
        case INT:
            value.i = ir->rsltop->value.i + 1;
            break;
        case UINT:
            value.ui = ir->rsltop->value.ui + 1;
            break;
        case LONG:
            value.l = ir->rsltop->value.l + 1;
            break;
        case ULONG:
            value.ul = ir->rsltop->value.ul + 1;
            break;
        case FLOAT:
            value.f = ir->rsltop->value.f + 1;
            break;
        case DOUBLE:
            value.d = ir->rsltop->value.d + 1;
            break;
    }
    if (ir->ircode == INC)
    {
        ir->rop = sym_add_constant(ir->rsltop->type, ir->rsltop->value);
    }
}
*/
int const_copy_prop_for_op(struct symbol **op, struct ir *src_ir, struct proc_block *pb, struct vector *ud_chain)
{
    int i;
    int flag = 0;
    struct ir *ir;
    struct init_data *init_data;
    struct symbol *const_sym = NULL, *last_const_sym = NULL;
    if (!ud_chain->len)
    {
        return 0;
    }
    if ((*op)->kind == SK_Constant || (*op)->kind == SK_EnumConstant || (*op)->kind == SK_String)
    {
        return 0;
    }
    for (i = 0; i < ud_chain->len; i++)
    {
        ir = ud_chain->get(ud_chain, i);
        
        if (!ir)
        {
            // means the op must a argument or a global variable and has an uninit path
            return 0;
        }

        if (ir->ircode != ASSIGN && !IS_CAST_IR(ir->ircode))
        {
            return 0;
        }

        const_sym = IS_CAST_IR(ir->ircode) ? do_constant_cast(ir) : ir->lop;
        
        if (IS_CONST_SYMBOL(const_sym)) // constant propagate
        {
            // do nothing
        }
        else if (const_sym->kind == SK_Temp || const_sym->kind == SK_Variable)
        {
            return 0;
            if (IS_CAST_IR(ir->ircode))     // copy propagate can not across CAST
            {
                return 0;
            }
        }
        else
        {
            return 0;
        }
        
        if ((*op)->kind == SK_Variable)
        {
            if (((struct symbol_var*)(*op))->is_global)    // for global variable
            {
                /* for global variable, the propagate is not proformed */
                if (ir->ircode == CALL || ir->ircode == STORE)
                {
                    // reach here means the op may assigned by a function call or a indirect accessing
                    return 0;
                }
               
            }
            if (has_been_addressed(*op, pb))
            {
                /*
                1、求ud链时，所有函数调用、rslt_op为间指的，视为定值
                2、传播优化（此处）时，若遇到call、间指则return 0
                */
                if (ir->ircode == CALL || ir->ircode == STORE)
                {
                    // reach here means the op may assigned by a function call or a indirect accessing
                    return 0;
                }
            }
        }

        if (last_const_sym)
        {
            if (last_const_sym != const_sym)
            {
                return 0;
            }
        }
        last_const_sym = const_sym;
    }
    if (const_sym == *op)  // there is no change in this turn
    {
        return 0;
    }
// do const & copy propagate
    *op = const_sym;

// end do const & copy propagate
// after propagate, we need to clear ud_chain for constant propagate or stop iter at a certain circumstance for copy propagate
    if (IS_CONST_SYMBOL(const_sym)) // constant propagate
    {
        ud_chain->clear(ud_chain);
    }
    
    return 1;
}

int const_copy_prop_for_ir(struct ir *ir, struct proc_block *pb)
{
    struct arg_list *arg;
    struct symbol *op;
    int flag = 0;
    if (ir->ircode == LABEL || ir->ircode == RET || ir->ircode == GETADDR || 
        ir->ircode == INC || ir->ircode == DEC)
    {
        return 0;
    }
    if (ir->ircode == CALL)
    {
        for (arg = ir->rop; arg; arg = arg->next)
        {
            flag |= const_copy_prop_for_op(&(arg->sym), ir, pb, arg->ud_chain);
        }
    }
    else
    {
        /*
        op = ir->rsltop;
        if (op && ir->pre_proc_rsltop == DEREF)
        {
            flag |= const_copy_prop_for_op(&(ir->rsltop), &(ir->pre_proc_rsltop), ir->ud_chain_rsltop);
        }
        */
        op = ir->lop;
        if (op)
        {
            flag |= const_copy_prop_for_op(&(ir->lop), ir, pb, ir->ud_chain_lop);
        }
        op = ir->rop;
        if (op)
        {
            flag |= const_copy_prop_for_op(&(ir->rop), ir, pb, ir->ud_chain_rop);
        }
    }
    return flag;
}

int const_copy_prop(struct proc_block *pb)
{
    int i;
    int flag = 0;
    int cnt = 0;
    struct ir *ir;
    struct basic_block *bb;
    do 
    {
        flag = 0;
        for (bb = pb->bblist_head; bb; bb = bb->next)
        {
            for (i = 0; i < bb->ir_list->len; i++)
            {
                ir = bb->ir_list->get(bb->ir_list, i);
                flag |= const_copy_prop_for_ir(ir, pb);
            }
        }
        cnt++;
    } while(flag);
    return cnt > 1;
}

/*-------------------------------------------------------------------------------*/
void exchange_op(struct ir *ir)
{
    int tmp;
    struct symbol *tmp_sym;
    struct vector *tmp_vec;

    tmp_sym = ir->lop;
    ir->lop = ir->rop;
    ir->rop = tmp_sym;

    tmp_vec = ir->ud_chain_lop;
    ir->ud_chain_lop = ir->ud_chain_rop;
    ir->ud_chain_rop = tmp_vec;
}

int algebraic_simplify_for_ir(struct ir *ir)
{
    union val value;
    struct symbol *sym;
    if (ir->lop && !IS_CONST_SYMBOL(ir->lop))
    {
        if (ir->rop && IS_CONST_SYMBOL(ir->rop))
        {
            goto ok;
        }
    }
    else if (ir->lop)
    {
        goto ok;
    }
    return 0;
ok:
    if (ir->ircode == ADD)
    {
        if (IS_CONST_SYMBOL(ir->lop))
        {
            exchange_op(ir);
        }
        if (IS_CONST_SYMBOL(ir->lop))   // i = 5 + 6
        {
            assert(ir->rsltop->type->size == ir->lop->type->size && 
                    ir->rsltop->type->size == ir->rop->type->size);
            EXECUTE_BOP_OPT(ir->rsltop->type, ir->lop->value, +, ir->rop->value, value);

            ir->ircode = ASSIGN;
            ir->lop = sym_add_constant(ir->rsltop->type, value);
            ir->rop = NULL;
            return 1;
        }
        else if (IS_EQUAL(ir->rop->type, ir->rop->value, 0))
        {
            ir->ircode = ASSIGN;
            ir->rop = NULL;
            return 1;
        }
    }
    else if (ir->ircode == SUB)
    {
        if (IS_CONST_SYMBOL(ir->lop) && IS_CONST_SYMBOL(ir->rop))
        {
            assert(ir->rsltop->type->size == ir->lop->type->size && 
                    ir->rsltop->type->size == ir->rop->type->size);
            EXECUTE_BOP_OPT(ir->rsltop->type, ir->lop->value, -, ir->rop->value, value);

            ir->ircode = ASSIGN;
            ir->lop = sym_add_constant(ir->rsltop->type, value);
            ir->rop = NULL;
            return 1;
        }
        else if (IS_CONST_SYMBOL(ir->rop) && IS_EQUAL(ir->rop->type, ir->rop->value, 0))
        {
            ir->ircode = ASSIGN;
            ir->rop = NULL;
            return 1;
        }
    }
    else if (ir->ircode == MUL)
    {
        if (IS_CONST_SYMBOL(ir->lop))
        {
            exchange_op(ir);
        }
        if (IS_CONST_SYMBOL(ir->lop))   // i = 5 * 6
        {
            assert(ir->rsltop->type == ir->lop->type && ir->rsltop->type == ir->rop->type);
            EXECUTE_BOP_OPT(ir->rsltop->type, ir->lop->value, *, ir->rop->value, value);

            ir->ircode = ASSIGN;
            ir->lop = sym_add_constant(ir->rsltop->type, value);
            ir->rop = NULL;
            return 1;
        }
        else if (IS_EQUAL(ir->rop->type, ir->rop->value, 0)) // i = j * 0;
        {
            ir->ircode = ASSIGN;
            exchange_op(ir);
            vector_destroy(ir->ud_chain_lop);
            ir->rop = NULL;
            return 1;
        }
        else if (IS_EQUAL(ir->rop->type, ir->rop->value, 1)) // i = j * 1
        {
            ir->ircode = ASSIGN;
            ir->rop = NULL;
            return 1;
        }
    }
    else if (ir->ircode == DIV)
    {
        if (IS_CONST_SYMBOL(ir->lop) && IS_CONST_SYMBOL(ir->rop)) // DIV i 5 6
        {
            assert(ir->rsltop->type == ir->lop->type && ir->rsltop->type == ir->rop->type);
            EXECUTE_BOP_OPT(ir->rsltop->type, ir->lop->value, /, ir->rop->value, value);

            ir->ircode = ASSIGN;
            ir->lop = sym_add_constant(ir->rsltop->type, value);
            ir->rop = NULL;
            return 1;
        }
        else if (IS_CONST_SYMBOL(ir->lop) && IS_EQUAL(ir->lop->type, ir->lop->value, 0)) // DIV i 0 i
        {
            ir->ircode = ASSIGN;
            ir->rop = NULL;
            vector_destroy(ir->ud_chain_rop);
            return 1;
        }
        else if (IS_CONST_SYMBOL(ir->rop) && IS_EQUAL(ir->rop->type, ir->rop->value, 1)) // DIV i j 1
        {
            ir->ircode = ASSIGN;
            ir->rop = NULL;
            return 1;
        }
    }
    else if (ir->ircode == MOD)
    {
        if (IS_CONST_SYMBOL(ir->lop) && IS_CONST_SYMBOL(ir->rop)) // MOD i 5 6
        {
            assert(ir->rsltop->type == ir->lop->type && ir->rsltop->type == ir->rop->type);
            EXECUTE_ARITH_BOP_OPT(ir->rsltop->type, ir->lop->value, %, ir->rop->value, value);

            ir->ircode = ASSIGN;
            ir->lop = sym_add_constant(ir->rsltop->type, value);
            ir->rop = NULL;
            return 1;
        }
        else if (IS_CONST_SYMBOL(ir->lop) && IS_EQUAL(ir->lop->type, ir->lop->value, 0)) // MOD i 0 i
        {
            ir->ircode = ASSIGN;
            ir->rop = NULL;
            vector_destroy(ir->ud_chain_rop);
            return 1;
        }
        else if (IS_CONST_SYMBOL(ir->rop) && IS_EQUAL(ir->rop->type, ir->rop->value, 1)) // MOD i j 1
        {
            ir->ircode = ASSIGN;
            value.i = 0;
            ir->lop = sym_add_constant(ir->rsltop->type, value);
            vector_destroy(ir->ud_chain_lop);
            ir->rop = NULL;
            return 1;
        }
    }
    else if (ir->ircode == SHR || ir->ircode == SHL)
    {
        if (IS_CONST_SYMBOL(ir->lop) && IS_CONST_SYMBOL(ir->rop)) // SHR i 5 6
        {
            assert(ir->rsltop->type == ir->lop->type && ir->rsltop->type == ir->rop->type);
            if (ir->ircode == SHR)
            {
                EXECUTE_ARITH_BOP_OPT(ir->rsltop->type, ir->lop->value, >>, ir->rop->value, value);
            }
            else
            {
                EXECUTE_ARITH_BOP_OPT(ir->rsltop->type, ir->lop->value, <<, ir->rop->value, value);
            }

            ir->ircode = ASSIGN;
            ir->lop = sym_add_constant(ir->rsltop->type, value);
            ir->rop = NULL;
            return 1;
        }
        else if (IS_CONST_SYMBOL(ir->lop) && IS_EQUAL(ir->lop->type, ir->lop->value, 0)) // SHR i 0 j
        {
            ir->ircode = ASSIGN;
            ir->rop = NULL;
            vector_destroy(ir->ud_chain_rop);
            return 1;
        }
        else if (IS_CONST_SYMBOL(ir->rop) && IS_EQUAL(ir->rop->type, ir->rop->value, 0)) // SHR i j 0
        {
            ir->ircode = ASSIGN;
            ir->rop = NULL;
            return 1;
        }
    }
    /*
    else if (ir->ircode == AND)
    {
        if (IS_CONST_SYMBOL(ir->lop))
        {
            exchange_op(ir);
        }
        if (IS_CONST_SYMBOL(ir->lop) && IS_CONST_SYMBOL(ir->rop)) // AND i 5 6
        {
            assert(ir->rsltop->type == ir->lop->type && ir->rsltop->type == ir->rop->type);
            EXECUTE_BOP_OPT(ir->rsltop->type, ir->lop->value, &&, ir->rop->value, value);

            ir->ircode = ASSIGN;
            ir->lop = sym_add_constant(ir->rsltop->type, value);
            ir->rop = NULL;
            return 1;
        }
        else if (IS_CONST_SYMBOL(ir->lop) && IS_EQUAL(ir->lop->type, ir->lop->value, 0)) // AND i j 0
        {
            exchange_op(ir);
            ir->ircode = ASSIGN;
            ir->rop = NULL;
            vector_destroy(ir->ud_chain_rop);
            return 1;
        }
        else if (IS_CONST_SYMBOL(ir->rop) && !IS_EQUAL(ir->rop->type, ir->rop->value, 0)) // AND i j 1
        {
            ir->ircode = ASSIGN;
            ir->rop = NULL;
            return 1;
        }
    }
    else if (ir->ircode == OR)
    {
        if (IS_CONST_SYMBOL(ir->lop))
        {
            exchange_op(ir);
        }
        if (IS_CONST_SYMBOL(ir->lop) && IS_CONST_SYMBOL(ir->rop)) // OR i 5 6
        {
            assert(ir->rsltop->type == ir->lop->type && ir->rsltop->type == ir->rop->type);
            EXECUTE_BOP_OPT(ir->rsltop->type, ir->lop->value, ||, ir->rop->value, value);

            ir->ircode = ASSIGN;
            ir->lop = sym_add_constant(ir->rsltop->type, value);
            ir->rop = NULL;
            return 1;
        }
        else if (IS_CONST_SYMBOL(ir->rop) && IS_EQUAL(ir->rop->type, ir->rop->value, 0)) // OR i j 0
        {
            ir->ircode = ASSIGN;
            ir->rop = NULL;
            return 1;
        }
        else if (IS_CONST_SYMBOL(ir->rop) && IS_EQUAL(ir->rop->type, ir->rop->value, 0)) // OR i j 1
        {
            exchange_op(ir);
            ir->ircode = ASSIGN;
            ir->rop = NULL;
            vector_destroy(ir->ud_chain_rop);
            return 1;
        }
    }
    */
    else if (ir->ircode == XOR)
    {
        if (IS_CONST_SYMBOL(ir->lop))
        {
            exchange_op(ir);
        }
        if (IS_CONST_SYMBOL(ir->lop) && IS_CONST_SYMBOL(ir->rop)) // XOR i 5 6
        {
            assert(ir->rsltop->type == ir->lop->type && ir->rsltop->type == ir->rop->type);
            EXECUTE_ARITH_BOP_OPT(ir->rsltop->type, ir->lop->value, ^, ir->rop->value, value);

            ir->ircode = ASSIGN;
            ir->lop = sym_add_constant(ir->rsltop->type, value);
            ir->rop = NULL;
            return 1;
        }
        else if (IS_CONST_SYMBOL(ir->rop) && IS_EQUAL(ir->rop->type, ir->rop->value, 0)) // XOR i j 0
        {
            ir->ircode = ASSIGN;
            ir->rop = NULL;
            return 1;
        }
    }
    else if (ir->ircode == BOR)
    {
        if (IS_CONST_SYMBOL(ir->lop))
        {
            exchange_op(ir);
        }
        if (IS_CONST_SYMBOL(ir->lop) && IS_CONST_SYMBOL(ir->rop)) // XOR i 5 6
        {
            assert(ir->rsltop->type == ir->lop->type && ir->rsltop->type == ir->rop->type);
            EXECUTE_ARITH_BOP_OPT(ir->rsltop->type, ir->lop->value, |, ir->rop->value, value);

            ir->ircode = ASSIGN;
            ir->lop = sym_add_constant(ir->rsltop->type, value);
            ir->rop = NULL;
            return 1;
        }
        else if (IS_CONST_SYMBOL(ir->rop) && IS_EQUAL(ir->rop->type, ir->rop->value, 0)) // XOR i j 0
        {
            ir->ircode = ASSIGN;
            ir->rop = NULL;
            return 1;
        }
    }
    else if (ir->ircode == BAND)
    {
        if (IS_CONST_SYMBOL(ir->lop))
        {
            exchange_op(ir);
        }
        if (IS_CONST_SYMBOL(ir->lop) && IS_CONST_SYMBOL(ir->rop)) // BAND i 5 6
        {
            assert(ir->rsltop->type == ir->lop->type && ir->rsltop->type == ir->rop->type);
            EXECUTE_ARITH_BOP_OPT(ir->rsltop->type, ir->lop->value, &, ir->rop->value, value);

            ir->ircode = ASSIGN;
            ir->lop = sym_add_constant(ir->rsltop->type, value);
            ir->rop = NULL;
            return 1;
        }
        else if (IS_CONST_SYMBOL(ir->rop) && IS_EQUAL(ir->rop->type, ir->rop->value, 0)) // BAND i j 0
        {
            exchange_op(ir);
            ir->ircode = ASSIGN;
            ir->rop = NULL;
            vector_destroy(ir->ud_chain_rop);
            return 1;
        }
    }
    else if (ir->ircode == NOT)
    {
        if (IS_CONST_SYMBOL(ir->lop)) // NOT 5
        {
            assert(ir->rsltop->type == ir->lop->type);
            EXECUTE_SOP_OPT(ir->rsltop->type, !, ir->lop->value, value);

            ir->ircode = ASSIGN;
            ir->lop = sym_add_constant(ir->rsltop->type, value);
            ir->rop = NULL;
            return 1;
        }
    }
    else if (ir->ircode == COMP)
    {
        if (IS_CONST_SYMBOL(ir->lop)) // COMP 5
        {
            assert(ir->rsltop->type == ir->lop->type);
            EXECUTE_ARITH_SOP_OPT(ir->rsltop->type, ~, ir->lop->value, value);

            ir->ircode = ASSIGN;
            ir->lop = sym_add_constant(ir->rsltop->type, value);
            ir->rop = NULL;
            return 1;
        }
    }
    else if (ir->ircode == NEG)
    {
        if (IS_CONST_SYMBOL(ir->lop)) // NEG 5
        {
            assert(ir->rsltop->type == ir->lop->type);
            EXECUTE_ARITH_SOP_OPT(ir->rsltop->type, -, ir->lop->value, value);

            ir->ircode = ASSIGN;
            ir->lop = sym_add_constant(ir->rsltop->type, value);
            ir->rop = NULL;
            return 1;
        }
    }
    else if (ir->ircode == EQ)
    {
        if (IS_CONST_SYMBOL(ir->lop) && IS_CONST_SYMBOL(ir->rop)) // BAND i 5 6
        {
            assert(ir->lop->type == ir->rop->type);
            EXECUTE_ROP_OPT(ir->lop->type, ir->lop->value, ==, ir->rop->value, value);

            ir->ircode = ASSIGN;
            ir->lop = sym_add_constant(ir->rsltop->type, value);
            ir->rop = NULL;
            return 1;
        }
    }
    else if (ir->ircode == NEQ)
    {
        if (IS_CONST_SYMBOL(ir->lop) && IS_CONST_SYMBOL(ir->rop)) // BAND i 5 6
        {
            assert(ir->lop->type == ir->rop->type);
            EXECUTE_ROP_OPT(ir->lop->type, ir->lop->value, !=, ir->rop->value, value);

            ir->ircode = ASSIGN;
            ir->lop = sym_add_constant(ir->rsltop->type, value);
            ir->rop = NULL;
            return 1;
        }
    }
    else if (ir->ircode == LS)
    {
        if (IS_CONST_SYMBOL(ir->lop) && IS_CONST_SYMBOL(ir->rop)) // BAND i 5 6
        {
            assert(ir->lop->type == ir->rop->type);
            EXECUTE_ROP_OPT(ir->lop->type, ir->lop->value, <, ir->rop->value, value);

            ir->ircode = ASSIGN;
            ir->lop = sym_add_constant(ir->rsltop->type, value);
            ir->rop = NULL;
            return 1;
        }
    }
    else if (ir->ircode == GT)
    {
        if (IS_CONST_SYMBOL(ir->lop) && IS_CONST_SYMBOL(ir->rop)) // BAND i 5 6
        {
            assert(ir->lop->type == ir->rop->type);
            EXECUTE_ROP_OPT(ir->lop->type, ir->lop->value, >, ir->rop->value, value);

            ir->ircode = ASSIGN;
            ir->lop = sym_add_constant(ir->rsltop->type, value);
            ir->rop = NULL;
            return 1;
        }
    }
    else if (ir->ircode == LE)
    {
        if (IS_CONST_SYMBOL(ir->lop) && IS_CONST_SYMBOL(ir->rop)) // BAND i 5 6
        {
            assert(ir->lop->type == ir->rop->type);
            EXECUTE_ROP_OPT(ir->lop->type, ir->lop->value, <=, ir->rop->value, value);

            ir->ircode = ASSIGN;
            ir->lop = sym_add_constant(ir->rsltop->type, value);
            ir->rop = NULL;
            return 1;
        }
    }
    else if (ir->ircode == GE)
    {
        if (IS_CONST_SYMBOL(ir->lop) && IS_CONST_SYMBOL(ir->rop)) // BAND i 5 6
        {
            assert(ir->lop->type == ir->rop->type);
            EXECUTE_ROP_OPT(ir->lop->type, ir->lop->value, >=, ir->rop->value, value);

            ir->ircode = ASSIGN;
            ir->lop = sym_add_constant(ir->rsltop->type, value);
            ir->rop = NULL;
            return 1;
        }
    }
    return 0;
}


int algebraic_simplify(struct proc_block *pb)
{
    int i;
    int flag = 0;
    int cnt = 0;
    struct ir *ir;
    struct basic_block *bb;
    do 
    {
        flag = 0;
        for (bb = pb->bblist_head; bb; bb = bb->next)
        {
            for (i = 0; i < bb->ir_list->len; i++)
            {
                ir = bb->ir_list->get(bb->ir_list, i);
                flag |= algebraic_simplify_for_ir(ir);
            }
        }
        cnt++;
    } while(flag);
    return cnt > 1;
}

int conditional_jmp_simplify_for_ir(struct ir *ir, struct basic_block *bb, int ir_no)
{
    int i, ret = 0;
    struct basic_block *p;
    if (ir->ircode == JT)
    {
        if (IS_CONST_SYMBOL(ir->lop))
        {
            if (IS_EQUAL(ir->lop->type, ir->lop->value, 0))
            {
                p = delete_another_item_from_succs(bb->succs, bb->next);
                bb->ir_list->erase(bb->ir_list, ir_no);
                ret = 1;
            }
            else
            {
                p = delete_item_from_array(bb->succs, bb->next);
                ir->ircode = JMP;
                ir->lop = NULL;
                vector_destroy(ir->ud_chain_lop);
                ret = 1;
            }
            delete_item_from_array(p->precs, bb);
        }
    }
    else if (ir->ircode == JNT)
    {
        if (IS_CONST_SYMBOL(ir->lop))
        {
            if (IS_EQUAL(ir->lop->type, ir->lop->value, 0))
            {
                p = delete_item_from_array(bb->succs, bb->next);
                ir->ircode = JMP;
                ir->lop = NULL;
                vector_destroy(ir->ud_chain_lop);
                ret = 1;
            }
            else
            {
                p = delete_another_item_from_succs(bb->succs, bb->next);
                bb->ir_list->erase(bb->ir_list, ir_no);
                ret = 1;
            }
            delete_item_from_array(p->precs, bb);
        }
    }
    /*
    else if (ir->ircode == JE || ir->ircode == JNE)
    {
    these irs are used for switch statement only, we have done optimize for switch during exprission check
    we don't need to do anything here
    }
    */
    return ret;
}

int conditional_jmp_simplify(struct proc_block *pb)
{
    int i;
    int flag = 0;
    int cnt = 0;
    struct ir *ir;
    struct basic_block *bb;
    do 
    {
        flag = 0;
        for (bb = pb->bblist_head; bb; bb = bb->next)
        {
            for (i = 0; i < bb->ir_list->len; i++)
            {
                ir = bb->ir_list->get(bb->ir_list, i);
                flag |= conditional_jmp_simplify_for_ir(ir, bb, i);
            }
        }
        cnt++;
    } while(flag);
    return cnt > 1;
}

int sequential_jmp_simplify_for_ir(struct ir *ir, struct basic_block *bb)
{
    int flag = 0;
    struct basic_block *next_bb, *next_next_bb;
    struct ir *next_ir;
    if (IS_JMP_IR(ir->ircode))
    {
        next_bb = ((struct symbol_label*)ir->rsltop)->belonging_bb;
        next_ir = next_bb->ir_list->get(next_bb->ir_list, 1);
        if (!next_ir)
        {
            return 0;
        }

        if (ir->ircode == JMP && next_ir->ircode == JMP)
        {
            ir->rsltop = next_ir->rsltop;
            next_next_bb = next_bb->succs->get(next_bb->succs, 0);

            bb->succs->clear(bb->succs);
            bb->succs->push_back(bb->succs, next_next_bb);

            next_next_bb->precs->push_back(next_next_bb->precs, bb);
            
            delete_item_from_array(next_bb->precs, bb);
            flag = 1;
        }
        else if (ir->ircode != JMP && next_ir->ircode == JMP)
        {
            next_next_bb = ((struct symbol_label*)next_ir->rsltop)->belonging_bb;

            if (bb->succs->len > 1)
            {
                delete_item_from_array(bb->succs, next_bb);
                delete_item_from_array(next_bb->precs, bb);
            }

            bb->succs->push_back(bb->succs, next_next_bb);
            next_next_bb->precs->push_back(next_next_bb->precs, bb);
            ir->rsltop = next_ir->rsltop;
            flag = 1;
        }
        else if (ir->ircode == JMP)
        {
            // the sequential next rather than logical next
            next_bb = bb->next;
            next_ir = next_bb->ir_list->get(next_bb->ir_list, 0);
            if (next_ir->ircode == LABEL && next_ir->rsltop == ir->rsltop)
            {
                ir->unused = 1;
                flag = 1;
            }
        }
    }
    return flag;
}

void ir_eliminate(struct ir *ir)
{
    vector_destroy(ir->du_chain);
    if (ir->lop)
    {
        vector_destroy(ir->ud_chain_lop);
    }
    if (ir->rop)
    {
        vector_destroy(ir->ud_chain_rop);
    }
    free(ir);  
}

int dead_code_eliminate_for_ir(struct ir *ir, struct proc_block *pb, struct basic_block *bb, int ir_no)
{
    int i;
    if ((IS_JMP_IR(ir->ircode) && !ir->unused) || ir->ircode == LABEL || ir->ircode == RET ||
            ir->ircode == STORE || ir->ircode == CALL ||
            (ir->rsltop->kind == SK_Variable && ((struct symbol_var *)ir->rsltop)->is_global))
    {
        return 0;
    }
    if ((!ir->du_chain->len || ir->unused) && !find_item_from_sets(pb->ref_vars, ir->rsltop))
    {
        ir = bb->ir_list->erase(bb->ir_list, ir_no);
        ir_eliminate(ir);
        return 1;
    }
    return 0;
}

int dead_basic_block_eliminate(struct proc_block *pb)
{
    int i, flag = 0;
    struct ir *ir;
    struct basic_block *bb, *last_bb, *next_bb;
    last_bb = pb->bblist_head;
    bb = pb->bblist_head->next; // skip the first bb (serve as local variable definition)

    while(bb)
    {
        if ((!bb->precs || !bb->precs->len || bb == bb->precs->get(bb->precs, 0)) && 
            bb->next)  // skip the last bb (serve as function return)
        {
            last_bb->next = bb->next;
            for (i = 0; i < bb->succs->len; i++)
            {
                next_bb = bb->succs->get(bb->succs, i);
                delete_item_from_array(next_bb->precs, bb);
            }

            vector_destroy(bb->precs);
            vector_destroy(bb->succs);
            hash_destory(bb->def_set);
            hash_destory(bb->out_set);
            hash_destory(bb->use_set);
            for (i = 0; i < bb->ir_list->len; i++)
            {
                ir = bb->ir_list->get(bb->ir_list, i);
                ir_eliminate(ir);
            }
            vector_destroy(bb->ir_list);
            free(bb);
            bb = last_bb->next;
            flag = 1;
        }
        else
        {
            last_bb = bb;
            bb = bb->next;   
        }
    }
    return flag;
}

void record_def_callback(struct symbol *sym)
{
    if (sym->kind == SK_Temp)
    {
        ((struct symbol_temp *)sym)->def_cnt++;
    }
}

void record_use_callback(struct symbol *sym)
{
    if (sym->kind == SK_Temp)
    {
        ((struct symbol_temp *)sym)->ref_cnt++;
    }
}

struct temp_var_info
{
    int d;
    int def;
    int use;
    struct symbol_temp *sym;
};
void *add_temp_info_to_array(struct vector *vec, struct temp_var_info *info)
{
    int i;
    for (i = 0; i < vec->len; i++)
    {
        if (((struct temp_var_info *)vec->get(vec, i))->d < info->d)
        {
            break;
        }
    }
    vec->insert(vec, i, info);
}

int share_temp_memory_for_bb(struct proc_block *pb, struct basic_block *bb)
{
    struct temp_var_info *info, *info1;
    struct vector *rslt;
    struct vector *prep;

    int i, ret = 0;
    struct ir *ir, *ref_ir;
    
    prep = vector_init(0);
    rslt = vector_init(0);
    
    for (i = 0; i < bb->ir_list->len; i++)
    {
        ir = bb->ir_list->get(bb->ir_list, i);
        /*
        the selected temp variable must satisfy the following conditions:
            1. it must be assigned only once and be used only once within the whole program
            2. irs which assign it and use it are in the same basic block
            3. the ir which assign it must in front of the ir which use it
        */
        if (ir->rsltop->kind == SK_Temp && ir->ircode != STORE &&
            ((struct symbol_temp *)ir->rsltop)->def_cnt == 1 && ((struct symbol_temp *)ir->rsltop)->ref_cnt == 1 &&
            !find_item_from_sets(bb->out_set, ir->rsltop) &&
            !find_item_from_sets(bb->use_set, ir->rsltop))
        {
            // we are sure that the ref_ir must in the same bb
            ref_ir = ir->du_chain->get(ir->du_chain, 0);
            assert(ref_ir->bb_belonging == bb);
            if (ir->ir_no < ref_ir->ir_no)
            {
                ALLOC(info, struct temp_var_info);
                assert(i == ir->ir_no);
                info->def = i; 
                info->use = ref_ir->ir_no;
                info->d = info->use - info->def;
                assert(info->d > 0);
                info->sym = ir->rsltop;
                add_temp_info_to_array(prep, info);
            }
        }
// for asm emit, we need to record all temporary variables at this point
// since all temporary variables must be assigned, we only need to check every rslt_op
        if (ir->rsltop->kind == SK_Temp)
        {
            test_and_add_item_to_sets(pb->temps, ir->rsltop);
        }
    }

    if (prep->len)
    {
        info = prep->pop_back(prep);
        rslt->push_back(rslt, info);

        while (prep->len)
        {
            info = prep->pop_back(prep);
            // non-intersect 
            for (i = 0; i < rslt->len; i++)
            {
                info1 = rslt->get(rslt, i);
                if ((info->def < info1->use && info->use > info1->def) || 
                    (info->use > info1->def && info->def < info1->use))
                {
                    free(info);
                    goto next;
                }
            }
            rslt->push_back(rslt, info);
next:;
        }
        // now we get all the variable in rslt which can share the same memory area
        for (i = 0; i < rslt->len; i++)
        {
            info = rslt->get(rslt, i);
            info->sym->is_shared_tmp = 1;
            ret = ret > info->sym->type->size ? ret : info->sym->type->size;
            free(info);
        }
    }
    else
    {
        for (i = 0; i < prep->len; i++)
        {
            info = prep->get(prep, i);
            free(info);
        }
    }
    vector_destroy(prep);
    vector_destroy(rslt);

    return ret;
}

int share_temp_memory(struct proc_block *pb)
{
    int size, max_size = 0;
    struct basic_block *bb;

    record_def_cb = record_def_callback;
    record_use_cb = record_use_callback;
    get_def_use_set(pb, 1);
    record_def_cb = NULL;
    record_use_cb = NULL;
    get_in_out_set(pb);
    get_du_chain(pb);
    get_ud_chain(pb);

    for (bb = pb->bblist_head; bb; bb = bb->next)
    {
        size = share_temp_memory_for_bb(pb, bb);
        max_size = max_size > size ? max_size : size;
    }
    return max_size;
}

int do_optimize(struct proc_block *pb)
{
    int i;
    int flag = 0;
    int cnt = 0;
    struct ir *ir;
    struct basic_block *bb;
    do 
    {
        flag = 0;
        for (bb = pb->bblist_head; bb; bb = bb->next)
        {
            for (i = 0; i < bb->ir_list->len; i++)
            {
                ir = bb->ir_list->get(bb->ir_list, i);
                flag |= algebraic_simplify_for_ir(ir);
                flag |= conditional_jmp_simplify_for_ir(ir, bb, i);
                flag |= sequential_jmp_simplify_for_ir(ir, bb);
                // must located at the end of the loop
                if (dead_code_eliminate_for_ir(ir, pb, bb, i))
                {
                    flag = 1;
                    i--;
                }
                else
                {
                    flag |= const_copy_prop_for_ir(ir, pb);
                }
            }
        }
        flag |= dead_basic_block_eliminate(pb);
        cnt++;
    } while(flag);
    return cnt > 1;
}


void optimize(struct proc_block *pb)
{
    int redo = 0;
    do
    {
        get_def_use_set(pb, redo);
        get_in_out_set(pb);
        get_du_chain(pb);
        get_ud_chain(pb);

        redo = 0;
        redo |= do_optimize(pb);
    } while (redo);
}


void opt_init()
{
    int i = 0, cnt = 0;
    struct symbol *sym;

    global_vars_static = hash_init();
    global_vars_extern = hash_init();
    while (sym = sym_id_traverse_in_cur_lvl(&i, &cnt))
    {
        if (sym->kind == SK_Variable)
        {
            if (sym->sto_class_spec_tok == TK_STATIC)
            {
                add_item_to_sets(global_vars_static, sym);
            }
            else
            {
                add_item_to_sets(global_vars_extern, sym);
            }
        }
    }
}




