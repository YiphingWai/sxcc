#include "fold.h"
#include <assert.h>


struct expr_node *constant_fold(struct expr_node *expr)
{
    if (expr->op >= OP_OR && expr->op <= OP_MOD && 
        expr->child[0]->op == OP_CONST && expr->child[1]->op == OP_CONST)
    {
        goto ok;
    }
    if (expr->op >= OP_POS && expr->op <= OP_NOT && expr->child[0]->op == OP_CONST)
    {
        goto ok;
    }
    if (expr->op == OP_QUESTION && expr->child[0]->op == OP_CONST)
    {
        if (expr->child[0]->value.i)
        {
            expr->child[1]->child[0]->next = expr->next;
            return expr->child[1]->child[0];
        }
        expr->child[1]->child[1]->next = expr->next;
        return expr->child[1]->child[1];
    }
    if (expr->op == OP_COMMA && expr->child[0]->op == OP_CONST)
    {
        expr->child[1]->next = expr->next;
        return expr->child[1];
    }
    return expr;
ok:
    switch (expr->op)
    {
        case OP_OR:
            EXECUTE_ROP(expr->child[0], ||, expr->child[1], expr->value);
            break;
        case OP_AND:
            EXECUTE_ROP(expr->child[0], &&, expr->child[1], expr->value);
            break;
        case OP_BITOR: 
            expr->value.i = expr->child[0]->value.i | expr->child[1]->value.i;
            break;
        case OP_BITXOR:
            expr->value.i = expr->child[0]->value.i ^ expr->child[1]->value.i;
            break;
        case OP_BITAND:
            expr->value.i = expr->child[0]->value.i & expr->child[1]->value.i;
            break;
        case OP_EQUAL:
            EXECUTE_ROP(expr->child[0], ==, expr->child[1], expr->value);
            break;
        case OP_UNEQUAL:
            EXECUTE_ROP(expr->child[0], !=, expr->child[1], expr->value);
            break;
        case OP_GREAT:
            EXECUTE_ROP(expr->child[0], >, expr->child[1], expr->value);
            break;
        case OP_LESS:
            EXECUTE_ROP(expr->child[0], <, expr->child[1], expr->value);
            break;
        case OP_GREAT_EQ:
            EXECUTE_ROP(expr->child[0], >=, expr->child[1], expr->value);
            break;
        case OP_LESS_EQ:
            EXECUTE_ROP(expr->child[0], <=, expr->child[1], expr->value);
            break;
        case OP_LSHIFT:
            expr->value.i = expr->child[0]->value.i << expr->child[1]->value.i;
            break;
        case OP_RSHIFT:
            expr->value.i = expr->child[0]->value.i >> expr->child[1]->value.i;
            break;
        case OP_ADD:
            EXECUTE_BOP(expr->child[0], +, expr->child[1], expr->value);
            break;
        case OP_SUB:
            EXECUTE_BOP(expr->child[0], -, expr->child[1], expr->value);
            break;
        case OP_MUL:
            EXECUTE_BOP(expr->child[0], *, expr->child[1], expr->value);
            break;
        case OP_DIV:
            EXECUTE_BOP(expr->child[0], /, expr->child[1], expr->value);
            break;
        case OP_MOD:
            if (expr->child[0]->type->categ == INT && expr->child[1]->type->categ == INT)
            {
                expr->value.i = expr->child[0]->value.i % expr->child[1]->value.i;
            }
            else if (expr->child[0]->type->categ == UINT && expr->child[1]->type->categ == UINT)
            {
                expr->value.ui = expr->child[0]->value.ui % expr->child[1]->value.ui;
            }
            else if (expr->child[0]->type->categ == LONG && expr->child[1]->type->categ == LONG)
            {
                expr->value.l = expr->child[0]->value.l % expr->child[1]->value.l;
            }
            else if (expr->child[0]->type->categ == ULONG && expr->child[1]->type->categ == ULONG)
            {
                expr->value.ul = expr->child[0]->value.ul % expr->child[1]->value.ul;
            }
            else
            {
                assert(0);
            }
            break;
        case OP_POS:
            expr->child[0]->next = expr->next;
            expr = expr->child[0];
            break;
        case OP_NEG:
            if ((expr->child[0]->type->categ == INT) || (expr->child[0]->type->categ == UINT))
            {
                expr->child[0]->value.i = -expr->child[0]->value.i;
            }
            else if ((expr->child[0]->type->categ == LONG) || (expr->child[0]->type->categ == ULONG))
            {
                expr->child[0]->value.l = -expr->child[0]->value.l;
            }
            else if (expr->child[0]->type->categ == FLOAT)
            {
                expr->child[0]->value.f = -expr->child[0]->value.f;
            }
            else if (expr->child[0]->type->categ == DOUBLE)
            {
                expr->child[0]->value.d = -expr->child[0]->value.d;
            }
            else
            {
                assert(0);
            }
            expr->child[0]->next = expr->next;
            expr = expr->child[0];
            break;
        case OP_COMP:
            if (expr->child[0]->type->categ == INT)
            {
                expr->value.i = ~(expr->child[0]->value.i);
            }
            else if (expr->child[0]->type->categ == UINT)
            {
                expr->value.ui = ~(expr->child[0]->value.ui);
            }
            else if (expr->child[0]->type->categ == LONG)
            {
                expr->value.l = ~(expr->child[0]->value.l);
            }
            else if (expr->child[0]->type->categ == ULONG)
            {
                expr->value.ul = ~(expr->child[0]->value.ul);
            }
            else
            {
                assert(0);
            }
            break;
        case OP_NOT:
            if (expr->child[0]->type->categ == INT)
            {
                expr->value.i = !(expr->child[0]->value.i);
            }
            else if (expr->child[0]->type->categ == UINT)
            {
                expr->value.i = !(expr->child[0]->value.ui);
            }
            else if (expr->child[0]->type->categ == LONG)
            {
                expr->value.i = !(expr->child[0]->value.l);
            }
            else if (expr->child[0]->type->categ == ULONG)
            {
                expr->value.i = !(expr->child[0]->value.ul);
            }
            else if (expr->child[0]->type->categ == FLOAT)
            {
                expr->value.i = !(expr->child[0]->value.f);
            }
            else if (expr->child[0]->type->categ == DOUBLE)
            {
                expr->value.i = !(expr->child[0]->value.d);
            }
            else
            {
                assert(0);
            }
            break;
        default:
            return expr;
    }

    expr->op = OP_CONST;
    expr->is_left_val = 0;
    return expr;
}

struct expr_node *cast_fold(struct type *dst_type, struct expr_node *expr)
{
    enum type_categ src_type_class, dst_type_class;
    if (dst_type->categ == VOID || expr->op != OP_CONST)
    {
        return cast(dst_type, expr);
    }
    dst_type_class = dst_type->categ;
    src_type_class = expr->type->categ;
    switch (src_type_class)
    {
        case CHAR:
        case UCHAR:
        case SHORT:
        case USHORT:
            assert(dst_type_class == INT);
            expr->type = &basic_types[INT];
            break;
        case INT:
            switch (dst_type_class)
            {
                case CHAR:
                    expr->type = &basic_types[CHAR];
                    expr->value.i = (char)expr->value.i;
                    break;
                case UCHAR:
                    expr->type = &basic_types[UCHAR];
                    expr->value.i = (unsigned char)expr->value.i;
                    break;
                case SHORT:
                    expr->type = &basic_types[SHORT];
                    expr->value.i = (short)expr->value.i;
                    break;
                case USHORT:
                    expr->type = &basic_types[USHORT];
                    expr->value.i = (unsigned short)expr->value.i;
                    break;
                case UINT:
                    expr->type = &basic_types[UINT];
                    expr->value.ui = (unsigned int)expr->value.i;
                    break;
                case LONG:
                    expr->type = &basic_types[LONG];
                    expr->value.l = (long)expr->value.i;
                    break;
                case ULONG:
                    expr->type = &basic_types[ULONG];
                    expr->value.ul = (unsigned long)expr->value.i;
                    break;
                case FLOAT:
                    expr->type = &basic_types[FLOAT];
                    expr->value.f = (float)expr->value.i;
                    break;
                case DOUBLE:
                    expr->type = &basic_types[DOUBLE];
                    expr->value.d = (double)expr->value.i;
                    break;
                case POINTER:
                    assert(expr->type->categ == INT);
                    expr->type = dst_type;
                    break;
                case INT:
                    assert(expr->type->categ == INT);
                    expr->type = dst_type;
                    break;
                default:
                    assert(0);
            }
            break;
        case UINT:
            switch (dst_type_class)
            {
                case INT:
                    expr->type = &basic_types[INT];
                    expr->value.i = (int)expr->value.ui;
                    break;
                case LONG:
                    expr->type = &basic_types[LONG];
                    expr->value.l = (long)expr->value.ui;
                    break;
                case ULONG:
                    expr->type = &basic_types[ULONG];
                    expr->value.ul = (unsigned long)expr->value.ui;
                    break;
                case FLOAT:
                    expr->type = &basic_types[FLOAT];
                    expr->value.f = (float)expr->value.ui;
                    break;
                case DOUBLE:
                    expr->type = &basic_types[DOUBLE];
                    expr->value.d = (double)expr->value.ui;
                    break;
                default:
                    assert(0);
            }
            break;
        case LONG:
            switch (dst_type_class)
            {
                case INT:
                    expr->type = &basic_types[INT];
                    expr->value.i = (int)expr->value.l;
                    break;
                case UINT:
                    expr->type = &basic_types[UINT];
                    expr->value.ui = (unsigned int)expr->value.l;
                    break;
                case ULONG:
                    expr->type = &basic_types[ULONG];
                    expr->value.ul = (unsigned long)expr->value.l;
                    break;
                case FLOAT:
                    expr->type = &basic_types[FLOAT];
                    expr->value.f = (float)expr->value.l;
                    break;
                case DOUBLE:
                    expr->type = &basic_types[DOUBLE];
                    expr->value.d = (double)expr->value.l;
                    break;
                case LONG:
                    break;
                default:
                    assert(0);
            }
            break;
        case ULONG:
            switch (dst_type_class)
            {
                case INT:
                    expr->type = &basic_types[INT];
                    expr->value.i = (int)expr->value.ul;
                    break;
                case LONG:
                    expr->type = &basic_types[LONG];
                    expr->value.l = (long)expr->value.ul;
                    break;
                case UINT:
                    expr->type = &basic_types[UINT];
                    expr->value.ui = (unsigned int)expr->value.ul;
                    break;
                case FLOAT:
                    expr->type = &basic_types[FLOAT];
                    expr->value.f = (float)expr->value.ul;
                    break;
                case DOUBLE:
                    expr->type = &basic_types[DOUBLE];
                    expr->value.d = (double)expr->value.ul;
                    break;
                default:
                    assert(0);
            }
            break;
        case FLOAT:
            switch (dst_type_class)
            {
                case INT:
                    expr->type = &basic_types[INT];
                    expr->value.i = (int)expr->value.f;
                    break;
                case LONG:
                    expr->type = &basic_types[LONG];
                    expr->value.l = (long)expr->value.f;
                    break;
                case UINT:
                    expr->type = &basic_types[UINT];
                    expr->value.ui = (unsigned int)expr->value.f;
                    break;
                case ULONG:
                    expr->type = &basic_types[ULONG];
                    expr->value.ul = (unsigned long)expr->value.f;
                    break;
                case DOUBLE:
                    expr->type = &basic_types[DOUBLE];
                    expr->value.d = (double)expr->value.f;
                    break;
                default:
                    assert(0);
            }
            break;
        case DOUBLE:
            switch (dst_type_class)
            {
                case INT:
                    expr->type = &basic_types[INT];
                    expr->value.i = (int)expr->value.d;
                    break;
                case LONG:
                    expr->type = &basic_types[LONG];
                    expr->value.l = (long)expr->value.d;
                    break;
                case UINT:
                    expr->type = &basic_types[UINT];
                    expr->value.ui = (unsigned int)expr->value.d;
                    break;
                case ULONG:
                    expr->type = &basic_types[ULONG];
                    expr->value.ul = (unsigned long)expr->value.d;
                    break;
                case FLOAT:
                    expr->type = &basic_types[FLOAT];
                    expr->value.f = (float)expr->value.d;
                    break;
                default:
                    assert(0);
            }
            break;
        default:
            assert(0);
    }
    expr->op = OP_CONST;
    return expr;
}
