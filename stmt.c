#include <stdlib.h>
#include <assert.h>
#include "expr.h"
#include "stmt.h"
#include "decl.h"
#include "sxcc.h"

/*
expression-statement:
                  expression<opt> ;
*/
struct stmt_expr_node *expression_statement()
{
    struct token *tok;
    struct stmt_expr_node *node;

    ALLOC(node, struct stmt_expr_node);
    
    node->kind = NK_ExpressionStatement;
    tok = lex_get_token();

    if (tok->type != TK_SEMICOLON)
    {
        lex_push(tok);
        node->expr = expression();
        expect(TK_SEMICOLON);
    }
    else
    {
        free(tok);
    }
    return node;
}


/*
labeled-statement:
                  identifier :  statement
                  case  constant-expression :  statement
                  default :  statement
*/
struct stmt_lbl_node *label_statement()
{
    struct token *tok;
    struct stmt_lbl_node *node;

    ALLOC(node, struct stmt_lbl_node);

    tok = lex_get_token();

    if (tok->type == TK_ID)
    {
        node->kind = NK_LabelStatement;
        node->name = tok->token_strings;
    }
    else 
    {
        error("expect identifier", tok->loc);
        exit(1);
    }
    free(tok);

    expect(TK_COLON);

    node->stmt = statement();
    return node;
}
struct stmt_case_node *case_statement()
{
    struct token *tok;
    struct stmt_case_node *node;

    ALLOC(node, struct stmt_case_node);
    
    tok = lex_get_token();

    if (tok->type == TK_CASE)
    {
        node->kind = NK_CaseStatement;
        node->expr = constant_expression();
    }
    else 
    {
        error("expect token 'case'", tok->loc);
        exit(1);
    }
    free(tok);

    expect(TK_COLON);
    /*
    tok = lex_get_token();
    if (tok->type != TK_CASE)
    {
        lex_push(tok);
        node->stmt = statement();
    }
    else
    {
        lex_push(tok);
    }
    */
    return node;
}
struct stmt_default_node *default_statement()
{
    struct token *tok;
    struct stmt_default_node *node;

    ALLOC(node, struct stmt_case_node);
    tok = lex_get_token();

    if (tok->type == TK_DEFAULT)
    {
        node->kind = NK_DefaultStatement;
    }
    else 
    {
        error("expect token 'default'", tok->loc);
        exit(1);
    }
    free(tok);

    expect(TK_COLON);

//    node->stmt = statement();
    return node;
}
/*
selection-statement:
                  if (  expression )  statement
                  if (  expression )  statement else  statement
                  switch (  expression )  statement
*/
struct stmt_if_node *if_statement()
{
    struct token *tok;
    struct stmt_if_node *node;

    ALLOC(node, struct stmt_if_node);
    tok = lex_get_token();
    
    if (tok->type == TK_IF)
    {
        node->kind = NK_IfStatement;
    }
    else
    {
        error("expect token 'if'", tok->loc);
        exit(1);
    }
    
    free(tok);

    expect(TK_LPAREN);
    node->expr = expression();
    expect(TK_RPAREN);

    node->then_stmt = statement();
    tok = lex_get_token();
    if (tok->type == TK_ELSE)
    {
        node->else_stmt = statement();
        free(tok);
    }
    else
    {
        lex_push(tok);
    }
    return node;
}

struct stmt_switch_node *switch_statement()
{
    struct token *tok;
    struct stmt_switch_node *node;

    ALLOC(node, struct stmt_switch_node);
    
    tok = lex_get_token();

    if (tok->type == TK_SWITCH)
    {
        node->kind = NK_SwitchStatement;
    }
    else
    {
        error("expect token 'switch'", tok->loc);
        exit(1);
    }
    
    free(tok);

    expect(TK_LPAREN);
    node->expr = expression();
    expect(TK_RPAREN);

    node->stmt = statement();
    
    return node;
}

/*
iteration-statement:
                  while (  expression )  statement
                  do  statement while (  expression ) ;
                  for ( expression<opt> ; expression<opt> ;
                      expression<opt> ) statement
*/
struct stmt_while_node *while_statement()
{
    struct token *tok;
    struct stmt_while_node *node;
    ALLOC(node, struct stmt_switch_node);
    tok = lex_get_token();

    if (tok->type == TK_WHILE)
    {
        free(tok);
        node->kind = NK_WhileStatement;
        expect(TK_LPAREN);
        node->expr = expression();
        expect(TK_RPAREN);
        node->stmt = statement();
    }
    else
    {
        error("expect token 'while'", tok->loc);
        exit(1);
    }
    return node;
}
struct stmt_do_node *do_statement()
{
    struct token *tok;
    struct stmt_do_node *node;
    ALLOC(node, struct stmt_switch_node);
    
    tok = lex_get_token();

    if (tok->type == TK_DO)
    {
        free(tok);
        node->kind = NK_DoStatement;
        node->stmt = statement();
        expect(TK_WHILE);
        expect(TK_LPAREN);
        node->expr = expression();
        expect(TK_RPAREN);
        expect(TK_SEMICOLON);
    }
    else
    {
        error("expect token 'do'", tok->loc);
        exit(1);
    }
    return node;
}
struct stmt_for_node *for_statement()
{
    struct token *tok;
    struct stmt_for_node *node;
    ALLOC(node, struct stmt_for_node);
    
    tok = lex_get_token();

    if (tok->type == TK_FOR)
    {
        free(tok);
        node->kind = NK_ForStatement;
        expect(TK_LPAREN);
        tok = lex_get_token();
        if (tok->type != TK_SEMICOLON)
        {
            lex_push(tok);
            node->expr1 = expression();
            expect(TK_SEMICOLON);
        }
        else
        {
            free(tok);
        }
        tok = lex_get_token();
        if (tok->type != TK_SEMICOLON)
        {
            lex_push(tok);
            node->expr2 = expression();
            expect(TK_SEMICOLON);
        }
        else
        {
            free(tok);
        }
        tok = lex_get_token();
        if (tok->type != TK_RPAREN)
        {
            lex_push(tok);
            node->expr3 = expression();
            expect(TK_RPAREN);
        }
        else
        {
            free(tok);
        }
        node->stmt = statement();
    }
    else
    {
        error("expect token 'for'", tok->loc);
        exit(1);
    }
    return node;
}
/*
jump-statement:
                  goto  identifier ;
                  continue ;
                  break ;
                  return  expression<opt> ;
*/
struct stmt_goto_node *goto_statement()
{
    struct token *tok;
    struct stmt_goto_node *node;

    ALLOC(node, struct stmt_goto_node);
    tok = lex_get_token();

    if (tok->type == TK_GOTO)
    {
        free(tok);
        node->kind = NK_GotoStatement;
        tok = lex_get_token();
        if (tok->type != TK_ID)
        {
            error("expect identifier!", tok->loc);
            exit(1);
        }
        node->lab_name = tok->token_strings;
        free(tok);
        expect(TK_SEMICOLON);
    }
    else
    {
        error("expect token 'goto'", tok->loc);
        exit(1);
    }
    return node;
}
struct stmt_break_node *break_statement()
{
    struct token *tok;
    struct stmt_break_node *node;

    ALLOC(node, struct stmt_break_node);
    tok = lex_get_token();

    if (tok->type == TK_BREAK)
    {
        free(tok);
        node->kind = NK_BreakStatement;
        expect(TK_SEMICOLON);
    }
    else
    {
        error("expect token 'break'", tok->loc);
        exit(1);
    }
    return node;
}
struct stmt_continue_node *continue_statement()
{
    struct token *tok;
    struct stmt_continue_node *node;

    ALLOC(node, struct stmt_continue_node);
    tok = lex_get_token();

    if (tok->type == TK_CONTINUE)
    {
        free(tok);
        node->kind = NK_ContinueStatement;
        expect(TK_SEMICOLON);
    }
    else
    {
        error("expect token 'continue'", tok->loc);
        exit(1);
    }
    return node;
}
struct stmt_return_node *return_statement()
{
    struct token *tok;
    struct stmt_return_node *node;

    ALLOC(node, struct stmt_return_node);
    
    tok = lex_get_token();

    if (tok->type == TK_RETURN)
    {
        free(tok);
        node->kind = NK_ReturnStatement;
        tok = lex_get_token();
        if (tok->type != TK_SEMICOLON)
        {
            lex_push(tok);
            node->expr = expression();
            expect(TK_SEMICOLON);
        }
        else
        {
            free(tok);
        }
    }
    else
    {
        error("expect token 'return'", tok->loc);
        exit(1);
    }
    return node;
}
/*
compound-statement:
                  {  declaration-list<opt> statement-list<opt> }

          declaration-list:
                  declaration
                  declaration-list declaration

          statement-list:
                  statement
                  statement-list statement
*/
struct stmt_comp_node *compound_statement()
{
    struct common_node **p;
    struct token *tok;
    struct stmt_comp_node *node;

    ALLOC(node, struct stmt_comp_node);
    
    node->kind = NK_CompoundStatement;
    expect(TK_LBRACE);
    tok = lex_get_token();
    if (is_decl_first(tok->type) && (tok->type != TK_ID || is_typedef_name(tok->token_strings)))
    {
        lex_push(tok);
        node->decl_list = declaration_list();
    }
    else
    {
        lex_push(tok);
    }
    
    p = &(node->stmt);
    tok = lex_get_token();
    while (is_stmt_first(tok->type))
    {
        lex_push(tok);
        *p = statement();
        p = &((*p)->next);
        tok = lex_get_token();
    }
    free(tok);
    return node;
}

/*
statement:
                  labeled-statement
                  compound-statement
                  expression-statement
                  selection-statement
                  iteration-statement
                  jump-statement
*/
struct common_node *statement()
{
    struct token *tok, *tok1;
    struct common_node *node;

    tok = lex_get_token();

    switch (tok->type)
	{
	case TK_ID:
		/************************************
			In ParseLabelStatement(), ucl peek the next token
			to determine whether it is label-statement or expression-statement.
			(1)
				loopAgain :		---------->   Starting with ID
					...
					goto loopAgain

				or
			(2)
				f(20,30)		---------->	Starting with ID

				
		 ************************************/
        tok1 = lex_get_token();
        if (tok1->type == TK_COLON)
        {
            lex_push(tok1);
            lex_push(tok);
            return label_statement();
        }
        lex_push(tok1);
        lex_push(tok);
        return expression_statement();
        

	case TK_CASE:
        lex_push(tok);
        return case_statement();
	case TK_DEFAULT:
        lex_push(tok);
		return default_statement();

	case TK_IF:
        lex_push(tok);
		return if_statement();
	case TK_SWITCH:
        lex_push(tok);
		return switch_statement();

	case TK_WHILE:
        lex_push(tok);
		return while_statement();
	case TK_DO:
        lex_push(tok);
		return do_statement();
	case TK_FOR:
        lex_push(tok);
		return for_statement();

	case TK_GOTO:
        lex_push(tok);
		return goto_statement();
	case TK_CONTINUE:
        lex_push(tok);
		return continue_statement();
	case TK_BREAK:
        lex_push(tok);
		return break_statement();
	case TK_RETURN:
        lex_push(tok);
		return return_statement();

	case TK_LBRACE:
        lex_push(tok);
		return compound_statement();

	default:
        lex_push(tok);
		return expression_statement();
	}
}