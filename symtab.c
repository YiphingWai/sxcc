#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "symtab.h"
#include "hash.h"
#include "type.h"
#include "sxcc.h"


int temp_counter = 0;
int label_counter = 0;
int string_counter = 0;
int float_counter = 0;

static struct hash_table *tag_tab[MAX_NESTING_LEVEL];   // tag means struct,union and enumeration name
static struct hash_table *id_tab[MAX_NESTING_LEVEL];    // id means veriable, typedef and function name
struct hash_table *constant_tab;
int cur_nesting_lvl = 0;

void symtab_init()
{
    tag_tab[0] = hash_init();
    if (!tag_tab[0])
    {
        printf("error: hash table init error!\n");
        exit(-1);
    }
    id_tab[0] = hash_init();
    if (!id_tab[0])
    {
        printf("error: hash table init error!\n");
        exit(-1);
    }
    constant_tab = hash_init();
    if (!constant_tab)
    {
        printf("error: hash table init error!\n");
        exit(-1);
    }
    cur_nesting_lvl = 0;
    temp_counter = 0;
    label_counter = 0;
    return 0;
}

void enter_scope()
{
    cur_nesting_lvl++;
    if (cur_nesting_lvl >= MAX_NESTING_LEVEL)
    {
        printf("error: exceed nesting limit\n");
        exit(1);
    }
    tag_tab[cur_nesting_lvl] = hash_init();
    if (!tag_tab[cur_nesting_lvl])
    {
        printf("error: hash table init error!\n");
        exit(-1);
    }
    id_tab[cur_nesting_lvl] = hash_init();
    if (!id_tab[cur_nesting_lvl])
    {
        printf("error: hash table init error!\n");
        exit(-1);
    }
}

void exit_scope()
{
    int cur, cnt = 0;
    void *p;
    if (cur_nesting_lvl == 0)
    {
        printf("error: exceed nesting limit\n");
        exit(1);
    }
/*    
    cur = 0;
    cnt = 0;
    while (p = tag_tab[cur_nesting_lvl]->hash_iter(tag_tab[cur_nesting_lvl], &cur, &cnt))
    {
        free(((struct symbol *)p)->name);
        free(p);
    }
    cur = 0;
    cnt = 0;
    while (p = id_tab[cur_nesting_lvl]->hash_iter(id_tab[cur_nesting_lvl], cur, &cur))
    {
        cnt++;
        free(((struct symbol *)p)->name);
        free(p);
        if (cnt >= id_tab[cur_nesting_lvl]->R)
        {
            break;
        }
    }*/
    hash_destory(tag_tab[cur_nesting_lvl]);
    hash_destory(id_tab[cur_nesting_lvl]);
    cur_nesting_lvl--;
}
#if 0
void exit_param_scope()
{
    int cur, cnt = 0;
    void *p;
    assert(cur_nesting_lvl == MAX_NESTING_LEVEL);
/*
    cur = 0;
    cnt = 0;
    while (p = tag_tab[cur_nesting_lvl]->hash_iter(tag_tab[cur_nesting_lvl], &cur, &cnt))
    {
        free(((struct symbol *)p)->name);
        free(p);
    }
    cur = 0;
    cnt = 0;
    while (p = id_tab[cur_nesting_lvl]->hash_iter(id_tab[cur_nesting_lvl], cur, &cur))
    {
        cnt++;
        free(((struct symbol *)p)->name);
        free(p);
        if (cnt >= id_tab[cur_nesting_lvl]->R)
        {
            break;
        }
    }*/
    hash_destory(tag_tab[cur_nesting_lvl]);
    hash_destory(id_tab[cur_nesting_lvl]);
    cur_nesting_lvl = _cur_nesting_lvl;
}
#endif
void number_get_name(struct type *type, union val value, char *name)
{
    switch (type->categ)
    {
        case CHAR:
            sprintf(name, "%d_c", (char)value.i);
        case UCHAR:
            sprintf(name, "%d_uc", (unsigned char)value.i);
        case SHORT:
            sprintf(name, "%d_s", (short)value.i);
        case USHORT:
            sprintf(name, "%d_us", (unsigned short)value.i);
        case INT:
            sprintf(name, "%d_i", value.i);
            break;
        case POINTER:
            sprintf(name, "%p_p", value.i);
            break;
        case UINT:
            sprintf(name, "%u_ui", value.ui);
            break;
        case LONG:
            sprintf(name, "%ld_l", value.l);
            break;
        case ULONG:
            sprintf(name, "%uld_ul", value.ul);
            break;
        case FLOAT:
            sprintf(name, "%f_f", value.f);
            break;
        case DOUBLE:
            sprintf(name, "%lf_d", value.d);
            break;
        case VOID:
            sprintf(name, "%d_i", value.i);
            break;
        default:
            printf("unsupport variable type!\n");
            exit(1);
    }
}

void number_get_aname(struct type *type, union val value, char *aname)
{
    switch (type->categ)
    {
        case CHAR:
            sprintf(aname, "%d", (char)value.i);
            break;
        case UCHAR:
            sprintf(aname, "%d", (unsigned char)value.i);
            break;
        case SHORT:
            sprintf(aname, "%d", (short)value.i);
            break;
        case USHORT:
            sprintf(aname, "%d", (unsigned short)value.i);
            break;
        case INT:
            sprintf(aname, "%d", value.i);
            break;
        case POINTER:
            sprintf(aname, "%d", value.i);
            break;
        case UINT:
            sprintf(aname, "%u", value.ui);
            break;
        case LONG:
            sprintf(aname, "%ld", value.l);
            break;
        case ULONG:
            sprintf(aname, "%u", value.ul);
            break;
        case FLOAT:
            sprintf(aname, "@fld%d", float_counter++);
            break;
        case DOUBLE:
            sprintf(aname, "@fld%d", float_counter++);
            break;
        case VOID:
            sprintf(aname, "%d", value.i);
            break;
        default:
            printf("unsupport variable type!\n");
            exit(1);
    }
}

struct symbol *sym_lookup_constant(struct type *type, union val value)
{
    struct symbol *sym;
    char name[MAX_SYM_KEY_LENTH];
    
    number_get_name(type, value, name);
    if (!constant_tab->find(constant_tab, name, &sym))
    {
        if (sym->kind == SK_Constant)
        {
            return sym;
        }
        assert(0);
    }
    return NULL;
}

struct symbol *sym_add_constant(struct type *type, union val value)
{
    struct symbol *sym, *p;
    if (sym = sym_lookup_constant(type, value))
    {
        if (sym->kind == SK_Constant)
        {
            return sym;
        }
        assert(0);
    }
    ALLOC(sym, struct symbol);
    
    sym->name = (char *)calloc(1, MAX_SYM_KEY_LENTH);
    if (!(sym->name))
    {
        printf("calloc err!\n");
        exit(-1);
    }
    sym->aname = (char *)calloc(1, MAX_SYM_KEY_LENTH);
    if (!(sym->aname))
    {
        printf("calloc err!\n");
        exit(-1);
    }
    sym->kind = SK_Constant;
    sym->value = value;
    sym->type = type;
    number_get_name(sym->type, value, sym->name);
    number_get_aname(sym->type, value, sym->aname);
    constant_tab->insert(constant_tab, sym->name, sym);

    return sym;
}

struct symbol *sym_add_enum_tag(char *id, struct type *type, struct location *loc)
{
    struct symbol *sym;

    ALLOC(sym, struct symbol);
    sym->kind = SK_Tag;
    sym->name = id;
    sym->type = type;
    sym->value.p = id;
    tag_tab[cur_nesting_lvl]->insert(tag_tab[cur_nesting_lvl], id, sym);
    return sym;
}

struct symbol *sym_add_enumerator(char *name, struct type *type, union val value, struct location *loc)
{
    struct symbol *sym;
    ALLOC(sym, struct symbol);
    
    sym->kind = SK_EnumConstant;
    sym->type = type;
    sym->name = name;
    sym->value = value;
    sym->level = cur_nesting_lvl;
    id_tab[cur_nesting_lvl]->insert(id_tab[cur_nesting_lvl], sym->name, sym);

    return sym;
}



struct symbol *sym_add_typedef(char *id, struct type *type, struct location *loc)
{
    struct symbol *sym;

    ALLOC(sym, struct symbol);
    sym->kind = SK_TypedefName;
    sym->name = id;
    sym->type = type;
    sym->level = cur_nesting_lvl;
    sym->value.p = id;
    id_tab[cur_nesting_lvl]->insert(id_tab[cur_nesting_lvl], id, sym);
    return sym;
}

struct symbol *sym_add_variable(char *id, struct type *type, int sto_class_spec_tok, int defined, struct location *loc)
{
    struct symbol_var *sym;
    ALLOC(sym, struct symbol_var);
    sym->kind = SK_Variable;
    sym->name = id;
    sym->type = type;
    sym->sto_class_spec_tok = sto_class_spec_tok;
    sym->level = cur_nesting_lvl;
    sym->value.p = id;
    id_tab[cur_nesting_lvl]->insert(id_tab[cur_nesting_lvl], id, sym);
    return (struct symbol *)sym;
}

struct symbol *sym_add_function(char *id, struct type *type, int sto_class_spec_tok, struct location *loc)
{
    struct symbol_func *sym;
    ALLOC(sym, struct symbol_func);
    sym->kind = SK_Function;
    sym->name = id;
    sym->aname = id;
    sym->type = type;
    sym->value.p = id;
    sym->sto_class_spec_tok = sto_class_spec_tok;
    /*
    if (cur_nesting_lvl)
    {
        id_tab[cur_nesting_lvl]->insert(id_tab[cur_nesting_lvl], id, sym);
    }
    */
    id_tab[0]->insert(id_tab[0], id, sym);
    return (struct symbol *)sym;
}

struct symbol *sym_add_tag(char *id, struct type *type, struct location *loc)
{
    struct symbol *sym;
    ALLOC(sym, struct symbol);
    sym->kind = SK_Tag;
    sym->name = id;
    sym->type = type;
    sym->level = cur_nesting_lvl;
    sym->value.p = id;
    tag_tab[cur_nesting_lvl]->insert(tag_tab[cur_nesting_lvl], id, sym);
    return sym;
}

struct symbol *sym_create_temp(struct type *type)
{
    struct symbol_temp *sym;
    ALLOC(sym, struct symbol_temp);
    
    sym->name = (char *)calloc(1, MAX_SYM_KEY_LENTH);
    if (!(sym->name))
    {
        printf("calloc err!\n");
        exit(-1);
    }
    sym->kind = SK_Temp;
    sym->type = type;
    sym->level = cur_nesting_lvl;
    sprintf(sym->name, "@T%d", temp_counter++);
    //tag_tab[cur_nesting_lvl]->insert(tag_tab[cur_nesting_lvl], sym->name, sym);
    return (struct symbol *)sym;
}

struct symbol *sym_create_label()
{
    struct symbol_label *sym;
    ALLOC(sym, struct symbol_label);
    
    sym->name = (char *)calloc(1, MAX_SYM_KEY_LENTH);
    if (!(sym->name))
    {
        printf("calloc err!\n");
        exit(-1);
    }
    sym->kind = SK_Label;
    sym->level = cur_nesting_lvl;
    sym->precs = vector_init(0);
    sym->aname = sym->name;
    sprintf(sym->name, "@L%d", label_counter++);
    return sym;
}

struct symbol *sym_add_string(struct type *type, char *str, struct location *loc)
{
    struct symbol *sym;
    if (!constant_tab->find(constant_tab, str, &sym))
    {
        if (sym->kind == SK_String)
        {
            return sym;
        }
        assert(0);
    }
    ALLOC(sym, struct symbol);
    sym->kind = SK_String;
    sym->type = type;
    if (!(sym->name = (char*)calloc(1, sizeof(strlen(str) + 3))))
    {
        printf("calloc err!\n");
        exit(-1);
    }
    sprintf(sym->name, "@STR%d", string_counter++);
    sym->aname = sym->name;
    sym->value.p = str;
    constant_tab->insert(constant_tab, str, sym);
    return sym;
}

struct symbol *sym_lookup_id(char *id)
{
    int i;
    struct symbol *sym;
    /*
    if (cur_nesting_lvl == MAX_NESTING_LEVEL)
    {
        if (!id_tab[cur_nesting_lvl]->find(id_tab[cur_nesting_lvl], id, &sym))
        {
            return sym;
        }
        if (!id_tab[0]->find(id_tab[0], id, &sym))
        {
            return sym;
        }
        return NULL;
    }
    */
    for (i = cur_nesting_lvl; i >= 0; i--)
    {
        if (!id_tab[i]->find(id_tab[i], id, &sym))
        {
            return sym;
        }
    }
    return NULL;
}

struct symbol *sym_lookup_tag(char *id)
{
    int i;
    struct symbol *sym;
    /*
    if (cur_nesting_lvl == MAX_NESTING_LEVEL)
    {
        if (!tag_tab[cur_nesting_lvl]->find(tag_tab[cur_nesting_lvl], id, &sym))
        {
            return sym;
        }
        if (!tag_tab[0]->find(tag_tab[0], id, &sym))
        {
            return sym;
        }
        return NULL;
    }
    */
    for (i = cur_nesting_lvl; i >= 0; i--)
    {
        if (!tag_tab[i]->find(tag_tab[i], id, &sym))
        {
            return sym;
        }
    }
    return NULL;
}

struct symbol *sym_id_traverse_in_cur_lvl(int *start, int *cnt)
{
    return id_tab[cur_nesting_lvl]->hash_iter(id_tab[cur_nesting_lvl], start, cnt);
}

struct symbol *sym_tag_traverse_in_cur_lvl(int *start, int *cnt)
{
    return tag_tab[cur_nesting_lvl]->hash_iter(tag_tab[cur_nesting_lvl], start, cnt);
}
