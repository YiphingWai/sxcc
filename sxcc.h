#ifndef __SXCC_H__
#define __SXCC_H__

#define rep_error(str, args...) do {\
                                    printf("(%s,%d):", __FILE__, __LINE__);\
                                    printf(str, ##args);\
                                    printf("\n");\
                                    assert(0); } while(0)
#define lex_error(str, args...) do {\
                                    printf("(%s,%d):", __FILE__, __LINE__);\
                                    printf(str, ##args);\
                                    printf("\n%c%c%c%c%c\n", pfile[cur], pfile[cur+1], pfile[cur+2], pfile[cur+3], pfile[cur+4]);\
                                    assert(0); } while(0)

#define ALLOC(ptr, type) do{ if (!(ptr = (type *)calloc(1, sizeof(type)))) {printf("calloc err!"); assert(0);}}while(0)

#endif