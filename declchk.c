
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "decl.h"
#include "stmt.h"
#include "symtab.h"
#include "sxcc.h"
#include "hash.h"
#include "type.h"

// for stmtchk & stmttran
struct decl_func_node *curr_func = NULL;
struct symbol_func *curr_func_sym = NULL;


static void typedef_name_check(struct decl_node *decl);
static struct decl_common_declor_node *declarator_check(struct decl_common_declor_node *declor);
static struct type *struct_or_union_specifier_check(struct decl_struct_spec_node *struct_specer);
struct type *enum_specifier_check(struct decl_enum_spec_node *enum_spec);

// the result of semantic analysis records in struct decl_specer_list_node
static void declaration_specifiers_check(struct decl_specer_list_node *specer_list)
{
	int qualer = 0;
	int long_short_flag = 0, int_flag = 0;
	int type = 0, sign = 0;
	int sign_cnt = 0, type_cnt = 0;
	struct symbol *sym;
	struct decl_specer_node *specer;

	/* 
	storage-class-specifier:
		typedef
		extern
		static
		auto
		register
	*/
	specer = specer_list->stg_specer;
	if (specer)
	{
		if (specer->next)
		{
			rep_error("storage class specifier can only have one");
			exit(1);
		}
		specer_list->sto_class_spec_tok = specer->tok_type;
	}
	/*
	type-qualifier:
		const
		volatile
	*/
	specer = specer_list->ty_qualer;
	if (specer)
	{
		if (specer->next)
		{
			rep_error("storage class specifier can only have one");
			exit(1);
		}
		if (specer->tok_type == TK_CONST)
		{
			qualer |= CONST;
		}
		else
		{
			qualer |= VOLATILE;
		}
	}
	/*
	type-specifier:
		void
		char
		short
		int
		long
		float
		double
		signed
		unsigned
		struct-or-union-specifier
		enum-specifier
		typedef-name
	*/
	specer = specer_list->ty_specer;
	while (specer)
	{
		switch (specer->tok_type)
		{
			case TK_STRUCT:
			case TK_UNION:
				specer_list->type = struct_or_union_specifier_check(specer->priv_node);
				type_cnt++;
				break;
			case TK_ENUM:
				specer_list->type = enum_specifier_check(specer->priv_node);
				type_cnt++;
				break;
			case TK_TYPEDEF:
				// typedef int INT;
				// specer_list->type = typedef_name_check(specer->priv_node);
				// do nothing, we get the type of typedef_name later in global/local_declaration_check()
				type_cnt++;
				break;
			case TK_SIGNED:
			case TK_UNSIGNED:
				sign = specer->tok_type;
				sign_cnt++;
				break;
			case TK_DOUBLE:
			case TK_FLOAT:
			case TK_CHAR:
			case TK_VOID:
				type = specer->tok_type;
				type_cnt++;
				break;
			// long int/short int
			case TK_LONG:
			case TK_SHORT:
				type = specer->tok_type;
				long_short_flag++;
				type_cnt = int_flag ? type_cnt : type_cnt + 1;
				break;
			case TK_INT:
				if (!long_short_flag)
				{
					type = specer->tok_type;
				}
				int_flag++;
				type_cnt = long_short_flag ? type_cnt : type_cnt + 1;
				break;
			case TK_ID:
				sym = sym_lookup_id(((struct decl_typedef_spec_node *)specer->priv_node)->name);
				assert(sym && sym->kind == SK_TypedefName);
				specer_list->type = sym->type;
				type_cnt++;
				break;
			default:
				assert(0);
		}
		specer = specer->next;
	}
	if (!type_cnt)
	{
		type = TK_INT;
	}
	if (type_cnt > 1 || sign_cnt > 1 || int_flag > 1 || long_short_flag > 1)
	{
		goto err;
	}
	switch (type)
	{
		case TK_DOUBLE:
			if (sign_cnt)
			{
				goto err;
			}
			specer_list->type = &basic_types[DOUBLE];
			break;
		case TK_FLOAT:
			if (sign_cnt)
			{
				goto err;
			}
			specer_list->type = &basic_types[FLOAT];
			break;
		case TK_VOID:
			if (sign_cnt)
			{
				goto err;
			}
			specer_list->type = &basic_types[VOID];
			break;
		case TK_CHAR:
			sign = sign == TK_UNSIGNED;
			specer_list->type = &basic_types[CHAR + sign];
			break;
		// long int/short int
		case TK_LONG:
			sign = sign == TK_UNSIGNED;
			specer_list->type = &basic_types[LONG + sign];
			break;
		case TK_SHORT:
			sign = sign == TK_UNSIGNED;
			specer_list->type = &basic_types[SHORT + sign];
			break;
		case TK_INT:
			sign = sign == TK_UNSIGNED;
			specer_list->type = &basic_types[INT + sign];
			break;
	}
	specer_list->type = qualify(specer_list->type, qualer);
	return;
err:
	rep_error("Illegal type specifier");
	exit(1);
}


static struct decl_array_declor_node *array_declor_check(struct decl_array_declor_node *arr)
{
	arr->next_declor = declarator_check(arr->next_declor);

	if (arr->const_expr)
	{
		arr->const_expr = expression_check(arr->const_expr);
		if ((arr->const_expr->op != OP_CONST) && (!IS_INTEG_TYPE(arr->const_expr->type)))
		{
			rep_error("the size of array must be const");
			exit(1);
		}
	}
	arr->len = arr->const_expr ? arr->const_expr->value.i : 0;
	return arr;
}
/*
	int i[5];
	in this case, 'int' is specifier, i[5] is declarator, and 'i' means an array which contain 5 int numbers.
	to know the full type informations of 'i', we must composite the information from both specifier and declarator.
*/
static struct type *type_composite(struct type *specer_type, struct decl_common_declor_node *declor)
{
	struct type *type;
	type = specer_type;
	if (declor)
	{
		switch(declor->kind)
		{
			case NK_ArrayDeclarator:
				type = type_composite(specer_type, declor->next_declor);
				if (((struct decl_array_declor_node*)declor)->len < 0)
				{
					rep_error("the length of array can not be negative");
					exit(-1);
				}
				if (type->categ == FUNCTION)
				{
					rep_error("function array is illegal");
					exit(-1);
				}
				if (is_incomplete_type(type, 0))
				{
					rep_error("array has incomplete element type");
					exit(1);
				}
				type = create_array(((struct decl_array_declor_node*)declor)->len, type);
				break;
			case NK_PointerDeclarator:
				type = type_composite(specer_type, declor->next_declor);
				type = qualify(create_pointer_type(type), ((struct decl_pointer_declor_node *)declor)->qual);
				break;
			case NK_FunctionDeclarator:
				type = type_composite(specer_type, declor->next_declor);
				if (type->categ == ARRAY)
				{
					rep_error("function can not return an array");
					exit(1);
				}
				if (type->categ == FUNCTION)
				{
					rep_error("function can not return a function");
					exit(1);
				}
				if (((struct decl_func_declor_node *)declor)->para_type_list)
				{
					type = create_function(type, ((struct decl_func_declor_node *)declor)->para_type_list->has_ellipsis, 
											((struct decl_func_declor_node *)declor)->params);
				}
				else
				{
					type = create_function(type, 0, NULL);
				}
			break;
			default:
				// Do nothing
				break;
		}
	}
	return type;
}

static void parameter_type_list_check(struct decl_func_declor_node *func)
{
	int i, cnt;
	struct decl_para_type_list_node *para_type_list;
	struct decl_para_node *para_node;
	struct parameter_type *para;
	struct type *type;
	struct symbol *sym, *tmpsym;


	para_type_list = func->para_type_list;
	if (para_type_list)
	{
		para_node = para_type_list->para_list;
	}
	else
	{
		para_node = NULL;
	}
	while (para_node)
	{

		if (func->is_definition)
		{
			// f(void) -- legal
			if (para_node->spec && 
				para_node->spec->ty_specer->tok_type == TK_VOID && 
				!para_node->spec->ty_specer->next &&
				!para_node->spec->ty_qualer &&
				!para_node->spec->stg_specer &&
				!para_node->spec->next &&
				!para_node->declor)
			{
				goto ok;
			}
			// f(int i)	-- legal
			if (para_node->declor)
			{
				goto ok;
			}
			rep_error("in function definition, parameters must have a name or be void");
			exit(1);
		}
ok:
		declaration_specifiers_check(para_node->spec);
		
		type = para_node->spec->type;
		if (para_node->spec && para_node->spec->sto_class_spec_tok && para_node->spec->sto_class_spec_tok != TK_REGISTER)
		{
			rep_error("illegal storage class specifier");
			exit(1);
		}
		
		para_node->declor = declarator_check(para_node->declor);
		// has parameter
		type = type_composite(type, para_node->declor);
		if (type)
		{
			// f(void)
			if (type->categ == VOID)
			{
				if (para_node->declor == NULL && para_node->next == NULL && type->qualer == 0 && func->params->len == 0)
				{
					return ;
				}
				rep_error("void must be the only parameter");
				exit(1);
			}

			type = unqualify(type);
			if (type->categ == FUNCTION)
			{
				type = create_pointer_type(type);
			}
			else if (type->categ == ARRAY)
			{
				type = create_pointer_type(type->basic_type);
			}
		}
		else
		{
			rep_error("illegal parameter declaration");
			exit(1);
		}

		if (func->is_definition && is_incomplete_type(type, 1))
		{
			/*
				in declaration, using a struct/union which has not been declared yet
				but in definition, they are not allowed.
				e.g.
				int f(struct aaa bbb); is legal
				int f(struct aaa bbb){...} is illegal
			*/
			rep_error("parameters can not contain incomplete type");
			exit(1);
		}
	
		for (i = 0; i < func->params->len; i++)
		{
			if (para_node->declor)
			{
				para = func->params->get(func->params, i);
				// f(int a, char b)
				if (para->id && !strcmp(para->id, para_node->declor->id))
				{
					rep_error("parameter '%s' redefined", para_node->declor->id);
					exit(-1);
				}
			}
			//else f(int, char)
		}
		ALLOC(para, struct parameter_type);
		if (para_node->declor)
		{
			// f(int a)
			para->id = para_node->declor->id;
		}
		else
		{
			// f (int)
			para->id = NULL;
		}
		para->sto_class_spec_tok = para_node->spec->sto_class_spec_tok;
		para->type = type;
		func->params->push_back(func->params, para);
		for (i = 0, cnt = 0; sym = sym_id_traverse_in_cur_lvl(&i, &cnt);)
		{
			if (sym->kind == SK_TypedefName)
			{
				func->typedef_name_sym->push_back(func->typedef_name_sym, sym);
			}
		}
		para_node = para_node->next;
	}

}

static struct decl_func_declor_node *function_declor_check(struct decl_func_declor_node *func)
{
	func->next_declor = declarator_check(func->next_declor);
	func->params = vector_init(0);
	func->typedef_name_sym = vector_init(0);
	enter_scope();
	parameter_type_list_check(func);
	exit_scope();
	return func;
}

// take the name of pointer up to the root
// if the pointer has no a name, there will be a error in previous stage
static struct decl_pointer_declor_node *pointer_declor_check(struct decl_pointer_declor_node *pointer)
{
	if (pointer && (pointer->kind == NK_PointerDeclarator))
	{
		// concrete declaration: int *xxx;
		if (pointer->next_declor)
		{
			pointer->next_declor = pointer_declor_check(pointer->next_declor);
		}
		/*
		else	// abstract declaration: int *;
		{
			p->id = NULL;
		}
		*/
		return pointer;
	}
	else if (pointer)
	{
		return declarator_check(pointer);
	}
	assert(0);
	
}

static struct decl_common_declor_node *declarator_check(struct decl_common_declor_node *declor)
{
	struct decl_common_declor_node *name_declor = NULL;
	if (!declor)
	{
		return NULL;
	}

	if (declor->kind == NK_NameDeclarator)
	{
		if (!declor->next_declor)
		{
			return declor;
		}
		name_declor = declor;
		declor = declor->next_declor;
	}

	switch (declor->kind)
	{
		case NK_PointerDeclarator:
			declor = pointer_declor_check(declor);
			break;
		
		case NK_ArrayDeclarator:
			declor = array_declor_check(declor);
			break;
		
		case NK_FunctionDeclarator:
			declor = function_declor_check(declor);
			break;
		
		default:
			assert(0);
	}
	if (name_declor)
	{
		declor->id = name_declor->id;
		free(name_declor);
	}
	return declor;
}
/*
type-name:
    specifier-qualifier-list abstract-declarator<opt>
*/
void type_name_check(struct decl_type_name_node *type_name)
{
	if (type_name->spec->stg_specer)
	{
		rep_error("the type name must not contain storage-class-specifier");
		exit(1);
	}
	declaration_specifiers_check(type_name->spec);
	type_name->type = type_name->spec->type;
	if (type_name->absdeclor)
	{
		type_name->absdeclor = declarator_check(type_name->absdeclor);
		type_name->type = type_composite(type_name->spec->type, type_name->absdeclor);
	}
}
/*
(1) 
		struct Data{
			a;	-----------	fty == NULL
		}
(2)
		struct Data{
			void f(){	-----------	fty-categ == FUNCTION
			}
		}
(3)		(fty->size == 0 && fty->categ != ARRAY)
		only  flexible array at the end of a struct is legal.

Bug:
	typedef struct{
		int b;
		struct{
		}a;	--------------> fty->size == 0
		int c;
	}Data;
*/

static void struct_declaration_check(struct record_type *struct_type, struct decl_struct_decl_node *decl)
{
	struct type *field_type;
	struct type *field_specer_type;
	struct decl_common_declor_node *field_declor; 
	if (decl->specer_list)
	{
		declaration_specifiers_check(decl->specer_list);
		field_specer_type = decl->specer_list->type;
	}
	else
	{
		field_specer_type = &basic_types[INT];
	}
	field_declor = decl->declor_list;
	// anonymous number (include nstruct or enum member)
	/*
		struct 
		{
			int ;
		};
	*/
	if (!field_declor)
	{
		add_field(struct_type, NULL, field_specer_type);
		return;
	}
	// int a, b;
	while (field_declor)
	{
		field_declor = declarator_check(field_declor);
		field_type = type_composite(field_specer_type, field_declor);

		if ((!field_type) || field_type->categ == FUNCTION || is_incomplete_rec_type(field_type) ||
			is_incomplete_enum_type(field_type))
		{
			rep_error("illegal type");
			exit(1);
		}

		if (field_type->categ == ARRAY && field_type->size == 0 && field_declor->next)
		{
			rep_error("zero-length array must be the last member in the struct");
			exit(1);
		}

		if (field_type->categ == ARRAY && field_type->size == 0 && (!(field_declor->next)))
		{
			((struct record_type *)struct_type)->has_flex_array = 1;
		}
		
		if (field_declor->id && lookup_field(struct_type, field_declor->id))
		{
			rep_error("re-definition member");
			exit(1);
		}

		add_field(struct_type, field_declor->id, field_type);
		
		field_declor = field_declor->next;
	}

}
/*
(1). struct Data1 
(2). struct {int a; int b;}
(3). struct Data2{int a; int b;}
(4). struct
*/
static struct type *struct_or_union_specifier_check(struct decl_struct_spec_node *struct_specer)
{
	struct symbol *sym;
	struct record_type *rec_type;
	enum type_categ categ;
	struct decl_struct_decl_node *decl;
	struct record_field *tmp;

	categ = struct_specer->kind == NK_StructSpecifier ? STRUCT : UNION;
	// (1). struct Data1 
	if (struct_specer->id && (!(struct_specer->decl_list)))
	{
		if (sym = sym_lookup_tag(struct_specer->id))
		{
			if (sym->type->categ != categ)
			{
				rep_error("inconsistant struct/union declaration");
				exit(1);
			}
			rec_type = (struct record_type *)(sym->type);
		}
		else
		{
			rec_type = create_record(struct_specer->id, categ);
			sym_add_tag(struct_specer->id, (struct type *)rec_type, NULL);
		}
		return (struct type*)rec_type;
	}
	// (2). struct {int a; int b;}
	if ((!(struct_specer->id)) && struct_specer->decl_list)
	{
		rec_type = create_record(struct_specer->id, categ);
		decl = struct_specer->decl_list;
		while (decl)
		{
			struct_declaration_check(rec_type, decl);
			decl = decl->next;
		} 
		layout_record(rec_type, 0);
		rec_type->complete = 1;
		tmp =rec_type->fields->get(rec_type->fields, 0);
		return (struct type*)rec_type;
	}
	// (3). struct Data2{int a; int b;}
	if (struct_specer->id && struct_specer->decl_list)
	{
		if ((sym = sym_lookup_tag(struct_specer->id)) && (sym->level == cur_nesting_lvl))
		{
			if (sym->type->categ != categ)
			{
				rep_error("inconsistant struct/union declaration");
				exit(1);
			}
			if (!is_incomplete_rec_type(sym->type))
			{
				rep_error("struct/union re-declaration");
				exit(1);
			}
			// reach here means we finally find the definition of struct Data2 which has declared before.
			rec_type = sym->type;
		}
		else
		{
			rec_type = create_record(struct_specer->id, categ);
			sym_add_tag(struct_specer->id, rec_type, NULL);
		}
		decl = struct_specer->decl_list;
		while (decl)
		{
			struct_declaration_check(rec_type, decl);
			decl = decl->next;
		} 
		layout_record(rec_type, 0);
		((struct record_type *)rec_type)->complete = 1;
		return (struct type*)rec_type;
	}
	// (4). struct
	// has casted a error at syntax stage
	// do nothing
	assert(0);
	return NULL;
}
/*
enumerator:
    enumeration-constant
    enumeration-constant = constant-expression
*/
static int enumerator_check(struct decl_enumor_node *enumor, int val)
{
	struct symbol *sym;
	union val value;
	struct type *enum_type;

	sym = sym_lookup_id(enumor->id);
	if (sym && sym->level == cur_nesting_lvl)
	{
		if (sym->type->categ == ENUM)
		{
			rep_error("enum %s re-declare", enumor->id);
			exit(1);
		}
		else
		{
			rep_error("enum %s has been declared as a different type", enumor->id);
			exit(1);
		}
	}
	if (enumor->const_expr)
	{
		enumor->const_expr = constant_expression_check(enumor->const_expr);
		if (!(enumor->const_expr))
		{
			rep_error("enumerator must be a integer constant");
			exit(1);
		}
		value = enumor->const_expr->value;
	}
	else
	{
		value.i = val;
	}
	sym_add_enumerator(enumor->id, &basic_types[INT], value, NULL);
	return value.i;
}
/*
(1). enum Data1 
(2). enum {a, b}
(3). enum Data2{a, b}
(4). enum
*/
struct type *enum_specifier_check(struct decl_enum_spec_node *enum_spec)
{
	int enum_val = 0;
	struct symbol *sym;
	struct type *enum_type;
	struct decl_enumor_node *enumor;

	// (1). enum Data1 
	if (enum_spec->id && (!(enum_spec->enumor)))
	{
		sym = sym_lookup_tag(enum_spec->id);
		if (sym)
		{
			if (sym->type->categ != ENUM)
			{
				rep_error("inconsistant enum declaration");
				exit(1);
			}
			enum_type = sym->type;
		}
		else
		{
			enum_type = create_enum(enum_spec->id);
			sym_add_enum_tag(enum_spec->id, enum_type, NULL);
		}
		return enum_type;
	}
	// (2). enum {a, b}
	if ((!(enum_spec->id)) && enum_spec->enumor)
	{
		enum_type = create_enum(NULL);
		enumor = enum_spec->enumor;
		while (enumor)
		{
			enum_val = enumerator_check(enumor, enum_val);
			enum_val++;
			enumor = enumor->next;
		}
		((struct enum_type *)enum_type)->complete = 1;
		//sym_add_enum_tag(NULL, enum_type, NULL);
		return enum_type;
	}
	// (3). enum Data2{a, b}
	if (enum_spec->id && enum_spec->enumor)
	{
		enumor = enum_spec->enumor;
		sym = sym_lookup_tag(enumor->id);
		if (sym)
		{
			if (sym->type->categ != ENUM)
			{
				rep_error("inconsistant enum declaration");
				exit(1);
			}
			if (!is_incomplete_enum_type(sym->type))
			{
				rep_error("struct/union re-declaration");
				exit(1);
			}
			enum_type = sym->type;
		}
		else
		{
			enum_type = create_enum(enum_spec->id);
			sym_add_enum_tag(enum_spec->id, enum_type, NULL);
		}
		enumor = enum_spec->enumor;
		while (enumor)
		{
			enum_val = enumerator_check(enumor, enum_val);
			enum_val++;
			enumor = enumor->next;
		}
		((struct enum_type *)enum_type)->complete = 1;
		return enum_type;
	}
	// (4). enum
	// has casted a error at syntax stage
	// do nothing
	assert(0);
	return NULL;
}
/*
(1). typedef int INT32[5];
(2). typedef int;
(3). typedef INT32;
(4). typedef int A_Name_Has_Declared_Before;
*/
static void typedef_name_check(struct decl_node *decl)
{
	struct symbol *sym;
	struct type *type;
	struct decl_specer_list_node *spec; 
    struct decl_init_declor_node *init_declor;

	// spec has been checked in caller
	spec = decl->spec;
	init_declor = decl->init_declor_list;
	if (init_declor)
	{
		init_declor->declor = declarator_check(init_declor->declor);
		if (!(init_declor->declor->id))
		{
			// (2)
			// in this case, we don't need to do anything
			return NULL;
		}
		if (init_declor->initer)
		{
			rep_error("can not initialize a typedef name");
			exit(1);
		}
		// spec has been checked in caller
		if (spec)
		{
			type = type_composite(spec->type, init_declor->declor);
		}
		else
		{
			// (3)
			type = &basic_types[INT];
		}
		sym = sym_lookup_id(init_declor->declor->id);
		if (sym && sym->level == cur_nesting_lvl && (sym->kind != SK_TypedefName || !is_compatible_type(sym->type, type)))
		{
			// (4)
			rep_error("re-declaration of %s", init_declor->declor->id);
			exit(1);
		}

		if (sym && sym->level == cur_nesting_lvl && sym->kind == SK_TypedefName && is_compatible_type(sym->type, type))
		{
			type = greatest_common_type(sym->type, type);
			sym->type = type;
			return;
		}

		sym_add_typedef(init_declor->declor->id, type, NULL);

		init_declor = init_declor->next;
	}
	//return type;
}

static struct decl_initer_node *CheckInitializerInternal(struct init_data **tail, struct decl_initer_node *init, struct type *type, 
                                               int *offset, int *error_no)
{
	struct decl_initer_node *p;
	int size = 0, cnt = 0;
	struct init_data *init_data;
	struct type *ltype, *rtype;
	struct record_field *field;

	if (IS_SCALAR_TYPE(type))
	{
		p = init;
		while(p->initer_list)
		{
			printf("braces around scalar initializer\n");
			p = ((struct decl_initer_node *)p)->initer_list;
		}	
		p->assign_expr = adjust(expression_check(p->assign_expr));
		if (! can_assign(type, p->assign_expr))
		{
			rep_error("Wrong initializer");
			*error_no = 1;
		}
		else
		{
			/******************************************************
					see 	(1)	examples/str/strAddress.c
						(2)	CheckInitConstant(AstInitializer init)
				If we cast const address to int here,
					CheckInitConstant won't treat it as an const address.
				So we don't do the casting in this case.
			 ******************************************************/
			ltype = type, rtype = p->assign_expr->type;
			if( !(((ltype->categ == POINTER) && IS_INTEG_TYPE(rtype) || 
					(rtype->categ == POINTER) && IS_INTEG_TYPE(ltype)) && (ltype->size == rtype->size)))
			{
				p->assign_expr = cast(type, p->assign_expr);		
			}
		
		}
		ALLOC(init_data, struct init_data);
		init_data->offset = *offset;
		init_data->expr = p->assign_expr;
		*tail = init_data;
		tail = &(init_data->next);
		
		return init->next;
	}
	else if (type->categ == UNION)
	{
		p = init->initer_list ? init->initer_list : init;
		/***********************************************
		(1)	Legal
			union Data{	
				char * str;
				double addr;
			};
			union Data dt = {"12345"};		
		(2)	Illegal
			union Data{	
				char * str;
				double addr;
			};
			union Data dt = {"12345"};		
		 ************************************************/
		type =  ((struct record_field *)(((struct record_type *)type)->fields->get(((struct record_type *)type)->fields, 0)))->type;

		p = CheckInitializerInternal(tail, p, type, offset, error_no);
		while (*tail)
		{
			tail = &((*tail)->next);
		}

		if (init->initer_list)
		{
			if (p != NULL)
			{
				printf("excess elements in union initializer");
			}
			return init->next;
		}
		return p;
	}
	else if (type->categ == ARRAY)
	{
		int start = *offset;
		p = init->initer_list ? init->initer_list : init;
		if (((init->initer_list && ! p->initer_list && p->next == NULL) || ! init->initer_list) && 
				type->basic_type->categ != ARRAY)		
		{
			p->assign_expr = adjust(expression_check(p->assign_expr));
			size = p->assign_expr->type->size;
			

			if(type->size == 0)
			{
				type->size = size;
				if(type->basic_type->size != 0)
				{
					((struct array_type *)type)->len = size / type->basic_type->size;
				}
			}
			else if (p->assign_expr->type->basic_type && 
					type->size == size - p->assign_expr->type->basic_type->size)
			{
				//	char str[3] = "abc";	or	int arr[3] = L"123";		
				p->assign_expr->type->size = size - p->assign_expr->type->basic_type->size;
			}
			else if (type->size < size)
			{
				//	L"123456789"		---->	On Linux, 	size is 40
				//	"abcdef"			---->	size is 7
				//PRINT_DEBUG_INFO(("str->size = %d",size));
				if (IS_ARITH_TYPE(p->assign_expr->type))
				{
					p->assign_expr->type = type->basic_type;
				}
				else
				{
					p->assign_expr->type->size = type->size;
				}
				printf("initializer-string for char/wchar array is too long");
			}
			ALLOC(init_data, struct init_data);
			init_data->offset = *offset;
			init_data->expr = p->assign_expr;
			*tail = init_data;
			tail = &(init_data->next);

			return init->next;
		}

		while (p != NULL)
		{
			p = CheckInitializerInternal(tail, p, type->basic_type, offset, error_no);
			while (*tail)
			{
				tail = &((*tail)->next);
			}
			size += type->basic_type->size;
			*offset = start + size;
			if (type->size == size)
			{
				break;
			}
		}
		
		if (type->size == 0){	
			/********************************************
				calculate the len of array
				int arr[] = {1,2,3,4,5};
			 ********************************************/

			if(type->basic_type->size != 0 && ((struct array_type *)type)->len == 0)
			{
				//PRINT_CUR_ASTNODE(init);
				((struct array_type *)type)->len = size / type->basic_type->size;
			}
			type->size = size;			
		}		
	
		if (init->initer_list)
		{
			if(p)
			{
				printf("excess elements in array initializer");
			}
			return init->next;
		}
		return p;
	}
	else if (type->categ == STRUCT)
	{
		int start = *offset;
		field = ((struct record_type *)type)->fields->get(((struct record_type *)type)->fields, cnt++);
		p = init->initer_list ? init->initer_list : init;
		
		while (cnt <= ((struct record_type *)type)->fields->len && p)
		{
			*offset = start + field->offset;
			/*
			// see examples/init/struct.c
			if( ((field->type->categ == STRUCT || field->type->categ == UNION) && field->id == NULL))
			{
				*offset = start;
			}	
			*/
			//PRINT_DEBUG_INFO(("*offset = %d , start = %d , fld->offset = %d",*offset, start, fld->offset));
			p = CheckInitializerInternal(tail, p, field->type, offset, error_no);
			while (*tail)
			{
				tail = &((*tail)->next);
			}
			field = ((struct record_type *)type)->fields->get(((struct record_type *)type)->fields, cnt++);
		}
		
		*offset = start + type->size;
		//PRINT_DEBUG_INFO(("*offset = %d , start = %d, ty->size= %d",*offset, start, ty->size));
	
		if (init->initer_list)
		{
			if (p != NULL)	
			{
				printf("excess elements in struct initializer");
			}	
			return init->next;
		}

		return p;
	}

	return init;
}
	
/**********************************************************
	char array[] = "abcdef";
	@expr		"abcdef"
	@bty		char
	@return		0		error
				1		pass
 **********************************************************/
static int CheckCharArrayInit (struct expr_node *expr, struct type *basic_ty)
{
	if(expr->op == OP_STR)
	{
		if( (basic_ty->categ == CHAR || basic_ty->categ == UCHAR) && expr->type->basic_type->categ == CHAR)
		{
			return 1;
		}
	}
	rep_error("array initializer must be an initializer list");
	return 0;
}

static void CheckInitializer(struct decl_initer_node *init, struct type *type)
{
	int offset = 0, error_no = 0;
	struct init_data **tail;

	tail = &(init->idata);

	if(type->categ == ARRAY && !init->initer_list)
	{
		if(!CheckCharArrayInit(init->assign_expr,type->basic_type))
		{
			return ;
		}		
	}
	else if ((type->categ == STRUCT || type->categ == UNION) && ! init->initer_list)
	{
		/**************************************************
			struct Data dt;
			struct Data dt2 = dt;		// Legal	in local scope, not in global scope
		 **************************************************/
		init->assign_expr = adjust(expression_check(init->assign_expr));
		if (! can_assign(type, init->assign_expr))
		{
			rep_error("Wrong initializer");
		}
		else
		{
			ALLOC(init->idata, struct init_data);
			init->idata->expr = init->assign_expr;
			init->idata->offset = 0;
			init->idata->next = NULL;
		}
		return;
	}

	CheckInitializerInternal(tail, init, type, &offset, &error_no);

	if (error_no)
	{
		return;
	}

}

/*
1. function_name
2. array_name
3. &(something)
4. struct.....array
5. multidimensional_array[1] 
6. add/sub a constant of the above items
*/
static struct expr_node *check_constant_address(struct expr_node *expr)
{
	int offset = 0;
	struct type *type; 
	struct expr_node *addr, *p;
	if (expr->type->categ != POINTER)
	{
		return NULL;
	}
	type = expr->type;
	//function_name
	//array_name	
	if (expr->op == OP_ID && (expr->is_func || expr->is_array))
	{
		addr = expr;
		offset = 0;
	}
	// &(something)
	else if (expr->op == OP_ADDRESS)
	{
		return expr;
		/*addr = expr->child[0];
		offset = 0;
		free(expr);*/
	}
	//struct.....array
	//struct array
	//a.b[5].c.d[6].multidimensional_array[1]
	else if (expr->op == OP_MEMBER || expr->op == OP_INDEX)
	{
		addr = check_constant_address(expr->child[0]);
		assert(addr && addr->op == OP_ADD && addr->child[1]->op == OP_CONST);
		
		offset = addr->child[1]->value.i;
		if (expr->op == OP_MEMBER)
		{
			offset += ((struct record_field *)(expr->child[1]->type))->offset;
		}
		else //expr->op == OP_INDEX
		{
			if (expr->child[1]->op != OP_CONST)
			{
				return NULL;
			}
			offset += (expr->child[1]->value.i);// * (expr->type->basic_type->size);
		}
		p = addr;
		addr = addr->child[0];
		free(p->child[1]);
		free(p);

/*	
		addr = expr;
		while (addr->op == OP_MEMBER || addr->op == OP_INDEX)
		{
			if (addr->op == OP_MEMBER)
			{
				offset += ((struct record_field *)(addr->child[1]->type))->offset;
			}
			else //expr->op == OP_INDEX
			{
				if (addr->child[1]->op != OP_CONST)
				{
					return NULL;
				}
				offset += (addr->child[1]->value.i)?;// * (addr->type->basic_type->size);
			}
			addr = addr->child[0];
		}
*/
	}
	else if (expr->op == OP_ADD || expr->op == OP_SUB)
	{
		addr = check_constant_address(expr->child[0]);
		assert(addr && expr->child[1]->op == OP_CONST);
		if (addr->op == OP_ADDRESS)
		{
			return expr;
		}
		if (!addr || expr->child[1]->op != OP_CONST)
		{
			return NULL;
		}
		offset = (expr->op == OP_ADD ? 1 : -1) * (expr->child[1]->value.i) + addr->value.i;
		p = addr;
		addr = addr->child[0];
		free(p->child[1]);
		free(p);
	}
	else
	{
		return NULL;
	}
	p = alloc_expr_node();
	p->op = OP_ADD;
	p->type = type;
	p->child[0] = addr;
	
	p->child[1] = alloc_expr_node();
	p->child[1]->op = OP_CONST;
	p->child[1]->type = &basic_types[INT];
	p->child[1]->value.i = offset;
	return p;
}


static void check_constant(struct decl_initer_node *init)
{
	struct init_data *init_data;
	init_data = init->idata;
	if (!init_data)
	{
		return ;
	}
	while (init_data)
	{
		if (!(init_data->expr->op == OP_CONST || init_data->expr->op == OP_STR || 
			(init_data->expr->op == OP_CAST && (init_data->expr->child[0]->op == OP_CONST || init_data->expr->child[0]->op == OP_STR)) ||
			(init_data->expr = check_constant_address(init_data->expr))))
		{
			goto err;
		}
		init_data = init_data->next;
	}
	return ;
err:
	rep_error("initializer must be constant");
	exit(1);
}

static void global_declaration_check(struct decl_node *decl)
{
	struct symbol *sym;
	struct type *val_type;
	struct type *spec_type;
    struct decl_init_declor_node *init_declor;

	declaration_specifiers_check(decl->spec);
	if (decl->spec->sto_class_spec_tok == TK_AUTO || decl->spec->sto_class_spec_tok == TK_REGISTER)
	{
		rep_error("illegal storage specifier!");
		exit(1);
	}
	if (decl->spec->sto_class_spec_tok == TK_TYPEDEF)
	{
		typedef_name_check(decl);
		return;
	}
	spec_type = decl->spec->type;

	
	init_declor = decl->init_declor_list;
	while (init_declor)
	{
		init_declor->declor = declarator_check(init_declor->declor);
		val_type = type_composite(spec_type, init_declor->declor);
		if (!init_declor->declor->id)
		{
			init_declor = init_declor->next;
			continue;
		}
		if (val_type->categ == FUNCTION)
		{
			if (init_declor->initer)
			{
				rep_error("function can not be initialize");
				exit(1);
			}
			sym = sym_lookup_id(init_declor->declor->id);
			if (sym)
			{
				if (sym->sto_class_spec_tok == TK_EXTERN && decl->spec->sto_class_spec_tok == TK_STATIC)
				{
					rep_error("conflict linkage of %s", init_declor->declor->id);
					exit(1);
				}
				if (!is_compatible_type(sym->type, val_type))
				{
					rep_error("Incompatible with previous definition");
					exit(1);
				}
				else
				{
					sym->type = val_type = greatest_common_type(val_type, sym->type);
				}
			}
			else
			{
				sym = sym_add_function(init_declor->declor->id, val_type, decl->spec->sto_class_spec_tok, NULL);
			}
			goto next;
		}

		sym = sym_lookup_id(init_declor->declor->id);
		if (sym)
		{
			/*
			(1).
				extern int err;
				static int err;
			(2).
				int err;
				static int err;
			*/
			if ((sym->sto_class_spec_tok == TK_EXTERN || sym->sto_class_spec_tok == 0) && 
				(decl->spec->sto_class_spec_tok == TK_STATIC))
			{
				rep_error("conflict linkage of %s", init_declor->declor->id);
				exit(1);
			}
			/*
			(3).
				static int err;
				int err;
			*/
			if (sym->sto_class_spec_tok == TK_STATIC && decl->spec->sto_class_spec_tok == 0)
			{
				rep_error("conflict linkage of %s", init_declor->declor->id);
				exit(1);
			}
			/*
			(4).
				int err = 0;
				int err = 0;
			*/
			if (sym->defined && decl->init_declor_list)
			{
				rep_error("redefinition of symbol %s", init_declor->declor->id);
				exit(1);
			}
			else if (sym->defined)
			{
				goto next;
			}
			/*
			(5).
				int i;
				double i;
			*/
			if (!is_compatible_type(sym->type, val_type))
			{	
				rep_error("incompatible with previous definition");
				exit(1);
			}
			else
			{
				sym->type = val_type = greatest_common_type(val_type, sym->type);
			}

			if (sym->sto_class_spec_tok == TK_EXTERN)
			{
				sym->sto_class_spec_tok = decl->spec->sto_class_spec_tok;
			}
			sym->defined = init_declor->initer != NULL;
		}
		else
		{
			sym = sym_add_variable(init_declor->declor->id, val_type, decl->spec->sto_class_spec_tok, 
					init_declor->initer != NULL, NULL);
		}

		if (init_declor->initer)
		{
			CheckInitializer(init_declor->initer, val_type);
			check_constant(init_declor->initer);

			((struct symbol_var *)sym)->init_data = init_declor->initer->idata;
		}
		
next:
		init_declor->declor->sym = sym;
		init_declor = init_declor->next;
	}
}

void local_declaration_check(struct decl_node *decl)
{
	struct symbol *sym;
	struct type *val_type;
	struct type *spec_type;
    struct decl_init_declor_node *init_declor;

	declaration_specifiers_check(decl->spec);
	
	if (decl->spec->sto_class_spec_tok == TK_TYPEDEF)
	{
		typedef_name_check(decl);
		return;
	}
	spec_type = decl->spec->type;
	
	init_declor = decl->init_declor_list;
	while (init_declor)
	{
		init_declor->declor = declarator_check(init_declor->declor);
		val_type = type_composite(spec_type, init_declor->declor);
		if (!init_declor->declor->id)
		{
			init_declor = init_declor->next;
			continue;
		}
		if (decl->spec->sto_class_spec_tok == TK_EXTERN && init_declor->initer)
		{
			rep_error("can not initialize a local variable with specifier 'extern'");
			exit(1);
		}
		if (val_type->categ == FUNCTION)
		{
			if (decl->spec->sto_class_spec_tok == TK_STATIC)
			{
				rep_error("can't specify static function as local variable");
				exit(1);
			}
			if (init_declor->initer)
			{
				rep_error("function can not be initialize");
				exit(1);
			}
			sym = sym_lookup_id(init_declor->declor->id);
			if (sym)
			{
				if (sym->sto_class_spec_tok == TK_EXTERN && decl->spec->sto_class_spec_tok == TK_STATIC)
				{
					rep_error("conflict linkage of %s", init_declor->declor->id);
					exit(1);
				}
				if (!is_compatible_type(sym->type, val_type))
				{
					rep_error("Incompatible with previous definition");
					exit(1);
				}
				else
				{
					sym->type = val_type = greatest_common_type(val_type, sym->type);
				}
			}
			else
			{
				sym = sym_add_function(init_declor->declor->id, val_type, decl->spec->sto_class_spec_tok, NULL);
			}
			goto next;
		}
		sym = sym_lookup_id(init_declor->declor->id);
		if ((!sym) || (sym->level < cur_nesting_lvl))
		{
			sym = sym_add_variable(init_declor->declor->id, val_type, decl->spec->sto_class_spec_tok, 
				init_declor->initer != NULL, NULL);
		}
		// duplicate variable
		else if (!((sym->sto_class_spec_tok == TK_EXTERN) && (decl->spec->sto_class_spec_tok == TK_EXTERN) && 
			is_compatible_type(sym->type, val_type)))
		{
			rep_error("redefinition of %s", init_declor->declor->id);
			exit(1);
		}
		// two variables with the same name and at the same scope
		else
		{
			if (!is_compatible_type(val_type, sym->type))
			{
				rep_error("Incompatible with previous definition");
				exit(1);
			}
			else
			{
				sym->type = val_type = greatest_common_type(val_type, sym->type);
			}

			if (sym->defined && decl->init_declor_list)
			{
				rep_error("redefinition of symbol %s", init_declor->declor->id);
				exit(1);
			}
			else if (sym->defined)
			{
				goto next;
			}

			if (sym->sto_class_spec_tok == TK_EXTERN)
			{
				sym->sto_class_spec_tok = decl->spec->sto_class_spec_tok;
			}
			sym->defined = init_declor->initer != NULL;
			
		}
		if (init_declor->initer)
		{
			CheckInitializer(init_declor->initer, val_type);
			if (decl->spec->sto_class_spec_tok == TK_STATIC)
			{
				check_constant(init_declor->initer);
			}
			((struct symbol_var *)sym)->init_data = init_declor->initer->idata;
		}
next:
		if (sym->sto_class_spec_tok != TK_EXTERN && is_incomplete_type(val_type, 0))
		{
			rep_error("veriable %s has incomplete type", init_declor->declor->id);
			exit(1);
		}
		init_declor->declor->sym = sym;
		init_declor = init_declor->next;
	}
}

static void function_check(struct decl_func_node *func)
{
	int i, cnt;
	struct parameter_type *para;
	struct symbol *sym, *typedef_name_sym;
	struct type *func_type;
	struct labels *lbl;

	declaration_specifiers_check(func->spec);
	func->declor = declarator_check(func->declor);
	func_type = type_composite(func->spec->type, func->declor);
	if (sym = sym_lookup_id(func->declor->id))
	{
		if (sym->type->categ != FUNCTION)
		{
			rep_error("redeclaration of symbol '%s'", func->declor->id);
			exit(1);
		}
		else
		{
			if (sym->sto_class_spec_tok == TK_EXTERN && func->spec->sto_class_spec_tok == TK_STATIC)
			{
				rep_error("conflict function linkage");
				exit(1);
			}
			if (!is_compatible_type(func_type, sym->type))
			{
				rep_error("conflict function declaration");
				exit(1);
			}
			else
			{
				sym->type = func_type = greatest_common_type(func_type, sym->type);
			}
			if (sym->defined)
			{
				rep_error("function %s is redifined", func->declor->id);
				exit(1);
			}
		}
	}
	else
	{
		sym = sym_add_function(func->declor->id, func_type, func->spec->sto_class_spec_tok, NULL);
	}
	sym->defined = 1;
	func->func_sym = sym;

	func->switch_iter_stack = vector_init(0);
    func->named_labels = hash_init();

	func->start_lbl = sym_create_label();
	free(func->start_lbl->name);
	func->start_lbl->name = sym->name;
	func->end_lbl = sym_create_label();
	func->return_val = sym_create_temp(func_type->basic_type);
	
	curr_func = func;
	curr_func_sym = sym;
	enter_scope();
		for (i = 0; i < func->func_declor->params->len; i++)
		{
			para = func->func_declor->params->get(func->func_declor->params, i);
			para->sym = sym_add_variable(para->id, para->type, para->sto_class_spec_tok, 0, NULL);
		}
		for (i = 0; i < func->func_declor->typedef_name_sym->len; i++)
		{
			typedef_name_sym = func->func_declor->typedef_name_sym->get(func->func_declor->typedef_name_sym, i);
			sym_add_typedef(typedef_name_sym->name, typedef_name_sym->type, NULL);
			//free(typedef_name_sym);
		}
		compound_statement_check(func->comp);
	exit_scope();
	curr_func_sym = NULL;
	curr_func = NULL;

	i = 0;
	cnt = 0;
	while (sym = func->named_labels->hash_iter(func->named_labels, &i, &cnt))
	{
		if (!sym->defined)
		{
			rep_error("undefined label of %s", sym->name);
			exit(1);
		}
	}
} 

void translation_unit_check(struct decl_trans_unit_node *trans_unit)
{
	int i = 0, cnt = 0;
	struct symbol *sym;
	struct common_node *p;
	p = trans_unit->ext_decl;
	while (p)
	{
		if (p->kind == NK_Declaration)
		{
			global_declaration_check(p);
		}
		else if (p->kind == NK_Function)
		{
			function_check(p);
		}
		p = p->next;
	}

	while (sym = sym_id_traverse_in_cur_lvl(&i, &cnt))
	{
		if (is_incomplete_type(sym->type, 0))
		{
			printf("type '%s' is incomplete\n", sym->name);
			//exit(1);
		}
	}
}


