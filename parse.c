#include <stdlib.h>
#include <assert.h>
#include "lex.h"
#include "parse.h"
#include "sxcc.h"

#define FIRST_DECLARATION                                           \
    TK_AUTO,   TK_EXTERN,   TK_REGISTER, TK_STATIC,   TK_TYPEDEF,   \
    TK_CONST,  TK_VOLATILE, TK_SIGNED,   TK_UNSIGNED, TK_SHORT,     \
    TK_LONG,   TK_CHAR,     TK_INT,      TK_FLOAT,	    \
    TK_DOUBLE, TK_ENUM,     TK_STRUCT,   TK_UNION,    TK_VOID, TK_ID, \
    TK_SFR,


#define FIRST_EXPRESSION                                                          \
    TK_SIZEOF,       TK_ID,         TK_INTCONST,    TK_UINTCONST,  TK_LONGCONST,  \
    TK_ULONGCONST,   TK_FLOATCONST, TK_DOUBLECONST,\
    TK_LDOUBLECONST, TK_STRING,     TK_BITAND,     TK_ADD,        \
    TK_SUB,          TK_MUL,        TK_INC,         TK_DEC,        TK_NOT,        \
    TK_COMP,         TK_LPAREN,

#define FIRST_STATEMENT                                                                   \
    TK_BREAK, TK_CASE,   TK_CONTINUE, TK_DEFAULT, TK_DO,    TK_FOR,       TK_GOTO,        \
    TK_IF,    TK_LBRACE, TK_RETURN,   TK_SWITCH,  TK_WHILE, TK_SEMICOLON, FIRST_EXPRESSION

static const enum token_type first_set_decl[] = 
{
    FIRST_DECLARATION
};

static const enum token_type first_set_expr[] =
{
    FIRST_EXPRESSION
};

static const enum token_type first_set_stmt[] =
{
    FIRST_STATEMENT
};

int is_decl_first(enum token_type type)
{
    int i;
    for (i = 0; i < sizeof(first_set_decl) / sizeof(first_set_decl[0]); i++)
    {
        if (first_set_decl[i] == type)
        {
            return 1;
        }
    }
    return 0;
}

int is_expr_first(enum token_type type)
{
    int i;
    for (i = 0; i < sizeof(first_set_expr) / sizeof(first_set_expr[0]); i++)
    {
        if (first_set_expr[i] == type)
        {
            return 1;
        }
    }
    return 0;
}

int is_stmt_first(enum token_type type)
{
    int i;
    for (i = 0; i < sizeof(first_set_stmt) / sizeof(first_set_stmt[0]); i++)
    {
        if (first_set_stmt[i] == type)
        {
            return 1;
        }
    }
    return 0;
}



int is_type_tok(struct token *tok)
{
    return (tok->type == TK_ID) ? is_typedef_name(tok->token_strings) : 
        (tok->type >= TK_AUTO && tok->type <= TK_VOID);
}

void do_error(char *str, struct location *loc)
{
    if (loc)
    {
        printf("[%s %d:%d] %s\n", loc->files, loc->line, loc->offset, str);
    }
    else
    {
        printf("[? ?:?] %s\n", str);
    }
    
    assert(0);
}

void do_expect(enum token_type type)
{
    struct token *tok;
    tok = lex_get_token();
    if (tok->type != type)
    {
        printf("[%s %d:%d] expect token '%s'\n", tok->loc->files, tok->loc->line, tok->loc->offset, tok_type2name(type));
        assert(0);
        exit(1);
    }
    free(tok);
}
