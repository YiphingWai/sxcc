#include "sxcc.h"
#include "lex.h"
#include "decl.h"
#include "stmt.h"
#include "type.h"
#include "tran.h"
#include "hash.h"
#include "opt.h"
#include "x86_reg.h"
#include "x86_emit.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <assert.h>
#include <sys/wait.h>

// 200 207
void ir_printf(struct basic_block *bb_list, FILE *fp)
{
    int i;
    struct basic_block *bb_tmp;
    char *rslts, *ls, *rs;
    struct arg_list *arg_list_head;
    struct ir *p;
    fprintf(fp, "*********************************\n");
    while (bb_list)
    {
        for(i = 0; i < bb_list->ir_list->len; i++)
        {
            p = bb_list->ir_list->get(bb_list->ir_list, i);
            if (p->rsltop->name)
            {
                if (p->rsltop->kind == SK_Temp && ((struct symbol_temp*)p->rsltop)->is_shared_tmp)
                {
                    rslts = "T.S";
                }
                else
                {   
                    rslts = p->rsltop->name;
                }
            }
            else
            {
                rslts = NULL;
            }
            if (p->lop && p->lop->name)
            {
                if (p->lop->kind == SK_Temp && ((struct symbol_temp*)p->lop)->is_shared_tmp)
                {
                    ls = "T.S";
                }
                else
                {   
                    ls = p->lop->name;
                }
            }
            else
            {
                ls = NULL;
            }
            if (p->rop && p->rop->name)
            {
                if (p->rop->kind == SK_Temp && ((struct symbol_temp*)p->rop)->is_shared_tmp)
                {
                    rs = "T.S";
                }
                else
                {   
                    rs = p->rop->name;
                }
            }
            else
            {
                rs = NULL;
            }

            fprintf(fp, "%s ", get_irstr(p->ircode));

            fprintf(fp, "%s ", rslts);

            if (ls)
            {
                fprintf(fp, "%s ", ls);
            }
            if (p->ircode == CALL)
            {
                arg_list_head = (struct arg_list *)(p->rop);
                for (; arg_list_head; arg_list_head = arg_list_head->next)
                {
                    if (arg_list_head->sym->kind == SK_Temp && ((struct symbol_temp*)arg_list_head->sym)->is_shared_tmp)
                    {
                        fprintf(fp, "T.S ");
                    }
                    else
                    {
                        fprintf(fp, "%s ", arg_list_head->sym->name);
                    }
                }
                fprintf(fp, "\n");
            }
            else
            {
                rs ? fprintf(fp, "%s \n", rs) : fprintf(fp, "\n");
            }
        }
        bb_list = bb_list->next;
        fprintf(fp, "---------------------------------\n");
    }
    fprintf(fp, "\n");
}

void show_data_flow(struct proc_block *pb)
{
    int len = 0, i = 0, cnt = 0;
    struct basic_block *bb;
    struct symbol *sym;
    for(pb = pb_head->next; pb; pb = pb->next)
    {
        for (bb = pb->bblist_head; bb; bb = bb->next)
        {
            i = 0;
            cnt = 0;
            printf("\ndef:\n", len++);
            while (sym = bb->def_set->hash_iter(bb->def_set, &i, &cnt))
            {
                printf("%s ", sym->name);
            }
            i = 0;
            cnt = 0;
            printf("\nin:\n", len++);
            while (sym = bb->use_set->hash_iter(bb->use_set, &i, &cnt))
            {
                printf("%s ", sym->name);
            }
            i = 0;
            cnt = 0;
            printf("\nout:\n", len++);
            while (sym = bb->out_set->hash_iter(bb->out_set, &i, &cnt))
            {
                printf("%s ", sym->name);
            }
            printf("\n---------------------------------\n");
        }
        printf("*********************************\n");
    }
}

void show_help()
{
    printf("usage:\n\tsxcc <file>\n");
}

int main(int argc, char *argv[])
{
    int i, len, status;
    char ch, *buf, file_name[256];
    FILE *fp;
    
    struct decl_trans_unit_node *p;
    struct proc_block *pb;

    if (argc != 2)
    {
        show_help();
        return -1;
    }
    len = strlen(argv[1]);
    assert((argv[1][len - 1] == 'i' || argv[1][len - 1] == 'I') && argv[1][len - 2] == '.');
    strcpy(file_name, argv[1]);
    argv[1][len - 2] = '\0';


    if (!(fp = fopen(file_name, "r")))
    {
        printf("can not open file: %s\n", file_name);
        exit(-1);
    }
    fseek(fp, 0, SEEK_END);
    len = ftell(fp);
    if (!(buf = calloc(1, len + 1)))
    {
        rep_error("calloc err");
        exit(-1);
    }
    fseek(fp, 0, SEEK_SET);
    len = fread(buf, 1, len, fp);
    fclose(fp);

    printf("read %d bytes\n", len);

    type_system_init();
    symtab_init();
    
    if (lex_init(buf, len))
    {
        printf ("lex init fail!\n");
        return -1;
    }
    
    p = translation_unit();
    if (!p)
    {
        printf("parse return null\n");
    }
    
    translation_unit_check(p);
    translation_unit_translate(p);
    opt_init();

    sprintf(file_name, "%s_o.ir", argv[1]);
    if (!(fp = fopen(file_name, "w")))
    {
        printf("can not open file: %s\n", file_name);
        exit(-1);
    }
    for(pb = pb_head; pb; pb = pb->next)
    {
        ir_printf(pb->bblist_head, fp);
    }
    fclose(fp);
    for(pb = pb_head->next; pb; pb = pb->next)
    {
        optimize(pb);
        pb->shared_mem_size = share_temp_memory(pb);
    }
    sprintf(file_name, "%s_n.ir", argv[1]);
    if (!(fp = fopen(file_name, "w")))
    {
        printf("can not open file: %s\n", file_name);
        exit(-1);
    }
    for(pb = pb_head; pb; pb = pb->next)
    {
        ir_printf(pb->bblist_head, fp);
    }
    fclose(fp);

//    show_data_flow(pb);
    sprintf(file_name, "%s.asm", argv[1]);
    if (!(fp = fopen(file_name, "w")))
    {
        printf("can not open file: %s\n", file_name);
        exit(-1);
    }
    translation_unit_emit(pb_head, fp);
    fclose(fp);
    
}