#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "tran.h"
#include "expr.h"
#include "decl.h"
#include "type.h"
#include "symtab.h"
#include "sxcc.h"

// for optimize
static int bit_count(int i) 
{
    i = i - ((i >> 1) & 0x55555555);
    i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
    i = (i + (i >> 4)) & 0x0f0f0f0f;
    i = i + (i >> 8);
    i = i + (i >> 16);
    return i & 0x3f;
}


struct symbol *expression_translate(struct expr_node *expr);

// &a
struct symbol *get_address(struct symbol *sym)
{
    struct symbol *tmp;

    // &(*a)
    // sym = *a
    if (sym->kind == SK_Temp && ((struct symbol_temp*)sym)->flag == SF_DEREF)
    {
        return ((struct symbol_temp*)sym)->link;
    }
    assert(sym->kind != SK_Temp);
    tmp = sym_create_temp(create_pointer_type(sym->type));
    ((struct symbol_temp*)tmp)->flag = SF_ADDR;
    ((struct symbol_temp*)tmp)->link = sym;
    add_ir(GETADDR, tmp, sym, NULL);
//    add_ir(ADDR, tmp, sym, NULL);
    return tmp;
}
// *a
struct symbol *get_deref(struct symbol *sym)
{
    struct symbol *tmp;

    // *(&a)
    // sym -> &a
    if (sym->kind == SK_Temp && ((struct symbol_temp*)sym)->flag == SF_ADDR)
    {
        return ((struct symbol_temp*)sym)->link;
    }

    tmp = sym_create_temp(sym->type->basic_type);
    ((struct symbol_temp *)tmp)->flag = SF_DEREF;
    ((struct symbol_temp*)tmp)->link = sym;
    add_ir(LOAD, tmp, sym, NULL);
//    add_ir(DEREF, tmp, sym, NULL);
//    add_ir(ASSIGN, tmp, sym, NULL);
    return tmp;
}

struct symbol *primary_expression_translate(struct expr_node *expr)
{
    struct symbol *sym;

    if (expr->op == OP_CONST)
    {
        sym = sym_add_constant(expr->type, expr->value);
        expr->sym = sym;
    }

    if (expr->op == OP_STR || expr->is_array || expr->is_func)
    {
		//#if  1	// added
		//assert(expr->op != OP_STR);
		//#endif
		return get_address(expr->sym);
	}

    return expr->sym;
}
// 1.translate the argument one by one, from right to left
// 2.reverse the argument list
struct arg_list *argument_list_translate(struct expr_node *arg, struct arg_list **head)
{
    struct arg_list *list, *node;
    if (!arg)
    {
        *head = NULL;
        return NULL; 
    }
    ALLOC(node, struct arg_list);
    node->ud_chain = vector_init(0);
    if (arg->next)
    {
        list = argument_list_translate(arg->next, head);
        node->sym = arg->sym = expression_translate(arg);
        list->next = node;
    }
    else
    {
        node->sym = arg->sym = expression_translate(arg);
        list = node;
        *head = node;
    }
    return node;
}

struct symbol *call_expression_translate(struct expr_node *expr)
{
    struct arg_list *arg_list_head;
    struct symbol *func, *tmp;
    argument_list_translate(expr->child[1], &arg_list_head);
    func = expression_translate(expr->child[0]);
    assert(IS_FUNCTION_POINTER(func->type));
    if (expr->child[0]->is_func)
    {
        func = get_deref(func);
    }
    tmp = sym_create_temp(expr->type);
    add_ir(CALL, tmp, func, (struct symbol *)arg_list_head);
    return tmp;
}

// return a label which point to the member wanted
struct symbol *index_expression_translate(struct expr_node *expr)
{
    struct ir *last_ir;
    struct symbol *lop, *rop, *addop, *derefop;
    union val value;
    lop = expression_translate(expr->child[0]);
    //assert(lop->type->categ == POINTER);
    if (expr->child[0]->op == OP_INDEX && expr->child[0]->child[1]->op == OP_CONST &&
        expr->child[1]->op == OP_CONST)
    {
        last_ir = cur_pb->bblist_tail->ir_list->get_back(cur_pb->bblist_tail->ir_list);
        value.i = last_ir->rop->value.i + expr->child[1]->value.i;
        rop = sym_add_constant(&basic_types[INT], value);
        last_ir->rop = rop;
        last_ir->rsltop->type = create_pointer_type(expr->type);
        addop = last_ir->rsltop;
//        return (*ir_list_tail)->rsltop;
    }
    else
    {
        rop = expression_translate(expr->child[1]);
        addop = sym_create_temp(create_pointer_type(expr->type));
        add_ir(ADD, addop, lop, rop);
    }
    
    if (expr->is_array)
    {
        expr->sym = addop;
    }
    else
    {
        expr->sym = get_deref(addop);
    }
    return expr->sym;
}

// return a label which point to the member wanted
struct symbol *member_expression_translate(struct expr_node *expr)
{
    struct ir *last_ir;
    struct symbol *lop, *rop, *addop, *derefop;
    union val value;
    lop = expression_translate(expr->child[0]);
//    assert(lop->type->categ == STRUCT);
    if (lop->type->categ == STRUCT || lop->type->categ == UNION)
    {
        lop = get_address(lop);
        last_ir = cur_pb->bblist_tail->ir_list->get_back(cur_pb->bblist_tail->ir_list);
        if (last_ir->ircode == LOAD)
        {
            cur_pb->bblist_tail->ir_list->pop_back(cur_pb->bblist_tail->ir_list);
        }
    }
    last_ir = cur_pb->bblist_tail->ir_list->get_back(cur_pb->bblist_tail->ir_list);
    if (expr->child[0]->op == OP_MEMBER || expr->child[0]->op == OP_PTR_MEMBER)
    {
        if (expr->op == OP_MEMBER)
        {
            // a.b.c
            // a->b.c
            // b is a struct or union and has been addressed above, so lop here is the address of b
            assert(last_ir->ircode == ADD);
            assert(last_ir->rop->kind == SK_Constant);
            last_ir = cur_pb->bblist_tail->ir_list->get_back(cur_pb->bblist_tail->ir_list);
            value.i = last_ir->rop->value.i + expr->child[1]->value.i;
            rop = sym_add_constant(&basic_types[INT], value);
            last_ir->rop = rop;
            last_ir->rsltop->type = create_pointer_type(expr->type);
            addop = last_ir->rsltop;
        }
        else
        {
            // a.b->c
            // a->b->c
            // b is a pointer and has not been addressed above, so lop here is the value of b
            rop = expression_translate(expr->child[1]);
            addop = sym_create_temp(create_pointer_type(expr->type));
            add_ir(ADD, addop, lop, rop);
        }
    }
    else
    {
        // xxx->b
        // xxx.b
        rop = expression_translate(expr->child[1]);
        addop = sym_create_temp(create_pointer_type(expr->type));
        add_ir(ADD, addop, lop, rop);
    }
    if (expr->is_array)
    {
        expr->sym = addop;
    }
    else
    {
        expr->sym = get_deref(addop);
    }
    return expr->sym;
}

struct symbol *cast_expression_translate(struct expr_node *expr)
{
    struct symbol *lop, *castop;
    int src_size, dst_size, opcode = 0;
    lop = expression_translate(expr->child[0]);
    src_size = get_type_size(expr->child[0]->type->categ);
    dst_size = get_type_size(expr->type->categ);
    if (src_size == dst_size)
    {
        expr->sym = lop;
        return lop;
    }
    switch (src_size)
    {
        case I1:
            switch (dst_size)
            {
            case U1:
                opcode = CAST_I1_U1;
                break;
            case I4:
                opcode = CAST_I1_I4;
                break;
            }
            break;
        case U1:
            switch (dst_size)
            {
            case I1:
                opcode = CAST_U1_I1;
                break;
            case I4:
                opcode = CAST_U1_I4;
                break;
            }
            break;
        case I2:
            switch (dst_size)
            {
            case U2:
                opcode = CAST_I2_U2;
                break;
            case I4:
                opcode = CAST_I2_I4;
                break;
            }
            break;
        case U2:
            switch (dst_size)
            {
            case I2:
                opcode = CAST_U2_I2;
                break;
            case I4:
                opcode = CAST_U2_I4;
                break;
            }
            break;
        case I4:
            switch (dst_size)
            {
            case I1:
                opcode = CAST_I4_I1;
                break;
            case U1:
                opcode = CAST_I4_U1;
                break;
            case I2:
                opcode = CAST_I4_I2;
                break;
            case U2:
                opcode = CAST_I4_U2;
                break;
            case U4:
                opcode = CAST_I4_U4;
                break;
            case F4:
                opcode = CAST_I4_F4;
                break;
            case F8:
                opcode = CAST_I4_F8;
                break;
            }
            break;
        case U4:
            switch (dst_size)
            {
            case I4:
                opcode = CAST_U4_I4;
                break;
            case F4:
                opcode = CAST_U4_F4;
                break;
            case F8:
                opcode = CAST_U4_F8;
                break;
            }
            break;
        case F4:
            switch (dst_size)
            {
            case I4:
                opcode = CAST_F4_I4;
                break;
            case F4:
                opcode = CAST_F4_U4;
                break;
            case F8:
                opcode = CAST_F4_F8;
                break;
            }
            break;
        case F8:
            switch (dst_size)
            {
            case I4:
                opcode = CAST_F8_I4;
                break;
            case U4:
                opcode = CAST_F8_U4;
                break;
            case F4:
                opcode = CAST_F8_F4;
                break;
            }
            break;
    }
    assert(opcode);
    castop = sym_create_temp(expr->type);
    add_ir(opcode, castop, lop, NULL);
    expr->sym = castop;
    return castop;
}
struct symbol *inc_dec_expression_translate(struct expr_node *expr)
{
    int flag = 0;
    struct symbol *lop, *rop, *plop, *tmp, *tmp1;
    lop = expression_translate(expr->child[0]);
    rop = expression_translate(expr->child[1]);
    // (*T0)++
    // in such cases, *T0 is a temp variable, it is useless modifying a temp variable
    // instead, we should find 
    if (lop->kind == SK_Temp && ((struct symbol_temp *)lop)->flag == SF_DEREF)
    {
        flag = 1;
        plop = get_address(lop);
    }
    else
    {
        plop = lop;
    }
    assert(rop->kind == SK_Constant);

    switch (expr->op)
    {
        // ADD tmp a 1
        // ASSIGN *a tmp null
        // return tmp
        case OP_PREINC:
        case OP_PREDEC:
            if (lop->type == POINTER)
            {
                tmp = sym_create_temp(expr->type);
                add_ir(expr->op == OP_PREINC ? ADD : SUB, tmp, lop, rop);
                if (flag)
                {
                    add_ir(STORE, plop, tmp, NULL);
                }
                else
                {
                    add_ir(ASSIGN, lop, tmp, NULL);
                }   
            }
            else
            {
                // tmp = sym_create_temp(expr->type);
                add_ir(expr->op == OP_PREINC ? INC : DEC, lop, lop, rop);
                if (flag)
                {
                    add_ir(STORE, plop, lop, NULL);
                }  
            }
            expr->sym = lop;
            return lop;
        // ADD tmp a 1
        // ASSIGN *a tmp null
        // return a
        case OP_POSTINC:
        case OP_POSTDEC:
            tmp = sym_create_temp(expr->type);
            add_ir(ASSIGN, tmp, lop, NULL);
            if (lop->type == POINTER)
            {
                tmp1 = sym_create_temp(expr->type);
                add_ir(expr->op == OP_POSTINC ? ADD : SUB, tmp1, lop, rop);
                if (flag)
                {
                    add_ir(STORE, plop, tmp1, NULL);
                }
                else
                {
                     add_ir(ASSIGN, lop, tmp1, NULL);
                }   
            }
            else
            {
                //tmp1 = sym_create_temp(expr->type);
                add_ir(expr->op == OP_POSTINC ? INC : DEC, lop, lop, rop);
                if (flag)
                {
                    add_ir(STORE, plop, lop, NULL);
                }
            }
            expr->sym = tmp;
            return tmp;
    }
    assert(0);
    return expr;
}

struct symbol *neg_expression_translate(struct expr_node *expr)
{
    struct symbol *lop, *tmp;
    lop = expression_translate(expr->child[0]);
//    plop = lop;//get_address(lop);

    tmp = sym_create_temp(expr->type);
    add_ir(NEG, tmp, lop, NULL);
//    add_ir(ASSIGN, plop, tmp, NULL);
    expr->sym = tmp;
    return tmp;
}

struct symbol *address_expression_translate(struct expr_node *expr)
{
    struct symbol *sym;
    sym = expression_translate(expr->child[0]);
    if (expr->child[0]->op == OP_STR || expr->child[0]->is_array || expr->child[0]->is_func)
    {
        // for this case, we have got the addr in primary_expression_translate() so we do nothing here
        expr->sym = sym;
    }
    else
    {
        expr->sym = get_address(sym);
    }
    return expr->sym;
}

struct symbol *deref_expression_translate(struct expr_node *expr)
{
    expr->sym = get_deref(expression_translate(expr->child[0]));
    return expr->sym;
}

struct symbol *comp_expression_translate(struct expr_node *expr)
{
    struct symbol *lop, *tmp;
    lop = expression_translate(expr->child[0]);
//    plop = lop;//get_address(lop);

    tmp = sym_create_temp(expr->type);
    add_ir(COMP, tmp, lop, NULL);
//    add_ir(ASSIGN, plop, tmp, NULL);
    expr->sym = tmp;
    return tmp;
}

struct symbol *not_expression_translate(struct expr_node *expr)
{
    struct symbol *lop, *tmp;
    lop = expression_translate(expr->child[0]);
//    plop = lop;//get_address(lop);

    tmp = sym_create_temp(expr->type);
    add_ir(NOT, tmp, lop, NULL);
//    add_ir(ASSIGN, plop, tmp, NULL);
    expr->sym = tmp;
    return tmp;
}

struct symbol *logical_expression_translate(struct expr_node *expr)
{
    int opcode;
    struct symbol *def_val;
    struct symbol *lop, *rop, *tmp;
    struct symbol *lbl;
    union val value;

    lbl = sym_create_label();
    tmp = sym_create_temp(expr->type);
    
    switch (expr->op)
    {
        case OP_AND:    // &&
            value.i = 0;
            def_val = sym_add_constant(&basic_types[INT], value);
            add_ir(ASSIGN, tmp, def_val, NULL);
            lop = expression_translate(expr->child[0]);
            add_ir(JNT, lbl, lop, NULL);
            rop = expression_translate(expr->child[1]);
            add_ir(JNT, lbl, rop, NULL);
            value.i = 1;
            def_val = sym_add_constant(&basic_types[INT], value);
            add_ir(ASSIGN, tmp, def_val, NULL);
            add_ir(LABEL, lbl, NULL, NULL);
            break;
        case OP_OR:     // ||
            value.i = 1;
            def_val = sym_add_constant(&basic_types[INT], value);
            add_ir(ASSIGN, tmp, def_val, NULL);
            lop = expression_translate(expr->child[0]);
            add_ir(JT, lbl, lop, NULL);
            rop = expression_translate(expr->child[1]);
            add_ir(JT, lbl, rop, NULL);
            value.i = 0;
            def_val = sym_add_constant(&basic_types[INT], value);
            add_ir(ASSIGN, tmp, def_val, NULL);
            add_ir(LABEL, lbl, NULL, NULL);
            break;
        default:
            assert(0);
    }
    expr->sym = tmp;
    return tmp; 
}

struct symbol *binrary_expression_translate(struct expr_node *expr)
{
    int opcode;
    struct symbol *lop, *rop, *tmp;

    lop = expression_translate(expr->child[0]);
    rop = expression_translate(expr->child[1]);

    switch (expr->op)
    {
        case OP_BITAND: // &
            opcode = BAND;
            break;
        case OP_BITOR:  // |
            opcode = BOR;
            break;
        case OP_BITXOR: // ^
            opcode = XOR;
            break;
        case OP_EQUAL:  //  ==
            opcode = EQ;
            break;
        case OP_UNEQUAL:    // !=
            opcode = NEQ;
            break;
        case OP_GREAT:  // >
            opcode = GT;
            break;
        case OP_GREAT_EQ:   // >=
            opcode = GE;
            break;
        case OP_LESS:   // <
            opcode = LS;
            break;
        case OP_LESS_EQ:    // <=
            opcode = LE;
            break;
        case OP_LSHIFT:     // <<
            opcode = SHL;
            break;
        case OP_RSHIFT:     // >>
            opcode = SHR;
            break;
        case OP_ADD:        // +
            opcode = ADD;
            break;
        case OP_SUB:        // -
            opcode = SUB;
            break;
        case OP_MUL:        // *
            opcode = MUL;
            break;
        case OP_DIV:        // /
            opcode = DIV;
            break;
        case OP_MOD:        // %
            opcode = MOD;
            break;
        default:
            assert(0);
    }
    tmp = sym_create_temp(expr->type);
    add_ir(opcode, tmp, lop, rop);
    expr->sym = tmp;
    return tmp;
}

struct symbol *simple_assignment_expression_translate(struct expr_node *expr)
{
    int flag = 0;
    struct symbol *lop, *plop, *rop;

    lop = expression_translate(expr->child[0]);
    rop = expression_translate(expr->child[1]);

    if (lop->kind == SK_Temp && ((struct symbol_temp *)lop)->flag == SF_DEREF)
    {
        flag = 1;
        plop = get_address(lop);
    }
    else
    {
        plop = lop;
    }
    if (flag)
    {
        add_ir(STORE, plop, rop, NULL);
    }
    else
    {
        add_ir(ASSIGN, lop, rop, NULL);
    }   
    expr->sym = lop;
    return lop;
}

struct symbol *compound_assignment_expression_translate(struct expr_node *expr)
{
    int flag = 0;
    struct symbol *lop, *plop, *rop;
    rop = expression_translate(expr->child[1]);
    assert(expr->child[0]->sym);
    lop = expr->child[0]->sym;
    if (lop->kind == SK_Temp && ((struct symbol_temp *)lop)->flag == SF_DEREF)
    {
        flag = 1;
        plop = get_address(lop);
    }
    else
    {
        plop = lop;
    }
    if (flag)
    {
        add_ir(STORE, plop, rop, NULL);
    }
    else
    {
        add_ir(ASSIGN, lop, rop, NULL);
    }   
    expr->sym = lop;
    return lop;
}
struct symbol *comma_expression_translate(struct expr_node *expr)
{
    expression_translate(expr->child[0]);
    return expression_translate(expr->child[1]);
}

// should be treated specially, as a statment

struct symbol *conditional_expression_translate(struct cond_expr_node *expr)
{
    struct symbol *op1, *op2, *op3, *rslt_op;

    rslt_op = sym_create_temp(expr->type);
    op1 = expression_translate(expr->child[0]);
    add_ir(JNT, expr->else_lbl, op1, NULL);
    op2 = expression_translate(expr->child[1]->child[0]);
    add_ir(ASSIGN, rslt_op, op2, NULL);
    add_ir(JMP, expr->end_lbl, NULL, NULL);
    add_ir(LABEL, expr->else_lbl, NULL, NULL);
    op3 = expression_translate(expr->child[1]->child[1]);
    add_ir(ASSIGN, rslt_op, op3, NULL);
    add_ir(LABEL, expr->end_lbl, NULL, NULL);

    return rslt_op;
}

struct symbol *expression_translate(struct expr_node *expr)
{
    struct symbol *sym;

    if (!expr)
    {
        return NULL;
    }

    switch (expr->op)
    {
        case OP_COMMA:
            sym = comma_expression_translate(expr);
            goto out;
        case OP_ASSIGN:
            sym = simple_assignment_expression_translate(expr);
            goto out;
        case OP_BITOR_ASSIGN:
        case OP_BITXOR_ASSIGN:
        case OP_BITAND_ASSIGN:
        case OP_LSHIFT_ASSIGN:
        case OP_RSHIFT_ASSIGN:
        case OP_ADD_ASSIGN:
        case OP_SUB_ASSIGN:
        case OP_MUL_ASSIGN:
        case OP_DIV_ASSIGN:
        case OP_MOD_ASSIGN:
            sym = compound_assignment_expression_translate(expr);
            goto out;
        case OP_QUESTION:
             sym = conditional_expression_translate(expr);
             goto out;
        case OP_OR:
        case OP_AND:
            sym = logical_expression_translate(expr);
            goto out;
        case OP_BITOR:
        case OP_BITXOR:
        case OP_BITAND:
        case OP_EQUAL:
        case OP_UNEQUAL:
        case OP_GREAT:
        case OP_LESS:
        case OP_GREAT_EQ:
        case OP_LESS_EQ:
        case OP_LSHIFT:
        case OP_RSHIFT:
        case OP_ADD:
        case OP_SUB:
        case OP_MUL:
        case OP_DIV:
        case OP_MOD:
            sym = binrary_expression_translate(expr);
            goto out;
        case OP_CAST:
            sym = cast_expression_translate(expr);
            goto out;
        case OP_PREINC:
        case OP_PREDEC:
        case OP_POSTINC:
        case OP_POSTDEC:
            sym = inc_dec_expression_translate(expr);
            goto out;
        case OP_ADDRESS:
            sym = address_expression_translate(expr);
            goto out;
        case OP_DEREF:
            sym = deref_expression_translate(expr);
            goto out;
        case OP_POS:
            assert(0);
        case OP_NEG:
            sym = neg_expression_translate(expr);
            goto out;
        case OP_COMP:
            sym = comp_expression_translate(expr);
            goto out;
        case OP_NOT:
            sym = not_expression_translate(expr);
            goto out;
        case OP_SIZEOF:
            assert(0);
        case OP_INDEX:
            sym = index_expression_translate(expr);
            goto out;
        case OP_CALL:
            sym = call_expression_translate(expr);
            goto out;
        case OP_MEMBER:
        case OP_PTR_MEMBER:
            sym = member_expression_translate(expr);
            goto out;
        case OP_ID:
        case OP_CONST:
        case OP_STR:
            sym = primary_expression_translate(expr);
            goto out;
    }
    assert(0);
    return NULL;
out:
    return sym;
}



