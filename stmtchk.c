#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "expr.h"
#include "decl.h"
#include "stmt.h"
#include "type.h"
#include "symtab.h"
#include "hash.h"
#include "sxcc.h"
#include "opt.h"

extern struct decl_func_node *curr_func;
extern struct symbol_func *curr_func_sym;

struct stmt_expr_node *get_dummy_stmt()
{
    struct stmt_expr_node *dummy_stmt;
    ALLOC(dummy_stmt, struct stmt_expr_node);
    dummy_stmt->kind = NK_ExpressionStatement;
    return dummy_stmt;
}

void switch_iter_stack_push(struct common_node *stmt)
{
    curr_func->switch_iter_stack->push_back(curr_func->switch_iter_stack, stmt);
}

struct common_node *switch_iter_stack_top()
{
    return (struct common_node*)curr_func->switch_iter_stack->get_back(curr_func->switch_iter_stack);
}

struct common_node *switch_iter_stack_pop()
{
    return (struct common_node*)curr_func->switch_iter_stack->pop_back(curr_func->switch_iter_stack);
}
int switch_iter_stack_len()
{
    return (struct common_node*)curr_func->switch_iter_stack->len;
}
/*
expression-statement:
                  expression<opt> ;
*/
struct stmt_expr_node *expression_statement_check(struct stmt_expr_node *expr_stmt)
{
    if (expr_stmt->expr)
    {
        expr_stmt->expr = expression_check(expr_stmt->expr);
    }
    return expr_stmt;
}

/*
identifier :  statement
*/
struct stmt_lbl_node *label_statement_check(struct stmt_lbl_node *lbl_stmt)
{
    struct symbol_label *lbl;
    
    if (!curr_func->named_labels->find(curr_func->named_labels, lbl_stmt->name, &lbl))
    {
        if (lbl->defined)
        {
            rep_error("re-define of label %s\n", lbl_stmt->name);
            exit(1);
        }
        lbl->defined = 1;
    }
    else
    {
        lbl = sym_create_label();
        free(lbl->name);
        lbl->name = lbl_stmt->name;
        lbl->aname = lbl->name;
        lbl->defined = 1;
        curr_func->named_labels->insert(curr_func->named_labels, lbl->name, lbl);
    }

    lbl_stmt->lbl_sym = lbl;
    lbl_stmt->stmt = statement_check(lbl_stmt->stmt);
    return lbl_stmt;
}
/*
case  constant-expression :  statement
*/
struct stmt_case_node *case_statement_check(struct stmt_case_node *case_stmt)
{
    struct vector *cases;
    struct stmt_switch_node *switch_stmt;
    if (!switch_iter_stack_len())
    {
        rep_error("a case label shall appear in a switch statement");
        exit(1);
    }
    if (switch_iter_stack_top()->kind != NK_SwitchStatement)
    {
        rep_error("a case label shall appear in a switch statement");
        exit(1);
    }
    
    case_stmt->expr = constant_expression_check(case_stmt->expr);
    if (!(case_stmt->expr))
    {
        rep_error("the case value must be integer constant");
        exit(1);
    }
    switch_stmt = switch_iter_stack_top();
    if (switch_stmt->expr->op == OP_CONST && switch_stmt->expr->value.i != case_stmt->expr->value.i)
    {
        return;
    }
    /*
    if (case_stmt->stmt)
    {
        case_stmt->stmt = statement_check(case_stmt->stmt);
    }
*/
    case_stmt->lbl_sym = sym_create_label();
//    curr_func->label_tab->insert(curr_func->label_tab, case_stmt->lbl_sym->name, case_stmt->lbl_sym);
    if (!((struct stmt_switch_node*)switch_iter_stack_top())->case_stmts)
    {
        ((struct stmt_switch_node*)switch_iter_stack_top())->case_stmts = vector_init(0);
    }
    cases = ((struct stmt_switch_node*)switch_iter_stack_top())->case_stmts;
    cases->push_back(cases, case_stmt);
    return case_stmt;
}
/*
default :  statement
*/
struct stmt_default_node *default_statement_check(struct stmt_default_node *default_stmt)
{
    if (!switch_iter_stack_len())
    {
        rep_error("a default label shall appear in a switch statement");
        exit(1);
    }
    if (switch_iter_stack_top()->kind != NK_SwitchStatement)
    {
        rep_error("a default label shall appear in a switch statement");
        exit(1);
    }
    /*
    default_stmt->stmt = statement_check(default_stmt->stmt);
*/
    default_stmt->lbl_sym = sym_create_label();
//    curr_func->label_tab->insert(curr_func->label_tab, default_stmt->lbl_sym->name, default_stmt->lbl_sym);

    ((struct stmt_switch_node*)switch_iter_stack_top())->default_stmt = default_stmt;

    return default_stmt;
}

/*
selection-statement:
                  if (  expression )  statement
                  if (  expression )  statement else  statement
*/
struct stmt_if_node *if_statement_check(struct stmt_if_node *if_stmt)
{
    struct stmt_expr_node *dummy_stmt;
    if_stmt->expr = adjust(expression_check(if_stmt->expr));
    // The controlling expression of an if statement shall have scalar type.
    if (!IS_SCALAR_TYPE(if_stmt->expr->type))
    {
        rep_error("the expression in if statement shall be scalar type");
        exit(1);
    }
    if_stmt->then_stmt = statement_check(if_stmt->then_stmt);
    if (if_stmt->else_stmt)
    {
        if_stmt->else_stmt = statement_check(if_stmt->else_stmt);
        if_stmt->else_lbl = sym_create_label();
//        curr_func->label_tab->insert(curr_func->label_tab, if_stmt->else_lbl->name, if_stmt->else_lbl);
    }
    if_stmt->end_lbl = sym_create_label();
// optimize
    if (if_stmt->expr->op == OP_CONST && IS_EQUAL(if_stmt->expr->type, if_stmt->expr->value, 0))
    {
        if (!if_stmt->else_stmt)
        {
            dummy_stmt = get_dummy_stmt();
            dummy_stmt->next = if_stmt->next;
            return dummy_stmt;
        }
        if_stmt->else_stmt->next = if_stmt->next;
        return if_stmt->else_stmt;
    }
    if (if_stmt->expr->op == OP_CONST && !IS_EQUAL(if_stmt->expr->type, if_stmt->expr->value, 0))
    {
        if_stmt->then_stmt->next = if_stmt->next;
        return if_stmt->then_stmt;
    }
    return if_stmt;
}
/*
switch (  expression )  statement
*/
struct stmt_switch_node *switch_statement_check(struct stmt_switch_node *switch_stmt)
{
    struct common_node *p;
    switch_stmt->expr = adjust(expression_check(switch_stmt->expr));
    if (!IS_INTEG_TYPE(switch_stmt->expr->type))
    {
        rep_error("the expression in switch statement shall be integer type");
        exit(1);
    }
    switch_stmt->end_lbl = sym_create_label();
    switch_iter_stack_push(switch_stmt);
    switch_stmt->stmt = statement_check(switch_stmt->stmt);
    switch_iter_stack_pop();
// optimize
    if (switch_stmt->expr->op == OP_CONST)
    {
        if (!switch_stmt->default_stmt)
        {
            assert(switch_stmt->case_stmts->len == 0 || switch_stmt->case_stmts->len == 1);
            if (switch_stmt->case_stmts->len)
            {
                p = switch_stmt->case_stmts->get(switch_stmt->case_stmts, 0);
                p->next = switch_stmt->next;
                return p;
            }
            else 
            {
                p = get_dummy_stmt();
                p->next = switch_stmt->next;
                return p;
            }
        }
        else
        {
            if (!switch_stmt->case_stmts->len)
            {
                switch_stmt->default_stmt->next = switch_stmt->next;
                return switch_stmt->default_stmt;
            }
        } 
    }
    return switch_stmt;
}

struct stmt_while_node *while_statement_check(struct stmt_while_node *while_stmt)
{
    struct common_node *p;
    
    while_stmt->expr = adjust(expression_check(while_stmt->expr));
    if (!IS_SCALAR_TYPE(while_stmt->expr->type))
    {
        rep_error("the expression in while statement shall be scalar type");
        exit(1);
    }
    while_stmt->begin_lbl = sym_create_label();
    while_stmt->end_lbl = sym_create_label();
    switch_iter_stack_push(while_stmt);
    while_stmt->stmt = statement_check(while_stmt->stmt);
    switch_iter_stack_pop();
// optimize
    if (while_stmt->expr->op == OP_CONST)
    {
        if (IS_EQUAL(while_stmt->expr->type, while_stmt->expr->value, 0))
        {
            p = get_dummy_stmt();
            p->next = while_stmt->next;
            return p;
        }
    }
    return while_stmt;
}

struct stmt_do_node *do_statement_check(struct stmt_do_node *do_stmt)
{
    do_stmt->expr = adjust(expression_check(do_stmt->expr));
    if (!IS_SCALAR_TYPE(do_stmt->expr->type))
    {
        rep_error("the expression in do-while statement shall be scalar type");
        exit(1);
    }
    do_stmt->begin_lbl = sym_create_label();
    do_stmt->cond_lbl = sym_create_label();
    do_stmt->end_lbl = sym_create_label();
    switch_iter_stack_push(do_stmt);
    do_stmt->stmt = statement_check(do_stmt->stmt);
    switch_iter_stack_pop();
// optimize
    if (do_stmt->expr->op == OP_CONST)
    {
        if (IS_EQUAL(do_stmt->expr->type, do_stmt->expr->value, 0))
        {
            do_stmt->stmt->next = do_stmt->next;
            return do_stmt->stmt;
        }
    }
    return do_stmt;
}

struct stmt_for_node *for_statement_check(struct stmt_for_node *for_stmt)
{
    if (for_stmt->expr1)
    {
        for_stmt->expr1 = expression_check(for_stmt->expr1);
    }
    if (for_stmt->expr2)
    {
        for_stmt->expr2 = adjust(expression_check(for_stmt->expr2));
        if (!IS_SCALAR_TYPE(for_stmt->expr2->type))
        {
            rep_error("the second expression in for statement shall be scalar type");
            exit(1);
        }
    }
    if (for_stmt->expr3)
    {
        for_stmt->expr3 = expression_check(for_stmt->expr3);
    }

    for_stmt->cond_lbl = sym_create_label();
    for_stmt->iter_lbl = sym_create_label();
    for_stmt->inc_lbl = sym_create_label();
    for_stmt->end_lbl = sym_create_label();

    switch_iter_stack_push(for_stmt);
    for_stmt->stmt = statement_check(for_stmt->stmt);
    switch_iter_stack_pop(for_stmt);
// optimize
    if (for_stmt->expr2 && for_stmt->expr2->op == OP_CONST)
    {
        if (IS_EQUAL(for_stmt->expr2->type, for_stmt->expr2->value, 0))
        {
            for_stmt->stmt->next = for_stmt->next;
            return for_stmt->stmt;
        }
    }
    return for_stmt;
}

struct stmt_goto_node *goto_statement_check(struct stmt_goto_node *goto_stmt)
{
    struct symbol_label *lbl;

    if (!curr_func->named_labels->find(curr_func->named_labels, goto_stmt->lab_name, &lbl))
    {
        // found
        goto_stmt->dest_lbl = lbl;
        return goto_stmt;
    }
    lbl = sym_create_label();
    free(lbl->name);
    lbl->name = goto_stmt->lab_name;
    lbl->aname = lbl->name;
    lbl->defined = 0;
    curr_func->named_labels->insert(curr_func->named_labels, lbl->name, lbl);

    goto_stmt->dest_lbl = lbl;
    return goto_stmt;
}

struct stmt_break_node *break_statement_check(struct stmt_break_node *break_stmt)
{
    struct common_node *cur_stmt;
    cur_stmt = switch_iter_stack_top();
    switch (cur_stmt->kind)
    {
        case NK_SwitchStatement:
            break_stmt->dest_lbl = ((struct stmt_switch_node*)cur_stmt)->end_lbl;
            break;
        case NK_ForStatement:
             break_stmt->dest_lbl = ((struct stmt_for_node*)cur_stmt)->end_lbl;
            break;
        case NK_WhileStatement:
             break_stmt->dest_lbl = ((struct stmt_while_node*)cur_stmt)->end_lbl;
            break;
        case NK_DoStatement:
             break_stmt->dest_lbl = ((struct stmt_do_node*)cur_stmt)->end_lbl;
            break;
        default:
            rep_error("the break statement shall appear in a switch or loop");
            exit(-1);
            break;
    }
    return break_stmt;
}

struct stmt_continue_node *continue_statement_check(struct stmt_continue_node *continue_stmt)
{
    struct common_node *cur_stmt;
    cur_stmt = switch_iter_stack_top();
    switch (cur_stmt->kind)
    {
        case NK_ForStatement:
            continue_stmt->dest_lbl = ((struct stmt_for_node*)cur_stmt)->inc_lbl;
            break;
        case NK_WhileStatement:
            continue_stmt->dest_lbl = ((struct stmt_while_node*)cur_stmt)->begin_lbl;
            break;
        case NK_DoStatement:
            continue_stmt->dest_lbl = ((struct stmt_do_node*)cur_stmt)->cond_lbl;
            break;
        default:
            rep_error("the continue statement shall appear in a loop");
            exit(1);
    }
    return continue_stmt;
}

struct stmt_return_node *return_statement_check(struct stmt_return_node *return_stmt)
{
    if (!curr_func_sym)
    {
        rep_error("the return statement shall appear in a function");
        exit(1);
    }

    if (return_stmt->expr)
    {
        if (curr_func_sym->type->basic_type->categ == VOID)
        {
            rep_error("void function should not return any value");
            exit(1);
        }
        return_stmt->expr = adjust(expression_check(return_stmt->expr));
        return_stmt->expr = cast(curr_func_sym->type->basic_type, return_stmt->expr);
        goto out;
    }

    if (curr_func_sym->type->basic_type->categ != VOID)
    {
        rep_error("the function should return a value");
        exit(1);
    }
out:
    return_stmt->return_val = curr_func->return_val;
    return_stmt->dest_lbl = curr_func->end_lbl;
    return return_stmt;
}

struct stmt_comp_node *compound_statement_check(struct stmt_comp_node *comp_stmt)
{
    struct common_node *p, **last_next = NULL;
    p = comp_stmt->decl_list;
    while (p)
    {
        local_declaration_check(p);
        p = p->next;
    }
    p = comp_stmt->stmt;
    last_next = &(comp_stmt->stmt);
    while (p)
    {
        p = statement_check(p);
        *last_next = p;
        last_next = &(p->next);
        p = p->next;
    }
    return comp_stmt;
}

struct stmt_comp_node *local_compound_statement_check(struct stmt_comp_node *comp_stmt)
{
    struct common_node *p;
    enter_scope();
    p = compound_statement_check(comp_stmt);
    exit_scope();
    return p;
}


struct common_node *statement_check(struct common_node *stmt)
{
    switch (stmt->kind)
	{
	case NK_ExpressionStatement:
        return expression_statement_check(stmt);
	case NK_LabelStatement:
        return label_statement_check(stmt);
    case NK_CaseStatement:
        return case_statement_check(stmt);
	case NK_DefaultStatement:
		return default_statement_check(stmt);
	case NK_IfStatement:
		return if_statement_check(stmt);
	case NK_SwitchStatement:
		return switch_statement_check(stmt);
	case NK_WhileStatement:
		return while_statement_check(stmt);
	case NK_DoStatement:
		return do_statement_check(stmt);
	case NK_ForStatement:
		return for_statement_check(stmt);
	case NK_GotoStatement:
		return goto_statement_check(stmt);
	case NK_ContinueStatement:
		return continue_statement_check(stmt);
	case NK_BreakStatement:
		return break_statement_check(stmt);
	case NK_ReturnStatement:
		return return_statement_check(stmt);
	case NK_CompoundStatement:
		return local_compound_statement_check(stmt);
	default:
        assert(0);
	}
}
