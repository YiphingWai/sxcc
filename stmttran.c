#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "tran.h"
#include "expr.h"
#include "decl.h"
#include "hash.h"
#include "stmt.h"
#include "type.h"
#include "symtab.h"
#include "sxcc.h"


struct proc_block *pb_head = NULL;
struct proc_block *cur_pb = NULL;
//struct ir **ir_list_tail;

static void create_pb(int kind)
{
    struct proc_block *pb;
    ALLOC(pb, struct proc_block);
    pb->kind = kind;
    ALLOC(pb->bblist_head, struct basic_block);
    pb->locals = vector_init(0);
    pb->params = vector_init(0);
    pb->externals = vector_init(0);
    pb->temps = hash_init();
    pb->bblist_tail = pb->bblist_head;
    pb->bblist_tail->ir_list = vector_init(0);
    pb->bblist_tail->succs = vector_init(0);
    pb->bblist_tail->use_set = hash_init();
    pb->bblist_tail->def_set = hash_init();
    //pb->bblist_tail->in_set = hash_init();
    pb->bblist_tail->out_set = hash_init();

    pb->ref_vars = hash_init();
    if (cur_pb)
    {
        cur_pb->next = pb;
        cur_pb = pb;
    }
    else
    {
        cur_pb = pb;
        pb_head = pb;
    }
}

void test_and_create_bb(int test)
{
    struct basic_block *bb = NULL;
    
    if (cur_pb->bblist_tail->ir_list->len)
    {
        ALLOC(bb, struct basic_block);
// structure flow graph construction
        if (cur_pb->link)
        {
            bb->precs = vector_init(0);
            bb->precs->push_back(bb->precs, cur_pb->bblist_tail);
            cur_pb->bblist_tail->succs->push_back(cur_pb->bblist_tail->succs, bb);
        }
        cur_pb->bblist_tail->next = bb;
        cur_pb->bblist_tail = bb;
        bb->ir_list = vector_init(0);
        bb->succs = vector_init(0);
        bb->use_set = hash_init();
        bb->def_set = hash_init();
//        bb->in_set = hash_init();
        bb->out_set = hash_init();
    }
    cur_pb->link = 1;
}


void expression_statement_translate(struct stmt_expr_node *expr_stmt)
{
    expression_translate(expr_stmt->expr);
}

void label_statement_translate(struct stmt_lbl_node *lbl_stmt)
{
    add_ir(LABEL, lbl_stmt->lbl_sym, NULL, NULL);
    statement_translate(lbl_stmt->stmt);
}

void case_statement_translate(struct stmt_case_node *case_stmt)
{
    add_ir(LABEL, case_stmt->lbl_sym, NULL, NULL);
    /*
    if (case_stmt->stmt)
    {
        statement_translate(case_stmt->stmt);
    }
    */
}

void default_statement_translate(struct stmt_default_node *default_stmt)
{
    add_ir(LABEL, default_stmt->lbl_sym, NULL, NULL);
    /*
    statement_translate(default_stmt->stmt);
    */
}

void if_statement_translate(struct stmt_if_node *if_stmt)
{
    struct symbol *sym;
    sym = expression_translate(if_stmt->expr);
    // The controlling expression of an if statement shall have scalar type.
    if (!IS_SCALAR_TYPE(if_stmt->expr->type))
    {
        rep_error("the expression in if statement shall be scalar type");
        exit(1);
    }
    if (if_stmt->else_stmt)
    {
        add_ir(JNT, if_stmt->else_lbl, sym, NULL);
    }
    else
    {
        add_ir(JNT, if_stmt->end_lbl, sym, NULL);
    }
    statement_translate(if_stmt->then_stmt);
    add_ir(JMP, if_stmt->end_lbl, NULL, NULL);
    if (if_stmt->else_stmt)
    {
        add_ir(LABEL, if_stmt->else_lbl, NULL, NULL);
        statement_translate(if_stmt->else_stmt);
    }
    add_ir(LABEL, if_stmt->end_lbl, NULL, NULL);
}

void switch_statement_translate(struct stmt_switch_node *switch_stmt)
{
    int i;
    struct stmt_case_node *case_stmt;
    struct symbol *sym, *case_sym;
    struct common_node *stmt;
    sym = expression_translate(switch_stmt->expr);
    if (!IS_INTEG_TYPE(switch_stmt->expr->type))
    {
        rep_error("the expression in switch statement shall be integer type");
        exit(1);
    }
// emit switch
    for (i = 0; switch_stmt->case_stmts && i < switch_stmt->case_stmts->len; i++)
    {
        case_stmt = (struct stmt_case_node *)switch_stmt->case_stmts->get(switch_stmt->case_stmts, i);
        case_sym = expression_translate(case_stmt->expr);
        add_ir(JE, case_stmt->lbl_sym, sym, case_sym);
    }
    if (switch_stmt->default_stmt)
    {
        add_ir(JMP, switch_stmt->default_stmt->lbl_sym, NULL, NULL);
    }
    else
    {
        add_ir(JMP, switch_stmt->end_lbl, NULL, NULL);
    }

// emit cases and default statements
/*
    for (stmt = switch_stmt->stmt; stmt; stmt = stmt->next)
    {
        statement_translate(stmt);
    }
*/
    for (stmt = switch_stmt->stmt; stmt; stmt = stmt->next)
    {
        statement_translate(stmt);
    }
    /*
    for (i = 0; switch_stmt->case_stmts && i < switch_stmt->case_stmts->len; i++)
    {
        case_stmt = (struct stmt_case_node *)switch_stmt->case_stmts->get(switch_stmt->case_stmts, i);
        case_statement_translate(case_stmt);
    }
    if (switch_stmt->default_stmt)
    {
        default_statement_translate(switch_stmt->default_stmt);
    }
    */
    add_ir(LABEL, switch_stmt->end_lbl, NULL, NULL);
}


void while_statement_translate(struct stmt_while_node *while_stmt)
{
    struct symbol *sym;
    add_ir(LABEL, while_stmt->begin_lbl, NULL, NULL);
    sym = expression_translate(while_stmt->expr);
    add_ir(JNT, while_stmt->end_lbl, sym, NULL);
    statement_translate(while_stmt->stmt);
    add_ir(JMP, while_stmt->begin_lbl, NULL, NULL);
    add_ir(LABEL, while_stmt->end_lbl, NULL, NULL);
}

void do_statement_translate(struct stmt_do_node *do_stmt)
{
    struct symbol *sym;
    add_ir(LABEL, do_stmt->begin_lbl, NULL, NULL);
    statement_translate(do_stmt->stmt);
    add_ir(LABEL, do_stmt->cond_lbl, NULL, NULL);
    sym = expression_translate(do_stmt->expr);
    add_ir(JT, do_stmt->begin_lbl, sym, NULL);
    add_ir(LABEL, do_stmt->end_lbl, NULL, NULL);
}

void for_statement_translate(struct stmt_for_node *for_stmt)
{
    struct symbol *sym;
    if (for_stmt->expr1)
    {
        sym = expression_translate(for_stmt->expr1);
    }
    add_ir(LABEL, for_stmt->cond_lbl, NULL, NULL);
    if (for_stmt->expr2)
    {
        sym = expression_translate(for_stmt->expr2);
        add_ir(JNT, for_stmt->end_lbl, sym, NULL);
    }
    add_ir(LABEL, for_stmt->iter_lbl, NULL, NULL);
    if (for_stmt->stmt)
    {
        statement_translate(for_stmt->stmt);
    }
    add_ir(LABEL, for_stmt->inc_lbl, NULL, NULL);
    if (for_stmt->expr3)
    {
        sym = expression_translate(for_stmt->expr3);
    }
    add_ir(JMP, for_stmt->cond_lbl, NULL, NULL);
    add_ir(LABEL, for_stmt->end_lbl, NULL, NULL);
}

void goto_statement_translate(struct stmt_goto_node *goto_stmt)
{
    add_ir(JMP, goto_stmt->dest_lbl, NULL, NULL);
}

void break_statement_translate(struct stmt_break_node *break_stmt)
{
    add_ir(JMP, break_stmt->dest_lbl, NULL, NULL);
}

void continue_statement_translate(struct stmt_continue_node *continue_stmt)
{
    add_ir(JMP, continue_stmt->dest_lbl, NULL, NULL);
}

void return_statement_translate(struct stmt_return_node *return_stmt)
{
    struct symbol *sym;
    union val value;
    value.i = 0;
    sym = expression_translate(return_stmt->expr);
    if (!sym)
    {
        sym = sym_add_constant(return_stmt->return_val->type, value);
    }
    add_ir(ASSIGN, return_stmt->return_val, sym, NULL);
    add_ir(JMP, return_stmt->dest_lbl, NULL, NULL);
}

void declaration_translate(struct decl_node *decl_list, int is_global)
{
    struct symbol *rsltop, *lop;
    struct decl_init_declor_node *init_declor;
    struct init_data *idata;
    union val value;
    if (decl_list->spec->sto_class_spec_tok != TK_TYPEDEF)
    {
        rsltop = sym_create_temp(&basic_types[INT]);
        for (init_declor = decl_list->init_declor_list; init_declor; init_declor = init_declor->next)
        {
            if (init_declor->declor->sym->kind == SK_Variable && 
                    init_declor->declor->sym->sto_class_spec_tok != TK_EXTERN)
            {
                if (init_declor->initer && !is_global)
                {
                    for (idata = init_declor->initer->idata; idata; idata = idata->next)
                    {
                        lop = expression_translate(idata->expr);

                        if (IS_SCALAR_TYPE(init_declor->declor->sym->type))     // int i = 5; --> ASSIGN i 5
                        {
                            add_ir(ASSIGN, init_declor->declor->sym, lop, NULL);    
                        }
                        else    // int a[5] = {1} --> ADD T.0 &a 0. ASSIGN *T.0 1
                        {
                            value.i = idata->offset;
                            add_ir(ADD, rsltop, get_address(init_declor->declor->sym), sym_add_constant(&basic_types[INT], value));
                            add_ir(STORE, rsltop, lop, NULL);
                        } 
                    }
                }
                else if (init_declor->initer)
                {
                    ((struct symbol_var*)init_declor->declor->sym)->init_data = init_declor->initer->idata;
                }
                cur_pb->locals->push_back(cur_pb->locals, init_declor->declor->sym);
                if (is_global)
                {
                    ((struct symbol_var*)init_declor->declor->sym)->is_global = 1;
                }
            }
            else
            {
                if (init_declor->declor->sym->kind == SK_Variable)
                {
                    init_declor->declor->sym->aname = init_declor->declor->sym->name;
                    cur_pb->externals->push_back(cur_pb->externals, init_declor->declor->sym);
                }
                else if (init_declor->declor->sym->kind == SK_Function && 
                       !init_declor->declor->sym->defined)
                {
                    init_declor->declor->sym->aname = init_declor->declor->sym->name;
                    cur_pb->externals->push_back(cur_pb->externals, init_declor->declor->sym);
                }
            }
            
        }
    }

    
}

void compound_statement_translate(struct stmt_comp_node *comp_stmt)
{
    struct decl_node *decl;
    struct common_node *stmt;
    decl = comp_stmt->decl_list;
    while (decl)
    {
        declaration_translate(decl, 0);
        decl = decl->next;
    }
    stmt = comp_stmt->stmt;
    while (stmt)
    {
        statement_translate(stmt);
        stmt = stmt->next;
    }
}

void statement_translate(struct common_node *stmt)
{
    switch (stmt->kind)
	{
	case NK_ExpressionStatement:
        expression_statement_translate(stmt);
        break;
	case NK_LabelStatement:
        label_statement_translate(stmt);
        break;
    case NK_CaseStatement:
        case_statement_translate(stmt);
        break;
	case NK_DefaultStatement:
		default_statement_translate(stmt);
        break;
	case NK_IfStatement:
		if_statement_translate(stmt);
        break;
	case NK_SwitchStatement:
		switch_statement_translate(stmt);
        break;
	case NK_WhileStatement:
		while_statement_translate(stmt);
        break;
	case NK_DoStatement:
		do_statement_translate(stmt);
        break;
	case NK_ForStatement:
		for_statement_translate(stmt);
        break;
	case NK_GotoStatement:
		goto_statement_translate(stmt);
        break;
	case NK_ContinueStatement:
		continue_statement_translate(stmt);
        break;
	case NK_BreakStatement:
		break_statement_translate(stmt);
        break;
	case NK_ReturnStatement:
		return_statement_translate(stmt);
        break;
	case NK_CompoundStatement:
		compound_statement_translate(stmt);
        break;
	default:
        assert(0);
	}
}

void function_translate(struct decl_func_node *func)
{
    int i;
    struct parameter_type *para;
    add_ir(LABEL, func->start_lbl, NULL, NULL);
    func->start_lbl->is_func_label = 1;
    compound_statement_translate(func->comp);
    add_ir(LABEL, func->end_lbl, NULL, NULL);
    add_ir(RET, func->return_val, NULL, NULL);
    for (i = 0; i < func->func_declor->params->len; i++)
    {
        para = func->func_declor->params->get(func->func_declor->params, i);
        cur_pb->params->push_back(cur_pb->params, para->sym);
        assert(para->sym->kind == SK_Variable);
        ((struct symbol_var*)para->sym)->is_para = 1;
    }
    cur_pb->func_sym = func->func_sym;
    cur_pb->return_val = func->return_val;
}

void translation_unit_translate(struct decl_trans_unit_node *trans_unit)
{
    struct symbol *sym;
    struct basic_block *bb = NULL;
    struct proc_block *global_decl_pb, *tmp_pb;
	struct common_node *p;
	p = trans_unit->ext_decl;
    create_pb(NK_Declaration);
    global_decl_pb = cur_pb;
	while (p)
	{
		if (p->kind == NK_Declaration)
		{
            // global variables initize
            tmp_pb = cur_pb; 
            cur_pb = global_decl_pb;
			declaration_translate(p, 1);
            cur_pb = tmp_pb; 
		}
		else if (p->kind == NK_Function)
		{
            create_pb(NK_Function);
			function_translate(p);
		}
		p = p->next;
	}
}

