#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "tran.h"
#include "type.h"
#include "symtab.h"
#include "sxcc.h"


const char *irstr[] = 
{
    #define IRINFO(ircode, str) {str},
        #include "irinfo.h"
    #undef  IRINFO
};

static void test_and_creat_bb_for_label(int ircode, struct symbol *rsltop, struct symbol *lop)
{
    int i, j;
    struct basic_block *bb;
    struct vector *bb_precs;
    struct vector *label_precs;
    // structure flow graph construction
    if (ircode == LABEL)
    {
        test_and_create_bb(0);

        ((struct symbol_label *)rsltop)->belonging_bb = cur_pb->bblist_tail;

        bb_precs = cur_pb->bblist_tail->precs;
        label_precs = ((struct symbol_label *)rsltop)->precs;
        // combine the two lists if bb_precs exist
        if (bb_precs)
        {
            for (i = 0; i < bb_precs->len; i++)
            {
                label_precs->push_back(label_precs, bb_precs->pop_back(bb_precs));
            }
            vector_destroy(bb_precs);
        }
        cur_pb->bblist_tail->precs = label_precs;

        // backfill every precursor's succ list
        for (i = 0; i < label_precs->len; i++)
        {
            bb = label_precs->get(label_precs, i);
            for (j = 0; j < bb->succs->len; j++)
            {
                if (bb->succs->get(bb->succs, j) == cur_pb->bblist_tail)
                {
                    goto next;
                }
            }
            bb->succs->push_back(bb->succs, cur_pb->bblist_tail);
next:;
        }
    }
}

static void test_and_creat_bb_for_jmp(int ircode, struct symbol *rsltop)
{
    if (IS_JMP_IR(ircode))
    {
        assert(rsltop->kind == SK_Label);
        ((struct symbol_label *)rsltop)->precs->push_back(
                ((struct symbol_label *)rsltop)->precs, cur_pb->bblist_tail);
        // if jump backward (which has had a basic block), we fill out succ list directily
        // otherwise, the succ list will be backfilled when the dest basic_block was builded (see test_and_creat_bb_for_label())  
        if (((struct symbol_label *)rsltop)->belonging_bb)
        {
            cur_pb->bblist_tail->succs->push_back(cur_pb->bblist_tail->succs, 
                    ((struct symbol_label *)rsltop)->belonging_bb);
        }
        if (ircode == JMP)
        {
            cur_pb->link = 0;
        }
        else
        {
            cur_pb->link = 1;
        }
        test_and_create_bb(0);
    }
}

struct ir *_add_ir(int ircode, struct symbol *rsltop, struct symbol *lop, struct symbol *rop)
{
    struct ir *p;
    ALLOC(p, struct ir);
    p->ircode = ircode;
    p->lop = lop;
    p->rop = rop;
    p->rsltop = rsltop;
    p->du_chain = vector_init(0);
    p->ud_chain_lop = vector_init(0);
    p->ud_chain_rop = vector_init(0);
//    p->ud_chain_rsltop = vector_init(0);
//    p->bb_belonging = cur_pb->bblist_tail;
//    p->ir_no = cur_pb->bblist_tail->ir_list->len;
    cur_pb->bblist_tail->ir_list->push_back(cur_pb->bblist_tail->ir_list, p);
    return p;
}

void add_ir(int ircode, struct symbol *rsltop, struct symbol *lop, struct symbol *rop)
{
    int i;
    struct ir *ir; 
// before fill in the ir, we check if the ir is label. if so, we create a new basic block first
    test_and_creat_bb_for_label(ircode, rsltop, lop);
// now, create and fill in the ir
    ir = _add_ir(ircode, rsltop, lop, rop);
// after filled in the ir, we check if the ir is jmp, if so, we create a new bb for following ir
    // structure flow graph construction
    test_and_creat_bb_for_jmp(ircode, rsltop);
}

char *get_irstr(int ir_code)
{
    char *ret;
    ret = irstr[ir_code];
    return ret;
}


int get_type_size(int categ)
{
    switch (categ)
    {
        case CHAR:
            return I1;
        case UCHAR:
            return U1;
        case SHORT:
            return I2;
        case USHORT:
            return U2;
        case INT:
            return I4;
        case UINT:
            return U4;
        case LONG:
            return I4;
        case ULONG:
            return U4;
        case FLOAT:
            return F4;
        case DOUBLE:
            return F8;
        case POINTER:
            return I4;
        case ENUM:
            return I4;
    }
    assert(0);
}
