#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#include "hash.h"

#define HASH_MAGIC ('h' << 24 + 'a' << 16 + 's' << 8 + 'h')

int get_prime(int n)
{
    int i;
    int flag;
    n++;
    while(1)
    {
        flag = 0;
        for (i = 2; i < sqrt((double)n) + 1; i++)
        {
            if(!(n % i))
            {
                flag = 1;
            }
        }
        if (!flag)
        {
            if ((n - 3) % 4 == 0)
            {
                return n;
            } 
        }
        n++;
    }
}

unsigned int hash_BKDRHash(char *str)
{
    unsigned int hash = 0;
    while(*str)
    {
        hash = (hash * 131) + (*str++); // 31 131 1313 13131 131313 etc...
    }
    return hash;
}

int _hash_insert(struct hash_table *ht, struct hash_item *it)
{
    int i, pos;
    int hash_key;
    hash_key = (it->id) % (ht->M);
    if ((ht->hash_barrel[hash_key].flag == 0) || (ht->hash_barrel[hash_key].flag == 2))
    {
        memcpy(&ht->hash_barrel[hash_key], it, sizeof(struct hash_item));
        ht->R++;
        return 0;
    }
    else
    {
        for(i = 0; i < ht->M / 2 + 1; i++)
        {
            // positive direction
            pos = (i * i + hash_key) % (ht->M);
            if (ht->hash_barrel[pos].flag == 0 || ht->hash_barrel[pos].flag == 2)
            {
                memcpy(&ht->hash_barrel[pos], it, sizeof(struct hash_item));
                ht->R++;
                return 0;
            }
            assert(ht->hash_barrel[pos].id != it->id);
            // negative direction
            if (i * i <= hash_key)
            {
                pos = hash_key - i * i;
            }
            else
            {
                pos = (hash_key - i * i) % (ht->M);
                if (pos != 0)
                {
                    pos += ht->M;
                }
            }
            if (ht->hash_barrel[pos].flag == 0 || ht->hash_barrel[pos].flag == 2)
            {
                memcpy(&ht->hash_barrel[pos], it, sizeof(struct hash_item));
                ht->R++;
                return 0;
            }
            assert(ht->hash_barrel[pos].id != it->id);
        }
    }
    return -1;
}

int hash_rehash(struct hash_table *ht)
{
    int i;
    int old_M;
    struct hash_item *old_barrel;
    
    old_M = ht->M;
    ht->M = get_prime(ht->M * 2);
    old_barrel = ht->hash_barrel;
    ht->hash_barrel = (struct hash_item *)calloc(ht->M, sizeof(struct hash_item));

    if (!(ht->hash_barrel))
    {
        printf("hash_rehash: calloc err!\n");
        return -1;
    }

    for(i = 0; i < old_M; i++)
    {
        if (old_barrel[i].flag == 1)
        {
            _hash_insert(ht, &old_barrel[i]);
        }
    }
    free(old_barrel);
    return 0;
}

int hash_insert(struct hash_table *ht, char *key, void *data)
{
    unsigned int id;
    struct hash_item item;
    assert(ht->magic == HASH_MAGIC);
    if (ht->R > (ht->M) * 0.9)
    {
        if (hash_rehash(ht))
        {
            return -1;
        }
    }

    item.flag = 1;
    item.id = hash_BKDRHash(key);
    item.user_data = data;
    _hash_insert(ht, &item);
}



struct hash_item *_hash_find(struct hash_table *ht, char *key)
{
    int i, pos;
    unsigned int id;
    int hash_key;

    id = hash_BKDRHash(key);
    hash_key = id % (ht->M);

    if (ht->hash_barrel[hash_key].flag == 0)
    {
        // not found
        return NULL;
    }
    else if ((ht->hash_barrel[hash_key].flag == 1) && (ht->hash_barrel[hash_key].id == id))
    {
        // found
        return &ht->hash_barrel[hash_key];
    }
    else 
    {
        for(i = 0; i < ht->M / 2 + 1; i++)
        {
            // positive direction
            pos = (i * i + hash_key) % (ht->M);
            if (ht->hash_barrel[pos].flag == 0)
            {
                // not found
                return NULL;
            }
            else if((ht->hash_barrel[pos].flag == 1) && (ht->hash_barrel[pos].id == id))
            {
                // found
                return &ht->hash_barrel[pos];
            }

            // negative direction
            if (i * i <= hash_key)
            {
                pos = hash_key - i * i;
            }
            else
            {
                pos = (hash_key - i * i) % (ht->M);
                if (pos != 0)
                {
                    pos += ht->M;
                }
            }
            if (ht->hash_barrel[pos].flag == 0)
            {
                // not found
                return NULL;
            }
            else if((ht->hash_barrel[pos].flag == 1) && (ht->hash_barrel[pos].id == id))
            {
                // found
                return &ht->hash_barrel[pos];
            }
        }
    }
    return NULL; //not found
}

int hash_delete(struct hash_table *ht, char *key)
{
    struct hash_item  *item;
    assert(ht->magic == HASH_MAGIC);
    if (!(item = _hash_find(ht, key)))
    {
        printf("hash item not found!\n");
        return -1;
    }
    ht->R--;
    item->flag = 2;
    return 0;
}

int hash_find(struct hash_table *ht, char *key, void **pdata)
{
    struct hash_item  *item;
    assert(ht->magic == HASH_MAGIC);
    if (!(item = _hash_find(ht, key)))
    {
        // not found
        return -1;
    }
    if (pdata)
    {
        *pdata = item->user_data;
    }
    return 0;
}

void *hash_iter(struct hash_table *ht, int *start, int *count)
{
    assert(ht->magic == HASH_MAGIC);
    if (ht->R == 0)
    {
        return NULL;
    }
    if ((*count)++ >= ht->R)
    {
        return NULL;
    }
    for (; (*start) < ht->M; (*start)++)
    {
        if (ht->hash_barrel[(*start)].flag == 1)
        {
            (*start)++;
            return ht->hash_barrel[(*start) - 1].user_data;
        }
    }
    return NULL;
}

void hash_clean(struct hash_table *ht)
{
    int i;
    assert(ht->magic == HASH_MAGIC);
    for (i = 0; i < ht->M; i++)
    {
        if (ht->hash_barrel[i].flag != 0)
        {
            ht->hash_barrel[i].flag = 0;
        }
    }
    ht->R = 0;
}

struct hash_table *hash_init()
{
    struct hash_table *ht = (struct hash_table *)calloc(1, sizeof(struct hash_table));
    if (!ht)
    {
        return NULL;
    }
    ht->hash_barrel = (struct hash_item *)calloc(DEFAULT_HASHTAB_SIZE, sizeof(struct hash_item));
    if (!ht->hash_barrel)
    {
        return NULL;
    }
    ht->M = DEFAULT_HASHTAB_SIZE;
    ht->R = 0;
    ht->insert = hash_insert;
    ht->delete = hash_delete;
    ht->find = hash_find;
    ht->hash_iter = hash_iter;
    ht->clear = hash_clean;
    ht->magic = HASH_MAGIC;
    return ht;
}

int hash_destory(struct hash_table *ht)
{
    assert(ht->magic == HASH_MAGIC);
    if (!ht)
    {
        return -1;
    }
    free(ht->hash_barrel);
    free(ht);
    return 0;
}

