#ifndef __EXPR_H__
#define __EXPR_H__

#include "parse.h"
#include "type.h"
#include "symtab.h"

enum expr_operator
{
    #define OPINFO(a, b, c, d, e) a,
        #include "opinfo.h"
    #undef  OPINFO
    OP_END
};

#define EXPR_NODE \
    NODE_COMMON\
    struct type *type;\
    struct symbol *sym;\
    enum expr_operator op;\
    int is_array;\
    int is_func;\
    int is_left_val;\
    int in_reg;\
    int inc_dec_op;\
    union val value;\
    struct expr_node *child[2];

struct expr_node
{
    EXPR_NODE
};

struct cond_expr_node
{
    EXPR_NODE
    struct symbol *end_lbl;
    struct symbol *else_lbl;
};

struct expr_node *alloc_expr_node();
struct expr_node *primary_expression();
struct expr_node *postfix_expression();
struct expr_node *unary_expression();
struct expr_node *cast_expression();
struct expr_node *binary_expression(int );
struct expr_node *conditional_expression();
struct expr_node *assignment_expression();
struct expr_node *expression();
struct expr_node *constant_expression();
struct expr_node *argument_expression_list();



struct expr_node *expression_check(struct expr_node *expr);
struct expr_node *constant_expression_check(struct expr_node *expr);
struct expr_node *cast(struct type *dst_type, struct expr_node *expr);
struct expr_node *adjust(struct expr_node *expr);



#endif
