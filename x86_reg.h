#ifndef __X86_REG_H__
#define __X86_REG_H__

enum reg
{
	EAX=0, EBX, ECX, EDX, AX, BX, CX, DX, AH, BH, CH, DH, AL, BL, CL, DL, ESI, EDI,
};

struct vector *forbidden_regs;

void x86reg_init();
struct symbol *alloc_reg(struct symbol_id *sym, int reg_size, int target, int reserve);
void write_back_reg(struct symbol_reg *reg);
void release_reg(struct symbol_reg *reg);
struct symbol_reg *reserve_reg(struct symbol_id *sym);
void unreserve_reg(struct symbol_reg *reg);
void reg_change_user(struct symbol_reg *reg, struct symbol_id *new_user);
extern struct symbol_reg regs[];
extern const char reg_to_size[];

void just_for_debug();



#endif
