#ifndef __DECL_H__
#define __DECL_H__

#include "parse.h"
#include "expr.h"
#include "symtab.h"
#include "vector.h"

/**************************************************
	direct-abstract-declarator:
		without identifier
	direct-declarator:
		with identifier		
 **************************************************/
enum { DEC_ABSTRACT = 0x01, DEC_CONCRETE = 0x02};
/*
identifier-list:
    identifier
    identifier-list ,  identifier
*/
struct decl_id_node
{
    NODE_COMMON
    char *id;
};
/*
typedef-name:
    identifier
*/
struct decl_typedef_spec_node
{
    NODE_COMMON
    char *name;
};
struct decl_specer_node
{
    NODE_COMMON
    enum token_type tok_type;
    void *priv_node; //for struct/union, enum and id, they have their own specer node
};
/*
type-qualifier:
    const
    volatile

type-specifier:
    void
    char
    short
    int
    long
    float
    double
    signed
    unsigned
    struct-or-union-specifier
    enum-specifier
    typedef-name

storage-class-specifier:
    typedef
    extern
    static
    auto
    register
*/
struct decl_specer_list_node
{
    NODE_COMMON
    
    struct decl_specer_node *ty_qualer;
    struct decl_specer_node *ty_specer;
    struct decl_specer_node *stg_specer;

    // After semantics check ,we know the storage-class
    int sto_class_spec_tok;
	struct type *type;
};

/*
type-name:
    specifier-qualifier-list abstract-declarator<opt>
*/
struct decl_type_name_node
{
    NODE_COMMON
    struct decl_specer_list_node *spec;
    struct decl_common_declor_node *absdeclor;
// After semantics check
	struct type *type;
};


/*
pointer:
    *  type-qualifier-list<opt>
    *  type-qualifier-list<opt> pointer
*/
struct decl_pointer_node
{
    NODE_COMMON
    struct decl_specer_list_node *specer; // type-qualifier-list
};
/*
declarator:
    pointer<opt> direct-declarator

abstract-declarator:
    pointer
    pointer<opt> direct-abstract-declarator
*/

/*
direct-declarator:
    identifier
    (  declarator ) 
    direct-declarator [  constant-expression<opt> ] 
    direct-declarator (  parameter-type-list ) 
    direct-declarator (  identifier-list<opt> )

direct-abstract-declarator:
    (  abstract-declarator ) 
    direct-abstract-declarator<opt> [  constant-expression<opt> ] 
    direct-abstract-declarator<opt> (  parameter-type-list<opt> )
*/
#define DECLARATOR_NODE_COMMON   \
    NODE_COMMON             \
    struct decl_common_declor_node *next_declor; \
    char *id; \
    /* fill by semetic check*/\
    struct symbol *sym;

struct decl_common_declor_node
{
    DECLARATOR_NODE_COMMON
};
struct decl_pointer_declor_node
{
    DECLARATOR_NODE_COMMON
    int qual;
// fill by semantical check 

};

/*
parameter-declaration:
    declaration-specifiers declarator
    declaration-specifiers abstract-declarator<opt>
*/
struct decl_para_node
{
    NODE_COMMON
    struct decl_specer_list_node *spec;
    struct decl_common_declor_node *declor;
};
/*
parameter-type-list:
    parameter-list
    parameter-list , ...
*/
struct decl_para_type_list_node
{
    NODE_COMMON
    struct decl_para_node *para_list;
    int has_ellipsis;
};

struct decl_func_declor_node
{
    DECLARATOR_NODE_COMMON
    struct decl_para_type_list_node *para_type_list;
    int is_definition;
// fill by semantical check 
	struct vector *params;
    struct vector *typedef_name_sym;
//    struct vector *tags_sym;
};
struct decl_array_declor_node
{
    DECLARATOR_NODE_COMMON
    struct expr_node *const_expr;
// fill by semantical check
    int len;
};
struct decl_name_declor_node
{
    DECLARATOR_NODE_COMMON
};
/*
enum decl_direct_declor_sel
{
    SEL_ID, SEL_DECLOR, SEL_CONST, SEL_PARA, SEL_IDLIST, SEL_PARA_NULL, SEL_BRACE_NULL
};
struct decl_direct_declor_node
{
    NODE_COMMON
    int sel;
    union
    {
        char *id;
        struct decl_declor_node *declor;
        struct expr_node *const_expr;
        struct decl_para_node *para;
        struct decl_id_node *id_list;
    };
};
*/
/*
enumerator:
    enumeration-constant
    enumeration-constant = constant-expression
*/
struct decl_enumor_node
{
    NODE_COMMON
    char *id;
    struct expr_node *const_expr;
};
/*
enum-specifier:
    enum  identifier<opt> { enumerator-list }
    enum  identifier

enumerator-list:
    enumerator
    enumerator-list , enumerator
*/
struct decl_enum_spec_node
{
    NODE_COMMON
    char *id;
    struct decl_enumor_node *enumor;
};
/*
struct-declarator:
    declarator
    //declarator<opt> :  constant-expression

struct decl_struct_declor_node
{
    NODE_COMMON
    struct decl_common_declor_node *declor;
//    struct expr_node *const_expr;
};*/
/*
struct-declaration:
    specifier-qualifier-list struct-declarator-list ;
*/
struct decl_struct_decl_node 
{
    NODE_COMMON
    struct decl_specer_list_node *specer_list; // type-specifier or type-qualifier
    struct decl_common_declor_node *declor_list;
};
/*
struct-or-union-specifier:
    struct-or-union identifier<opt> {  struct-declaration-list } 
    struct-or-union identifier
*/
struct decl_struct_spec_node
{
    NODE_COMMON
    char *id;
    struct decl_struct_decl_node *decl_list;
};

struct init_data
{
	int offset;
	struct expr_node *expr;
	struct init_data *next;
};
/*
initializer-list:
    initializer
    initializer-list ,  initializer
*/
/*
struct decl_initer_list_node
{
    NODE_COMMON
    struct decl_initer_node *initer;
};
*/
/*
initializer:
    assignment-expression
    {  initializer-list } 
    {  initializer-list , }
*/
struct decl_initer_node
{
    NODE_COMMON
    struct expr_node *assign_expr;
    struct decl_initer_node *initer_list;
// fill by semantical check
    struct init_data *idata;
};
/*
init-declarator-list:
    init-declarator
    init-declarator-list ,  init-declarator

init-declarator:
    declarator
    declarator =  initializer
*/
struct decl_init_declor_node
{
    NODE_COMMON
    struct decl_common_declor_node *declor;
    struct decl_initer_node *initer;
};
/*
declaration:
    declaration-specifiers init-declarator-list<opt> ;
*/
struct decl_node
{
    NODE_COMMON
    struct decl_specer_list_node *spec;   //storage-class-specifier or type-specifier or type-qualifier
    struct decl_init_declor_node *init_declor_list;
};

/*
function-definition:
    declaration-specifiers<opt> declarator compound-statement
*/
/*
struct labels
{
    char *id;
    int ref_cnt;
    int is_defined;
    struct labels *next;
};
*/
struct decl_func_node
{
	NODE_COMMON
	struct decl_specer_list_node *spec;
    struct decl_common_declor_node *declor;
    struct decl_func_declor_node *func_declor;
    struct stmt_comp_node *comp;
	struct symbol_func *func_sym;
// fill by semantical check
    struct vector *switch_iter_stack;
    struct hash_table *named_labels;
// fill by ir emit
    struct symbol_label *start_lbl;
    struct symbol_label *end_lbl;
    struct symbol *return_val;
/*	
	struct vector *loops;
	struct vector *swtches;
	struct vector *breakable;
	int hasReturn;

    //Label labels;
*/
};
/*
translation-unit:
    external-declaration
    translation-unit external-declaration

external-declaration:
    function-definition
    declaration
*/
struct decl_trans_unit_node
{
    NODE_COMMON
    struct common_node *ext_decl;
};

/*
struct decl_ext_decl_node
{
    NODE_COMMON
    struct decl_func_node *func_decl;
    struct decl_node *decl;
};*/





int is_typedef_name(char *name);
struct decl_init_declor_node *init_declarator();
struct decl_init_declor_node *init_declarator_list();
struct decl_typedef_spec_node *typedef_name();
int is_type_qualifier(struct token *);
struct decl_specer_node *type_qualifier();
int is_type_specifier(struct token *, int);
struct decl_specer_node *type_specifier();
int is_storage_class_specifier(struct token *);
struct decl_specer_node *storage_class_specifier();
struct decl_struct_decl_node *struct_declaration();
struct decl_struct_decl_node *struct_declaration_list();
struct decl_struct_spec_node *struct_or_union_specifier();
struct decl_specer_list_node *specifier_qualifier_list();
struct decl_common_declor_node *struct_declarator_list();
struct decl_common_declor_node *struct_declarator();
struct decl_enumor_node *enumerator();
struct decl_enumor_node *enumerator_list();
struct decl_enum_spec_node *enum_specifier();
struct decl_common_declor_node *declarator(int);
struct decl_common_declor_node *_declarator(int , struct decl_common_declor_node **);
struct decl_type_name_node *type_name();
struct decl_specer_list_node *declaration_specifiers();
struct decl_para_node *parameter_declaration();
struct decl_specer_list_node *type_qualifier_list();
struct decl_pointer_node *pointer();
struct decl_initer_node *initializer();
struct decl_initer_node *initializer_list();
struct decl_node *declaration();
struct decl_trans_unit_node *translation_unit();

///////////////////////////////////////////////////////////
void type_name_check(struct decl_type_name_node *);
void local_declaration_check(struct decl_node *decl);
void translation_unit_check(struct decl_trans_unit_node *trans_unit);

#endif
